package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class SelectorDialogFragment extends DialogFragment {

    private SelectorDialogParent mParent;

    public interface SelectorDialogParent {

        public void onSelection(int result);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof SelectorDialogParent)) {

            throw new ClassCastException(activity.toString() + " must implement SelectorDialogParent");
        } else {

            mParent = (SelectorDialogParent) activity;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle arguments = getArguments();
        String title = arguments.getString(MainActivity.SELECTOR_TITLE);
        final int offset = arguments.getInt(MainActivity.SELECTOR_OFFSET);
        CharSequence[] options = arguments.getCharSequenceArray(MainActivity.SELECTOR_OPTIONS);

        Activity activity = getActivity();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setTitle(title);
        dialogBuilder.setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int option) {

                mParent.onSelection(offset + option);
                dialog.dismiss();
            }
        });

        return dialogBuilder.create();
    }
}
