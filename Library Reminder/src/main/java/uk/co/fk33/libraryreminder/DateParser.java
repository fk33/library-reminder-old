/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.content.Context;

import java.util.EnumSet;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class DateParser {

    enum Units {YEAR, MONTH, WEEK, DAY}

    private final EnumSet<Units> YEAR_MONTH_WEEK_DAY = EnumSet.of(Units.YEAR, Units.MONTH, Units.WEEK, Units.DAY);
    private final EnumSet<Units> YEAR_MONTH_WEEK = EnumSet.of(Units.YEAR, Units.MONTH, Units.WEEK);
    private final EnumSet<Units> YEAR_MONTH = EnumSet.of(Units.YEAR, Units.MONTH);
    private final EnumSet<Units> YEAR = EnumSet.of(Units.YEAR);
    private final EnumSet<Units> YEAR_MONTH_DAY = EnumSet.of(Units.YEAR, Units.MONTH, Units.DAY);
    private final EnumSet<Units> YEAR_WEEK_DAY = EnumSet.of(Units.YEAR, Units.WEEK, Units.DAY);
    private final EnumSet<Units> YEAR_WEEK = EnumSet.of(Units.YEAR, Units.WEEK);
    private final EnumSet<Units> YEAR_DAY = EnumSet.of(Units.YEAR, Units.DAY);
    private final EnumSet<Units> MONTH_WEEK_DAY = EnumSet.of(Units.MONTH, Units.WEEK, Units.DAY);
    private final EnumSet<Units> MONTH_WEEK = EnumSet.of(Units.MONTH, Units.WEEK);
    private final EnumSet<Units> MONTH_DAY = EnumSet.of(Units.MONTH, Units.DAY);
    private final EnumSet<Units> MONTH = EnumSet.of(Units.MONTH);
    private final EnumSet<Units> WEEK_DAY = EnumSet.of(Units.WEEK, Units.DAY);
    private final EnumSet<Units> WEEK = EnumSet.of(Units.WEEK);
    private final EnumSet<Units> DAY = EnumSet.of(Units.DAY);

    /**
     *
     * This method checks whether the second DateTime argument is "yesterday", "today" or
     * "tomorrow" relative to the first one, and returns the relevant string if so. Otherwise
     * it constructs a joda time period, and passes it to the makeString method which takes a
     * period as an argument, which will express the period in words.
     *
     * @param firstDateTime The first datetime
     * @param secondDateTime The second datetime
     * @return A string describing the relation between them
     */
    public String makeString(DateTime firstDateTime, DateTime secondDateTime) {

        LocalDate firstLocalDate = firstDateTime.toLocalDate();
        LocalDate secondLocalDate = secondDateTime.toLocalDate();

        if (firstLocalDate.equals(secondLocalDate)) return "today";

        if (firstLocalDate.plusDays(1).equals(secondLocalDate)) return "tomorrow";

        if (firstLocalDate.minusDays(1).equals(secondLocalDate)) return "yesterday";

        Period period = new Period(firstLocalDate, secondLocalDate);

        return makeString(period);

    }

    /**
     * Express a joda time period in words (e.g. "two weeks from now" etc) using a period
     * formatter which expresses to two most significant components of the period. I.e. if all fields
     * are present (years, months, weeks and days) only express years and months. If three fields are
     * present (months, weeks and day), express months and weeks. If only two fields are present
     * (e.g. years and days, years and weeks, months and days, months and weeks, weeks and days)
     * then express both. If only one is present, express that. EnumSets are used rather than if
     * clauses, to make the code more readable.
     *
     * @param period The period to express
     * @return The string expressing the period in words
     */
    public String makeString(Period period) {

        Boolean negativePeriod = false;

        PeriodFormatter formatter = PeriodFormat.getDefault();

        if (period.getYears() < 0 || period.getMonths() < 0
                || period.getWeeks() < 0 || period.getDays() < 0) {

            negativePeriod = true;
            period = period.negated();
        }

        EnumSet<Units> periodUnits = EnumSet.noneOf(Units.class);

        if (period.getYears() > 0) {
            periodUnits.add(Units.YEAR);
        }
        if (period.getMonths() > 0) {
            periodUnits.add(Units.MONTH);
        }
        if (period.getWeeks() > 0) {
            periodUnits.add(Units.WEEK);
        }
        if (period.getDays() > 0) {
            periodUnits.add(Units.DAY);
        }

        if (periodUnits.equals(YEAR_MONTH_WEEK_DAY)
                || periodUnits.equals(YEAR_MONTH_WEEK)
                || periodUnits.equals(YEAR_MONTH_DAY)
                || periodUnits.equals(YEAR_MONTH)
                || periodUnits.equals(YEAR)) {

            /* Show years and months. (If there are no months only years will be shown). */
            formatter = new PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendYears()
                    .appendSuffix(" year", " years")
                    .appendSeparator(" and ")
                    .appendMonths()
                    .appendSuffix(" month", " months")
                    .toFormatter();
        }

        if (periodUnits.equals(YEAR_WEEK_DAY)
                || periodUnits.equals(YEAR_WEEK)) {

            /* Show years and weeks */
            formatter = new PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendYears()
                    .appendSuffix(" year", " years")
                    .appendSeparator(" and ")
                    .appendWeeks()
                    .appendSuffix(" week", " weeks")
                    .toFormatter();
        }

        if (periodUnits.equals(YEAR_DAY)) {

            /* Show years and days */
            formatter = new PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendYears()
                    .appendSuffix(" year", " years")
                    .appendSeparator(" and ")
                    .appendDays()
                    .appendSuffix(" day", " days")
                    .toFormatter();
        }

        if (periodUnits.equals(MONTH_WEEK_DAY)
                || periodUnits.equals(MONTH_WEEK)
                || periodUnits.equals(MONTH)) {

            /* Show months and weeks. (If there are no weeks, only months will be shown). */
            formatter = new PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendMonths()
                    .appendSuffix(" month", " months")
                    .appendSeparator(" and ")
                    .appendWeeks()
                    .appendSuffix(" week", " weeks")
                    .toFormatter();
        }

        if (periodUnits.equals(MONTH_DAY)) {

            /* Show months and days */
            formatter = new PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendMonths()
                    .appendSuffix(" month", " months")
                    .appendSeparator(" and ")
                    .appendDays()
                    .appendSuffix(" day", " days")
                    .toFormatter();
        }

        if (periodUnits.equals(WEEK_DAY)
                || periodUnits.equals(WEEK)
                || periodUnits.equals(DAY)) {

            /* Show weeks and days. If either is missing, only the other will be shown. */
            formatter = new PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendWeeks()
                    .appendSuffix(" week", " weeks")
                    .appendSeparator(" and ")
                    .appendDays()
                    .appendSuffix(" day", " days")
                    .toFormatter();
        }

        if (negativePeriod) {
            return period.toString(formatter) + " ago";
        } else {
            return "in " + period.toString(formatter);

        }
    }

    public String[] makeNotificationStrings(Context context, String accountName, int margin, int dueItems,
                                         int overdueItems) {

        String shortNotificationString;

        int totalItems = dueItems + overdueItems;

        if (totalItems == 1) {

            shortNotificationString = "1 "+ context.getString(R.string.loan_needs_action_for)
                    + " " + accountName + ".";
        } else {

            shortNotificationString = String.valueOf(totalItems) + " " + context.getString(
                    R.string.loans_need_action_for)
                    + " " + accountName + ".";
        }

        String longNotificationString = context.getString(R.id.you_have);

        if (overdueItems > 0) {

            longNotificationString += " " + String.valueOf(overdueItems);

            if (overdueItems == 1) {

                longNotificationString += " " + context.getString(R.string.overdue_loan);
            } else {

                longNotificationString += " " + context.getString(R.string.overdue_loans);
            }

            if (dueItems > 0) {

                longNotificationString += " " + context.getString(R.string.and);
            }
        }

        if (dueItems == 1) {

            longNotificationString += " 1 " + context.getString(R.string.loan_due);
        }

        if (dueItems > 1) {

            longNotificationString += " " + String.valueOf(dueItems) + " "
                + context.getString(R.string.loans_due);
        }

        if (dueItems > 0) {

            if (margin == 0) {

                longNotificationString += " " + context.getString(R.string.today);
            } else {

                if (margin == 1) {

                    longNotificationString += " " + context.getString(R.string.within_1_day);
                } else {

                    longNotificationString += " " + context.getString(R.string.within)
                            + " " + String.valueOf(margin) + " " + context.getString(R.string.days);
                }
            }
        }

        longNotificationString += " " + context.getString(R.string.on_account) +" " + accountName + ".";

        return new String[] {shortNotificationString, longNotificationString};
    }

    public String makeTitle(Context context, int numberOfLoans) {

        String title = DbFragmentRef.get().getAccountBeingViewed();

        if (numberOfLoans == 0) {

            title += " - " + context.getString(R.string.no_loans_for_title);
        } else {

            if (numberOfLoans == 1) {

                title += " - " + context.getString(R.string.one_loan);
            } else {

                title += " - " + String.valueOf(numberOfLoans) + " " + context.getString(R.string.loans);
            }
        }

        return title;
    }

}
