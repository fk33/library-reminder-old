package uk.co.fk33.libraryreminder;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LibRemindLog {

    public static void write(Context context, String logMessage, boolean addTimeStamp) {

        /* Also log to logcat.*/
        Log.i("dbug", logMessage);

        File logFile = new File(context.getFilesDir(), "log.txt");
        OutputStream outputStream = null;

        try {

            if (addTimeStamp) {

                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                logMessage = dateFormat.format(System.currentTimeMillis()) + " " + logMessage;
            }

            logMessage += "\n";

            outputStream = new BufferedOutputStream(new FileOutputStream(logFile, true));
            outputStream.write(logMessage.getBytes());
        } catch (Exception exception) {

            Log.i("dbug", "Exception writing to log file: " + exception.getMessage());
        } finally {

            if (outputStream != null) {

                try {

                    outputStream.close();
                } catch (Exception exception) {

                    Log.i("dbug", "Exception closing output stream: " + exception.getMessage());
                }
            }
        }
    }
}
