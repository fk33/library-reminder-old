/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;


import android.content.Context;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

public class SyncFailureReason {

    private String mReasonString;
    private Context mContext;

    public SyncFailureReason(Context context, int reasonCode) {

        mContext = context;
        setReasonString(reasonCode);
    }

    public SyncFailureReason(Context context, long reasonCodeLong) {

        mContext = context;
        int reasonCode = (int) reasonCodeLong;
        setReasonString(reasonCode);
    }

    private void setReasonString(int reasonCode) {

        switch (reasonCode) {

            /* If a sync has never been attempted (first case) don't show anything in the last synced field as
            a sync will be attempted immediately and there is no point showing any text for a fraction
            of a second. */
            case LibAccount.SYNC_NEVER_ATTEMPTED: {

                mReasonString = "";
                break;
            }

            case LibAccount.SYNC_FAILED_NO_CONNECTION: {

                mReasonString = mContext.getString(R.string.sync_failed_no_internet);
                break;
            }

            case LibAccount.SYNC_FAILED_LOGIN_ERROR: {

                mReasonString = mContext.getString(R.string.sync_failed_check_card_number);
                break;
            }

            case LibAccount.SYNC_FAILED_NETWORK_ERROR: {

                mReasonString = mContext.getString(R.string.sync_failed_network_error);
                break;
            }

            case LibAccount.SYNC_FAILED_URL_ERROR: {

                mReasonString = mContext.getString(R.string.sync_failed_check_catalogue_url);
                break;
            }

            case LibAccount.SYNC_FAILED_FORMAT_ERROR: {

                mReasonString = mContext.getString(R.string.sync_failed_unexpected_page_format);
                break;
            }

            case LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE: {

                mReasonString = mContext.getString(R.string.sync_failed_unexpected_response);
                break;
            }

            case LibAccount.SYNC_FAILED_TIMED_OUT: {

                mReasonString = mContext.getString(R.string.sync_failed_catalogue_timeout);
                break;
            }

            case LibAccount.SYNC_FAILED_ENCODER_EXCEPTION: {

                mReasonString = mContext.getString(R.string.sync_failed_encode_error);
                break;
            }

            default: {

                mReasonString = mContext.getString(R.string.sync_failed);
            }
        }
    }

    public String getReasonString() {
        return mReasonString;
    }
}

