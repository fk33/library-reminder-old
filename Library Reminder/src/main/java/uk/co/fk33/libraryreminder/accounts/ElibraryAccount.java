/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;

import uk.co.fk33.libraryreminder.network.ElibraryUpdateTask;

public class ElibraryAccount extends LibAccount {

    public ElibraryAccount(Context context, boolean silentMode, long rowId, String name, String catUrl, String cardNumber, String pinOrPassword,
                            boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.ELIBRARY;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        ElibraryUpdateTask elibraryUpdateTask = new ElibraryUpdateTask(mContext);
        elibraryUpdateTask.execute(this);
    }
}
