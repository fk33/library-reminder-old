/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LicenseDisplayFragment extends CloseableDialogFragment {

    private View mScrollView;
    public static String LICENSE_TYPE = "license_type";
    private boolean mCancelled = false;

    public LicenseDisplayFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();

        /* When orientation is changed, to avoid state loss, we have to reconstruct this fragment
        * ourselves, rather than allow the system to recreate it. So we have to manually store
        * and restore the scroll position. */
        final int licenseFragScrollPos = DbFragmentRef.get().getLicenseFragScrollPos();

        if (licenseFragScrollPos != 0) {

            mScrollView.post(new Runnable() {

                @Override
                public void run() {

                    mScrollView.scrollTo(0, licenseFragScrollPos);
                }
            });
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int licenseType = getArguments().getInt(LICENSE_TYPE);

        final Dialog licenseDialog = new Dialog(getActivity());
        licenseDialog.setContentView(R.layout.license_fragment);

        TextView licenseTextView = (TextView) licenseDialog.findViewById(R.id.license_text_view);

        switch (licenseType) {

            case MainActivity.PRIVACY_POLICY: {

                licenseDialog.setTitle(getString(R.string.privacy_policy_title));
                licenseTextView.setText(getString(R.string.privacy));
                break;
            }

            case MainActivity.TERMS_OF_USE: {

                licenseDialog.setTitle(getString(R.string.terms_of_use_title));
                licenseTextView.setText(getString(R.string.terms_of_use));
                break;
            }

            case MainActivity.JSOUP_LICENSE: {

                licenseDialog.setTitle(getString(R.string.jsoup_license_title));
                licenseTextView.setText(getString(R.string.mit_license));
                break;
            }

            case MainActivity.JODA_TIME_LICENSE: {

                licenseDialog.setTitle(getString(R.string.joda_time_license_title));
                licenseTextView.setText(getString(R.string.apache_license));
                break;
            }

            case MainActivity.PRETTYTIME_LICENSE: {

                licenseDialog.setTitle(getString(R.string.prettytime_license_title));
                licenseTextView.setText(getString(R.string.apache_license));
                break;
            }

            case MainActivity.JASYPT_LICENSE: {

                licenseDialog.setTitle(getString(R.string.jasypt_license_title));
                licenseTextView.setText(getString(R.string.apache_license));
                break;
            }

            case MainActivity.ACTION_BAR_SHERLOCK_LICENSE: {

                licenseDialog.setTitle(getString(R.string.action_bar_sherlock_license_title));
                licenseTextView.setText(getString(R.string.apache_license));
                break;
            }

            case MainActivity.APACHE_LICENSE: {

                licenseDialog.setTitle(getString(R.string.apache_license_title));
                licenseTextView.setText(getString(R.string.apache_license));
                break;
            }
        }

        Button closeButton = (Button) licenseDialog.findViewById(R.id.license_dialog_close_button);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                licenseDialog.cancel();
            }
        });

        mScrollView = licenseDialog.findViewById(R.id.license_scroll_view);

        return licenseDialog;
    }

    @Override
    public void onPause() {
        super.onPause();

        /* This method's called after onCancel(). If we have been cancelled, erase the stored
        * scroll position in the dbfrag which we use to resurrect the frag after orientation
        * change (which we do ourselves, removing and re-adding the frag manually to avoid
        * illegalstateexceptions caused when Android recreates the frag and then the frag tries
        * to call fragment transactions). */
        if (!mCancelled) {

            DbFragmentRef.get().storeLicenseFragScrollPos(mScrollView.getScrollY());
        } else {

            DbFragmentRef.get().storeLicenseFragScrollPos(0);
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        mCancelled = true;
    }
}
