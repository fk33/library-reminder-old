/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

public class ApiDependentCodeFactory {

    public static ApiDependentPrefWriter getApiDependentPrefWriter() {

        if (android.os.Build.VERSION.SDK_INT >= 9) {
            return new Api9To17DependentPrefWriter();
        } else {
            return new Api8DependentPrefWriter();
        }
    }

    public static ApiDependentSQLiteStatementExecutor getApiDependentSQLiteStatementExecutor() {

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            return new Api11To17DependentSQLiteStatementExecutor();
        } else {
            return new Api8To10DependentSQLiteStatementExecutor();
        }
    }

    public static ApiDependentMenuCode getApiDependentMenuCode() {

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            return new Api11To17DependentMenuCode();
        } else {
            return new Api8To10DependentMenuCode();
        }
    }
}
