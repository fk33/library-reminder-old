/*
 * Copyright (c) 2015 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PortfolioUpdateTask extends UpdateTask {

    public PortfolioUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        /* For Portfolio catalogues, we store two URLs in the organisation definition, because the URL we post
         * to needs to have the name of the organisation on the end, therefore it can't be constructed here from
         * the base catalogue URL, being different for each organisation. */
        String[] catUrls = mLibAccount.getCatUrl().split("\\|");
        String catUrl = catUrls[0];
        String postUrl = catUrls[1];
        String loginPageUrl = catUrl + "search/patronlogin/";
        String accountUrl = catUrl + "search/account/";
        String loansUrl = catUrl + "search/account.checkouts.librarycheckoutsaccordion";

        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        try {

            /* First load the login page, get a session cookie, and get a "formData" value which we will need to send
	        * with the login request for the login to work. */
            publishProgress(mContext.getString(R.string.connecting_to_catalogue));
            catalogueResponse = Jsoup.connect(loginPageUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            String sessionId = catalogueResponse.cookie("JSESSIONID");
            if (sessionId == null) {

                mStatus = LibAccount.SYNC_FAILED_URL_ERROR;
                return null;
            }

            cataloguePage = catalogueResponse.parse();

            Element formDataElement = cataloguePage.select("input[name=t:formdata]").first();
            if (formDataElement == null) {

                mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                return null;
            }

            String formData = formDataElement.val();
            if (formData.equals("")) {

                mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                return null;
            }

            publishProgress(mContext.getString(R.string.sending_login_details));
            catalogueResponse = Jsoup.connect(postUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .cookie("JSESSIONID", sessionId)
                    .data("t:formdata", formData,
                            "j_username", mLibAccount.getCardNumber(),
                            "j_password", mLibAccount.getPinOrPassword())
                    .method(Connection.Method.POST)
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

    	    /* We don't need any info from the account page, but it must be loaded before the server will
	         * send the loan details (since the loan details are requested via Ajax).*/
            publishProgress(mContext.getString(R.string.getting_account_page));
            catalogueResponse = Jsoup.connect(accountUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .cookie("JSESSIONID", sessionId)
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

    	    /* Send the request which in a browser would be an Ajax request. The X-Requested-With
	         header must be sent or the server will return an error. */
            publishProgress(mContext.getString(R.string.getting_list_of_loans));
            catalogueResponse = Jsoup.connect(loansUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .cookie("JSESSIONID", sessionId)
                    .data("t:zoneid", "libraryCheckoutsAccordion")
                    .header("X-Requested-With", "XMLHttpRequest")
                    .ignoreContentType(true)
                    .method(Connection.Method.POST)
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            cataloguePage = catalogueResponse.parse();

            /* JSON includes escape characters which we must remove. */
            String cataloguePageString = cataloguePage.html();
            cataloguePageString = cataloguePageString.replaceAll("\\\\/", "\\/");
            cataloguePageString = cataloguePageString.replaceAll("&lt;", "\\<");
            cataloguePageString = cataloguePageString.replaceAll("&gt;", "\\>");

            cataloguePage = Jsoup.parse(cataloguePageString);

            Elements loanRows = cataloguePage.select("tr.checkoutsLine");

            for (Element loanRow:loanRows) {

                String details = loanRow.getElementsByClass("checkoutsBookInfo").first().text();
                String[] detailsStringArray = details.split("\\\\n");

                String title = detailsStringArray[0].trim();
                String author = detailsStringArray[1].trim();
                String uniqueIdentifier = detailsStringArray[2].trim();

                String renewCountString = loanRow.getElementsByClass("checkoutsRenewCount").first().text();
                int renewCount = Integer.parseInt(renewCountString);

                String dueDateString = loanRow.getElementsByClass("checkoutsDueDate").first().text();
                DateFormat dateFormat = LocaleDependentClass.getPortfolioDateFormat();
                Date dueDate = dateFormat.parse(dueDateString);

                if (dueDate == null) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                long dueDateLong = dueDate.getTime();

                libItem = new LibItem(mLibAccount.getName(), title, author, uniqueIdentifier,
                        "", dueDateLong, renewCount, true);

                itemList.add(libItem);
            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;

        } catch (IndexOutOfBoundsException outOfBoundsException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;

        } catch (ParseException parseException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;

        } catch (NullPointerException nullPointerException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
