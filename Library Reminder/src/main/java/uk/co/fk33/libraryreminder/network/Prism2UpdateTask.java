/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import android.util.Log;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Prism2UpdateTask extends UpdateTask {

    /* Prism 2 is obsolete now, all libraries which use it seem to have been upgraded
    * to Prism 3. But just in case anyone does not upgrade, I have not deleted the code. */

    public Prism2UpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        /* Connect to the library catalogue, get the list of user's loans */

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        String catUrl = mLibAccount.getCatUrl();
        String pinOrPassword = mLibAccount.getPinOrPassword();
        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        String accessAccountUrl = catUrl + "accessAccount.do";
        String loginUrl = catUrl + "logon.do";

        try {
            /*
            First get a valid cookie, the POST login request below doesn't work unless a session cookie is
            sent with it.
            */
            publishProgress(mContext.getString(R.string.connecting_to_catalogue));
            catalogueResponse = Jsoup.connect(accessAccountUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            String sessionId = catalogueResponse.cookie("JSESSIONID");
            if (sessionId == null) {
                mStatus = LibAccount.SYNC_FAILED_URL_ERROR;
                return null;
            }

            /* Put the cookie and post data into the request and send it. */
            publishProgress(mContext.getString(R.string.sending_login_details));

            Connection connection = Jsoup.connect(loginUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .cookie("JSESSIONID", sessionId)
                    .method(Connection.Method.POST);

            if (pinOrPassword.equals("")) {

                catalogueResponse = connection.data("talissession", "",
                        "hidden_username", mLibAccount.getCardNumber(),
                        "LoginSubmit", "Logon")
                        .execute();
            } else {

                catalogueResponse = connection.data("talissession", "",
                        "hidden_username", mLibAccount.getCardNumber(),
                        "hidden_password", mLibAccount.getPinOrPassword(),
                        "LoginSubmit", "Logon")
                        .execute();
            }

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            /* Prism 2 returns the list of user's loans immediately upon login so we should have it here */
            publishProgress(mContext.getString(R.string.getting_list_of_loans));
            cataloguePage = catalogueResponse.parse();

            /* Prism 2 doesn't set a login cookie, so make sure we're logged in by checking for the logout link */
            Elements logOutLink = cataloguePage.select("[href$=logout.do]");
            if (logOutLink.isEmpty()) {
                mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                return null;
            }

            /* If the user doesn't have any loans, just return the empty itemList */
            if (cataloguePage.text().contains("Loans: 0")) {
                mStatus = LibAccount.SYNC_SUCCEEDED;
                return itemList;
            }

            /* Variables used below for parsing out loan information */
            Element titleElement;
            Element barcodeElement;
            Element dueDateElement;
            Element timesRenewedElement;
            String dueDateString;
            DateFormat dateFormat = LocaleDependentClass.getPrism2DateFormat();
            Date dueDate;

            /* The checkboxes in the first column of each row in the loans table all have the name "loanRows".
            Searching for this name is a good way to isolate rows in the table in order to parse out the
            details of each loan from the cells in each row. */
            Elements checkboxes = cataloguePage.getElementsByAttributeValue("name", "loanRows");

            for (Element checkbox : checkboxes) {

                /* Get hold of the cells in the table by getting the ancestor elements of the checkbox. If a loan is
                overdue, it will be in red text, which is done with an additional font tag in the html, so we will
                have to get the great-grandparent element instead of grandparent. */
                if (checkbox.parent().parent().tag().toString().equals("td")) {
                    titleElement = checkbox.parent().parent().nextElementSibling().nextElementSibling();
                } else {
                    titleElement = checkbox.parent().parent().parent().nextElementSibling().nextElementSibling();
                }

                barcodeElement = titleElement.nextElementSibling();
                dueDateElement = barcodeElement.nextElementSibling().nextElementSibling();
                timesRenewedElement = dueDateElement.nextElementSibling();

                /* Talis 2 puts lots of info into the title field in the loan list. It puts the standard
                identifier last, so we can isolate it and store it, to use later when retrieving correct titles. */
                String title = titleElement.text();
                String standardIdentifier = title.substring(title.lastIndexOf(" ") + 1);

                /* Get the date from the date string and parse it into a Date object*/
                dueDateString = dueDateElement.text().split(" ")[0];

                dueDate = dateFormat.parse(dueDateString);

                long dueDateLong = dueDate.getTime();

                /* Put the information into item objects and put them in the item list */
                libItem = new LibItem(mLibAccount.getName(), title, "", barcodeElement.text(),
                        standardIdentifier, dueDateLong, Integer.parseInt(timesRenewedElement.text()),
                        true);

                itemList.add(libItem);

            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;
        } catch (NullPointerException nullPointerException) {

            /* We shouldn't get an NPE unless Prism 2 starts to spit out different
            HTML for the loan table, which there is no reason to think it should. */
            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (ParseException parseException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
