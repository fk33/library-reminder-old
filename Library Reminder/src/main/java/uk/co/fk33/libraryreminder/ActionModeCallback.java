/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.*;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

import android.widget.ListView;

import java.util.List;

public class ActionModeCallback implements ActionMode.Callback {

    private FragmentParent mFragmentParent;
    private ListView mListView;
    private int mMenuId;

    public ActionModeCallback(FragmentParent fragmentParent, ListView listView, int menuId) {

        mFragmentParent = fragmentParent;
        mListView = listView;
        mMenuId = menuId;
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {

        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(mMenuId, menu);

//        /* If we're handling action mode for the view account frag, store the
//        details so the action mode can be cancelled when view account frag
//        is being hidden in large land mode. */
//        if (mMenuId == R.menu.view_account_context_menu) {
//
//            DbFragmentRef.get().storeActionModeRef(actionMode);
//        }

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.sync: {

                List<String> checkedAccounts = DbFragmentRef.get().getCheckedAccounts();
                String[] accountsToSync = checkedAccounts.toArray(new String[checkedAccounts.size()]);

                /* The sync option is only shown when the account list is put into context mode,
                therefore there's no way mFragmentParent can be anything other than an AccountListParent,
                but check anyway. Then cast it to AccountListParent and call the syncAccounts method with
                the array from the checked accounts. */
                if (mFragmentParent instanceof AccountListFragment.AccountListParent)
                    ((AccountListFragment.AccountListParent) mFragmentParent).syncAccounts(accountsToSync);

                actionMode.finish();
                return true;
            }

            case R.id.delete: {

                List<String> checkedAccounts = DbFragmentRef.get().getCheckedAccounts();
                String[] accountsToDelete = checkedAccounts.toArray(new String[checkedAccounts.size()]);

                /* The delete option is only shown when the account list is put into context mode,
                therefore there's no way mFragmentParent can be anything other than an AccountListParent,
                but check anyway. Then cast it to AccountListParent and call the confirmDeleteAccounts
                method with the array from the checked accounts. */
                if (mFragmentParent instanceof AccountListFragment.AccountListParent)
                    ((AccountListFragment.AccountListParent) mFragmentParent).confirmDeleteAccounts(accountsToDelete);

                actionMode.finish();
                return true;
            }

            case R.id.select_all: {

                DbInteractionsFragment dbFrag = DbFragmentRef.get();
                SimpleCursorAdapter cursorAdapter = dbFrag.getAccountListCursorAdapter();
                Cursor cursor = cursorAdapter.getCursor();

                /* Go through every account in the database, and add its name to the
                list of checked accounts. Then refresh the listview so all checked
                accounts are highlighted. */

                dbFrag.clearCheckedAccounts();
                dbFrag.resetNumberOfCheckedAccounts();

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    /* In the account list cursor, column 1 is the account name. */
                    dbFrag.addCheckedAccount(cursor.getString(1));
                    dbFrag.incrementNumberOfCheckedAccounts();
                    cursor.moveToNext();
                }

                mFragmentParent.updateAccountListActionModeTitle();
                cursorAdapter.notifyDataSetChanged();
                return true;
            }

/******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/
//            case R.id.view_account_select_all: {
//
//                DbInteractionsFragment dbFrag = DbFragmentRef.get();
//                SimpleCursorAdapter cursorAdapter = dbFrag.getItemListCursorAdapter();
//                Cursor cursor = cursorAdapter.getCursor();
//
//                /* Go through every item for this account in the database, and add its
//                name to the  list of checked items. Then refresh the listview so all checked
//                items are highlighted. */
//                dbFrag.clearCheckedItems();
//                dbFrag.resetNumberOfCheckedItems();
//
//                cursor.moveToFirst();
//                while (!cursor.isAfterLast()) {
//
//                    /* In the item list cursor, column 0 is the db row number (which we use to
//                    keep track of which items are checked). */
//                    dbFrag.addCheckedItem(cursor.getLong(0));
//                    dbFrag.incrementNumberOfCheckedItems();
//                    cursor.moveToNext();
//                }
//
//                mFragmentParent.updateViewAccountActionModeTitle();
//                cursorAdapter.notifyDataSetChanged();
//
//                return true;
//            }

            default: {

                return false;
            }
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

/******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/
//        /* Remove the stored details about the action mode. */
//        if (mMenuId == R.menu.view_account_context_menu) {
//
//            dbFrag.storeActionModeRef(null);
//
//            dbFrag.setViewAccountInActionMode(false);
//            dbFrag.clearCheckedItems();
//            dbFrag.resetNumberOfCheckedItems();
//
//            resetItemListCheckBoxes();
//        } else
        {

            /* It was the account list in action mode. */
            dbFrag.setAccountListInActionMode(false);
            dbFrag.clearCheckedAccounts();
            dbFrag.resetNumberOfCheckedAccounts();

            resetAccountListCheckBoxes();
        }
    }

    private void resetAccountListCheckBoxes() {

        int visibleListLength = mListView.getLastVisiblePosition() - mListView.getFirstVisiblePosition();

        for (int p = 0; p <= visibleListLength; p++) {

            View listRow = mListView.getChildAt(p);
            if (listRow != null) {

                CheckBox checkBox = (CheckBox) listRow.findViewById(R.id.account_list_checkbox);
                if (checkBox != null) checkBox.setChecked(false);
            }
        }

        /* Refresh the listview so that any views highlighted during context action mode will
        have the highlighting removed (in the setViewValue of the LibRemindViewBinder. */
        DbFragmentRef.get().getAccountListCursorAdapter().notifyDataSetChanged();
    }

    private void resetItemListCheckBoxes() {

        /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

        /* This is part of the code which allows you to put the list into action mode. There are
        * no bulk functions for items in this version of the app so it has been removed.
        * It may be re-enabled if future versions of the app include bulk item fuctions.*/

//        int visibleListLength = mListView.getLastVisiblePosition() - mListView.getFirstVisiblePosition();
//
//        for (int p = 0; p <= visibleListLength; p++) {
//
//            View listRow = mListView.getChildAt(p);
//            if (listRow != null) {
//
//                CheckBox checkBox = (CheckBox) listRow.findViewById(R.id.view_account_checkbox);
//                if (checkBox != null) checkBox.setChecked(false);
//            }
//        }
//
//        /*
//        Refresh the listview so that any views highlighted during context action mode will
//        have the highlighting removed (in the setViewValue of the ItemListViewBinder.
//        */
//        DbFragmentRef.get().getItemListCursorAdapter().notifyDataSetChanged();
    }
}
