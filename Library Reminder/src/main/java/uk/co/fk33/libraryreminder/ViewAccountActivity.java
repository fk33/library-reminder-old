/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.ads.AdView;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;
import uk.co.fk33.libraryreminder.database.LibRemindDbAdapter;

public class ViewAccountActivity extends SherlockFragmentActivity
        implements ViewAccountFragment.ViewAccountParent,
        DbInteractionsFragment.EditAccountActivity,
        LoaderManager.LoaderCallbacks<Cursor>,
        ConfirmDeleteDialogFragment.DeleteFragParent,
        EditAccountDialogFragment.EditFragParent,
        ItemInfoDialogFragment.ItemInfoDialogParent {

    public static final String ACCOUNT_NAME = "account_name";
    public static final String ACCOUNT_REMIND_SETTING = "account_remind_setting";
    public static final String VIEW_ACCOUNT_FRAGMENT_TAG = "view_account_fragment";
    public static final String CLOSE_VIEW_ACCOUNT_ACTIVITY = "uk.co.fk33.libraryreminder.CLOSE_VIEW_ACCOUNT_ACTIVITY";
    public static final String ON_CHECKBOX_CLICKED = "uk.co.fk33.libraryreminder.ON_CHECKBOX_CLICKED";
    public static final String CHANGE_ACCOUNT_BEING_SHOWN = "uk.co.fk33.libraryreminder.CHANGE_ACCOUNT_BEING_SHOWN";
    public static final String ACCOUNT_TO_SHOW = "account_to_show";
    public static final int ACCOUNT_ITEMS_QUERY = 100;
    public static final int RENEW_ALL_MENU_ID = 101;

    private String mAccountName;
    private BroadcastReceiver mMessageReceiver;

    private AdView mAdView = null;
    private static final String AD_UNIT_ID = "ca-app-pub-7201467881072683/9922157250";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* This call has to be made here. We want it to be called when the view account fragment
        is destroyed, but when the orientation changes, the view account fragment is destroyed
        twice, as the Android system automatically destroys and recreates it, and then we have to
        destroy it and recreate it ourselves, because we have to put it in its own activity if we
        are in portrait mode, but not if we are in landscape mode, and this necessitates destroying
        and recreating it when changing between orientations. We don't want the following call to be
        made twice as that would lead to certain code being called twice, which is exactly what
        we are trying to avoid by having this setting in the first place, so we have to put it here
        instead of in the onDestroy method of the viewAccount fragment. */
        dbFrag.clearNoItemsHintShownFor();

        /* If Activity has been switched to landscape mode on a large screen finish this activity as
        it won't be needed. */
        if (dbFrag.getInLargeLandMode()) {

            finish();
            return;
        }

        /* Register a broadcast receiver so this activity can be closed from elsewhere in the app,
        and so that the db frag can call the onCheckBoxClicked method to the view account frag when
        it's hosted by this activity rather than by the activity the db frag is attached to. */
        mMessageReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction() == null) return;

                if (intent.getAction().equals(CLOSE_VIEW_ACCOUNT_ACTIVITY)) {

                    /* Finish this activity when the message is received. */
                    DbFragmentRef.get().clearAccountBeingViewed();
                    finish();
                }

                if (intent.getAction().equals(CHANGE_ACCOUNT_BEING_SHOWN)) {

                    String accountToShow = intent.getStringExtra(ACCOUNT_TO_SHOW);
                    if (accountToShow != null) {

                        mAccountName = accountToShow;
                        setTitle(mAccountName);
                        DbInteractionsFragment dbFrag = DbFragmentRef.get();
                        dbFrag.setAccountBeingViewed(mAccountName);

                        boolean accountRemindSetting = intent.getBooleanExtra(ACCOUNT_REMIND_SETTING,
                                false);
                        dbFrag.storeRemindSettingOfAccountBeingViewed(accountRemindSetting);
                        dbFrag.setAccountRemindSettingChangedSinceAccountOpened(false);

                        Bundle viewAccountBundle = new Bundle();
                        viewAccountBundle.putString(ViewAccountActivity.ACCOUNT_NAME, mAccountName);

                        runCursorLoader(ViewAccountActivity.ACCOUNT_ITEMS_QUERY, viewAccountBundle);
                    }
                }

                if (intent.getAction().equals(ON_CHECKBOX_CLICKED)) {

                    View checkBox = DbFragmentRef.get().getCheckBoxRef();

                    if (checkBox != null) {

                        ViewAccountFragment viewAccountFragment = (ViewAccountFragment)
                                getSupportFragmentManager().findFragmentByTag(VIEW_ACCOUNT_FRAGMENT_TAG);
                        viewAccountFragment.onCheckBoxClicked(checkBox);
                        DbFragmentRef.get().storeCheckBoxRef(null);
                    }
                }
            }
        };

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(mMessageReceiver,
                new IntentFilter(CLOSE_VIEW_ACCOUNT_ACTIVITY));
        localBroadcastManager.registerReceiver(mMessageReceiver,
                new IntentFilter(CHANGE_ACCOUNT_BEING_SHOWN));
        localBroadcastManager.registerReceiver(mMessageReceiver,
                new IntentFilter(ON_CHECKBOX_CLICKED));

        /* Retrieve the name of the account being viewed, and the account's remind setting. */
        Intent viewAccountActivityIntent = getIntent();
        mAccountName = viewAccountActivityIntent.getStringExtra(ACCOUNT_NAME);
        setTitle(mAccountName);

        boolean accountRemindSetting = viewAccountActivityIntent.getBooleanExtra(ACCOUNT_REMIND_SETTING,
                false);

        if (savedInstanceState == null) {

            ViewAccountFragment viewAccountFragment = new ViewAccountFragment();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(android.R.id.content, viewAccountFragment,
                    VIEW_ACCOUNT_FRAGMENT_TAG);

            /* If an account was being edited, show an edit account frag so editing
             can continue. (It is possible for there to be an account being edited
             even though the savedInstanceState is null - this would happen if the edit
             frag was opened by the main activity while the app was in dual-pane mode and
             the device was then switched to single-pane mode, creating a new view account
             activity. The view account activity would then take responsibility for
             recreating the edit frag.) */
            if (dbFrag.getAccountBeingEdited()) {

                Fragment.SavedState editFragSavedState = dbFrag.getEditFragSavedState();
                if (editFragSavedState != null) {

                    EditAccountDialogFragment editAccountDialogFragment =
                            new EditAccountDialogFragment();
                    editAccountDialogFragment.setInitialSavedState(editFragSavedState);

                    transaction.add(editAccountDialogFragment, MainActivity.EDIT_ACCOUNT_FRAGMENT_TAG);
                }
            }

            transaction.commit();

            /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/
            /* (View account action mode currently disabled, may be re-enabled in future). */
//            if (dbFrag.getRestoreViewAccountActionMode()) {
//
//                /* We crash if we try to restore action mode now so set it to be restored later. */
//                viewAccountFragment.setActionModeToBeRestored(true);
//            }
        }

        /* Store name of account being viewed in the DbInteractionsFragment, which is not destroyed on config
        changes. This provides a way of checking whether the view account fragment was active before a config
        change. This way, if the config changes from portrait (single-pane) to landscape (dual-pane) while an
        account is being viewed, we'll be able to show the account which was being viewed in the newly-created
        right-hand pane. Also store the account's remind boolean in the dbfrag, unless it's been changed
        and the account hasn't been closed (meaning the setting in the db hasn't been updated yet). */
        dbFrag.setAccountBeingViewed(mAccountName);

        if (!dbFrag.accountRemindSettingChangedSinceAccountOpened()) {

            dbFrag.storeRemindSettingOfAccountBeingViewed(accountRemindSetting);
        }

        /* Set up (or refresh if this activity is being recreated) the cursor containing the list of items
        for the account. */
        Bundle viewAccountBundle = new Bundle();
        viewAccountBundle.putString(ViewAccountActivity.ACCOUNT_NAME, mAccountName);

        runCursorLoader(ViewAccountActivity.ACCOUNT_ITEMS_QUERY, viewAccountBundle);

        dbFrag.setViewAccountActivityState(MainActivity.STATE_CREATED);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdView == null) {

            /* Hide the ad separator. */
            View adSeparator = findViewById(R.id.view_account_ad_separator);
            if (adSeparator != null) {

                adSeparator.setVisibility(View.GONE);
            }

        } else {

            mAdView.resume();
        }

        DbFragmentRef.get().setViewAccountActivityState(MainActivity.STATE_RESUMED);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mAdView != null) {

            mAdView.pause();
        }

        DbFragmentRef.get().setViewAccountActivityState(MainActivity.STATE_PAUSED);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mAdView != null) {

            mAdView.destroy();
        }

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);

        /* Unregister the receiver so no references remain to this activity after it's closed. */
        localBroadcastManager.unregisterReceiver(mMessageReceiver);

        DbFragmentRef.get().setViewAccountActivityState(MainActivity.STATE_DESTROYED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        boolean result = super.onCreateOptionsMenu(menu);

        getSupportMenuInflater().inflate(R.menu.view_account, menu);
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int option = item.getItemId();

        switch (option) {

            case RENEW_ALL_MENU_ID: {
                DbFragmentRef.get().renewAllItemsForAccount(mAccountName);
                return true;
            }

            case R.id.action_sync_open_account: {

                /* Close this activity, after sending a message to the account list to
                sync the account being shown. */

                Intent closeViewAccountActivityIntent =
                        new Intent(MainActivity.ACCOUNT_LIST_ACTION);
                closeViewAccountActivityIntent.putExtra(MainActivity.ACCOUNT_LIST_ACTION_FLAG,
                        MainActivity.ACCOUNT_LIST_ACTION_FLAG_SYNC);
                closeViewAccountActivityIntent.putExtra(MainActivity.ACCOUNT_TO_DO_ACTION_TO, mAccountName);
                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(closeViewAccountActivityIntent);

                DbFragmentRef.get().clearAccountBeingViewed();
                finish();

                return true;
            }

            case R.id.action_delete_open_account: {

                ConfirmDeleteDialogFragment confirmationDialog = new ConfirmDeleteDialogFragment();
                Bundle deleteArgs = new Bundle();
                deleteArgs.putStringArray(ConfirmDeleteDialogFragment.ACCOUNTS_TO_DELETE, new String[]{mAccountName});
                confirmationDialog.setArguments(deleteArgs);
                confirmationDialog.show(getSupportFragmentManager(), MainActivity.CONFIRM_DELETE_FRAGMENT_TAG);

                return true;
            }

            case R.id.action_edit_account: {

                DbInteractionsFragment dbInteractionsFragment = DbFragmentRef.get();
                String accountBeingViewed = dbInteractionsFragment.getAccountBeingViewed();
                if (!accountBeingViewed.equals("")) {

                    /* Show the edit account frag, it will be populated when the db
                    query started below returns. */
                    showEditAccountFragment();

                    /* This call starts the async task to read the info about the account we
                    need to edit from the db. When the task is finished it calls the method
                    to populate the edit account fragment (populateEditAccountFragment). */
                    dbInteractionsFragment.runEditAccountQuery(this, accountBeingViewed);
                }

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void runCursorLoader(int loaderId, Bundle argsBundle) {

        getSupportLoaderManager().restartLoader(loaderId, argsBundle, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle argsBundle) {

        DbInteractionsFragment dbInteractionsFragment = DbFragmentRef.get();

        switch (loaderId) {

            case ACCOUNT_ITEMS_QUERY: {

                dbInteractionsFragment.setItemsLoading(true);

                String[] accountName = new String[]
                        {argsBundle.getString(ViewAccountActivity.ACCOUNT_NAME)};
                String selectionString = LibRemindDbAdapter.KEY_ACCOUNT + "=?";
                String[] columnsToReturn = new String[]{LibRemindDbAdapter.KEY_ROWID,
                        LibRemindDbAdapter.KEY_TITLE, LibRemindDbAdapter.KEY_AUTHOR,
                        LibRemindDbAdapter.KEY_DUE_DATE, LibRemindDbAdapter.KEY_UNIQUE_IDENTIFIER};

                return dbInteractionsFragment.getDatabaseAdapter().createCursorLoader(
                        LibRemindDbAdapter.DATABASE_ITEMS_TABLE, columnsToReturn,
                        selectionString, accountName, null);
            }

            default: {

                return null;
            }
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        switch (loader.getId()) {

            case ACCOUNT_ITEMS_QUERY: {

                /* Put the cursor into the adapter used with the item list listview*/
                SimpleCursorAdapter itemListCursorAdapter = dbFrag.getItemListCursorAdapter();
                itemListCursorAdapter.swapCursor(cursor);

                setTitle(new DateParser().makeTitle(this, itemListCursorAdapter.getCount()));

                dbFrag.setItemsLoading(false);

                ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                        .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                /* Check if the account being shown has never been synced, or has no loans. In either of these
                cases a message will be shown letting the user know why there are no loans displayed.
                This method is also called in onActivityCreated in the view account fragment, so it will
                be run when a new view frag is created in portrait mode. */
                if (viewAccountFragment != null) viewAccountFragment.showHintOnEmptyAccount();

                break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        switch (loader.getId()) {

            case ACCOUNT_ITEMS_QUERY: {

                DbFragmentRef.get().getItemListCursorAdapter().swapCursor(null);
                break;
            }
        }

        /* If this method is being called in large landscape mode, it means the app was paused in
        portrait mode with the view account activity open, then resumed in landscape mode. In
        this case we need to re-run to items query so the new view account fragment created in
        large landscape mode will be showing correct information. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (dbFrag.getInLargeLandMode()) {

            dbFrag.setNeedToRerunItemsQuery(true);
        }
    }

    public void renewItems(String[] itemsToRenew) {

    }

    public void showEditAccountFragment() {

       /* Create an edit account fragment. It will be populated with the account info when
        the async db write called immediately after this method is called returns. */
        new EditAccountDialogFragment().show(getSupportFragmentManager(),
                MainActivity.EDIT_ACCOUNT_FRAGMENT_TAG);
    }

    public void populateEditAccountFragment(String[] accountDetails) {

        EditAccountDialogFragment editAccountDialogFragment = (EditAccountDialogFragment)
                getSupportFragmentManager().findFragmentByTag(MainActivity.EDIT_ACCOUNT_FRAGMENT_TAG);

        if (editAccountDialogFragment != null) {

            editAccountDialogFragment.storeExistingAccountDetails(accountDetails);

            int region = Integer.parseInt(accountDetails[5]);
            editAccountDialogFragment.loadOrganisationData(region);

            String orgName = accountDetails[1];
            editAccountDialogFragment.setSelectedOrganisationOrStoreNameToSetLater(orgName);

            Dialog editAccountDialog = editAccountDialogFragment.getDialog();

            /* Set the reminder switch to the stored value.*/
            boolean remind = Boolean.valueOf(accountDetails[4]);
            CompoundButton reminderSwitch = (CompoundButton) editAccountDialog
                    .findViewById(R.id.edit_account_reminder_switch);
            reminderSwitch.setChecked(remind);

            /* Fill in the fields in the form with the existing account information. */
            TextView editAccountTextView = (TextView) editAccountDialog
                    .findViewById(R.id.edit_account_organisation_textview);
            editAccountTextView.setText(orgName);

            EditText editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_card_number);

            /* Some Spydus catalogues add a prefix when sending the card number to login. Hide the prefix
             * from the user.*/
            String cardNumber = accountDetails[2];
            if (orgName.equals("Bolton") || orgName.equals("Calderdale") || orgName.equals("Salford")) {

                cardNumber = removePrefix(orgName, cardNumber);
            }

            editAccountEditText.setText(cardNumber);

            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_pin_or_password);
            editAccountEditText.setText(accountDetails[3]);

            /* Populate this one last so it will have focus. */
            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_account_name_edittext);
            editAccountEditText.setText(accountDetails[0]);

            /* Store this boolean. When edit account frag is destroyed this boolean lets us
            figure out whether it's being destroyed due to an orientation change, which we
            need to know so that, we can manually recreate the edit frag in a different
            activity, which is the only way to go from have a single activity to two
            activites with the edit account frag remaining in the foreground. */
            DbFragmentRef.get().setAccountBeingEdited(true);
        }
    }

    public void storeEditFragSavedState() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment editAccountFrag = fragmentManager.findFragmentByTag(MainActivity.EDIT_ACCOUNT_FRAGMENT_TAG);
        if (editAccountFrag != null) {

            DbFragmentRef.get().storeEditFragSavedState(fragmentManager
                    .saveFragmentInstanceState(editAccountFrag));
        }
    }

    @Override
    public void deleteAccounts(String[] accountsToDelete) {

        /* Delete the account being displayed by this activity, then close this activity. */
        DbInteractionsFragment dbInteractionsFragment = DbFragmentRef.get();
        dbInteractionsFragment.clearAccountBeingViewed();
//        dbInteractionsFragment.clearLastAccountViewed();
        dbInteractionsFragment.deleteAccounts(accountsToDelete);
        finish();
    }

    /**
     * Pressing back when contextual action mode is in effect fortunately doesn't
     * cause this function to be called. This function is only called back is pressed
     * while not in action mode.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        dbFrag.setAccountRemindSettingChangedSinceAccountOpened(false);
        dbFrag.clearAccountBeingViewed();
    }

    public void updateAccountListActionModeTitle() {

        /* This activity never hosts an account list fragment so this method
        doesn't need to be implemented. */
    }

    public void updateViewAccountActionModeTitle() {

        ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                .findFragmentByTag(VIEW_ACCOUNT_FRAGMENT_TAG);

        if (viewAccountFragment != null) {

            viewAccountFragment.updateActionModeTitle();
        }
    }

    public void removeItemInfoFrag() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        ItemInfoDialogFragment itemInfoDialogFragment = (ItemInfoDialogFragment)
                fragmentManager.findFragmentByTag(MainActivity.ITEM_INFO_FRAGMENT_TAG);
        if (itemInfoDialogFragment != null)
            fragmentManager.beginTransaction().remove(itemInfoDialogFragment).commit();
    }

    public void removeDualPaneItemInfoFrag() {
    }

    private String removePrefix(String orgName, String cardNumber) {

        if (orgName.equals("Bolton")) {

            cardNumber = cardNumber.substring(3);
        }

        if (orgName.equals("Calderdale")) {

            cardNumber = cardNumber.substring(1);
        }

        if (orgName.equals("Salford")) {

            cardNumber = cardNumber.substring(2);
        }

        return cardNumber;
    }
}
