/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.content.ContentValues;

public class DataToWriteOnRenew extends DataToWrite {

    private long[] mNewDueDates;
    private int[] mNewRenewCounts;

    /**
     * @param rowIds         rowIds of rows to update
     * @param newDueDates    An array containing a new due_date for each row being updated
     * @param newRenewCounts An array containing a new times_renewed for each row being updated
     */
    public DataToWriteOnRenew(long[] rowIds, long[] newDueDates, int[] newRenewCounts) {

        super(rowIds);

        if (newDueDates.length != mSize || newRenewCounts.length != mSize) {
            throw new IllegalArgumentException("DataToWrite: The newDueDates and newRenewCounts arrays must both " +
                    "be the same size as the rowIds array. One or both did not match.");
        }

        mNewDueDates = newDueDates;
        mNewRenewCounts = newRenewCounts;
        mTable = LibRemindDbAdapter.DATABASE_ITEMS_TABLE;

    }

    /**
     * Put the values of each element from the member arrays into a ContentValues object in an array of ContentValues
     * objects. Set the key for each value to be the column to be written to in the db.
     *
     * @return The array of ContentValues instances, one for each db row to be updated
     */
    @Override
    public ContentValues[] getContentValues() {

        ContentValues[] newValues = new ContentValues[mSize];

        for (int i = 0; i < mSize; i++) {
            newValues[i] = new ContentValues();
            newValues[i].put(LibRemindDbAdapter.KEY_DUE_DATE, mNewDueDates[i]);
            newValues[i].put(LibRemindDbAdapter.KEY_TIMES_RENEWED, mNewRenewCounts[i]);
        }
        return newValues;
    }
}
