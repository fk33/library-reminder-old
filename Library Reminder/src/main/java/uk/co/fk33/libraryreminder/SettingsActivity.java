
package uk.co.fk33.libraryreminder;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.*;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.*;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* We are using some deprecated methods but as we are supporting pre-11 API devices
        we don't really have any choice.*/
        addPreferencesFromResource(R.xml.preferences);

        Preference marginPreference = findPreference(getString(R.string.margin_pref));
        updateMarginPrefSummary(marginPreference, DbFragmentRef.get().getMarginAsString());

        marginPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference marginPreference, Object margin) {

                String marginAsString = (String) margin;
                updateMarginPrefSummary(marginPreference, marginAsString);

                DbInteractionsFragment dbFrag = DbFragmentRef.get();
                SimpleCursorAdapter cursorAdapter = dbFrag.getAccountListCursorAdapter();

                if (cursorAdapter != null) {

                    LibRemindViewBinder accountListViewBinder =
                            (LibRemindViewBinder) cursorAdapter.getViewBinder();

                    if (accountListViewBinder != null) {

                        accountListViewBinder.updateMarginPref(Integer.parseInt(marginAsString));
                    }
                }

                cursorAdapter = dbFrag.getItemListCursorAdapter();

                if (cursorAdapter != null) {

                    ItemListViewBinder itemListViewBinder =
                            (ItemListViewBinder) cursorAdapter.getViewBinder();

                    if (itemListViewBinder != null) {

                        itemListViewBinder.updateMarginPref(Integer.parseInt(marginAsString));
                    }
                }

                return true;
            }
        });

        Preference syncNetworkPreference = findPreference(getString(R.string.sync_network_pref_key));
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String syncNetworkPrefKey = getString(R.string.sync_network_pref_key);
        String syncNetworkPrefDefault = getString(R.string.wifi);
        String syncNetworkPrefSummary = prefs.getString(syncNetworkPrefKey, syncNetworkPrefDefault);
        syncNetworkPreference.setSummary(syncNetworkPrefSummary);

        syncNetworkPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference syncNetworkPreference, Object newValue) {

                syncNetworkPreference.setSummary((String) newValue);
                return true;
            }
        });

        boolean autoSyncEnabled = prefs.getBoolean(getString(R.string.auto_sync_pref_key), true);

        if (!autoSyncEnabled) {

            Preference autoSyncTimePreference = findPreference(getString(R.string.sync_time_pref_key));
            autoSyncTimePreference.setEnabled(false);
        }

        Preference autoSyncPreference = findPreference(getString(R.string.auto_sync_pref_key));
        autoSyncPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                boolean autoSyncEnabled = (Boolean) newValue;

                /* Grey out the sync time pref if auto sync has been turned off. */
                Preference autoSyncTimePreference = findPreference(getString(R.string.sync_time_pref_key));
                autoSyncTimePreference.setEnabled(autoSyncEnabled);

                if (autoSyncEnabled) {

                    MainActivity.activateAutoSyncAlarm(SettingsActivity.this);
                } else {

                    MainActivity.deactivateAutoSyncAlarm(SettingsActivity.this);

                    /* Warn the user that with auto-sync off they won't get reminders.
                    * We can't use dialog fragments, because preferenceactivity doesn't include
                    * getSupportFragmentManager(), and we are supporting APIs prior to 11, so
                    * we have to use alert dialog. This means that if the user changes orientation
                    * while the dialog is open, it will disappear. */
                    new AlertDialog.Builder(SettingsActivity.this)
                            .setTitle(R.string.autosync_off)
                            .setMessage(R.string.autosync_off_reminder_warning)
                            .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setIcon(R.drawable.ic_dialog_alert)
                            .show();
                }

                return true;
            }
        });

        if (Build.VERSION.SDK_INT >= 11) {

            hideVibratePrefIfNoVibrator();
        }

        BroadcastReceiver messageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                finish();
            }
        };

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(messageReceiver,
                new IntentFilter(MainActivity.CLOSE_SETTINGS_ACTIVITY));

        DbFragmentRef.get().setSettingsActivityState(MainActivity.STATE_CREATED);
    }

    @Override
    protected void onResume() {
        super.onResume();

        DbFragmentRef.get().setSettingsActivityState(MainActivity.STATE_RESUMED);
    }

    @Override
    protected void onPause() {
        super.onPause();

        DbFragmentRef.get().setSettingsActivityState(MainActivity.STATE_PAUSED);
    }

    private void updateMarginPrefSummary(Preference marginPreference, String margin) {

        String marginPrefSummary;

        if (margin.equals("0")) {

            marginPrefSummary = getString(R.string.due_that_day);
        } else {

            if (margin.equals("1")) {

                marginPrefSummary = getString(R.string.due_within_1_day);
            } else {

                marginPrefSummary = getString(R.string.due_within) + " " + margin + " "
                        + getString(R.string.days);
            }
        }

        marginPreference.setSummary(marginPrefSummary);
    }

    @TargetApi(11)
    private void hideVibratePrefIfNoVibrator() {

        boolean vibrator = ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).hasVibrator();

        if (!vibrator) {

            PreferenceCategory remindersPrefCategory = (PreferenceCategory)
                    findPreference("pref_key_reminder_settings");
            Preference vibratePreference = findPreference("pref_vibrate");

            remindersPrefCategory.removePreference(vibratePreference);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        DbFragmentRef.get().setSettingsActivityState(MainActivity.STATE_DESTROYED);
    }
}