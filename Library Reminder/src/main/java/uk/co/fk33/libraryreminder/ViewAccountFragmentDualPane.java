/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.content.res.Configuration;
import android.database.Cursor;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class ViewAccountFragmentDualPane extends ViewAccountFragment {

    @Override
    public void onDetach() {
        super.onDetach();

        mFragmentParent = null;

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (dbFrag.getViewAccountInActionMode()) {

            dbFrag.setRestoreViewAccountActionMode(true);
            dbFrag.getActionModeRef().finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (!dbFrag.getItemBeingViewed().equals("")) {

            /* If this fragment is being created, and an item was already open, we must have
            * just changed orientation. If we are in landscape mode, and we are on a large screen,
            * we must have just changed from single-pane to dual-pane mode. Make a new item
            * info dialog to replace the one which was open, as it would have been removed during
            * the change from two activities to one.*/
            if (dbFrag.getOnLargeLandDevice() && getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_LANDSCAPE) {

                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();
                ItemInfoDialogFragmentDualPane itemInfoDialogFragmentDualPane =
                        new ItemInfoDialogFragmentDualPane();
                transaction.add(itemInfoDialogFragmentDualPane,
                        MainActivity.ITEM_INFO_FRAGMENT_TAG_DUAL_PANE).commit();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

        /* This is part of the code which allows you to put the list into action mode. There are
        * no bulk functions for items in this version of the app so it has been removed.
        * It may be re-enabled if future versions of the app include bulk item fuctions.*/

//        /* If we're in action mode, clicking the item checks that item's checkbox. */
//        if (dbFrag.getViewAccountInActionMode()) {
//
//            CheckBox checkBox = (CheckBox) view.findViewById(R.id.view_account_checkbox);
//            if (checkBox != null) {
//
//                if (checkBox.isChecked()) {
//
//                    checkBox.setChecked(false);
//
//                } else {
//
//                    checkBox.setChecked(true);
//                }
//
//                onCheckBoxClicked(checkBox);
//            }
//        } else {

            /* Get the uniqueIdentifier of the item which has been clicked.*/
            Cursor cursor = ((SimpleCursorAdapter) parent.getAdapter()).getCursor();
            cursor.moveToPosition(position);
            String itemUniqueId = cursor.getString(4);

            /* Store the unique id of the item being viewed in the db frag. When the item info
            * dialog is created, it retrieves this id from the dbrag, and runs the appropriate
            * async db query to get the item's info, to populate itself with.
            * The dbFrag itemBeingViewed string is cleared when the item dialog is cancelled, but
            * not when it is destroyed. This is so it will repopulate itself after an orientation
            * change.*/
            dbFrag.setItemBeingViewed(itemUniqueId);

            /*If we're not in action mode, open an item info dialog to show details of the
            * clicked item.*/
            FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                    .beginTransaction();
            ItemInfoDialogFragmentDualPane itemInfoDialogFragmentDualPane =
                    new ItemInfoDialogFragmentDualPane();
            transaction.add(itemInfoDialogFragmentDualPane,
                    MainActivity.ITEM_INFO_FRAGMENT_TAG_DUAL_PANE).commit();

        /* THE BRACE BELOW IS PART OF THE REMOVED FUNCTIONALITY. */
//        }

    }

    @Override
    public ItemInfoDialogFragment getItemInfoDialogFragment() {

        /* This needs to be overridden for the dual-pane view account frag, as the dual pane
        * view account frag needs to find the dual pane item info dialog, rather than the single-
        * pane one.*/
        return (ItemInfoDialogFragment) getActivity()
                .getSupportFragmentManager().findFragmentByTag(MainActivity.ITEM_INFO_FRAGMENT_TAG_DUAL_PANE);
    }
}
