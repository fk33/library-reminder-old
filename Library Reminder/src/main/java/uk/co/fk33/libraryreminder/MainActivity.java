/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import uk.co.fk33.libraryreminder.accounts.ArenaAccount;
import uk.co.fk33.libraryreminder.accounts.DummyAccount;
import uk.co.fk33.libraryreminder.accounts.ElibraryAccount;
import uk.co.fk33.libraryreminder.accounts.HeritageAccount;
import uk.co.fk33.libraryreminder.accounts.LibAccount;
import uk.co.fk33.libraryreminder.accounts.PortfolioAccount;
import uk.co.fk33.libraryreminder.accounts.Prism2Account;
import uk.co.fk33.libraryreminder.accounts.Prism3Account;
import uk.co.fk33.libraryreminder.accounts.SpydusAccount;
import uk.co.fk33.libraryreminder.accounts.ViewpointAccount;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentCodeFactory;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentPrefWriter;
import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;
import uk.co.fk33.libraryreminder.database.LibRemindDbAdapter;
import uk.co.fk33.libraryreminder.network.AutoSyncService;

public class MainActivity extends SherlockFragmentActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        DbInteractionsFragment.AccountListActivity,
        DbInteractionsFragment.EditAccountActivity,
        AccountListFragment.AccountListParent,
        ViewAccountFragment.ViewAccountParent,
        ConfirmDeleteDialogFragment.DeleteFragParent,
        EditAccountDialogFragment.EditFragParent,
        ItemInfoDialogFragment.ItemInfoDialogParent,
        AboutDialogFragment.AboutDialogParent,
        SelectorDialogFragment.SelectorDialogParent {

    public static final String DB_FRAGMENT_TAG = "db_interactions_fragment";
    public static final String ADD_ACCOUNT_FRAGMENT_TAG = "add_account_dialog_fragment";
    public static final String EDIT_ACCOUNT_FRAGMENT_TAG = "edit_account_dialog_fragment";
    public static final String ITEM_INFO_FRAGMENT_TAG = "item_info_dialog_fragment";
    public static final String ITEM_INFO_FRAGMENT_TAG_DUAL_PANE = "item_info_dialog_fragment_dual_pane";
    public static final String SET_ORGANISATION_FRAGMENT_TAG = "set_organisation_dialog_fragment";
    public static final String ABOUT_FRAGMENT_TAG = "about_dialog_fragment";
    public static final String WAIT_FOR_PROGRESS_FRAGMENT_TAG = "wait_for_progress_fragment";
    public static final String LICENSE_FRAGMENT_TAG = "license_fragment_tag";
    public static final String CONFIRM_DELETE_FRAGMENT_TAG = "confirm_delete_fragment";
    public static final String SELECTOR_FRAGMENT_TAG = "selector_fragment_tag";
    public static final String ACCOUNT_LIST_ACTION = "uk.co.fk33.libraryreminder.ACCOUNT_LIST_ACTION";
    public static final String SCROLL_TO_ACCOUNT = "uk.co.fk33.libraryreminder.SCROLL_TO_ACCOUNT";
    public static final String LIB_ACCOUNT_NULLIFY_LISTVIEW_REF = "uk.co.fk33.libraryreminder.LIB_ACCOUNT_NULLIFY_LISTVIEW_REF";
    public static final String LIB_ACCOUNT_UPDATE_POSITION = "uk.co.fk33.libraryreminder.LIB_ACCOUNT_UPDATE_POSITION";
    public static final String CLOSE_ALL_FRAGMENTS = "uk.co.fk33.libraryreminder.CLOSE_ALL_FRAGMENTS";
    public static final String CLOSE_SETTINGS_ACTIVITY = "uk.co.fk33.libraryreminder.CLOSE_SETTINGS_ACTIVITY";
    public static final String ACTION_RESPOND_TO_NOTIFICATION = "uk.co.fk33.libraryreminder.ACTION_RESPOND_TO_NOTIFICATION";
    public static final String ACTION_RESPOND_TO_NOTIFICATION_SYNC_PROBLEM = "uk.co.fk33.libraryreminder.ACTION_RESPOND_TO_NOTIFICATION_SYNC_PROBLEM";
    public static final String ACTION_REGISTER_NOTIFICATION_DISMISSAL = "uk.co.fk33.libraryreminder.ACTION_REGISTER_NOTIFICATION_DISMISSAL";
    public static final String ACTION_OPEN_ACCOUNT = "uk.co.fk33.libraryreminder.ACTION_OPEN_ACCOUNT";
    public static final String APP_OPENED = "uk.co.fk33.libraryreminder.APP_OPENED";
    public static final String ACCOUNT_LIST_ACTION_FLAG = "account_list_action_flag";
    public static final String ACCOUNT_TO_DO_ACTION_TO = "account_to_do_action_to";
    public static final String ACCOUNT_TO_OPEN = "account_to_open";
    public static final String SUGGEST_NAME = "suggest_name";
    public static final String MARGIN_PREF = "margin_pref";
    public static final String SYNC_TIME_PREF_KEY = "sync_time_pref_key";
    public static final String SYNC_NETWORK_PREF_KEY = "sync_network_pref";
    public static final String NET_PREF_WIFI = "Wi-Fi";
    public static final String NET_PREF_WIFI_DATA = "Wi-Fi or mobile data";
    public static final String PREF_PLAY_A_SOUND = "pref_play_a_sound";
    public static final String PREF_VIBRATE = "pref_vibrate";
    public static final String SELECTOR_OPTIONS = "selector_options";
    public static final String SELECTOR_TITLE = "selector_title";
    public static final String SELECTOR_OFFSET = "selector_last_choice";
    public static final String REGION = "REGION";
    public static final String FREE = "free";
    public static final String PAID = "paid";
    public static final int ACCOUNT_LIST_ACTION_FLAG_SYNC = 1;
    public static final int ACCOUNT_HEADERS_QUERY = 0;
    public static final int NEW_ACCOUNT_QUERY = 1;
    public static final int SYNC_ACCOUNTS_QUERY = 2;
    public static final int PRIVACY_POLICY = 1;
    public static final int TERMS_OF_USE = 2;
    public static final int JSOUP_LICENSE = 3;
    public static final int JODA_TIME_LICENSE = 4;
    public static final int PRETTYTIME_LICENSE = 5;
    public static final int JASYPT_LICENSE = 6;
    public static final int ACTION_BAR_SHERLOCK_LICENSE = 7;
    public static final int APACHE_LICENSE = 8;
    public static final int STATE_CREATED = 1;
    public static final int STATE_RESUMED = 3;
    public static final int STATE_PAUSED = 4;
    public static final int STATE_DESTROYED = 6;
    public static final int PUBLIC_ACCOUNT = 0;
    public static final int COLLEGE_ACCOUNT = 1;
    public static final int UNI_ACCOUNT = 2;
    public static final int HEALTH_ACCOUNT = 3;
    public static final int REGION_ENGLAND_NORTH_MID = 10;
    public static final int REGION_ENGLAND_EAST_SOUTH = 11;
    public static final int REGION_IRELAND = 12;
    public static final int REGION_SCOTLAND = 13;
    public static final int REGION_WALES = 14;
    public static final int REGION_UK_IE_COLLEGE = 15;
    public static final int REGION_UK_IE_UNIVERSITY = 16;
    public static final int REGION_UK_HEALTH = 17;
    public static final int LOCALE_UK = 0;

    private BroadcastReceiver mMessageReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Send a local broadcast that the app has been opened. This is just in case
        the automatic background sync service happens to be running. We don't want
        it making db writes while we are looking at accounts as this would causes issues. */
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        Intent appOpenedIntent = new Intent(APP_OPENED);
        localBroadcastManager.sendBroadcast(appOpenedIntent);

        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.main);
        setSupportProgressBarIndeterminateVisibility(true);

        /* Hide the ad separator. */
        View adSeparator = findViewById(R.id.account_list_ad_separator);
        if (adSeparator != null) {

            adSeparator.setVisibility(View.GONE);
        }


        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        /* We'll be using fragments whether or not we're starting for the first time or being restored. Get fragment
        manager and start a transaction. */
        FragmentManager fragmentManager = getSupportFragmentManager();

        /* If we're starting for the first time, create the DbInteractionsFragment. If we're just
        resuming, get a reference to it. The DbInteractionsFragment persists through configuration
        changes etc. as it keeps an open connection to the db. It handles all db queries. */
        if (savedInstanceState == null) {

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            DbInteractionsFragment dbFrag = ensureDbFragSetup();
            transaction.add(dbFrag, DB_FRAGMENT_TAG);

            /* Determine if we're on a screen which will use resources from the "large-land"
            layout folder when in landscape mode. */
            determineScreenSize();

            /* If we're in large-land mode, add the view account fragment. (The account list
            fragment is added in xml).
            At this stage, the db frag can't call getResources to find out config, so we'll
            do that here for now. */
            if (dbFrag.getOnLargeLandDevice() &&
                    getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                transaction.add(R.id.fragment_container, new ViewAccountFragmentDualPane(),
                        ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);
            }

            /* Commit the fragment transactions. */
            transaction.commit();

            /* Set up the cursor adapters */
            dbFrag.initAccountListCursorAdapter();
            dbFrag.initItemViewAdapter();
        } else {

            DbInteractionsFragment dbFrag = (DbInteractionsFragment) fragmentManager.findFragmentByTag(DB_FRAGMENT_TAG);
            if (dbFrag == null) {

                dbFrag = ensureDbFragSetup();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(dbFrag, DB_FRAGMENT_TAG).commit();
            }

            if (!dbFrag.getScreenSizeDetermined()) determineScreenSize();

            /* Changing orientation or restarting the phone can make the account list cursor null,
            or close it, or make the cursorAdapter null if the dbFrag is destroyed and recreated
            with empty constructor. */
            SimpleCursorAdapter accountListCursorAdapter = dbFrag.getAccountListCursorAdapter();
            boolean accountListCursorInvalid = false;

            if (accountListCursorAdapter == null) {

                accountListCursorInvalid = true;
            } else {

                if (accountListCursorAdapter.getCursor() == null) {

                    accountListCursorInvalid = true;
                } else {

                    if (accountListCursorAdapter.getCursor().isClosed()) {

                        accountListCursorAdapter.swapCursor(null);
                        accountListCursorInvalid = true;
                    }
                }
            }

            if (accountListCursorInvalid) {

                dbFrag.initAccountListCursorAdapter();
            }

            /* Also make sure the item list cursor adapter and cursor aren't null or closed. */
            SimpleCursorAdapter itemListCursorAdapter = dbFrag.getItemListCursorAdapter();
            boolean itemListCursorInvalid = false;

            if (itemListCursorAdapter == null) {

                itemListCursorInvalid = true;
            } else {

                if (itemListCursorAdapter.getCursor() == null) {

                    itemListCursorInvalid = true;
                } else {

                    if (itemListCursorAdapter.getCursor().isClosed()) {

                        itemListCursorAdapter.swapCursor(null);
                        itemListCursorInvalid = true;
                    }
                }
            }

            if (itemListCursorInvalid) {

                dbFrag.initItemViewAdapter();
            }

            /* Check if we're in large-land mode. If so, check there is a view account fragment
            present. If not, that means we've just switched from single-fragment portrait mode,
            so we have to create the view account fragment here. We should also restore the edit
            account dialog fragment if it was open when the orientation changed, as it will be
            destroyed when the view account activity tests for large land mode and destroys itself. */
            if (dbFrag.getInLargeLandMode()) {

                ViewAccountFragment viewAccountFragment = (ViewAccountFragment) fragmentManager
                        .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                if (viewAccountFragment == null) {

                    viewAccountFragment = new ViewAccountFragmentDualPane();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.add(R.id.fragment_container, viewAccountFragment,
                            ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                    if (dbFrag.getAccountBeingEdited()) {

                        Fragment.SavedState editFragSavedState = dbFrag.getEditFragSavedState();
                        if (editFragSavedState != null) {

                            /* If there's already an edit account frag in this activity, get rid of
                            it. This can happen if you go from portrait to landscape then back to
                            portrait then back to landscape. */
                            EditAccountDialogFragment editAccountDialogFragment = (EditAccountDialogFragment)
                                    fragmentManager.findFragmentByTag(EDIT_ACCOUNT_FRAGMENT_TAG);

                            if (editAccountDialogFragment != null) {

                                editAccountDialogFragment.dismiss();
                            }

                            editAccountDialogFragment = new EditAccountDialogFragment();
                            editAccountDialogFragment.setInitialSavedState(editFragSavedState);

                            transaction.add(editAccountDialogFragment,
                                    EDIT_ACCOUNT_FRAGMENT_TAG);
                        }
                    }

                    transaction.commit();

                    /* Switching to portrait mode removes the possibility of the SherlockFragment 2.3.7
                    bug being present (see a bit further down for a description of this bug)*/
                    dbFrag.setPotentialABSBug(false);
                }

                /* Was the fragment in action mode prior to pause/resume? (We can't put this
                in a savedInstanceState because the info might be required by a different
                activity if we changed orientation (the view account activity). */
                if (dbFrag.getRestoreViewAccountActionMode()) {

                    viewAccountFragment.setActionModeToBeRestored(true);
                }
            } else {

                /* If we're not in large landscape mode, but there is a view account fragment
                present in this activity's fragment manager, it means we've just switched
                to portrait from large landscape mode. Replace the fragment with a fullscreen
                activity and show whatever was being shown before orientation change. */
                ViewAccountFragment viewAccountFragment = (ViewAccountFragment) fragmentManager
                        .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                if (viewAccountFragment != null) {

                    /* Remove the view account fragment. */
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.remove(viewAccountFragment);

                    /* Find if there's an edit account frag as we'll need to remove it or it will be
                    concealed by the new view account activity. (It will be recreated for us by the
                    activity as the details will have been stored in the db frag). */
                    EditAccountDialogFragment editAccountFragment = (EditAccountDialogFragment)
                            fragmentManager.findFragmentByTag(EDIT_ACCOUNT_FRAGMENT_TAG);
                    if (editAccountFragment != null) transaction.remove(editAccountFragment);

                    /* Commit the fragment transaction. */
                    transaction.commit();

                    /* If an account was being viewed, start a new view account activity to show
                    the account. */
                    String accountBeingViewed = dbFrag.getAccountBeingViewed();
                    if (!accountBeingViewed.equals("")) {

                        /* Get the viewed account's remind setting from the db frag and
                        * put it into the intent. (The ViewAccountActivity puts whatever
                        * is in the intent into the dbfrag so if we don't put it in here,
                        * there will be nothing in the intent, so the viewaccountactivity
                        * will overwrite the correct setting stored in the db frag from when
                        * the account was first opened with the default one used when
                        * there is no boolean supplied in the intent). */
                        boolean accountRemindSetting = dbFrag.getRemindSettingOfAccountBeingViewed();

                        Intent intent = new Intent(this, ViewAccountActivity.class);
                        intent.putExtra(ViewAccountActivity.ACCOUNT_NAME, accountBeingViewed);
                        intent.putExtra(ViewAccountActivity.ACCOUNT_REMIND_SETTING, accountRemindSetting);
                        startActivity(intent);

                        /* On Android 2.3.7 there is a bug, presumably in SherlockFragment, where, if the app
                        * is switched from dual-pane to single-pane mode with an account open, and a new view
                        * account frag is therefore created in a new activity on top of the account list frag,
                        * and this activity is then finished, revealing the account list frag, the account list
                        * frag will not respond to clicks. So make a note that we will potentially encounter this
                        * bug, and then elsewhere (i.e. if the view account activity is finished while still in
                        * single-pane mode), take action to remedy the bug. (If the user changes back to dual-pane
                        * mode, the bug goes away).*/
                        dbFrag.setPotentialABSBug(true);
                    }
                }
            }

            /* If the about dialog fragment or the license display fragment are in the fragment
            manager (meaning they were open before the app was paused), remove them and recreate
            them manually. The reason for doing this is that the about dialog fires fragment
            transactions to show the license display fragment, and it cannot fire such
            transactions until the main activity has been "resumed", but the main activity won't
            be resumed (i.e. onResume() won't be called) if the app is reinstantiated with the
            about dialog fragment or the license display fragment open, because they will be on
            top of the activity, covering it. In such cases it seems it isn't considered "resumed"
            until those fragments are dismissed and it is bought to the foreground again.
            Therefore if the about fragment tries to fire a fragment transaction after the system
            has destroyed and recreated the app (e.g. during an orientation change) we get an
            illegalstateexception, as the main activity will presumably not have restored its saved
            state yet (because it is still hidden), and any transactions carried out will be reverted
            when that happens. So we have get rid of the about dialog fragment and any license
            display fragments (thereby allowing the main activity to restore its saved stated) and
            then recreate them. The manually recreated about dialog fragment will then be allowed
            by the system to fire fragment transactions.
            It's possible this workaround could have been avoided by making the license display
            fragment into an alertdialog so that the about fragment wouldn't be required to fire
            fragment transactions to show it, but at the same time it is worth establishing a
            method which allows fragments to fire transactions after an orientation change or
            after being destroyed and automatically recreated by the system.
            Note that we check for and re-add the About fragment here, but we check for and re-add
            the license display fragment in onStart(), because this is the only was to guarantee it
            will be created ABOVE and not BELOW the About fragment (i.e. by delaying the re-add).*/
            AboutDialogFragment aboutDialogFragment = (AboutDialogFragment) fragmentManager
                    .findFragmentByTag(ABOUT_FRAGMENT_TAG);

            if (aboutDialogFragment != null) {

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.remove(aboutDialogFragment);
                aboutDialogFragment = new AboutDialogFragment();
                transaction.add(aboutDialogFragment, ABOUT_FRAGMENT_TAG);
                transaction.commit();
            }

            /* If there was a waitForProgress fragment open during orientation change,
            close it.*/
            DialogFragment waitForProgressFrag = (DialogFragment) fragmentManager
                    .findFragmentByTag(WAIT_FOR_PROGRESS_FRAGMENT_TAG);
            if (waitForProgressFrag != null) {

                waitForProgressFrag.dismiss();
            }
        }

        mMessageReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                if (action == null) {
                    return;
                }

                if (action.equals(ACTION_OPEN_ACCOUNT)) {

                    String accountToOpen = intent.getStringExtra(ACCOUNT_TO_OPEN);
                    if (accountToOpen != null) {

                        if (!accountToOpen.equals("")) {

                            openAccountFromNotification(accountToOpen, true);
                        } else {

                            closeAnyOpenAccount();
                        }
                    }
                }

                if (action.equals(ACCOUNT_LIST_ACTION)) {
                    int actionFlag = intent.getIntExtra(ACCOUNT_LIST_ACTION_FLAG, 0);
                    switch (actionFlag) {

                        case ACCOUNT_LIST_ACTION_FLAG_SYNC: {

                            String accountToSync = intent.getStringExtra(ACCOUNT_TO_DO_ACTION_TO);
                            if (accountToSync != null) {

                                if (!accountToSync.equals("")) {

                                    syncAccounts(new String[]{accountToSync});
                                }
                            }

                            break;
                        }
                    }
                }
            }
        };

        localBroadcastManager.registerReceiver(mMessageReceiver, new IntentFilter(ACCOUNT_LIST_ACTION));
        localBroadcastManager.registerReceiver(mMessageReceiver, new IntentFilter(ACTION_OPEN_ACCOUNT));

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* getSyncTimeOrSetRandomOne checks if the sync time pref is set. If it is, it
        * returns it, otherwise it generates a random sync time, writes it to prefs, and
        * returns it (the sync time is stored as a string). It also set first run true if
        * it doesn't find a sync time in the prefs, so check doubles as a way to check if
        * this is the first run. If it is the first run, set an alarm according for the alarm
        * time returned.*/
        String alarmTimeString = dbFrag.getSyncTimeOrSetRandomOne();
        if (dbFrag.getFirstRun()) {

            dbFrag.setFirstRun(false);
            activateAutoSyncAlarm(this, alarmTimeString);
        }

        Intent intent = getIntent();
        String accountToOpen = intent.getStringExtra(ACCOUNT_TO_OPEN);
        if (accountToOpen != null) {

            if (!accountToOpen.equals("")) {

                openAccountFromNotification(accountToOpen, true);
            }

            intent.removeExtra(ACCOUNT_TO_OPEN);
        }

        /* Initialise the cursor loader which gets account names and summaries from the database,
        to populate the account list. (The cursor is put into a cursoradapter which is set as an
        adapter for the account list listview). */
        runCursorLoader(ACCOUNT_HEADERS_QUERY, null);

        dbFrag.setMainActivityState(STATE_CREATED);
    }

    @Override
    protected void onNewIntent(Intent intent) {

        String accountToOpen = intent.getStringExtra(ACCOUNT_TO_OPEN);
        if (accountToOpen != null) {

            if (!accountToOpen.equals("")) {

                openAccountFromNotification(accountToOpen, true);
            } else {

                closeAnyOpenAccount();
            }

            intent.removeExtra(ACCOUNT_TO_OPEN);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        /* This is the only place we can re-add the license dialog fragment after orientation
        * change and ensure both that it will appear ABOVE the About dialog fragment, and will
        * also correctly scroll to the stored scroll position. Adding it earlier than here means
        * it sometimes goes below the About fragment, and adding it later means the call to scroll
        * it to the stored scroll position does not work.*/
        LicenseDisplayFragment licenseDisplayFragment = (LicenseDisplayFragment)
                getSupportFragmentManager().findFragmentByTag(LICENSE_FRAGMENT_TAG);

        if (licenseDisplayFragment != null) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.remove(licenseDisplayFragment);
            licenseDisplayFragment = new LicenseDisplayFragment();
            Bundle licenseArgs = new Bundle();
            licenseArgs.putInt(LicenseDisplayFragment.LICENSE_TYPE, DbFragmentRef.get().getLastLicenseViewed());
            licenseDisplayFragment.setArguments(licenseArgs);
            transaction.add(licenseDisplayFragment, LICENSE_FRAGMENT_TAG);
            transaction.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* If the app has been hidden, update the listview so it shows the correct due dates and
        last-synced times relative to now. If the app is starting up, no need to update the
        listview as when it's created it will be showing accurate times. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* Make sure we know what kind of device we're on. */
        if (!dbFrag.getScreenSizeDetermined()) determineScreenSize();

        /* First resume here doesn't mean first resume after installation, it just means we aren't
        * resuming a pre-existing instance of the app.*/
        if (dbFrag.thisIsFirstResume()) {

            dbFrag.setFirstResumeFalse();

        } else {

            /* Call the method which updates the list adapter's view binder's member variable
            which stores current date. */
//            if (!dbFrag.getAccountListInActionMode()) {

            AccountListFragment accountListFragment = (AccountListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.account_list_fragment);
            if (accountListFragment != null) {

                ListView accountListListView = (ListView) accountListFragment.getView()
                        .findViewById(R.id.accountListListView);
                if (accountListListView != null) {

                    SimpleCursorAdapter accountListCursorAdapter =
                            (SimpleCursorAdapter) accountListListView.getAdapter();
                    LibRemindViewBinder viewBinder = (LibRemindViewBinder) accountListCursorAdapter
                            .getViewBinder();
                    viewBinder.updateTime();

                    /* Update the list view, in case a significant amount of time has passed since it was
                    created. It will now show dates relative to the correct current time. */
                    updateAccountList();

                    /* If there are any async update tasks running, pass a reference to the listview to the
                    * dbfrag, so it can pass it to the libaccount objects which update the listview with the status
                    * of the sync tasks.*/
                    if (dbFrag.getNumberOfAccountsBeingSynced() > 0) {

                        dbFrag.storeAccountListListViewRef(accountListListView);
                    }
                }
            }
//            }
        }


        dbFrag.setMainActivityState(STATE_RESUMED);
    }

    public DbInteractionsFragment ensureDbFragSetup() {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (dbFrag == null) {

            dbFrag = new DbInteractionsFragment();
            dbFrag.initialise(this);
            DbFragmentRef.set(dbFrag);
        } else {

            if (dbFrag.getContext() == null) {

                dbFrag.initialise(this);
            }
        }

        return dbFrag;
    }

    @Override
    protected void onPause() {
        super.onPause();


        DbFragmentRef.get().setMainActivityState(STATE_PAUSED);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);

        localBroadcastManager.unregisterReceiver(mMessageReceiver);

        /* If an async network task is running, it will have a ref to a libaccount object, which
         * will have a ref to the account list ListView. It's possible this activity is being
         * destroyed due to an orientation change, so we should make sure the libaccount will
         * relinquish its reference to the listview to allow the listview to be garbage-collected.
         * The libaccount needs to get a reference to the new listview which will be created after
         * orientation change. We can use the DbInteractionsFragment, which persists through
         * orientation changes, to facilitate this. For now, just send a message to any libaccount
         * objects to nullify their references. When this activity is recreated after the
         * orientation change, it can if necessary pass a ref to the new listview to the dbfrag,
         * which will supply it to the libaccounts if they are called upon to update the sync
         * status for their entry in the listview. */
        Intent nullifyListViewRefsIntent = new Intent(LIB_ACCOUNT_NULLIFY_LISTVIEW_REF);
        localBroadcastManager.sendBroadcast(nullifyListViewRefsIntent);

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        dbFrag.setMainActivityState(STATE_DESTROYED);
        dbFrag.closeDatabase();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        /* If in large land mode with an account being viewed, add the options which apply
        to accounts. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (dbFrag.getInLargeLandMode()) {

            if (!dbFrag.getAccountBeingViewed().equals("")) {

                MenuItem editAccountMenuItem = menu.findItem(R.id.action_edit_account);
                if (editAccountMenuItem == null) {

                    getSupportMenuInflater().inflate(R.menu.dual_pane_options, menu);
                }
            } else {

                /* Remove the menu items if account no longer being viewed. */
                MenuItem editAccountMenuItem = menu.findItem(R.id.action_edit_account);
                if (editAccountMenuItem != null) {
                    menu.removeItem(R.id.action_edit_account);
                }

                MenuItem syncAccountMenuItem = menu.findItem(R.id.action_sync_open_account);
                if (syncAccountMenuItem != null) {
                    menu.removeItem(R.id.action_sync_open_account);
                }

                MenuItem deleteAccountMenuItem = menu.findItem(R.id.action_delete_open_account);
                if (deleteAccountMenuItem != null) {
                    menu.removeItem(R.id.action_delete_open_account);
                }
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getSupportMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int option = item.getItemId();

        switch (option) {

            case R.id.action_add_account: {

                /* If in landscape mode with any accounts open, close them. Otherwise when the
                account is added, it takes a second before the loans are shown, so it looks like
                wrong loans are being shown for the new account. */
                DbInteractionsFragment dbFrag = DbFragmentRef.get();
                if (!dbFrag.getAccountBeingViewed().equals("")) {

                    if (dbFrag.getInLargeLandMode()) hideViewAccountFragment();
                }

//                LocaleDependentClass localeDependentClass = new LocaleDependentClass(this);
//                localeDependentClass.showOrgTypeChooserDialog();

                showAccountTypeSelector();

                return true;
            }

            case R.id.action_settings: {

                /* If in landscape mode with any accounts open, close them. This saves having
                to refresh the view account frag if the user updates the margin within which
                they wish to be reminded about loans. If they update this margin, it might
                mean that text in the item list should change colour (from red to normal or
                back). Closing the account avoids us having to do this. */
                DbInteractionsFragment dbFrag = DbFragmentRef.get();
                if (!dbFrag.getAccountBeingViewed().equals("")) {

                    if (dbFrag.getInLargeLandMode()) {

                        hideViewAccountFragment();
                    }
                }

                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);

                return true;
            }

            /* The only way this can be called in this activity is in dual-pane mode. */
            case R.id.action_sync_open_account: {

                String accountToSync = DbFragmentRef.get().getAccountBeingViewed();

                if (!accountToSync.equals("")) {

                    hideViewAccountFragment();
                    String[] accountsToSync = new String[]{accountToSync};
                    syncAccounts(accountsToSync);
                }

                return true;
            }

            /* The only way this can be called in this activity is in dual-pane mode. */
            case R.id.action_delete_open_account: {

                String accountToDelete = DbFragmentRef.get().getAccountBeingViewed();
                if (!accountToDelete.equals("")) {

                    String[] accountsToDelete = new String[]{accountToDelete};
                    confirmDeleteAccounts(accountsToDelete);
                }

                return true;
            }

            case R.id.action_edit_account: {

                DbInteractionsFragment dbFrag = DbFragmentRef.get();
                String accountBeingViewed = dbFrag.getAccountBeingViewed();
                if (!accountBeingViewed.equals("")) {

                    /* Show the edit account frag, it will be populated when the db
                    query started below returns. */
                    showEditAccountFragment();

                    /* This call starts the async task to read the info about the account we
                    need to edit from the db. When the task is finished it calls the method
                    to populate the edit account fragment (populateEditAccountFragment). */
                    dbFrag.runEditAccountQuery(this, accountBeingViewed);
                }
                return true;
            }

            case R.id.action_about: {

                /* If in landscape mode with any accounts open, close them. Otherwise if
                orientation changes the fragments settings fragment will be hidden under
                the view account activity. */
                DbInteractionsFragment dbFrag = DbFragmentRef.get();
                if (!dbFrag.getAccountBeingViewed().equals("")) {

                    if (dbFrag.getInLargeLandMode()) {

                        hideViewAccountFragment();
                    }
                }

                AboutDialogFragment aboutDialogFragment = new AboutDialogFragment();
                aboutDialogFragment.show(getSupportFragmentManager(), ABOUT_FRAGMENT_TAG);

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void determineScreenSize() {

        DbInteractionsFragment dbInteractionsFragment = DbFragmentRef.get();

        boolean onLargeLandDevice = false;

        /* The only foolproof way to know if Android will determine we're on a large screen or not
        * on all APIs. According to the docs: "These minimum screen sizes were not as well defined prior
        * to Android 3.0, so you may encounter some devices that are mis-classified between normal and large."
        * (The string screen_size is defined differently in the values and values-large resource folders).*/
        if (getString(R.string.screen_size).equals("large")) {

            onLargeLandDevice = true;
        }

        dbInteractionsFragment.setLargeLandDevice(onLargeLandDevice);
        dbInteractionsFragment.setScreenSizeDeterminedTrue();
    }

    public void confirmDeleteAccounts(String[] accountsToDelete) {

        ConfirmDeleteDialogFragment confirmationDialog = new ConfirmDeleteDialogFragment();
        Bundle deleteArgs = new Bundle();
        deleteArgs.putStringArray(ConfirmDeleteDialogFragment.ACCOUNTS_TO_DELETE, accountsToDelete);
        confirmationDialog.setArguments(deleteArgs);
        confirmationDialog.show(getSupportFragmentManager(), CONFIRM_DELETE_FRAGMENT_TAG);
    }

    public void deleteAccounts(String[] accountsToDelete) {

        /* The only time this method is called is if this activity is hosting a
        view account frag, i.e. we are in dual-pane mode. Hide the view account
        frag before deleting the account it displays.
        (The reason for not hiding the frag earlier is so the user doesn't
        think the account has been deleted before they have confirmed the action). */
        hideViewAccountFragment();

        /* Clear any notifications, as they may contain references to some of the accounts
        being deleted. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (dbFrag.getNotificationCount() > 0) {

            dbFrag.clearNotifiedAccounts();
            NotificationManager notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(LocaleDependentClass.ACTION_NEEDED_NOTIFICATION_ID);
        }

        DbFragmentRef.get().deleteAccounts(accountsToDelete);
    }

    public void syncAccounts(String[] accountsToSync) {

        DbFragmentRef.get().setNumberOfAccountsBeingSynced(accountsToSync.length);

        Bundle syncBundle = new Bundle();
        syncBundle.putStringArray("accountsToSync", accountsToSync);

        runCursorLoader(SYNC_ACCOUNTS_QUERY, syncBundle);
    }

    /**
     * Take a cursor pointing to an accounts table query, and for each row, create a LibAccount populated with the
     * values from the relevant columns.
     *
     * @param cursor the cursor resulting from the db query of the accounts table.
     * @return A list of the newly-created LibAccounts
     */
    public static List<LibAccount> createAccountObjectsFromCursor(Cursor cursor, Context context, boolean silentMode) {

        List<LibAccount> libAccountsList = new ArrayList<LibAccount>();
        LibAccount libAccount;
        String pinOrPassword;
        LibAccount.CatType catType;

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            /* Decrypt the password */
            pinOrPassword = TextEncryptor.decrypt(cursor.getString(5));

            /* Convert catType string to enum. */
            catType = LibAccount.CatType.valueOf(cursor.getString(2));

            /* Convert remind int flag to boolean*/
            int remindInt = cursor.getInt(6);
            boolean remind = (remindInt == 1);

            /* Create different acount object depending on the catalogue type of the account */
            switch (catType) {

                /* Get the data from each column in the cursor row, and pass them as args to the LibAccount constructor.
                Column 0 is the row id, 1 is the name, 3 is the catalogue url, 4 is the card number, 5 is the PIN, 6 is
                a boolean indicating whether reminders are enabled for the account, 7 is number of days before items
                are due that reminders are given. Column 5 (PIN) is decrypted (above) before being passed to the
                constructor, and column 6 is converted from an int (which is how SQLite stores booleans) to a boolean.
                Column 2 (the CatType) is not passed to the constructor, or stored by the account objects, it only exists
                in the database row for each account, and is only used here, to determine which account type is created. */

                case ARENA: {
                    libAccount = new ArenaAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case ELIBRARY: {
                    libAccount = new ElibraryAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case PRISM2: {
                    libAccount = new Prism2Account(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case PRISM3: {
                    libAccount = new Prism3Account(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case VIEWPOINT: {
                    libAccount = new ViewpointAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case SPYDUS: {
                    libAccount = new SpydusAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case HERITAGE: {
                    libAccount = new HeritageAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case PORTFOLIO: {
                    libAccount = new PortfolioAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }

                case DUMMY: {
                    libAccount = new DummyAccount(context, silentMode, cursor.getLong(0), cursor.getString(1),
                            cursor.getString(3), cursor.getString(4), pinOrPassword, remind);
                    libAccountsList.add(libAccount);
                    break;
                }
            }

            cursor.moveToNext();
        }

        return libAccountsList;
    }

    public void runCursorLoader(int loaderId, Bundle argsBundle) {

        getSupportLoaderManager().restartLoader(loaderId, argsBundle, this);
    }

    public void updateAccountList() {

        getSupportLoaderManager().getLoader(MainActivity.ACCOUNT_HEADERS_QUERY).onContentChanged();
    }

    public void showAccount(String accountName, boolean remindSetting) {

        /* This method is only called when we're in large land mode. Should
        * not be called otherwise. (onAccountClick can be used to show an account
        * regardless of mode).*/
        FragmentManager fragmentManager = getSupportFragmentManager();
        ViewAccountFragment viewAccountFragment = (ViewAccountFragment)
                fragmentManager.findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);
        if (viewAccountFragment != null)
            viewAccountFragment.displayAccount(accountName, remindSetting);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle argsBundle) {

        DbInteractionsFragment dbInteractionsFragment = DbFragmentRef.get();

        switch (loaderId) {

            case ACCOUNT_HEADERS_QUERY: {

                dbInteractionsFragment.setAccountsLoading(true);

                /* Create a cursor loader with a query which returns everything in the specified
                columns. The cursor is used to populate the accounts listview (with KEY_NAME and
                KEY_SUMMARY). The names of the accounts, and the card numbers, are added to lists
                (mAccountNamesList and mAccountCardNumbersList) so we can check against them when
                creating a new account, to prevent duplicates. KEY_REMIND is included so that if
                an account is opened and an item detail dialog opened, the remind setting can be
                hidden for accounts where reminding is switched off. */
                String[] columnsToReturn = new String[]{LibRemindDbAdapter.KEY_ROWID,
                        LibRemindDbAdapter.KEY_NAME, LibRemindDbAdapter.KEY_CARD_NUMBER,
                        LibRemindDbAdapter.KEY_SUMMARY, LibRemindDbAdapter.KEY_LAST_SYNCED,
                        LibRemindDbAdapter.KEY_REMIND};

                return dbInteractionsFragment.getDatabaseAdapter().createCursorLoader(
                        LibRemindDbAdapter.DATABASE_ACCOUNTS_TABLE, columnsToReturn, null, null,
                        LibRemindDbAdapter.ORDER_BY_FIRST_ITEM_DUE);
            }

            case NEW_ACCOUNT_QUERY: {

                /* This just returns whichever account has just been created (by checking which one
                hasn't been synced)*/
                String syncNeverAttemptedClause = LibRemindDbAdapter.KEY_LAST_SYNCED + "="
                        + String.valueOf(LibAccount.SYNC_NEVER_ATTEMPTED);

                String[] columnsToReturn = new String[]{LibRemindDbAdapter.KEY_ROWID,
                        LibRemindDbAdapter.KEY_NAME, LibRemindDbAdapter.KEY_CAT_TYPE,
                        LibRemindDbAdapter.KEY_CAT_URL, LibRemindDbAdapter.KEY_CARD_NUMBER,
                        LibRemindDbAdapter.KEY_PIN_OR_PASSWORD, LibRemindDbAdapter.KEY_REMIND};

                return dbInteractionsFragment.getDatabaseAdapter().createCursorLoader(
                        LibRemindDbAdapter.DATABASE_ACCOUNTS_TABLE, columnsToReturn,
                        syncNeverAttemptedClause, null, null);
            }

            case SYNC_ACCOUNTS_QUERY: {

                /*
                If the orientation changes and this app is destroyed before the loader query
                finishes, we'll have to re-run the query (this does happen in certain circs).
                Store query info so the query can be re-run if necessary.
                */
                dbInteractionsFragment.storeSyncAccountsQueryData(SYNC_ACCOUNTS_QUERY,
                        argsBundle);

                /* Get list of accounts to sync from arguments bundle. */
                String[] accountsToSync = argsBundle.getStringArray("accountsToSync");
                if (accountsToSync == null) {

                    Toast.makeText(this, R.string.sync_accounts_query_null,
                            Toast.LENGTH_SHORT).show();
                    return null;
                }

                /* Query the db to get login info etc. for each account to be synced. */


                int numberOfAccountsToSync = accountsToSync.length;
                String selectionString = LibRemindDbAdapter.KEY_NAME + "=?";
                if (numberOfAccountsToSync > 1) {

                    for (int i = 1; i < numberOfAccountsToSync; i++) {

                        selectionString += " OR " + LibRemindDbAdapter.KEY_NAME + "=?";
                    }
                }

                String[] columnsToReturn = new String[]{LibRemindDbAdapter.KEY_ROWID,
                        LibRemindDbAdapter.KEY_NAME, LibRemindDbAdapter.KEY_CAT_TYPE,
                        LibRemindDbAdapter.KEY_CAT_URL, LibRemindDbAdapter.KEY_CARD_NUMBER,
                        LibRemindDbAdapter.KEY_PIN_OR_PASSWORD, LibRemindDbAdapter.KEY_REMIND};

                return dbInteractionsFragment.getDatabaseAdapter().createCursorLoader(
                        LibRemindDbAdapter.DATABASE_ACCOUNTS_TABLE, columnsToReturn,
                        selectionString, accountsToSync, null);
            }

            case ViewAccountActivity.ACCOUNT_ITEMS_QUERY: {

                dbInteractionsFragment.setItemsLoading(true);

                String[] accountName = new String[]
                        {argsBundle.getString(ViewAccountActivity.ACCOUNT_NAME)};
                String selectionString = LibRemindDbAdapter.KEY_ACCOUNT + "=?";
                String[] columnsToReturn = new String[]{LibRemindDbAdapter.KEY_ROWID,
                        LibRemindDbAdapter.KEY_TITLE, LibRemindDbAdapter.KEY_AUTHOR,
                        LibRemindDbAdapter.KEY_DUE_DATE, LibRemindDbAdapter.KEY_UNIQUE_IDENTIFIER};

                return dbInteractionsFragment.getDatabaseAdapter().createCursorLoader(
                        LibRemindDbAdapter.DATABASE_ITEMS_TABLE, columnsToReturn,
                        selectionString, accountName, null);
            }

            default: {

                return null;
            }
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {


        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        switch (loader.getId()) {

            case ACCOUNT_HEADERS_QUERY: {

                cursor.moveToFirst();
                /* Put list of account names into the member list */
                List<String> accountNamesList = new ArrayList<String>();
                List<String> accountCardNumbersList = new ArrayList<String>();
                while (!cursor.isAfterLast()) {
                    accountNamesList.add(cursor.getString(1));
                    accountCardNumbersList.add(cursor.getString(2));
                    cursor.moveToNext();
                }

                dbFrag.setAccountNamesList(accountNamesList);
                dbFrag.setAccountCardNumbersList(accountCardNumbersList);

                /* Put the cursor into the adapter used with the account list listview*/
                SimpleCursorAdapter cursorAdapter =
                        dbFrag.getAccountListCursorAdapter();
                cursorAdapter.swapCursor(cursor);

                dbFrag.setAccountsLoading(false);

                setSupportProgressBarIndeterminateVisibility(false);

	        	/* If there are no accounts to show, show a hint explaining what to do
                If there are accounts, make sure the hint is removed if it exists */
                if (cursorAdapter.getCount() == 0) {

                    FrameLayout rootLayout = (FrameLayout) findViewById(android.R.id.content);
                    TextView noAccountsHint = new TextView(this);
                    noAccountsHint.setText(getString(R.string.no_accounts_hint));
                    noAccountsHint.setTextSize(17);
                    noAccountsHint.setGravity(Gravity.CENTER_HORIZONTAL);
                    noAccountsHint.setPadding(0, 150, 0, 0);
                    noAccountsHint.setId(R.id.no_accounts_hint_textview);
                    rootLayout.addView(noAccountsHint);
                } else {

                    TextView noAccountsHint = (TextView) findViewById(R.id.no_accounts_hint_textview);
                    if (noAccountsHint != null) {
                        FrameLayout rootLayout = (FrameLayout) findViewById(android.R.id.content);
                        rootLayout.removeView(noAccountsHint);
                    }
                }

                /* The headers query is re-run after an account is synced. If an account has just
                synced its position may have changed in the listview. This broadcast will tell
                any existing account objects to move their entry onscreen in the listview so the
                results of the sync will be visible to the user. This broadcast will only be sent
                if the number of accounts being synced is one, to avoid the account list jumping
                around as multiple accounts finish syncing. If multiple accounts are synced at
                once this means it will scroll to the last one which finishes syncing. */
                int numberOfAccountsBeingSynced = dbFrag.getNumberOfAccountsBeingSynced();

                if (numberOfAccountsBeingSynced == 1) {

                    Intent intent = new Intent(SCROLL_TO_ACCOUNT);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                }

                /* If there are other accounts still being synced, we should tell them to update
                their record of their position in the listview, as it may have changed as a result
                of the account list being resorted after another account finished syncing. This
                will make sure any other accounts currently being synced show their sync status in
                the correct list view entry. Also, if any accounts are being synced, make sure the
                status is correctly shown as "Syncing...". */
                if (numberOfAccountsBeingSynced > 0) {

                    Intent intent = new Intent(LIB_ACCOUNT_UPDATE_POSITION);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                }

                ViewAccountFragment viewAccountFragment =
                        (ViewAccountFragment) getSupportFragmentManager()
                                .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                /* Check if the account being shown has never been synced, or has no loans. In
                either of these cases a message will be shown letting the user know why there are
                no loans displayed. This method is also called in onActivityCreated in the view
                account fragment, so it will be run when a new view frag is created in portrait
                mode. This method is also called when an item list query has finished loading. It
                will do nothing if either an accounts headers query or an item list query is
                still ongoing, as it will be re-called when either of those queries finishes. */
                if (viewAccountFragment != null) viewAccountFragment.showHintOnEmptyAccount();

                break;
            }

            case NEW_ACCOUNT_QUERY: {

                List<LibAccount> libAccountList = createAccountObjectsFromCursor(cursor, this, false);

                /*
                The list should only contain one account, as the new account query only retrieves
                accounts which have never been synced, and all accounts are synced (here) when created.
                We might as well check that it's not empty though.
                */
                if (!libAccountList.isEmpty()) {

                    LibAccount newAccount = libAccountList.get(0);

                    /*
                    Set the string which, if not empty, will make whichever account is named in it
                    automatically show in the view account fragment if one is present (i.e. if
                    we're in dual-pane mode at the time), after the account headers query is run
                    after the sync. Also store the account's remind setting.
                    */
                    dbFrag.setAccountBeingSynced(newAccount.getName());
                    dbFrag.storeRemindSettingOfAccountBeingSynced(newAccount.getRemindSetting());

                    /* The sync list lets the account list viewbinder know to set the status to
                    * of currently-syncing accounts to "syncing" after an orientation changes
                    * (rather than to whatever is in the db).*/
                    newAccount.registerNameInSyncList();

                    newAccount.firstSync();
                }
                break;
            }

            case SYNC_ACCOUNTS_QUERY: {

                dbFrag.clearSyncAccountsQueryData();

                List<LibAccount> libAccountList = createAccountObjectsFromCursor(cursor, this, false);

                /* If one account is being synced by itself, set the string which, if not empty,
                will make whichever account is named in it automatically show in the view account
                fragment if one is present (i.e. if we're in dual-pane mode at the time), after
                the account headers query is run after the sync. Also store the account's remind
                setting. */
                if (dbFrag.getNumberOfAccountsBeingSynced() == 1) {

                    LibAccount accountBeingSynced = libAccountList.get(0);
                    dbFrag.setAccountBeingSynced(accountBeingSynced.getName());
                    dbFrag.storeRemindSettingOfAccountBeingSynced(accountBeingSynced
                            .getRemindSetting());
                }

                /* Before syncing, show the user that all the selected accounts are going
                to be synced. If we don't do this here it takes a little while for "Syncing..."
                to appear on the account and the user might think there is a problem. */
                for (LibAccount libAccount : libAccountList) {

                    libAccount.showSyncProgress(getString(R.string.syncing));

                    /* Store the names of the accounts being synced in the dbFrag. This is so that,
                    * if the orientation of the device changes whilst the syncs are ongoing, and the
                    * account list is destroyed and recreated, we can set the status of the currently
                    * syncing accounts back to "syncing...", so the user doesn't think the sync attempts
                    * have been abandoned.*/
                    libAccount.registerNameInSyncList();

                }

                for (LibAccount libAccount : libAccountList) {

                    libAccount.sync();
                }
                break;
            }

            case ViewAccountActivity.ACCOUNT_ITEMS_QUERY: {

                /* Put the cursor into the adapter used with the item list listview*/
                SimpleCursorAdapter itemListCursorAdapter = dbFrag.getItemListCursorAdapter();
                itemListCursorAdapter.swapCursor(cursor);

                setTitle(new DateParser().makeTitle(this, itemListCursorAdapter.getCount()));

                dbFrag.setItemsLoading(false);

                ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                        .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                /*
                Check if the account being shown has never been synced, or has no loans. In either of these
                cases a message will be shown letting the user know why there are no loans displayed.
                This method is also called in onActivityCreated in the view account fragment, so it will
                be run when a new view frag is created in portrait mode.
                */
                if (viewAccountFragment != null) viewAccountFragment.showHintOnEmptyAccount();

                break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        switch (loader.getId()) {

            case ACCOUNT_HEADERS_QUERY: {

                DbFragmentRef.get().getAccountListCursorAdapter().swapCursor(null);
                break;
            }

            case ViewAccountActivity.ACCOUNT_ITEMS_QUERY: {

                DbFragmentRef.get().getItemListCursorAdapter().swapCursor(null);
                break;
            }
        }
    }

    public void onAccountClick(String accountName, boolean accountRemindSetting) {

        /*
        If the view account frag is already showing an account, and it is in action mode, end
        the action mode. (It can't be in action mode in this version of the app but in future
        versions the functionality might be restored).
        */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (dbFrag.getViewAccountInActionMode()) {

            dbFrag.getActionModeRef().finish();
            dbFrag.getItemListCursorAdapter().swapCursor(null);
        }

        ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

        if (viewAccountFragment != null && viewAccountFragment.getView().getVisibility() == View.VISIBLE) {

            viewAccountFragment.displayAccount(accountName, accountRemindSetting);
            setTitle(accountName);
        } else {

            /* If the view account frag isn't found we must be in single-pane mode, so start
            * a new view account activity.*/
            Intent intent = new Intent(this, ViewAccountActivity.class);
            intent.putExtra(ViewAccountActivity.ACCOUNT_NAME, accountName);
            intent.putExtra(ViewAccountActivity.ACCOUNT_REMIND_SETTING, accountRemindSetting);
            startActivity(intent);
        }
    }

    public void openAccountFromNotification(String accountName, boolean accountRemindSetting) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* If the view account frag is already showing an account, and it is in action mode, end
        the action mode. (It can't be in action mode in this version of the app but in future
        versions the functionality might be restored). */
        /*
        if (dbFrag.getViewAccountInActionMode()) {

            dbFrag.getActionModeRef().finish();
            dbFrag.getItemListCursorAdapter().swapCursor(null);
        }
        */

        ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

        if (viewAccountFragment != null && viewAccountFragment.getView().getVisibility() == View.VISIBLE) {

            viewAccountFragment.displayAccount(accountName, accountRemindSetting);
            scrollToAccount(accountName);
            setTitle(accountName);
        } else {

            if (dbFrag.getOnLargeLandDevice() && getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_LANDSCAPE) {

                /* We are starting up with an account to show, from a notification, but the view
                account frag isn't ready yet. Put the name of the account to be shown in the db frag
                and when the view account frag is created it will check for an account name to show.*/
                dbFrag.setAccountToBeOpenedFromNotification(accountName);
                setTitle(accountName);
            } else {

                /* We must be in single-pane mode. If there is no account open, open one. If there
                is an account open, and it's not the one we want to look at, update the view
                activity showing the account to show the new one. If it is the one we want to look
                at, don't do anything. */
                String accountBeingViewed = dbFrag.getAccountBeingViewed();

                if (accountBeingViewed.equals("")) {

                    Intent intent = new Intent(this, ViewAccountActivity.class);
                    intent.putExtra(ViewAccountActivity.ACCOUNT_NAME, accountName);
                    intent.putExtra(ViewAccountActivity.ACCOUNT_REMIND_SETTING, accountRemindSetting);
                    startActivity(intent);
                } else {

                    if (!accountBeingViewed.equals(accountName)) {

                        Intent changeAccountBeingShownIntent =
                                new Intent(ViewAccountActivity.CHANGE_ACCOUNT_BEING_SHOWN);
                        changeAccountBeingShownIntent.putExtra(ViewAccountActivity.ACCOUNT_TO_SHOW,
                                accountName);
                        changeAccountBeingShownIntent.putExtra(
                                ViewAccountActivity.ACCOUNT_REMIND_SETTING, accountRemindSetting);
                        LocalBroadcastManager.getInstance(this)
                                .sendBroadcast(changeAccountBeingShownIntent);
                    }
                }
            }
        }
    }

    public void renewItems(String[] itemsToRenew) {

    }

    public void showEditAccountFragment() {

        /* Create an edit account fragment. It will be populated with the account info when
        the async db read called immediately after this method is called returns. */
        new EditAccountDialogFragment().show(getSupportFragmentManager(), EDIT_ACCOUNT_FRAGMENT_TAG);
    }

    public void populateEditAccountFragment(String[] accountDetails) {

        EditAccountDialogFragment editAccountDialogFragment = (EditAccountDialogFragment)
                getSupportFragmentManager().findFragmentByTag(MainActivity.EDIT_ACCOUNT_FRAGMENT_TAG);

        if (editAccountDialogFragment != null) {

            editAccountDialogFragment.storeExistingAccountDetails(accountDetails);

            int region = Integer.parseInt(accountDetails[5]);
            editAccountDialogFragment.loadOrganisationData(region);

            String orgName = accountDetails[1];
            editAccountDialogFragment.setSelectedOrganisationOrStoreNameToSetLater(orgName);

            Dialog editAccountDialog = editAccountDialogFragment.getDialog();

            /* Set the remind switch to the stored value.*/
            boolean remind = Boolean.valueOf(accountDetails[4]);
            CompoundButton remindSwitch = (CompoundButton) editAccountDialog
                    .findViewById(R.id.edit_account_reminder_switch);
            remindSwitch.setChecked(remind);

            /* Fill in the fields in the form with the existing account information. */
            TextView editAccountTextView = (TextView) editAccountDialog
                    .findViewById(R.id.edit_account_organisation_textview);
            editAccountTextView.setText(orgName);

            EditText editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_card_number);

            /* Some Spydus catalogues add a prefix when sending the card number to login. Hide the prefix
            * from the user.*/
            String cardNumber = accountDetails[2];
            if (orgName.equals("Bolton") || orgName.equals("Calderdale") || orgName.equals("Salford")) {

                cardNumber = removePrefix(orgName, cardNumber);
            }

            editAccountEditText.setText(cardNumber);

            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_pin_or_password);
            editAccountEditText.setText(accountDetails[3]);

            /* Populate this one last so it will have focus. */
            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_account_name_edittext);
            editAccountEditText.setText(accountDetails[0]);

            /* Store this boolean. When edit account frag is destroyed this boolean lets us
            figure out whether it's being destroyed due to an orientation change, which we
            need to know so that we can manually recreate the edit frag in a different
            activity, which is the only way to go from have a single activity to two
            activites with the edit account frag remaining in the foreground. */
            DbFragmentRef.get().setAccountBeingEdited(true);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

            /*
            If there is a view account fragment open in large landscape mode, close it.
            */
            DbInteractionsFragment dbFrag = DbFragmentRef.get();
            if (dbFrag.getInLargeLandMode()) {
                if (!dbFrag.getAccountBeingViewed().equals("")) {

                    /*
                    If the view account fragment isn't in action mode, close the fragment and consume the
                    keypress. Otherwise don't consume the keypress, and allow the system to end the action
                    mode when it receives the keypress.
                    */
                    if (!dbFrag.getViewAccountInActionMode()) {

                        hideViewAccountFragment();

                        /* Consume the keypress. */
                        return true;
                    }
                }
            }
        }

        return super.dispatchKeyEvent(event);
    }

    public void hideViewAccountFragment() {

        /* This method is only called when in dual-pane mode, to hide the right-hand pane. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (dbFrag.getViewAccountInActionMode()) {

            dbFrag.getActionModeRef().finish();
        }

        dbFrag.clearAccountBeingViewed();

        /* If in large-land mode, remove highlighting from open account in account list. Having
        called dbFrag.clearAccountBeingViewed above, calling notifyDataSetChanged will
        ensure the viewbinder makes all listview entries non-highlighted. */
        if (dbFrag.getInLargeLandMode()) {

            dbFrag.getAccountListCursorAdapter().notifyDataSetChanged();
        }

        /* Swapping the cursor used in the view account fragment will delete any items in that fragment's
        listview, thus making the fragment seem to have vanished. */
        dbFrag.getItemListCursorAdapter().swapCursor(null);

        TextView noLoansHintTextView = (TextView) findViewById(R.id.no_loans_hint_textview);
        if (noLoansHintTextView != null) {
            LinearLayout viewAccountLayout = (LinearLayout) findViewById(R.id.view_account_layout);
            viewAccountLayout.removeView(noLoansHintTextView);
            dbFrag.clearNoItemsHintShownFor();
        }

        supportInvalidateOptionsMenu();

        setTitle(R.string.app_name);
    }

    public void storeEditFragSavedState() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment editAccountFrag = fragmentManager.findFragmentByTag(MainActivity.EDIT_ACCOUNT_FRAGMENT_TAG);
        if (editAccountFrag != null) {

            DbFragmentRef.get().storeEditFragSavedState(fragmentManager
                    .saveFragmentInstanceState(editAccountFrag));
        }
    }

    public void updateAccountListActionModeTitle() {

        AccountListFragment accountListFragment = (AccountListFragment)
                getSupportFragmentManager().findFragmentById(R.id.account_list_fragment);

        if (accountListFragment != null) {

            accountListFragment.updateActionModeTitle();
        }
    }

    public void updateViewAccountActionModeTitle() {

        ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

        if (viewAccountFragment != null) {

            viewAccountFragment.updateActionModeTitle();
        }
    }

    public void removeItemInfoFrag() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        ItemInfoDialogFragment itemInfoDialogFragment = (ItemInfoDialogFragment)
                fragmentManager.findFragmentByTag(MainActivity.ITEM_INFO_FRAGMENT_TAG);
        if (itemInfoDialogFragment != null)
            fragmentManager.beginTransaction().remove(itemInfoDialogFragment).commit();
    }

    public void removeDualPaneItemInfoFrag() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        ItemInfoDialogFragmentDualPane itemInfoDialogFragmentDualPane = (ItemInfoDialogFragmentDualPane)
                fragmentManager.findFragmentByTag(ITEM_INFO_FRAGMENT_TAG_DUAL_PANE);
        if (itemInfoDialogFragmentDualPane != null)
            fragmentManager.beginTransaction().remove(itemInfoDialogFragmentDualPane).commit();
    }

    @Override
    public void showLicenseFragment(int licenseType) {

        LicenseDisplayFragment licenseDisplayFragment = new LicenseDisplayFragment();
        Bundle licenseArgs = new Bundle();
        licenseArgs.putInt(LicenseDisplayFragment.LICENSE_TYPE, licenseType);
        licenseDisplayFragment.setArguments(licenseArgs);

        DbFragmentRef.get().setLastLicenseViewed(licenseType);

        getSupportFragmentManager()
                .beginTransaction()
                .add(licenseDisplayFragment, LICENSE_FRAGMENT_TAG)
                .commit();
    }

    public void notify(String[] notificationStrings, String accountName) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* This will only add the account to the list of accounts notification have been
        sent for if it isn't already in the list (i.e. if a notification hasn't already been
        sent for this account). This prevents a multi-account notification string being sent
        when both notifications are for the same account. */
        dbFrag.addNotifiedAccount(accountName);
        int notificationCount = dbFrag.getNotificationCount();

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.libreminder_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.drawable.libreminder_notification_lrg))
                        .setContentTitle(getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setOnlyAlertOnce(true);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean playSound = prefs.getBoolean(PREF_PLAY_A_SOUND, true);
        boolean vibrate = prefs.getBoolean(PREF_VIBRATE, false);

        if (playSound) {

            if (vibrate) {

                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND |
                        Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
            } else {

                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
            }
        } else {

            if (vibrate) {

                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
            }
        }

        Intent notificationIntent = new Intent(this, RespondToNotificationService.class);
        notificationIntent.setAction(ACTION_RESPOND_TO_NOTIFICATION);

        if (notificationCount == 1) {

            notificationBuilder.setContentText(notificationStrings[0])
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(notificationStrings[1]));

            notificationIntent.putExtra(ACCOUNT_TO_OPEN, accountName);
        } else {

            notificationBuilder.setContentText(getString(R.string.loans_need_action_on) + " "
                    + String.valueOf(notificationCount) + " " + getString(R.string.accounts));

            /* If more than one account needs action, put an empty account name, as no
            * account will be opened.*/
            notificationIntent.putExtra(ACCOUNT_TO_OPEN, "");
        }

        Intent notificationDeleteIntent = new Intent(this, RespondToNotificationService.class);
        notificationDeleteIntent.setAction(ACTION_REGISTER_NOTIFICATION_DISMISSAL);

        PendingIntent notificationPendingIntent = PendingIntent.getService(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent notificationDeletePendingIntent = PendingIntent.getService(this, 0,
                notificationDeleteIntent, 0);

        notificationBuilder.setContentIntent(notificationPendingIntent);
        notificationBuilder.setDeleteIntent(notificationDeletePendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /* When sending the notification, we use an id which depends on our locale, this is that if a user
        * installs versions of the app from two different locals on the same devices, the notifications
        * won't override or cancel each other. */
        notificationManager.notify(LocaleDependentClass.ACTION_NEEDED_NOTIFICATION_ID, notificationBuilder.build());
    }

    private void scrollToAccount(String accountName) {

        SimpleCursorAdapter accountListCursorAdapter = DbFragmentRef.get()
                .getAccountListCursorAdapter();

        for (int j = 0; j < accountListCursorAdapter.getCount(); j++) {

            Cursor cursor = (Cursor) accountListCursorAdapter.getItem(j);
            if (cursor.getString(1).equals(accountName)) {

                AccountListFragment accountListFragment = (AccountListFragment)
                        getSupportFragmentManager().findFragmentById(R.id.account_list_fragment);
                if (accountListFragment != null) {
                    ListView accountListListView = (ListView) accountListFragment.getView()
                            .findViewById(R.id.accountListListView);

                    if (accountListListView != null) {

                        accountListListView.setSelection(j);

                        /* Re-binding the views will highlight the account being viewed, as the view binder
                        checks with the db frag to see if an account is being viewed, and if so highlights
                        that account's entry in the account list.*/
                        accountListCursorAdapter.notifyDataSetChanged();
                    }
                }
                break;
            }
        }
    }

    private void closeAnyOpenAccount() {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (!dbFrag.getAccountBeingViewed().equals("")) {

            /* If the view account frag is already showing an account, and it is in action mode, end
            the action mode. (It can't be in action mode in this version of the app but in future
            versions the functionality might be restored). */
            /*
            if (dbFrag.getViewAccountInActionMode()) {

                dbFrag.getActionModeRef().finish();
                dbFrag.getItemListCursorAdapter().swapCursor(null);
            }
            */

            ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getSupportFragmentManager()
                    .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

            if (viewAccountFragment != null
                    && viewAccountFragment.getView().getVisibility() == View.VISIBLE) {

                hideViewAccountFragment();
            } else {

                Intent closeViewAccountActivityIntent =
                        new Intent(ViewAccountActivity.CLOSE_VIEW_ACCOUNT_ACTIVITY);
                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(closeViewAccountActivityIntent);
            }
        }
    }

    public static void activateAutoSyncAlarm(Context context) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String alarmTimeString = prefs.getString(MainActivity.SYNC_TIME_PREF_KEY,
                context.getString(R.string.default_remind_time));

        activateAutoSyncAlarm(context, alarmTimeString);
    }

    public static void activateAutoSyncAlarm(Context context, String alarmTimeString) {

        SyncManager syncManager = SyncManager.getInstance();
        syncManager.setNextSyncTime(context, alarmTimeString);

        /* Enable the boot_completed broadcast receiver so alarm can persist across device
        * shutdowns. */
        ComponentName bootCompletedReceiver = new ComponentName(context, SetAlarmOnBoot.class);
        PackageManager packageManager = context.getPackageManager();

        packageManager.setComponentEnabledSetting(bootCompletedReceiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }

    public static void deactivateAutoSyncAlarm(Context context) {

        SyncManager syncManager = SyncManager.getInstance();
        syncManager.cancel(context);

        /* The autosync service may have saved its state if it failed to sync on the first
        * attempt (it counts the number of attempts and whether it is doing a full or
        * partial sync). Clear any saved info. */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        ApiDependentPrefWriter apiDependentPrefWriter =
                ApiDependentCodeFactory.getApiDependentPrefWriter();
        apiDependentPrefWriter
                .writeIntPref(prefs, AutoSyncService.AUTO_SYNC_FAILED_ATTEMPTS, 0);
        apiDependentPrefWriter
                .writeIntPref(prefs, AutoSyncService.AUTO_SYNC_FAILED_ATTEMPTS_DUE_TO_APP_OPEN, 0);
        apiDependentPrefWriter.writeBooleanPref(prefs, AutoSyncService.CATCHUP_SYNC, false);

        /* Disable the boot_completed broadcast receiver as no need to do anything on device boot. */
        ComponentName bootCompletedReceiver = new ComponentName(context, SetAlarmOnBoot.class);
        PackageManager packageManager = context.getPackageManager();

        packageManager.setComponentEnabledSetting(bootCompletedReceiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
    }

    private String removePrefix(String orgName, String cardNumber) {

        if (orgName.equals("Bolton")) {

            cardNumber = cardNumber.substring(3);
        }

        if (orgName.equals("Calderdale")) {

            cardNumber = cardNumber.substring(1);
        }

        if (orgName.equals("Salford")) {

            cardNumber = cardNumber.substring(2);
        }

        return cardNumber;
    }

    private void showAccountTypeSelector() {

        CharSequence[] options;
        int offset;

        if (LocaleDependentClass.LOCALE == LOCALE_UK) {

            options = new CharSequence[]{
                    getString(R.string.public_library),
                    getString(R.string.college_library),
                    getString(R.string.university_library),
                    getString(R.string.health_library)};

            offset = 0;
        }

        Bundle arguments = new Bundle();
        arguments.putString(SELECTOR_TITLE, getString(R.string.library_type));
        arguments.putCharSequenceArray(SELECTOR_OPTIONS, options);
        arguments.putInt(SELECTOR_OFFSET, offset);

        SelectorDialogFragment dialogFragment = new SelectorDialogFragment();
        dialogFragment.setArguments(arguments);
        dialogFragment.show(getSupportFragmentManager(), SELECTOR_FRAGMENT_TAG);
    }

    public void onSelection(int result) {

        /* This method is called by the SelectorDialogFragment when an option is chosen. */

        if (result < 10) {

            /* The result represents an account type. */
            if (result == PUBLIC_ACCOUNT) {

                CharSequence[] options;
                int offset;

                if (LocaleDependentClass.LOCALE == LOCALE_UK) {
                    options = new CharSequence[]{
                            getString(R.string.england_north_mid),
                            getString(R.string.england_east_south),
                            getString(R.string.ireland),
                            getString(R.string.scotland),
                            getString(R.string.wales)};

                    offset = 10;
                }

                Bundle arguments = new Bundle();
                arguments.putString(SELECTOR_TITLE, getString(R.string.region));
                arguments.putCharSequenceArray(SELECTOR_OPTIONS, options);
                arguments.putInt(SELECTOR_OFFSET, offset);

                SelectorDialogFragment dialogFragment = new SelectorDialogFragment();
                dialogFragment.setArguments(arguments);
                dialogFragment.show(getSupportFragmentManager(), SELECTOR_FRAGMENT_TAG);
            }

            if (result == COLLEGE_ACCOUNT) {

                if (LocaleDependentClass.LOCALE == LOCALE_UK) {

                    /* There are no subcategies for UK colleges, so show the add account fragment.*/
                    showAddAccountFragment(REGION_UK_IE_COLLEGE);
                }
            }

            if (result == UNI_ACCOUNT) {

                if (LocaleDependentClass.LOCALE == LOCALE_UK) {

                    /* There are no subcategies for UK unis, so show the add account fragment.*/
                    showAddAccountFragment(REGION_UK_IE_UNIVERSITY);
                }
            }

            if (result == HEALTH_ACCOUNT) {

                if (LocaleDependentClass.LOCALE == LOCALE_UK) {

                    /* There are no subcategies for UK unis, so show the add account fragment.*/
                    showAddAccountFragment(REGION_UK_HEALTH);
                }
            }
        } else {

            /* The result represents region. Show the add account dialog for the region. */
            showAddAccountFragment(result);
        }
    }

    private void showAddAccountFragment(int region) {

        /* As the add account dialog is being opened due to user action, (i.e. the
        * user is going through the process of adding an account), we would like
        * the set organisation dialog fragment to open immediately, as soon as the
        * add account dialog fragment appears. This is because the user has just been
        * shown a series of dialog asking for the location of the library they
        * are adding, after which it can be confusing just to see the blank form
        * which requires them to take further action in order to set the precise
        * location of the library. It seems more intuitive if, after setting the
        * general region, they are then immediately shown the list of local regions,
        * without having to take action to bring this list up. So set the boolean
        * which indicates the setOrganisation dialog is open, which will cause it
        * to appear as soon as the add account dialog opens.*/
        DbFragmentRef.get().setSetOrganisationFragmentOpen(true);

        /* Show the dialog to add an account. */
        AddAccountDialogFragment addAccountDialogFragment = new AddAccountDialogFragment();
        Bundle fragmentArguments = new Bundle();
        fragmentArguments.putInt(REGION, region);
        addAccountDialogFragment.setArguments(fragmentArguments);
        addAccountDialogFragment.show(getSupportFragmentManager(), ADD_ACCOUNT_FRAGMENT_TAG);
    }
}
