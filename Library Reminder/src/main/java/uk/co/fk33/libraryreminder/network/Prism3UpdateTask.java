/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Prism3UpdateTask extends UpdateTask {

    public Prism3UpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        /* Connect to the library catalogue, get the list of user's loans */

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        String catUrl = mLibAccount.getCatUrl();
        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Locale languageLocale = LocaleDependentClass.getLanguageLocale();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        try {

            String loginUrl = catUrl + "sessions/";

            publishProgress(mContext.getString(R.string.sending_login_details));

            /* Some Prism 3 catalogues are set up not to require a PIN. If the library
            account has been created without a PIN, just send the barcode. Otherwise
            send both. */
            if (mLibAccount.getPinOrPassword().equals("")) {

                catalogueResponse = Jsoup.connect(loginUrl)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .userAgent(mContext.getString(R.string.user_agent))
                        .data("barcode", mLibAccount.getCardNumber(),
                                "borrowerLoginButton", "Login")
                        .method(Connection.Method.POST)
                        .execute();
            } else {

                catalogueResponse = Jsoup.connect(loginUrl)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .userAgent(mContext.getString(R.string.user_agent))
                        .data("barcode", mLibAccount.getCardNumber(),
                                "pin", mLibAccount.getPinOrPassword(),
                                "borrowerLoginButton", "Login")
                        .method(Connection.Method.POST)
                        .execute();
            }

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            /* This page should contain the loan information */
            publishProgress(mContext.getString(R.string.getting_list_of_loans));
            cataloguePage = catalogueResponse.parse();

            /* Check if login failed. If we're logged in, we shouldn't be able to find the
            login button. */
            Elements loginButton = cataloguePage.select("input[name=borrowerLoginButton]");
            if (!loginButton.isEmpty()) {

                /* On rare occasions when the catalogue doesn't work, it says "Unexpected problem."
                Make sure this hasn't happened before assuming the card/pin no. were wrong. */
                if (cataloguePage.text().toLowerCase(languageLocale).contains("unexpected problem")) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                } else {

                    mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                    return null;
                }
            }

            /* If user has no loans, just return the empty itemList. */
            if (cataloguePage.text().toLowerCase(languageLocale).contains("no loans")) {
                mStatus = LibAccount.SYNC_SUCCEEDED;
                return itemList;
            }

            DateTime dueDate;

            Elements loanRows = cataloguePage.select("th[scope=row]");

            for (Element loanRow : loanRows) {

                Elements titleElement = loanRow.select("a");
                Elements authorElement = loanRow.select("span.author");
                Element dueDateElement = loanRow.nextElementSibling();
                Element renewCountElement = dueDateElement.nextElementSibling().nextElementSibling();
                Element renewFormElement = renewCountElement.nextElementSibling();
                Elements uniqueIdElement = renewFormElement.select("input[name=loan_ids[]]");

                String title = titleElement.text();
                String author = authorElement.text();
                String dueDateString = dueDateElement.text();
                String uniqueId = uniqueIdElement.attr("value");
                String[] standardIdentifierParts = titleElement.attr("href").split("/");
                String standardIdentifier = standardIdentifierParts[1];

                DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());
                long currentTimeMillis = System.currentTimeMillis();

                /* Check if the due date is "today" or "tomorrow" */
                if (dueDateString.toLowerCase(languageLocale).contains("today") ||
                        dueDateString.toLowerCase().contains("tomorrow")) {

                    DateTime currentDate = new DateTime(currentTimeMillis, dateTimeZone);

                    if (dueDateString.toLowerCase(languageLocale).contains("today")) {

                        dueDate = currentDate;
                    } else {

                        dueDate = currentDate.plusDays(1);
                    }

                } else {

                    /* Remove the word "overdue" if present. */
                    if (dueDateString.toLowerCase(languageLocale).contains("overdue")) {

                        /* Just in case the overdue phrasing varies, find the first numeral and remove
                        whatever's in front of it (the word "overdue" will thus be removed). */
                        Matcher matcher = Pattern.compile("\\d").matcher(dueDateString);
                        matcher.find();
                        int firstNumeral = matcher.start();
                        dueDateString = dueDateString.substring(firstNumeral, dueDateString.length());
                    }

                    /* Check whether a year is included in the due date, if not, append the current year.
                    (If the book is due back in a different year to the current year, Prism 3 includes
                    the year). */
                    int length = dueDateString.length();
                    if (!dueDateString.substring(length - 4, length).matches("\\d{4}")) {
                        DateTime currentDate = new DateTime(currentTimeMillis, dateTimeZone);
                        dueDateString += " " + Integer.toString(currentDate.getYear());
                    }

                    /* Chop the date string into the format dd mmm yyyy */
                    String[] dueDateStringParts = dueDateString.split(" ");
                    String day = dueDateStringParts[0];

                    /* Remove st, rd, th suffixes and put a zero in front of single digit dates */
                    day = day.substring(0, day.length() - 2);
                    if (day.length() == 1) {
                        day = "0" + day;
                    }
                    String month = dueDateStringParts[1].substring(0, 3);
                    String year = dueDateStringParts[2];
                    dueDateString = day + " " + month + " " + year;

                    /* Create joda DateTime from the due date string */
                    DateTimeFormatter dateTimeFormatter = LocaleDependentClass.getPrism3DateTimeFormatter();

                    try {

                        dueDate = dateTimeFormatter.parseDateTime(dueDateString);

                    } catch (IllegalArgumentException illegalArgumentException) {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }
                }

                if (dueDate == null) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                long dueDateLong = dueDate.getMillis();

                /* Get the renew count as an int */
                int renewCount = Integer.parseInt(renewCountElement.text());

                libItem = new LibItem(mLibAccount.getName(), title, author, uniqueId, standardIdentifier, dueDateLong,
                        renewCount, true);
                itemList.add(libItem);

            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;
        } catch (NullPointerException nullPointerException) {

            /* We shouldn't get an NPE unless catalogue starts to spit out different HTML, which there
            is no reason to think it should. */
            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;

        } catch (IndexOutOfBoundsException outOfBoundsException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;

        } catch (IllegalFieldValueException illegalFieldValueException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
