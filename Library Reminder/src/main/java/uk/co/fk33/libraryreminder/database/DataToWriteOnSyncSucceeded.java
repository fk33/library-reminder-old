/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.content.ContentValues;

public class DataToWriteOnSyncSucceeded extends DataToWrite {

    private String[] mNewSummaries;
    private long[] mNewLastSyncedDates;
    private long[] mNewFirstDueDates;

    public DataToWriteOnSyncSucceeded(long[] rowIds, String[] newSummaries, long[] newLastSyncedDates,
                                        long[] newFirstDueDates) {

        super(rowIds);

        if (newSummaries.length != mSize || newLastSyncedDates.length != mSize
                || newFirstDueDates.length != mSize) {
            throw new IllegalArgumentException("DataToWrite: The newSummaries, newLastSyncedDates " +
                    "and newFirstDueDates arrays must both be the same size as the rowIds array. " +
                    "One or both did not match.");
        }

        mNewSummaries = newSummaries;
        mNewLastSyncedDates = newLastSyncedDates;
        mNewFirstDueDates = newFirstDueDates;
        mTable = LibRemindDbAdapter.DATABASE_ACCOUNTS_TABLE;
    }

    /**
     * Put the values of each element from the member arrays into a ContentValues object in an array of ContentValues
     * objects. Set the key for each value to be the column to be written to in the db.
     *
     * @return The array of ContentValues instances, one for each db row to be updated
     */
    @Override
    public ContentValues[] getContentValues() {

        ContentValues[] newValues = new ContentValues[mSize];

        for (int i = 0; i < mSize; i++) {
            newValues[i] = new ContentValues();
            newValues[i].put(LibRemindDbAdapter.KEY_SUMMARY, mNewSummaries[i]);
            newValues[i].put(LibRemindDbAdapter.KEY_LAST_SYNCED, mNewLastSyncedDates[i]);
            newValues[i].put(LibRemindDbAdapter.KEY_FIRST_ITEM_DUE, mNewFirstDueDates[i]);
        }
        return newValues;
    }
}
