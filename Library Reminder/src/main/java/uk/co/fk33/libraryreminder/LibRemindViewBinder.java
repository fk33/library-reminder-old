/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.joda.time.*;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class LibRemindViewBinder implements SimpleCursorAdapter.ViewBinder {

    private DateTime mCurrentDate;
    private final Context mContext;
    private final static String LISTENER_ADDED = "listener_added";
    public static final String VIEW_BINDER_UPDATE_TIME = "uk.co.fk33.libraryreminder.view_binder_update_time";
    private final static long TWENTY_FIVE_HOURS = 25 * 60 * 60 * 1000;
    private int mMarginPref;

    public LibRemindViewBinder(Context context, int marginPref) {

        mContext = context;
        mMarginPref = marginPref;

        mCurrentDate = new DateTime(System.currentTimeMillis(),
                DateTimeZone.forTimeZone(TimeZone.getDefault()));

        BroadcastReceiver updateTimeBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                updateTime();
            }
        };

        LocalBroadcastManager.getInstance(mContext).registerReceiver(updateTimeBroadcastReceiver,
                new IntentFilter(VIEW_BINDER_UPDATE_TIME));
    }

    @Override
    public boolean setViewValue(View view, Cursor cursor, int column) {

        if (column == 1) {

            DbInteractionsFragment dbFrag = DbFragmentRef.get();
            String accountName = cursor.getString(1);
            View listRow = (ViewGroup) view.getParent().getParent();

            CheckBox checkbox = (CheckBox) listRow.findViewById(R.id.account_list_checkbox);

            /* If the checkbox doesn't have an onClickListener, set it here. */
            if (checkbox.getTag() == null) {

                checkbox.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View clickedCheckBox) {

                        DbFragmentRef.get().onCheckBoxClicked(clickedCheckBox);
                    }
                });

                checkbox.setTag(LISTENER_ADDED);
            }

            /* Make sure views are selected/checked as appropriate, including if
            they have been recycled after the list has been scrolled. Get the list of
            checked accounts, and if the view we are currently setting is for
            one of the checked accounts, check the appropriate checkbox and highlight
            the view (change the backgroud and text color). */
            if (dbFrag.getAccountListInActionMode()) {

                List<String> checkedAccounts = dbFrag.getCheckedAccounts();

                if (checkedAccounts.contains(accountName)) {

                    checkbox.setChecked(true);
                    listRow.setBackgroundResource(R.drawable.list_selected_holo_dark_old);

                } else {

                    checkbox.setChecked(false);
                    listRow.setBackgroundResource(android.R.color.transparent);
                }
            } else {

                /* If not in action mode, make sure no checkboxes are checked. */
                checkbox.setChecked(false);

                /* If we're in large-land mode and an account is open, make sure the
                open account's entry in the listview is highlighted. */
                if (dbFrag.getInLargeLandMode()) {

                    if (dbFrag.getAccountBeingViewed().equals(accountName)) {

                        listRow.setBackgroundResource(R.drawable.list_selected_holo_dark);
                    } else {

                        listRow.setBackgroundResource(android.R.color.transparent);
                    }
                } else {

                    /* If we're not in large-land mode, nor in action mode, no list rows
                    should be highlighted, nor checkboxes checked. */
                    listRow.setBackgroundResource(android.R.color.transparent);
                }
            }

            /* We haven't put the account name into the textview, so return false.*/
            return false;
        }

        /* Once synced the acount summary contains text formatted like this: "x loans. First due
        back:[date]" where [date] is a long value. Before displaying this in the account list,
        we should convert the date value to a human-readable format. Joda time is more suitable
        for our purposes here than PrettyTime. */
        if (column == 3) {

            TextView textView = (TextView) view;
            String summary = cursor.getString(3);
            int difference = 10;
            boolean accountHasLoans = false;

            if (summary.contains(":")) {

                accountHasLoans = true;
                String[] summaryParts = summary.split(":");
                DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());
                DateTime firstDueDate = new DateTime(Long.parseLong(summaryParts[1]), dateTimeZone);
                DateParser dateParser = new DateParser();
                summary = summaryParts[0] + dateParser.makeString(mCurrentDate, firstDueDate)
                        + ".";

                LocalDate today = new DateTime(System.currentTimeMillis(), dateTimeZone)
                        .toLocalDate();
                Days differenceInDays = Days.daysBetween(today, firstDueDate.toLocalDate());
                difference = differenceInDays.getDays();
            }

            if (!accountHasLoans) {

                textView.setText(summary);
            } else {

                /* Set text red and italicised if first item is due within margin. We only set
                * the second sentence of the summary to be red, the one which says "First due..." (or
                * "Due" if there's only one loan). */
                if (difference <= mMarginPref) {

                    int startOfSecondSentence;
                    if (summary.contains("First")) {

                        startOfSecondSentence = summary.indexOf(mContext.getString(R.string.first));
                    } else {

                        startOfSecondSentence = summary.indexOf(mContext.getString(R.string.due));
                    }

                    if (startOfSecondSentence > 0) {

                        Spannable summarySpannable = new SpannableString(summary);

                        summarySpannable.setSpan(new ForegroundColorSpan(Color.RED),
                                startOfSecondSentence,
                                summary.length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        summarySpannable.setSpan(new StyleSpan(Typeface.ITALIC),
                                startOfSecondSentence,
                                summary.length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(summarySpannable);
                    } else {

                        textView.setText(summary);
                    }

                } else {

                    textView.setText(summary);
                }
            }

            /* If we're in large-land mode and an account is open, change the text
            of the selected account to white. Also change the text to white if
            we're in context action mode and the account is checked. */
            DbInteractionsFragment dbFrag = DbFragmentRef.get();

            if (dbFrag.getAccountListInActionMode()) {

                List<String> checkedAccounts = dbFrag.getCheckedAccounts();

                if (checkedAccounts.contains(cursor.getString(1))) {

                    textView.setTextColor(Color.WHITE);
                } else {

                    textView.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                }
            } else {

                if (dbFrag.getInLargeLandMode()) {

                    if (dbFrag.getAccountBeingViewed().equals(cursor.getString(1))) {

                        textView.setTextColor(Color.WHITE);
                    } else {

                        textView.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                    }
                } else {

                    textView.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                }
            }

            return true;
        }

        /* The last_synced column contains a long value representing the last sync date, and can
        also contain error codes (as longs). Set how we want these values converted to text
        for display in the list of accounts. Use PrettyTime for dates. */
        if (column == 4) {

            TextView textView = (TextView) view;

            /*If the account's currently being synced, don't show the details from the db, just set
            the status to "Syncing..." */
            if (DbFragmentRef.get().isThisAccountBeingSynced(cursor.getString(1))) {

                textView.setText(mContext.getString(R.string.syncing));
            } else {

                long lastSyncedDateLong = cursor.getLong(4);
                String lastSyncedString;

                if (lastSyncedDateLong < 0) {

                    lastSyncedString = new SyncFailureReason(mContext, lastSyncedDateLong)
                            .getReasonString();
                } else {

                    /* Create a string such as "Last synced moments ago" using PrettyTime. */
                    Date lastSyncedDate = new Date(lastSyncedDateLong);
                    PrettyTime prettyTime = new PrettyTime(mCurrentDate.toDate());
                    lastSyncedString = mContext.getString(R.string.last_synced_prefix) + " "
                            + prettyTime.format(lastSyncedDate) + ".";
                }

                /* Make text red & italicised if account not synced for 24 hours or last attempt
                failed. First get current time*/
                long currentTime = System.currentTimeMillis();

               /* If the last-synced long is an error code (i.e. less than zero) this will
                * still make the text red, as the currentTime minus the error code will be
                * more than twenty-five hours in millis (since it will be greater than
                * the current time in millis). */
                long timeSinceLastSync = currentTime - lastSyncedDateLong;

                /* We use 25 hours to determine if account last synced time needs to be in
                * red. This is because the regular sync time may vary due to randomness we add
                * (up to 15 mins) and the extra time allowed by setInexactRepeating, therefore
                * it's possible that the time between autosync on consecutive days may be more
                * than 24 hours. But it should still be less than 25 hours (testing suggests
                * longest time might be around 24hrs 30 mins.*/
                if (timeSinceLastSync > TWENTY_FIVE_HOURS) {

                    Spannable lastSyncedSpannable = new SpannableString(lastSyncedString);

                    lastSyncedSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0,
                            lastSyncedString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    lastSyncedSpannable.setSpan(new StyleSpan(Typeface.ITALIC),
                            0, lastSyncedString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    textView.setText(lastSyncedSpannable);
                } else {

                    textView.setText(lastSyncedString);
                }
            }

            /* If we're in large-land mode and an account is open, change the text
            of the selected account to white. */
            DbInteractionsFragment dbFrag = DbFragmentRef.get();

            if (dbFrag.getAccountListInActionMode()) {

                List<String> checkedAccounts = dbFrag.getCheckedAccounts();

                if (checkedAccounts.contains(cursor.getString(1))) {

                    textView.setTextColor(Color.WHITE);
                } else {

                    textView.setTextColor(mContext.getResources()
                            .getColor(android.R.color.darker_gray));
                }
            } else {

                if (dbFrag.getInLargeLandMode()) {

                    if (dbFrag.getAccountBeingViewed().equals(cursor.getString(1))) {

                        textView.setTextColor(Color.WHITE);
                    } else {

                        textView.setTextColor(mContext.getResources()
                                .getColor(android.R.color.darker_gray));
                    }
                } else {

                    /* If not in action mode, and account is not open, last synced text
                    should always be grey. */
                    textView.setTextColor(mContext.getResources()
                            .getColor(android.R.color.darker_gray));
                }
            }

            return true;
        }

        return false;
    }

    public void updateTime() {

        mCurrentDate = new DateTime(System.currentTimeMillis(),
                DateTimeZone.forTimeZone(TimeZone.getDefault()));
    }

    public void updateMarginPref(int marginPref) {

        mMarginPref = marginPref;
    }
}

