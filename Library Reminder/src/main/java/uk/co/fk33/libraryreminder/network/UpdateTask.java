package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.MainActivity;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.util.List;

public class UpdateTask extends AsyncTask<LibAccount, String, List<LibItem>> {

    protected Context mContext;
    protected LibAccount mLibAccount;
    protected int mStatus = 0;

    public UpdateTask(Context context) {

        mContext = context;
    }

    protected boolean validNetworkAvailable() {

        return validNetworkAvailable(mContext);
    }

    protected static boolean validNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        if (activeNetwork == null) {

            return false;
        }

        if (!activeNetwork.isConnected()) {

            return false;
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String syncNetworkPref = prefs.getString(MainActivity.SYNC_NETWORK_PREF_KEY,
                MainActivity.NET_PREF_WIFI);

        int activeNetworkType = activeNetwork.getType();

        if (syncNetworkPref.equals(MainActivity.NET_PREF_WIFI)) {

            return (activeNetworkType == ConnectivityManager.TYPE_WIFI
                    || activeNetworkType == ConnectivityManager.TYPE_WIMAX);
        }

        if (syncNetworkPref.equals(MainActivity.NET_PREF_WIFI_DATA)) {

            return (activeNetworkType == ConnectivityManager.TYPE_WIFI
                    || activeNetworkType == ConnectivityManager.TYPE_WIMAX
                    || activeNetworkType == ConnectivityManager.TYPE_MOBILE
                    || activeNetworkType == ConnectivityManager.TYPE_MOBILE_DUN
                    || activeNetworkType == ConnectivityManager.TYPE_MOBILE_HIPRI
                    || activeNetworkType == ConnectivityManager.TYPE_MOBILE_MMS
                    || activeNetworkType == ConnectivityManager.TYPE_MOBILE_SUPL );
        }

        return false;
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        /* This is overridden properly in the classes which extend this class.*/

        return null;
    }

    @Override
    protected void onProgressUpdate(String... progressUpdate) {

        if (!mLibAccount.getInSilentMode()) {

            mLibAccount.showSyncProgress(progressUpdate[0]);
        }
    }

    @Override
    protected void onPostExecute(List<LibItem> itemList) {

        if (!mLibAccount.getInSilentMode()) {

            if (mStatus < 1) {

                mLibAccount.syncFailed(mStatus);
            } else {

                mLibAccount.writeSyncResults(itemList);
            }
        } else {

            mLibAccount.onBackgroundSyncComplete(itemList, mStatus);
        }
    }
}
