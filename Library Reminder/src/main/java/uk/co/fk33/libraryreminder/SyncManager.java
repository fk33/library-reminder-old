package uk.co.fk33.libraryreminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import uk.co.fk33.libraryreminder.network.AutoSyncService;

import java.util.Calendar;
import java.util.Random;

public class SyncManager {

    private static SyncManager mInstance = null;
    public static final int TOMORROW = -1;

    protected SyncManager() {

    }

    public static SyncManager getInstance() {

        if (mInstance == null) {

            mInstance = new SyncManager();
        }

        return mInstance;
    }

    public void setNextSyncTime(Context context, int minutes) {

        /* The ALARM_ID is declared in the locale-dependent class, so that alarms set by versions of
        * the app from different countries will not conflict with each other when installed on the
        * same device. */

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent autoSyncIntent = new Intent(context, AutoSyncService.class);
        PendingIntent autoSyncPendingIntent = PendingIntent.getService(context, LocaleDependentClass.ALARM_ID,
                autoSyncIntent, 0);

        Calendar alarmTimeCalendar = Calendar.getInstance(LocaleDependentClass.getDateLocale());
        alarmTimeCalendar.setTimeInMillis(System.currentTimeMillis());

        if (minutes > 0) {

            /* Calling this method with a number of minutes is used to set the sync time
            * if the first scheduled auto sync attempt of the day fails. For this reason, we
            * are not going to add any random minutes here, because the random minutes will
            * already have been added when the time scheduled for the first attempt was set.
            * When we finish our syncing for today, and set the alarm for tomorrow, we will
            * add some random minutes again to the time from the stored preferences. */
            alarmTimeCalendar.add(Calendar.MINUTE, minutes);

        } else {

            if (minutes == TOMORROW) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                String alarmTimeString = prefs.getString(MainActivity.SYNC_TIME_PREF_KEY, "");
                String[] alarmTimeStringPieces = alarmTimeString.split(":");
                int hour = Integer.parseInt(alarmTimeStringPieces[0]);
                int minute = Integer.parseInt(alarmTimeStringPieces[1]);
                alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, hour);
                alarmTimeCalendar.set(Calendar.MINUTE, minute);
                alarmTimeCalendar.add(Calendar.DATE, 1);

                /* Add a random number of minutes (up to 15) to reduce chance of too many instances of
                the app syncing at the same time, which might cause too many simulatenous requests to
                catalogue servers. Additional randomness is introduced by the usage of setInexactRepeating
                to set the alarm, which can often seems to add around 12 minutes.*/
                Random random = new Random();
                int additionalMinutes = random.nextInt(16);
                alarmTimeCalendar.add(Calendar.MINUTE, additionalMinutes);
            }
        }

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, alarmTimeCalendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, autoSyncPendingIntent);
    }

    public void setNextSyncTime(Context context, String alarmTimeString) {

        /* This is the method called when the user sets the sync time, or it is set
        * after being read from the stored sync time preference.*/
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent autoSyncIntent = new Intent(context, AutoSyncService.class);
        PendingIntent autoSyncPendingIntent = PendingIntent.getService(context, LocaleDependentClass.ALARM_ID,
                autoSyncIntent, 0);

        String[] alarmTimeStringPieces = alarmTimeString.split(":");
        int hour = Integer.parseInt(alarmTimeStringPieces[0]);
        int minute = Integer.parseInt(alarmTimeStringPieces[1]);

        Calendar alarmTimeCalendar = Calendar.getInstance(LocaleDependentClass.getDateLocale());
        long currentTimeInMillis = System.currentTimeMillis();
        alarmTimeCalendar.setTimeInMillis(currentTimeInMillis);
        alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, hour);
        alarmTimeCalendar.set(Calendar.MINUTE, minute);

        /* If we've gone past the alarm time today, set for tomorrow so the alarm doesn't
        * trigger immediately. */
        if (alarmTimeCalendar.getTimeInMillis() <= currentTimeInMillis) {

            alarmTimeCalendar.add(Calendar.DATE, 1);
        }

        /* Add a random number of minutes (up to 15) to reduce chance of too many instances of
        the app syncing at the same time, which might cause too many simultaneous requests to
        catalogue servers. Additional randomness is introduced by the usage of setInexactRepeating
        to set the alarm, which can often seems to add around 12 minutes. */
        Random random = new Random();
        int additionalMinutes = random.nextInt(16);
        alarmTimeCalendar.add(Calendar.MINUTE, additionalMinutes);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, alarmTimeCalendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, autoSyncPendingIntent);
    }

    public void cancel(Context context) {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent autoSyncIntent = new Intent(context, AutoSyncService.class);
        PendingIntent autoSyncPendingIntent = PendingIntent.getService(context, LocaleDependentClass.ALARM_ID,
                autoSyncIntent, 0);

        alarmManager.cancel(autoSyncPendingIntent);
    }
}
