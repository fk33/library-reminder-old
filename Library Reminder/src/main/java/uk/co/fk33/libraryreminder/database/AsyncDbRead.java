/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.database.Cursor;
import android.os.AsyncTask;

import uk.co.fk33.libraryreminder.DbFragmentRef;

public class AsyncDbRead extends AsyncTask<String, Void, Cursor> {

    private final int mFlag;

    public AsyncDbRead (int flag) {

        mFlag = flag;
    }

    @Override
    protected Cursor doInBackground(String... searchTerm) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        switch (mFlag) {

            case DbInteractionsFragment.DB_READ_EDIT_ACCOUNT_DETAILS: {

                return dbFrag.getDatabaseAdapter().getAccountDetails(searchTerm[0]);
            }

            case DbInteractionsFragment.DB_READ_GET_ITEM_DETAILS: {

                return dbFrag.getDatabaseAdapter().getItemDetails(searchTerm[0]);
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (cursor != null) {

            dbFrag.readFinished(cursor, mFlag);
        }
    }
}
