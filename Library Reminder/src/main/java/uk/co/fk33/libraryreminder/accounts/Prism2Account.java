/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;

import uk.co.fk33.libraryreminder.network.Prism2UpdateTask;

public class Prism2Account extends LibAccount {

    public Prism2Account(Context context, boolean silentMode, long rowId, String name, String catUrl, String cardNumber, String pinOrPassword,
                         boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.PRISM2;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        Prism2UpdateTask prism2UpdateTask = new Prism2UpdateTask(mContext);
        prism2UpdateTask.execute(this);
    }
}
