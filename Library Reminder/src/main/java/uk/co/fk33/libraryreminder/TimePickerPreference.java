package uk.co.fk33.libraryreminder;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimePickerPreference extends DialogPreference {

    private int mHour = 0;
    private int mMinute = 0;
    private String mTimeString;
    private TimePicker mTimePicker = null;

    public TimePickerPreference(Context context, AttributeSet attrs) {

        super(context, attrs);

        /* Negative button text is set on onPrepareDialogBuilder. */
        setPositiveButtonText(context.getString(R.string.set));
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialogInterface) {

                DbFragmentRef.get().storeTimePickerTime(-1, -1);
            }
        });

        /* Annoyingly, Android pickers don't call the onCancelListener when
        their cancel buttons are pressed, so we have to put a click listener
        on the negative button. */
        builder.setNegativeButton(getContext().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        DbFragmentRef.get().storeTimePickerTime(-1, -1);
                    }
                }
        );
    }

    @Override
    protected View onCreateView(ViewGroup parent) {

        readPreference();

        setSummary(formatTimeString(mTimeString));

        return super.onCreateView(parent);
    }

    @Override
    protected View onCreateDialogView() {

        /* Read in the stored preference value again. This has to be done because otherwise if
        * the time picker is open and the orientation changes, the values for mHour and mMinute
        * will be cleared, and the timepicker will be recreated with zero values for the time. */
        readPreference();

        mTimePicker = new TimePicker(getContext());

        if (android.text.format.DateFormat.is24HourFormat(getContext())) {

            mTimePicker.setIs24HourView(true);
        } else {

            /* The following workarounds are required due to a bug in certain Android APIs which
            * means the onTimeChangedListener is not invoked when the user changes the AM/PM
            * setting. See here: https://code.google.com/p/android/issues/detail?id=24388. */
            if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 15) {

                api11To15TimePickerBugWorkAround();
            }

            if (Build.VERSION.SDK_INT == 9 || Build.VERSION.SDK_INT == 10) {

                api9To10TimePickerBugWorkAround();
            }
        }

        int[] storedTimePickerTime = DbFragmentRef.get().getStoredTimePickerTime();

        /* If either index of stored time picker time is > -1, they both are.*/
        if (storedTimePickerTime[0] > -1) {

            mTimePicker.setCurrentHour(storedTimePickerTime[0]);
            mTimePicker.setCurrentMinute(storedTimePickerTime[1]);
        } else {

            mTimePicker.setCurrentHour(mHour);
            mTimePicker.setCurrentMinute(mMinute);
        }

        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                DbFragmentRef.get().storeTimePickerTime(hourOfDay, minute);
            }
        });

        return mTimePicker;
    }

    @Override
    protected void showDialog(Bundle savedInstanceState) {
        super.showDialog(savedInstanceState);

        /* ANOTHER TimePicker bug makes the hour disappear when you change orientation
        on certain APIs. This call makes the hour come back. */
        if (Build.VERSION.SDK_INT < 11) {

            mTimePicker.setCurrentHour(mTimePicker.getCurrentHour());
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {

            DbFragmentRef.get().storeTimePickerTime(-1, -1);

            mHour = mTimePicker.getCurrentHour();
            mMinute = mTimePicker.getCurrentMinute();
            mTimeString = String.valueOf(mHour) + ":" + String.valueOf(mMinute);

            if (!callChangeListener(mTimeString)) {

                return;
            }

            persistString(mTimeString);
            setSummary(formatTimeString(mTimeString));
            notifyDependencyChange(shouldDisableDependents());
            notifyChanged();

            /* update the time of the alarm which runs the autosync service. */
            SyncManager syncManager = SyncManager.getInstance();
            syncManager.setNextSyncTime(getContext(), mTimeString);
        }
    }

    public String formatTimeString(String inTimeString) {

        Date inTimeDate;
        Locale locale = LocaleDependentClass.getDateLocale();

        try {

            DateFormat dateFormat = new SimpleDateFormat("HH:mm", locale);
            inTimeDate = dateFormat.parse(inTimeString);

            String outTimeString;

            if (android.text.format.DateFormat.is24HourFormat(getContext())) {

                dateFormat = new SimpleDateFormat("HH:mm", locale);
                outTimeString = dateFormat.format(inTimeDate);
            } else {

                dateFormat = new SimpleDateFormat("hh:mma", locale);
                outTimeString = dateFormat.format(inTimeDate);

                if (outTimeString.startsWith("0")) {

                    outTimeString = outTimeString.substring(1);
                }

                /* I just think lower case looks nicer. */
                String suffix = outTimeString.substring(outTimeString.length() - 2,
                        outTimeString.length());
                outTimeString = outTimeString.substring(0, outTimeString.length() - 2)
                        + suffix.toLowerCase();
            }

            return outTimeString;

        } catch (ParseException parseException) {

             return inTimeString;
        }
    }

    private void readPreference() {

        mTimeString = getPersistedString(getContext().getString(R.string.default_remind_time));
        String[] timeStringPieces = mTimeString.split(":");
        mHour = Integer.parseInt(timeStringPieces[0]);
        mMinute = Integer.parseInt(timeStringPieces[1]);
    }

    private void api9To10TimePickerBugWorkAround() {

        View timePickerView = mTimePicker.getChildAt(0);
        if (timePickerView instanceof ViewGroup) {

            View amPmView = ((ViewGroup) timePickerView).getChildAt(2);

            if (amPmView instanceof Button) {

                amPmView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mTimePicker.setCurrentHour((mTimePicker.getCurrentHour() + 12) % 24);
                    }
                });
            }
        }
    }

    @TargetApi(11)
    private void api11To15TimePickerBugWorkAround() {

        View timePickerView = mTimePicker.getChildAt(0);
        if (timePickerView instanceof ViewGroup) {

            View amPmView = ((ViewGroup) timePickerView).getChildAt(3);

            if (amPmView instanceof NumberPicker) {

                NumberPicker amPmPicker = (NumberPicker) amPmView;

                amPmPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                        mTimePicker.setCurrentHour((mTimePicker.getCurrentHour() + 12) % 24);
                    }
                });
            }
        }
    }
}