/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class AddAccountOnShowListener implements DialogInterface.OnShowListener {

    private final AddAccountDialogFragment mAddAccountDialogFragment;
    private final AlertDialog mAddAccountDialog;
    private final Context mContext;

    public AddAccountOnShowListener(Context context,
                                    AlertDialog addAccountDialog,
                                    AddAccountDialogFragment addAccountDialogFragment) {

        mContext = context;
        mAddAccountDialog = addAccountDialog;
        mAddAccountDialogFragment = addAccountDialogFragment;
    }

    private class AddAccountOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            /* Query the selected org now as it might have changed since the listener was attached to the dialog! */
            LibOrganisation libOrganisation = mAddAccountDialogFragment.getSelectedOrganisation();

            if ((libOrganisation != null) && formIsFilledIn(mAddAccountDialog)) {

                /* Get the information entered into the add account form */
                EditText addAccountEditText = (EditText) mAddAccountDialog
                        .findViewById(R.id.account_name_edittext);
                String accountName = addAccountEditText.getText().toString();

                /* Make sure the account name supplied is not already in use for another account */
                if (nameNotInUse(accountName)) {

                    addAccountEditText = (EditText) mAddAccountDialog.findViewById(R.id.card_number);
                    String cardNumber = addAccountEditText.getText().toString();

                    /* Some Spydus catalogues add a prefix to the card number when logging in,
                    * so we have to add the prefix too. Adding it here means it will be saved
                    * into the db.*/
                    String orgName = libOrganisation.getName();
                    if (orgName.equals("Bolton") || orgName.equals("Calderdale")  || orgName.equals("Salford") ) {

                        cardNumber = addPrefix(orgName, cardNumber);
                    }

                    if (cardNumberNotInUse(cardNumber)) {

                        addAccountEditText = (EditText) mAddAccountDialog
                                .findViewById(R.id.pin_or_password);
                        String pinOrPassword = addAccountEditText.getText().toString();

                        CompoundButton reminderSwitch = (CompoundButton) mAddAccountDialog
                                .findViewById(R.id.reminder_switch);
                        boolean reminders = reminderSwitch.isChecked();

                        int region = mAddAccountDialogFragment.getRegion();

                        /* Add the account */
                        DbFragmentRef.get().addAccount(accountName, libOrganisation, cardNumber,
                                pinOrPassword, reminders, region);
                        mAddAccountDialog.dismiss();

                    } else {

                        Toast.makeText(mContext, R.string.already_account_with_card_number,
                                Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(mContext, R.string.already_account_with_name,
                            Toast.LENGTH_SHORT).show();
                }

            } else {

                if (libOrganisation == null) {

                    Toast.makeText(mContext, R.string.organisation_not_set, Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(mContext, R.string.form_not_completed, Toast.LENGTH_SHORT).show();
                }
            }
        }

        private boolean formIsFilledIn(DialogInterface addAccountDialogInterface) {

            /* Make sure the account name and card number are filled in. Pin/password
             can be left empty, as some library catalogues do not require one.*/
            Dialog addAccountDialog = (Dialog) addAccountDialogInterface;

            EditText addAccountEditText = (EditText) addAccountDialog.findViewById(R.id.account_name_edittext);
            if (addAccountEditText.getText().toString().equals("")) {

                return false;
            }

            addAccountEditText = (EditText) addAccountDialog.findViewById(R.id.card_number);
            return !addAccountEditText.getText().toString().equals("");
        }

        private boolean nameNotInUse(String accountName) {

            List<String> accountNamesList = DbFragmentRef.get().getAccountNamesList();
            return !accountNamesList.contains(accountName);
        }

        private boolean cardNumberNotInUse(String cardNumber) {

            List<String> accountCardNumbersList = DbFragmentRef.get().getAccountCardNumbersList();
            return !accountCardNumbersList.contains(cardNumber);
        }

        private String addPrefix(String orgName, String cardNumber) {

            if (orgName.equals("Bolton")) {

                if (!cardNumber.startsWith("16P")) {

                    cardNumber = "16P" + cardNumber;
                }
            }

            if (orgName.equals("Calderdale")) {

                if (!cardNumber.startsWith("R")) {

                    cardNumber = "R" + cardNumber;
                }
            }

            if (orgName.equals("Salford")) {

                if (cardNumber.length() == 8) {

                    cardNumber = "00" + cardNumber;
                }
            }

            return cardNumber;
        }
    }

    @Override
    public void onShow(DialogInterface dialogInterface) {

        /* Set the custom onClickListener for the OK button (checks for is filled in
        before dismissing the dialog. */
        Button okButton = mAddAccountDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        okButton.setOnClickListener(new AddAccountOnClickListener());
    }
}
