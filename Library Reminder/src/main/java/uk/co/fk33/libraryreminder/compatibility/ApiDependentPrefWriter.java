/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;


import android.content.SharedPreferences;

public abstract class ApiDependentPrefWriter {

    public abstract void writeBooleanPref(SharedPreferences sharedPreferences, String key, boolean value);

    public abstract void writeIntPref(SharedPreferences sharedPreferences, String key, int value);

    public abstract void writeStringPref(SharedPreferences sharedPreferences, String key, String value);

}
