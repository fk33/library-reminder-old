/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Period;

import java.util.List;
import java.util.TimeZone;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class ItemListViewBinder implements SimpleCursorAdapter.ViewBinder {

    private final String listenerAdded = "LISTENER_ADDED";
    private final Context mContext;
    private int mMarginPref;

    public ItemListViewBinder(Context context, int marginPref) {

        mContext = context;
        mMarginPref = marginPref;
    }


    @Override
    public boolean setViewValue(View view, Cursor cursor, int column) {

        /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

        /* This is part of the code which allows you to put the item list into action mode.
        * There are no bulk functions for items in this version of the app so it has been removed.
        * It may be re-enabled if future versions of the app include bulk item fuctions.*/

        //        if (column == 1) {
//
//            DbInteractionsFragment dbFrag = DbFragmentRef.get();
//            Long itemRow = cursor.getLong(0);
//
//            View listRow = (ViewGroup) view.getParent().getParent();
//
//            CheckBox checkbox = (CheckBox) listRow.findViewById(R.id.view_account_checkbox);
//
//            /* If the checkbox doesn't have an onClickListener, set it here. */
//            if (checkbox.getTag() == null) {
//
//                checkbox.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View clickedCheckBox) {
//
//                        DbFragmentRef.get().onCheckBoxClicked(clickedCheckBox);
//                    }
//                });
//
//                /*
//                Tag the checkbox so we can check whether it's got a listener.
//                (This check takes place above, at the start of this block).
//                */
//                checkbox.setTag(listenerAdded);
//            }
//
//            /*
//            Make sure views are selected/checked as appropriate, including if
//            they have been recycled after the list has been scrolled. Get the list of
//            checked items, and if the view we are currently setting is for
//            one of the checked accounts, check the appropriate checkbox and highlight
//            the view (change the backgroud and text color).
//            */
//            if (dbFrag.getViewAccountInActionMode()) {
//
//                List<Long> checkedItems = dbFrag.getCheckedItems();
//
//                if (checkedItems.contains(itemRow)) {
//
//                    checkbox.setChecked(true);
//                    listRow.setBackgroundResource(R.drawable.list_selected_holo_dark);
//                } else {
//
//                    checkbox.setChecked(false);
//                    listRow.setBackgroundResource(android.R.color.transparent);
//                }
//            } else {
//
//                /*
//                If not in action mode, make sure no checkboxes are checked and no rows
//                highlighted.
//                */
//                checkbox.setChecked(false);
//                listRow.setBackgroundResource(android.R.color.transparent);
//            }
//        }

        /***********************************************************************************/

        if (column == 2) {

            /* Hide the view if there's no text to go in it. */
            if (cursor.getString(2).equals("")) {

                view.setVisibility(View.GONE);
            } else {

                view.setVisibility(View.VISIBLE);

                   /* THIS CODE NOT NEEDED IN THIS VERSION OF APP AS NO ACTOUN MODE IN ITEM LIST */
//                /* If we're in action mode and item is selected, change the text
//                of the author field to white. */
//                DbInteractionsFragment dbFrag = DbFragmentRef.get();
//                TextView authorTextView = (TextView) view;
//
//                if (dbFrag.getViewAccountInActionMode()) {
//
//                    List<Long> checkedItems = dbFrag.getCheckedItems();
//
//
//                    if (checkedItems.contains(cursor.getLong(0))) {
//
//                        authorTextView.setTextColor(Color.WHITE);
//                    } else {
//
//                        authorTextView.setTextColor(mContext.getResources()
//                                .getColor(android.R.color.darker_gray));
//                    }
//                } else {
//
//                    /* If not in action mode, author field text should always be grey. */
//                    authorTextView.setTextColor(mContext.getResources()
//                            .getColor(android.R.color.darker_gray));
//                }
            }
        }

        if (column == 3) {

            TextView dueDateTextView = (TextView) view;

            DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());

            DateTime currentDate = new DateTime(System.currentTimeMillis(), dateTimeZone);
            DateTime dueDate = new DateTime(cursor.getLong(3), dateTimeZone);

            DateParser dateParser = new DateParser();
            String relativeDateString =
                    "Due " + dateParser.makeString(currentDate, dueDate) + ".";

            Days differenceInDays = Days.daysBetween(currentDate.toLocalDate(), dueDate.toLocalDate());
            int difference = differenceInDays.getDays();

            if (difference <= mMarginPref) {

                Spannable summarySpannable = new SpannableString(relativeDateString);
                summarySpannable.setSpan(new ForegroundColorSpan(Color.RED), 0,
                        relativeDateString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                summarySpannable.setSpan(new StyleSpan(Typeface.ITALIC), 0,
                        relativeDateString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                dueDateTextView.setText(summarySpannable);
            } else {

                dueDateTextView.setText(relativeDateString);
            }

            /* If we're in action mode and item is selected, change the text
            of the due date field to white. */
            DbInteractionsFragment dbFrag = DbFragmentRef.get();

            if (dbFrag.getViewAccountInActionMode()) {

                List<Long> checkedItems = dbFrag.getCheckedItems();

                if (checkedItems.contains(cursor.getLong(0))) {

                    dueDateTextView.setTextColor(Color.WHITE);
                } else {

                    dueDateTextView.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                }
            } else {

                /* If not in action mode, due date text should always be grey. */
                dueDateTextView.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
            }

            /* We've set the view's text ourselves, so we can return true. */
            return true;
        }

        return false;
    }

    public void updateMarginPref(int marginPref) {

        mMarginPref = marginPref;
    }
}
