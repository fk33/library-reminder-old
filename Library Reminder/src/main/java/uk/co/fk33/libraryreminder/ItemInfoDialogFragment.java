/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class ItemInfoDialogFragment extends CloseableDialogFragment {

    protected ItemInfoDialogParent mItemInfoDialogParent;

    public interface ItemInfoDialogParent {

        public void removeItemInfoFrag();
        public void removeDualPaneItemInfoFrag();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Activity activity = getActivity();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        /* Passing null here causes a warning, but the example in the Google docs for a dialog
        * fragment recommends passing null, and if I pass the fragment_container layout
        * from main.xml, it causes a crash in dual-pane mode. So I'm passing null.*/
        View dialogView = activity.getLayoutInflater().inflate(R.layout.item_info_dialog, null);

        CheckBox remindCheckBox = (CheckBox) dialogView.findViewById(
                R.id.item_info_remind_checkbox);

        remindCheckBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View checkboxView) {

                CheckBox checkBox = (CheckBox) checkboxView;
                boolean newremindSetting = checkBox.isChecked();
                DbFragmentRef.get().updateItemRemindSetting(newremindSetting);
            }
        });

        /* Make the checkbox toggle if the user clicks the label.*/
        View remindLinearLayout = dialogView.findViewById(R.id.item_info_remind_linear_layout);
        remindLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                CheckBox remindCheckBox = (CheckBox) view.findViewById(
                        R.id.item_info_remind_checkbox);

                if (remindCheckBox != null) {

                    remindCheckBox.performClick();
                }
            }
        });

        dialogBuilder.setView(dialogView)

                .setNeutralButton(R.string.close, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        DbFragmentRef.get().onItemInfoDialogClose();
                    }
                });

        Dialog itemInfoDialog = dialogBuilder.create();

        itemInfoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return itemInfoDialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof ItemInfoDialogParent) {
            mItemInfoDialogParent = (ItemInfoDialogParent) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement ItemInfoDialogParent.");
        }

        runItemInfoQueryOrRemoveThisFragmentIfNecessary(activity);
    }

    protected void runItemInfoQueryOrRemoveThisFragmentIfNecessary(Activity activity) {

        /* This particular fragment is only ever created when in single-pane mode, so if we
        are in dual-pane mode here, it means the configuration has changed while this fragment
        was open, and the system has recreated the fragment. But this fragment will have to be
        replaced with one hosted in the MainActivity, because when going from single to
        dual-pane mode, the ViewAccountActivity is removed (which would have been hosting this
        fragment). So don't bother running the db query, and remove this fragment. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (!dbFrag.getInLargeLandMode()) {

            String itemBeingViewed = dbFrag.getItemBeingViewed();
            if (!itemBeingViewed.equals("")) {

                dbFrag.runItemInfoQuery((FragmentActivity) activity, itemBeingViewed);
            }
        } else {

            mItemInfoDialogParent.removeItemInfoFrag();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        DbFragmentRef.get().onItemInfoDialogClose();
    }
}
