/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.app.Activity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;

public abstract class ApiDependentMenuCode {

    public abstract void addViewAccountMenuItems(Menu menu, MenuInflater menuInflater);
    public abstract void removeViewAccountMenuItems(Menu menu);
    public abstract void callInvalidateOptionsMenu(Activity activity);
}
