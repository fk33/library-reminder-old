/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;
import uk.co.fk33.libraryreminder.network.HeritageUpdateTask;

public class HeritageAccount extends LibAccount {

    public HeritageAccount(Context context, boolean silentMode, long rowId, String name, String catUrl,
                           String cardNumber, String pinOrPassword, boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.HERITAGE;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        HeritageUpdateTask heritageUpdateTask = new HeritageUpdateTask(mContext);
        heritageUpdateTask.execute(this);
    }
}

