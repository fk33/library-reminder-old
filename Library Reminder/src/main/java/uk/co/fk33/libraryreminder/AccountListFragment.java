/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.ActionMode;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class AccountListFragment extends SherlockFragment implements
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener,
        AbsListView.OnScrollListener {

    private ActionMode mActionMode;
    private AccountListParent mFragmentParent;
    private ListView mListView;

    public interface AccountListParent extends FragmentParent {

        public void onAccountClick(String accountName, boolean accountRemindSetting);

        public void syncAccounts(String[] accountsToSync);

        public void confirmDeleteAccounts(String[] accountsToDelete);

        public void hideViewAccountFragment();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Cursor cursor = (Cursor) parent.getItemAtPosition(position);

        /* Pass the name of the account to the onAccountClick method. First check we're
        not in action mode, if we are don't open account. */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (!dbFrag.getAccountListInActionMode()) {

            boolean accountRemindSetting = false;
            int accountRemindInt = cursor.getInt(5);

            if (accountRemindInt == 1) {
                accountRemindSetting = true;
            }

            mFragmentParent.onAccountClick(cursor.getString(1), accountRemindSetting);

            /* If in large-land mode, refresh the listview so the clicked account will be highlighted. */
            if (dbFrag.getInLargeLandMode()) dbFrag.getAccountListCursorAdapter().notifyDataSetChanged();
        } else {

            /* If we're in action mode, clicking the account checks the account's checkbox. */
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.account_list_checkbox);
            if (checkBox != null) {

                if (checkBox.isChecked()) {

                    checkBox.setChecked(false);

                } else {

                    checkBox.setChecked(true);
                }

                onCheckBoxClicked(checkBox);
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (!dbFrag.getAccountListInActionMode()) {

            CheckBox checkBox = (CheckBox) view.findViewById(R.id.account_list_checkbox);
            checkBox.setChecked(true);
            onCheckBoxClicked(checkBox);
        }

        return true;
    }

    public void onCheckBoxClicked(View checkbox) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* Keep track of the number of items with checked checkboxes. */
        boolean checked = ((CheckBox) checkbox).isChecked();
        ViewGroup parent = (ViewGroup) checkbox.getParent().getParent();
        TextView accountName = (TextView) parent.findViewById(R.id.list_view_account_name);

        if (checked) {

            dbFrag.incrementNumberOfCheckedAccounts();

            /* Store the name of the checked account. */
            dbFrag.addCheckedAccount(accountName.getText().toString());
        } else {

            dbFrag.decrementNumberOfCheckedAccounts();

            /* Remove the name of the checked account. */
            dbFrag.removeCheckedAccount(accountName.getText().toString());
        }

        /* If one or more checkboxes is checked, we should be in context action mode. */
        if (dbFrag.getNumberOfCheckedAccounts() > 0) {

            /* Start the action mode if not already started. */
            if (!dbFrag.getAccountListInActionMode()) {

                /* If in large land mode, close the view account frag. */
                if (dbFrag.getInLargeLandMode()) mFragmentParent.hideViewAccountFragment();

                mActionMode = getSherlockActivity().startActionMode(new
                        ActionModeCallback(mFragmentParent, mListView, R.menu.context_menu));
                dbFrag.setAccountListInActionMode(true);
            }

            /* Update the title bar with the number of selected items. */
            mActionMode.setTitle(Integer.toString(dbFrag.getNumberOfCheckedAccounts()) + " selected");

        } else {

            /* If the last checkbox has been deselected, end the action mode. */
            if (dbFrag.getAccountListInActionMode()) {

                /* The accountListInActionMode boolean in db frag will be set
                false in onDestroyActionMode of the ActionModeCallback. */
                mActionMode.finish();
            }
        }

        /* If we're in action mode and a checkbox is clicked, refresh the listview so the row will be
        highlighted. */
        if (dbFrag.getAccountListInActionMode()) dbFrag.getAccountListCursorAdapter()
                .notifyDataSetChanged();
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {

    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {

        if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {

            if (DbFragmentRef.get().getInLargeLandMode()) {

                mFragmentParent.hideViewAccountFragment();
            }
        }
    }

    @Override
    public void onResume () {
        super.onResume();

        /* There is a bug, presumably in SherlockFragment, which causes the account
        * list to lose its onClickListener on Pre-Honeycomb APIs in a specific
        * situation (i.e. when we've switched from dual to single-pane mode when
        * an account is open and have then closed the open account, revealing the account
        * list frag underneath). The code here involves checking for that situation and
        * restoring the onClickListener to the account list. We check for having
        * switched modes with an account open elsewhere and set a boolean in the dbfrag
        * which is checked for here.*/
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {

            DbInteractionsFragment dbFrag = DbFragmentRef.get();
            if (dbFrag.getAccountBeingViewed().equals("")) {
                if (!dbFrag.getInLargeLandMode()) {
                    if (dbFrag.getPotentialABSBug()) {

                        /* Neutralise the presumed ABS bug be resetting the onClickListeners etc.*/

                        SimpleCursorAdapter cursorAdapter = dbFrag.getAccountListCursorAdapter();

                        mListView = (ListView) this.getView().findViewById(R.id.accountListListView);

                        mListView.setAdapter(cursorAdapter);

                        mListView.setChoiceMode(AbsListView.CHOICE_MODE_NONE);
                        mListView.setOnItemClickListener(this);
                        mListView.setOnItemLongClickListener(this);
                        mListView.setOnScrollListener(this);

                        dbFrag.setPotentialABSBug(false);
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

        if (activity instanceof AccountListParent) {

            mFragmentParent = (AccountListParent) activity;
        } else {

            throw new ClassCastException(activity.toString() + " must implement AccountListParent.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentParent = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.account_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        SimpleCursorAdapter cursorAdapter = dbFrag.getAccountListCursorAdapter();

        mListView = (ListView) this.getView().findViewById(R.id.accountListListView);

        mListView.setAdapter(cursorAdapter);

        /* Set this fragment as the onClick listener and onScroll listener for its listview */
        mListView.setChoiceMode(AbsListView.CHOICE_MODE_NONE);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        mListView.setOnScrollListener(this);

        /*
        If the frag has been recreated after orientation change while in action mode, restart
        the action mode.
        */
        if (dbFrag.getAccountListInActionMode()) {

            mActionMode = getSherlockActivity().startActionMode(new
                    ActionModeCallback(mFragmentParent, mListView, R.menu.context_menu));

            /* Update the title bar with the number of selected items. */
            mActionMode.setTitle(Integer.toString(dbFrag.getNumberOfCheckedAccounts()) + " selected");
        }
    }

    public void updateActionModeTitle() {

        /* Update the title bar with the number of selected items. */
        mActionMode.setTitle(Integer.toString(DbFragmentRef.get()
                .getNumberOfCheckedAccounts()) + " selected");

    }
}