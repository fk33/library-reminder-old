/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;

import uk.co.fk33.libraryreminder.network.ViewpointUpdateTask;

public class ViewpointAccount extends LibAccount {

    public ViewpointAccount(Context context, boolean silentMode, long rowId, String name, String catUrl, String cardNumber, String pinOrPassword,
                            boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.VIEWPOINT;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        ViewpointUpdateTask viewpointUpdateTask = new ViewpointUpdateTask(mContext);
        viewpointUpdateTask.execute(this);
    }
}
