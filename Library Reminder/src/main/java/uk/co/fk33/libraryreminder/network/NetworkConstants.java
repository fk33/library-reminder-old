/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

public class NetworkConstants {

    final public static int TIMEOUT_MILLIS = 10000;
    final public static String READ_TIMED_OUT = "Read timed out";
    final public static String TIMEOUT_EXCEPTION = "class java.net.SocketTimeoutException";
    final public static String HTTP_STATUS_ERROR = "HTTP error fetching URL";
}
