/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.app.Activity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;

public class Api8To10DependentMenuCode extends ApiDependentMenuCode {

    @Override
    public void addViewAccountMenuItems(Menu menu, MenuInflater menuInflater) {

    }

    @Override
    public void removeViewAccountMenuItems(Menu menu) {

    }

    @Override
    public void callInvalidateOptionsMenu(Activity activity) {

    }
}
