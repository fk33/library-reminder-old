/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragmentActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class AddAccountDialogFragment extends CloseableDialogFragment
        implements SetOrganisationDialogFragment.SetOrganisationDialogFragmentParent {

    private static final String SELECTED_ORGANISATION = "SELECTED_ORGANISATION";
    private static final String ACCOUNT_NAME = "ACCOUNT_NAME";
    private static final String CARD_NUMBER = "CARD_NUMBER";
    private static final String PIN_OR_PASSWORD = "PIN_OR_PASSWORD";
    private static final String REMINDER_BOOLEAN = "REMINDER_BOOLEAN";

    private LibOrganisation mSelectedOrganisation = null;
    private volatile boolean mOrgsLoaded = false;
    private volatile List<LibOrganisation> mOrgList = null;
    private String[] mOrgNames = null;
    private OrgsLoader mOrgsLoader;
    private Thread mOrgsLoaderThread = null;
    private int mRegion;

    /* Class to load list of supported organisations from an xml file, in a thread */
    private static class OrgsLoader implements Runnable {

        private final WeakReference<AddAccountDialogFragment> mAddAccountFragRef;
        private final XmlResourceParser mParser;
        private volatile boolean mKilled = false;

        public OrgsLoader(AddAccountDialogFragment addAccountDialogFragment, XmlResourceParser parser) {

            mAddAccountFragRef = new WeakReference<AddAccountDialogFragment>(addAccountDialogFragment);
            mParser = parser;
        }

        public void killThread() {

            mKilled = true;
        }

        @Override
        public void run() {

            try {

                /* Write the organisation data from organisations.xml into a list
                of LibOrganisation objects. */
                List<LibOrganisation> orgList = new ArrayList<LibOrganisation>();
                LibOrganisation libOrganisation;

                mParser.next();
                int eventType = mParser.getEventType();
                while (eventType != XmlResourceParser.END_DOCUMENT) {

                    if (eventType == XmlResourceParser.START_TAG &&
                            mParser.getName().equals("organisation")) {

                        /* Create new LibOrganisation object with values from
                        the xml org definition. */
                        libOrganisation = new LibOrganisation(
                                mParser.getAttributeValue(null, "name"),
                                mParser.getAttributeValue(null, "short_name"),
                                mParser.getAttributeValue(null, "cat_type"),
                                mParser.getAttributeValue(null, "cat_url"));

                        /* Add it to the list */
                        orgList.add(libOrganisation);

                    }

                    eventType = mParser.next();

                    if (mKilled) {

                        break;
                    }
                }

                if (!mKilled) {

                    /* Update the mOrgList variable of the AddAccountDialogFragment */
                    AddAccountDialogFragment addAccountDialogFragment =
                            mAddAccountFragRef.get();
                    if (addAccountDialogFragment != null) {

                        addAccountDialogFragment.setOrgList(orgList);
                        addAccountDialogFragment.setOrgsLoaded(true);
                        addAccountDialogFragment.showSetOrgDialogIfProgBarShown();
                    }
                }
            } catch (Exception exception) {

                final Activity activity = mAddAccountFragRef.get().getActivity();
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        new AlertDialog.Builder(activity)
                                .setTitle(activity.getString(R.string.error))
                                .setMessage(activity.getString(R.string.error_loading_organisations))
                                .setPositiveButton(activity.getString(R.string.OK),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int which) { /*Do nothing */}
                                        }
                                )
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (savedInstanceState != null) {

            mRegion = savedInstanceState.getInt(MainActivity.REGION);
        } else {

            mRegion = getArguments().getInt(MainActivity.REGION);
        }

        /* Start the thread which loads the organisation info from an xml file into memory */
        XmlResourceParser parser = getResourceParserForRegion();
        mOrgsLoader = new OrgsLoader(this, parser);
        mOrgsLoaderThread = new Thread(mOrgsLoader);
        mOrgsLoaderThread.start();

        final Activity activity = getActivity();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater layoutInflater = activity.getLayoutInflater();

        /* Null is passed here according to Google docs for inflating a view in a dialog,
        although it causes a lint warning. */
        View addAccountDialogView = layoutInflater.inflate(R.layout.add_account_dialog, null);

        /* If the frag has been resumed after organisation selected, restore the
        name of the organisation to the organisation text view, and any other
        info which had been entered in the editable fields. */
        if (savedInstanceState != null) {

            mSelectedOrganisation = savedInstanceState.getParcelable(SELECTED_ORGANISATION);
            String accountName = savedInstanceState.getString(ACCOUNT_NAME);
            String cardNumber = savedInstanceState.getString(CARD_NUMBER);
            String pinOrPassword = savedInstanceState.getString(PIN_OR_PASSWORD);
            boolean reminderSwitchState = savedInstanceState.getBoolean(REMINDER_BOOLEAN);

            if (mSelectedOrganisation != null) {

                TextView organisationTextView = (TextView) addAccountDialogView
                        .findViewById(R.id.organisation_textview);
                if (organisationTextView != null) {

                    organisationTextView.setText(mSelectedOrganisation.getName());
                }
            }

            EditText addAccountEditText;

            if (accountName != null) {

                addAccountEditText = (EditText) addAccountDialogView
                        .findViewById(R.id.account_name_edittext);
                addAccountEditText.setText(accountName);
            }

            if (cardNumber != null) {

                addAccountEditText = (EditText) addAccountDialogView
                        .findViewById(R.id.card_number);
                addAccountEditText.setText(cardNumber);
            }

            if (pinOrPassword != null) {

                addAccountEditText = (EditText) addAccountDialogView
                        .findViewById(R.id.pin_or_password);
                addAccountEditText.setText(pinOrPassword);
            }

            CompoundButton reminderSwitch = (CompoundButton) addAccountDialogView
                    .findViewById(R.id.reminder_switch);
            reminderSwitch.setChecked(reminderSwitchState);
        } else {

            /* Set the Automatic renewals toggle button to On (default for new accounts). */
            CompoundButton toggleButton = (CompoundButton) addAccountDialogView.findViewById(
                    R.id.reminder_switch);
            toggleButton.setChecked(true);
        }

        /* Set the onClickListener for the set organisation button */
        View setOrganisationButton = addAccountDialogView.findViewById(R.id.set_organisation_button);
        setOrganisationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddAccountDialogFragment.this.setOrganisation();
            }
        });

        /* Put the view we've just made into the dialogBuilder*/
        dialogBuilder.setView(addAccountDialogView)

                .setTitle(R.string.add_account_dialog_title).setIcon(R.drawable.ic_add)

                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {

                    /* Do nothing with this onClickListener, we add a new onclicklistener below where we check
                    the form is filled in. If we use this onClickListener there is no way to stop the dialog
                    being dismissed automatically when the OK button is pressed. */
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) { /* do nothing */}
                })

                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog addAccountDialog = dialogBuilder.create();

        /* Add a custom OnShowListener so we can check the form is filled in fully before
        dismissing the dialog. The AddAccountOnShowListener type includes an onClickListener
        which attaches to the OK button. This listener has methods to validate form input,
        which it then passes to the addAccount method (in the DbInteractionsFragment). */
        AddAccountOnShowListener addAccountOnShowListener = new AddAccountOnShowListener(
                activity, addAccountDialog, this);
        addAccountDialog.setOnShowListener(addAccountOnShowListener);

        return addAccountDialog;
    }

    private XmlResourceParser getResourceParserForRegion() {

        XmlResourceParser parser = null;

        switch (mRegion) {
            case MainActivity.REGION_ENGLAND_NORTH_MID: {
                parser = getResources().getXml(R.xml.english_north_mid_organisations);
                break;
            }
            case MainActivity.REGION_ENGLAND_EAST_SOUTH: {
                parser = getResources().getXml(R.xml.english_east_south_organisations);
                break;
            }
            case MainActivity.REGION_IRELAND: {
                parser = getResources().getXml(R.xml.irish_organisations);
                break;
            }
            case MainActivity.REGION_SCOTLAND: {
                parser = getResources().getXml(R.xml.scottish_organisations);
                break;
            }
            case MainActivity.REGION_WALES: {
                parser = getResources().getXml(R.xml.welsh_organisations);
                break;
            }
            case MainActivity.REGION_UK_IE_COLLEGE: {
                parser = getResources().getXml(R.xml.uk_ie_college_organisations);
                break;
            }
            case MainActivity.REGION_UK_IE_UNIVERSITY: {
                parser = getResources().getXml(R.xml.uk_ie_university_organisations);
                break;
            }
            case MainActivity.REGION_UK_HEALTH: {
                parser = getResources().getXml(R.xml.uk_ie_health_organisations);
                break;
            }
        }

        return parser;
    }

    @Override
    public void onStart() {
        super.onStart();

        /* We have to manually restore the set organisation dialog fragment if it was open
        during an orientation change, otherwise it will go beneath this fragment rather than
        on top of it. */
        if (DbFragmentRef.get().getSetOrganisationDialogFragmentOpen()) {

            setOrganisation();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);

        /* Kill the loader thread if it exists, we don't want multiple threads if
        user reopens this dialog */
        if (mOrgsLoaderThread != null) {

            if (mOrgsLoaderThread.isAlive()) {

                mOrgsLoader.killThread();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putInt(MainActivity.REGION, mRegion);

        if (mSelectedOrganisation != null) {

            outState.putParcelable(SELECTED_ORGANISATION, mSelectedOrganisation);
        }

        Dialog addAccountDialog = getDialog();
        EditText addAccountEditText;

        addAccountEditText = (EditText) addAccountDialog.findViewById(R.id.account_name_edittext);
        String accountName = addAccountEditText.getText().toString();

        addAccountEditText = (EditText) addAccountDialog.findViewById(R.id.card_number);
        String cardNumber = addAccountEditText.getText().toString();

        addAccountEditText = (EditText) addAccountDialog.findViewById(R.id.pin_or_password);
        String pinOrPassword = addAccountEditText.getText().toString();

        if (!accountName.equals("")) {

            outState.putString(ACCOUNT_NAME, accountName);
        }

        if (!cardNumber.equals("")) {

            outState.putString(CARD_NUMBER, cardNumber);
        }

        if (!pinOrPassword.equals("")) {

            outState.putString(PIN_OR_PASSWORD, pinOrPassword);
        }

        CompoundButton reminderSwitch = (CompoundButton) addAccountDialog
                .findViewById(R.id.reminder_switch);
        outState.putBoolean(REMINDER_BOOLEAN, reminderSwitch.isChecked());
    }

    public void setOrganisation() {

        /* Check organisation data has been loaded from xml by thead started in
        onCreateDialog. If it hasn't and the thread is still running, show an
        indeterminate progress dialog which the user can dismiss if they don't
        want to wait. The thread won't be killed though unless they close the
        AddAccountDialog, so they can always press the Set Organisation button again. */
        if (!mOrgsLoaded) {

            if (mOrgsLoaderThread != null) {

                if (mOrgsLoaderThread.isAlive()) {

                    /* When the thread which loads the data from the xml file finishes,
                    it will check if this progress indicator exists, and get rid of it
                    if it does. If the indicator does exist, the thread will call
                    setOrganisationDialog() itself, as otherwise it wouldn't be called. */
                    WaitForThreadProgressFragment waitForProgressFrag = new
                            WaitForThreadProgressFragment();
                    waitForProgressFrag.show(getActivity().getSupportFragmentManager(),
                            MainActivity.WAIT_FOR_PROGRESS_FRAGMENT_TAG);
                }
            }
        } else {

            showSetOrganisationDialog();
        }
    }

    public void showSetOrganisationDialog() {

        /* This dialog is just a list of library organisations supported by the app
        The user picks one to set up an account for that organisation. */
        SetOrganisationDialogFragment setOrganisationDialogFragment = new SetOrganisationDialogFragment();
        setOrganisationDialogFragment.setParent(this, mRegion);
        setOrganisationDialogFragment.show(getActivity().getSupportFragmentManager(),
                MainActivity.SET_ORGANISATION_FRAGMENT_TAG);

        DbFragmentRef.get().setSetOrganisationFragmentOpen(true);
    }

    public void showSetOrgDialogIfProgBarShown() {

        /* If the progress indicator is shown, that means the thread which loads
        the xml data took a long time to run and the user has in the meantime
        clicked the button to show the Set Organisation dialog, and is waiting
        for it to appear. We should get rid of the progress indicator and show
        the dialog. */
        SherlockFragmentActivity activity = (SherlockFragmentActivity) getActivity();
        if (activity != null) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            if (fragmentManager != null) {
                DialogFragment waitForProgressFrag = (DialogFragment) fragmentManager
                        .findFragmentByTag(MainActivity.WAIT_FOR_PROGRESS_FRAGMENT_TAG);
                if (waitForProgressFrag != null) {

                    waitForProgressFrag.dismiss();
                    showSetOrganisationDialog();
                }
            }
        }
    }

    public void onSetOrganisationListViewClick(SetOrganisationDialogFragment
                                                       setOrganisationDialogFragment, int selectedItem) {

		/* The user has selected an organisation from the list of organisations.
        Get the organisation name and put it into the appropriate textview in the
		AddAccountDialogFragment. */
        mSelectedOrganisation = mOrgList.get(selectedItem);
        setOrganisationDialogFragment.dismiss();

        TextView organisationTextView = (TextView) getDialog()
                .findViewById(R.id.organisation_textview);
        organisationTextView.setText(mSelectedOrganisation.getName());

		/* Create a name for the account, if pref enabled, and put it into
		accountname text field.*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean suggestName = prefs.getBoolean(MainActivity.SUGGEST_NAME, true);
        if (suggestName) {

            fillInSuggestedName();
        } else {

            focusOnFirstEmptyField(false);
        }
    }

    private void focusOnFirstEmptyField(boolean skipAccountName) {

        Dialog addAccountDialog = getDialog();
        String contents;

        if (!skipAccountName) {

            /* Check the account name field. */
            final EditText accountNameEditText = (EditText) addAccountDialog
                    .findViewById(R.id.account_name_edittext);
            contents = accountNameEditText.getText().toString();

            if (contents.length() == 0) {

                accountNameEditText.requestFocus();
                accountNameEditText.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager)
                                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(accountNameEditText, 0);
                    }
                }, 100);

                return;
            }
        }

        /* There was text in the account field, try the next field. */
        final EditText cardNumberEditText = (EditText) addAccountDialog
                .findViewById(R.id.card_number);
        contents = cardNumberEditText.getText().toString();

        if (contents.length() == 0) {

            cardNumberEditText.requestFocus();

            cardNumberEditText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(cardNumberEditText, 0);
                }
            }, 100);

            return;
        }

        /* There was text in the card number field, check the pin number field. */
        final EditText pinOrPasswordEditText = (EditText) addAccountDialog
                .findViewById(R.id.pin_or_password);
        contents = pinOrPasswordEditText.getText().toString();
        if (contents.length() == 0) {

            pinOrPasswordEditText.requestFocus();

            pinOrPasswordEditText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(pinOrPasswordEditText, 0);
                }
            }, 100);
        }
    }

    public LibOrganisation getSelectedOrganisation() {

        return mSelectedOrganisation;
    }

    public String[] getOrgNamesArray() {

        if (mOrgList != null) {

            /* Check whether the names array has already been loaded, if not, load it. */
            if (mOrgNames == null) {

                /* Load organisation names into a string array so they can be used in the setorganisation listview */
                mOrgNames = new String[mOrgList.size()];

                int i = 0;
                for (LibOrganisation libOrganisation : mOrgList) {

                    mOrgNames[i++] = libOrganisation.getName();
                }
            }
        }

        /* mOrgnames will still be null if the orglist hasn't finished loading,
        this is checked for by calling code. In any case this method isn't called
        until orgs have finished loading. */
        return mOrgNames;
    }

    public int getRegion() {

        return mRegion;
    }

    private void fillInSuggestedName() {

        /* Enter a suggested account name into the account name field. It will be the
        short name of the selected organisation plus a number to make it unique. The
        code checks against existing names. */
        String suggestedNameStub = mSelectedOrganisation.getShortName();
        String suggestedName;
        Integer i = 0;

        List<String> accountNamesList = DbFragmentRef.get().getAccountNamesList();
        /* Add a number to the end of the short name until we find a unique name. */
        do {
            i++;
            suggestedName = suggestedNameStub + " " + i.toString();
        } while (accountNamesList.contains(suggestedName));

        /* Update the text in the add account dialog, and move focus appropriately. */
        Dialog addAccountDialog = getDialog();
        EditText accountNameEditText = (EditText) addAccountDialog
                .findViewById(R.id.account_name_edittext);
        accountNameEditText.setText(suggestedName);

        focusOnFirstEmptyField(true);
    }

    private void setOrgList(List<LibOrganisation> orgList) {

        mOrgList = orgList;
    }

    private void setOrgsLoaded(boolean orgsLoaded) {

        mOrgsLoaded = orgsLoaded;
    }
}
