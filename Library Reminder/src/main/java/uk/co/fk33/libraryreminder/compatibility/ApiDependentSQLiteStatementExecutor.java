/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.database.sqlite.SQLiteStatement;

public abstract class ApiDependentSQLiteStatementExecutor {

    public abstract int executeSQLiteStatement(SQLiteStatement statement);

}
