/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class EditAccountOnShowListener implements DialogInterface.OnShowListener {

    private final EditAccountDialogFragment mEditAccountDialogFragment;
    private final AlertDialog mEditAccountDialog;
    private final Context mContext;
    private LibOrganisation mLibOrganisation;

    public EditAccountOnShowListener(Context context,
                                     AlertDialog editAccountDialog,
                                     EditAccountDialogFragment editAccountDialogFragment) {

        mContext = context;
        mEditAccountDialog = editAccountDialog;
        mEditAccountDialogFragment = editAccountDialogFragment;
    }

    private class EditAccountOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            /* Query the selected org now as it might have changed since the listener was attached to the dialog! */
            mLibOrganisation = mEditAccountDialogFragment.getSelectedOrganisation();

            if (mLibOrganisation != null) {

                if (formIsFilledIn(mEditAccountDialog)) {

                    /* Make sure that some information has been changed, or there is no
                    point in continuing. */

                    boolean nameChanged = false;
                    boolean cardNumberChanged = false;
                    boolean organisationChanged = false;
                    boolean pinOrPasswordChanged = false;
                    boolean remindSettingChanged = false;

                    String existingAccountName = mEditAccountDialogFragment.getExistingAccountDetail(0);
                    String existingOrganisation = mEditAccountDialogFragment.getExistingAccountDetail(1);
                    String existingCardNumber = mEditAccountDialogFragment.getExistingAccountDetail(2);
                    String existingPinOrPassword = mEditAccountDialogFragment.getExistingAccountDetail(3);
                    String existingRemindSettingString = mEditAccountDialogFragment
                            .getExistingAccountDetail(4);
                    boolean existingRemindSetting = Boolean.valueOf(existingRemindSettingString);

                    /* Get the information entered into the form */
                    CompoundButton remindSwitch = (CompoundButton) mEditAccountDialog
                            .findViewById(R.id.edit_account_reminder_switch);
                    boolean remindSettingInForm = remindSwitch.isChecked();

                    EditText editAccountEditText = (EditText) mEditAccountDialog
                            .findViewById(R.id.edit_account_account_name_edittext);
                    String accountNameInForm = editAccountEditText.getText().toString();

                    editAccountEditText = (EditText) mEditAccountDialog
                            .findViewById(R.id.edit_account_card_number);
                    String cardNumberInForm = editAccountEditText.getText().toString();

                    /* Some Spydus catalogues add a prefix to the card number when logging in,
                    * so we have to add the prefix too. Adding it here means it will be saved
                    * into the db.*/
                    String orgName = mLibOrganisation.getName();
                    if (orgName.equals("Bolton") || orgName.equals("Calderdale") || orgName.equals("Salford")) {

                        cardNumberInForm = addPrefix(orgName, cardNumberInForm);
                    }

                    editAccountEditText = (EditText) mEditAccountDialog
                            .findViewById(R.id.edit_account_pin_or_password);
                    String pinOrPasswordInForm = editAccountEditText.getText().toString();

                    /* Check the new details against the existing ones. */
                    if (!orgName.equals(existingOrganisation)) {

                        organisationChanged = true;
                    }

                    if (!accountNameInForm.equals(existingAccountName)) {

                        nameChanged = true;
                    }

                    if (!cardNumberInForm.equals(existingCardNumber)) {

                        cardNumberChanged = true;
                    }

                    if (!pinOrPasswordInForm.equals(existingPinOrPassword)) {

                        pinOrPasswordChanged = true;
                    }

                    if (remindSettingInForm != existingRemindSetting) {

                        remindSettingChanged = true;
                    }

                    if (nameChanged || cardNumberChanged || organisationChanged || pinOrPasswordChanged
                            || remindSettingChanged) {

                        /* Apply the changes to the db, if they validate. */
                        validateAndCommitChanges(nameChanged, cardNumberChanged, organisationChanged,
                                pinOrPasswordChanged, remindSettingChanged, existingAccountName,
                                accountNameInForm, existingCardNumber, cardNumberInForm,
                                pinOrPasswordInForm, remindSettingInForm);
                    } else {

                        /* Nothing has been changed in the form, so take no action, just close
                        the dialog. */
                        DbFragmentRef.get().setAccountBeingEdited(false);
                        mEditAccountDialog.dismiss();
                    }
                } else {

                    Toast.makeText(mContext, R.string.form_not_completed, Toast.LENGTH_SHORT).show();
                }
            } else {

                if (mEditAccountDialogFragment.orgsLoaded()) {

                    Toast.makeText(mContext, R.string.form_not_completed, Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(mContext, R.string.cant_save_try_again, Toast.LENGTH_SHORT).show();
                }
            }
        }

        private boolean formIsFilledIn(DialogInterface editAccountDialogInterface) {

            /* Make sure the account name and card number are filled in. Pin/password
             can be left empty, as some library catalogues do not require one.*/
            Dialog editAccountDialog = (Dialog) editAccountDialogInterface;

            EditText editAccountEditText = (EditText) editAccountDialog
                    .findViewById(R.id.edit_account_account_name_edittext);

            if (editAccountEditText.getText().toString().equals("")) {

                return false;
            }

            editAccountEditText = (EditText) editAccountDialog
                    .findViewById(R.id.edit_account_card_number);
            return !editAccountEditText.getText().toString().equals("");

        }

        private void validateAndCommitChanges(boolean nameChanged, boolean cardNumberChanged,
                                              boolean organisationChanged, boolean pinOrPasswordChanged,
                                              boolean remindSettingChanged, String existingAccountName,
                                              String accountNameInForm, String existingCardNumber,
                                              String cardNumberInForm, String pinOrPasswordInForm,
                                              boolean remindSettingInForm) {

            DbInteractionsFragment dbFrag = DbFragmentRef.get();

            boolean newNameNotInUse = true;
            boolean newCardNumberNotInUse = true;

            /* If the account name or card number has been changed, make sure the new details
            aren't already used by another account, to avoid duplication of names/card numbers. */
            if (nameChanged) {

                /* As the account name's been changed we should check the new name which has
                been supplied doesn't already exist. */
                newNameNotInUse = nameNotInUse(accountNameInForm);
            }

            /* Check if the card number has changed. */
            if (cardNumberChanged) {

                /* As the card number's been changed we should check that the new number which has
                been supplied doesn't already exist. */
                newCardNumberNotInUse = cardNumberNotInUse(cardNumberInForm);
            }

            if (newNameNotInUse) {

                if (newCardNumberNotInUse) {

                    /* If the account name or card number have been changed,
                    update the db frag's in-memory list of account names and card numbers,
                    which is used when checking whether a new name or card number
                    already exists when adding or editing accounts. */
                    if (cardNumberChanged) {

                        /* Replace the existing card number in the db frag's list of
                        card numbers. */
                        List<String> accountCardNumbersList =
                                dbFrag.getAccountCardNumbersList();
                        accountCardNumbersList.remove(existingCardNumber);
                        accountCardNumbersList.add(cardNumberInForm);
                        dbFrag.setAccountCardNumbersList(accountCardNumbersList);
                    }

                    if (nameChanged) {

                        /* Replace the existing account name in the db frag's list of
                        account names. */
                        List<String> accountNamesList =
                                dbFrag.getAccountNamesList();
                        accountNamesList.remove(existingAccountName);
                        accountNamesList.add(accountNameInForm);
                        dbFrag.setAccountNamesList(accountNamesList);

                        /* Clear any notifications, as they may contain references to old account names. */
                        if (dbFrag.getNotificationCount() > 0) {

                            dbFrag.clearNotifiedAccounts();
                            NotificationManager notificationManager = (NotificationManager)
                                    mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.cancel(LocaleDependentClass.ACTION_NEEDED_NOTIFICATION_ID);
                        }
                    }

                    if (nameChanged) {

                        if (!cardNumberChanged && !organisationChanged && !pinOrPasswordChanged
                                && !remindSettingChanged) {

                            /* Only the name of the account has been changed, nothing else.
                            Update the name of the account in the accounts table, and if there
                            are any items on the account, put the new account name on each
                            item record, so it will show up when viewing the renamed account. */
                            dbFrag.renameAccount(existingAccountName, accountNameInForm);
                        } else {

                            if (!cardNumberChanged && !organisationChanged && !pinOrPasswordChanged) {

                                /* Only the name and remind setting have changed.*/
                                dbFrag.renameAccountAndChangeRemindSetting(existingAccountName,
                                        accountNameInForm, remindSettingInForm);
                            } else {

                                /* The name has been changed and one or more of the organisation, the card
                                number, and the pin. Change the name and overwrite the org, card no, pin
                                and reminders setting in the db with the values from the form. (Calling
                                this method will cause the account to be synced after the update). */
                                dbFrag.editAndRenameAccount(existingAccountName,
                                        accountNameInForm, mLibOrganisation, cardNumberInForm,
                                        pinOrPasswordInForm, remindSettingInForm);
                            }
                        }
                    } else {

                        /* The name hasn't been changed, so one or more of the organisation, card
                        number, pin and remind setting must have been (this method wouldn't have
                        been called if nothing had been changed). Overwrite everything except the name
                        in the db with the values from the form. However, if only the remind
                        setting has changed, call a different method (one which won't call an account
                        sync upon completion, since we don't need to resync if only the remind
                        setting has been changed). */
                        if (!cardNumberChanged && !organisationChanged && !pinOrPasswordChanged) {

                            /* Only the remind setting has been changed.*/
                            dbFrag.changeRemindSettingForAccount(existingAccountName,
                                    remindSettingInForm);

                            /* Store the new remind setting in case the user subsequently
                            * opens an item info dialog. If reminders are off, we shouldn't
                            * show the item remind checkbox - the following setting will be
                            * checked to determine this if an item info dialog is opened.
                            * This only has to be done because the view account frag isn't
                            * closed after this update (it is for other updates), so the
                            * db frag won't get an up-to-date reading of the setting from
                            * the db (which happens when the account is first opened).
                            * Also take note that the remind setting of the account has been
                            * changed since the account was opened. This prevents a bug where,
                            * if the user changed the remind setting, then changed orientation,
                            * the viewaccountactivity would re-read the account's remind
                            * setting from the intent it was originaly started with, and would
                            * overwrite the remind setting we're not putting in the dbfrag with
                            * the original one. */
                            dbFrag.storeRemindSettingOfAccountBeingViewed(remindSettingInForm);
                            dbFrag.setAccountRemindSettingChangedSinceAccountOpened(true);

                        } else {

                            /* Get this because, although we're not changing it, we're using the
                            * same AsyncDbWrite constructor as used when adding an account, and
                            * that takes region as an argument, so it's easiest just to pass it.*/
                            int region = mEditAccountDialogFragment.getRegion();

                            /* At least one of card number, pin and account no have been changed.*/
                            dbFrag.editAccount(existingAccountName, mLibOrganisation, cardNumberInForm,
                                    pinOrPasswordInForm, remindSettingInForm, region);
                        }
                    }

                    DbFragmentRef.get().setAccountBeingEdited(false);
                    mEditAccountDialog.dismiss();

                } else {

                    Toast.makeText(mContext, R.string.already_account_with_card_number,
                            Toast.LENGTH_SHORT).show();
                }

            } else {

                Toast.makeText(mContext, R.string.already_account_with_name,
                        Toast.LENGTH_SHORT).show();
            }
        }

        private boolean nameNotInUse(String accountName) {

            List<String> accountNamesList = DbFragmentRef.get().getAccountNamesList();
            return !accountNamesList.contains(accountName);
        }

        private boolean cardNumberNotInUse(String cardNumber) {

            List<String> accountCardNumbersList = DbFragmentRef.get().getAccountCardNumbersList();
            return !accountCardNumbersList.contains(cardNumber);
        }

        private String addPrefix(String orgName, String cardNumber) {

            if (orgName.equals("Bolton")) {

                if (!cardNumber.startsWith("16P")) {

                    cardNumber = "16P" + cardNumber;
                }
            }

            if (orgName.equals("Calderdale")) {

                if (!cardNumber.startsWith("R")) {

                    cardNumber = "R" + cardNumber;
                }
            }

            if (orgName.equals("Salford")) {

                if (cardNumber.length() == 8) {

                    cardNumber = "00" + cardNumber;
                }
            }

            return cardNumber;
        }
    }

    @Override
    public void onShow(DialogInterface dialogInterface) {
        Button okButton = mEditAccountDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        okButton.setOnClickListener(new EditAccountOnClickListener());
    }

}
