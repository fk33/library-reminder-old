package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import uk.co.fk33.libraryreminder.R;

import java.io.InputStream;
import java.security.KeyStore;

public class LibRemindHttpClient extends DefaultHttpClient {

    final Context mContext;

    public LibRemindHttpClient(Context context) {

        mContext = context;
    }

    @Override
    protected ClientConnectionManager createClientConnectionManager() {

        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        registry.register(new Scheme("https", newSSLSocketFactory(), 443));
        return new SingleClientConnManager(getParams(), registry);
    }

    private SSLSocketFactory newSSLSocketFactory() {
        try {

            KeyStore keyStore = KeyStore.getInstance("BKS");
            InputStream inputStream = mContext.getResources().openRawResource(R.raw.cust_keystore);

            try {

                keyStore.load(inputStream, "alligator653".toCharArray());
            } finally {

                inputStream.close();
            }

            SSLSocketFactory sslSocketFactory = new SSLSocketFactory(keyStore);
            sslSocketFactory.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
            return sslSocketFactory;

        } catch (Exception exception) {

            throw new AssertionError(exception);
        }
    }
}
