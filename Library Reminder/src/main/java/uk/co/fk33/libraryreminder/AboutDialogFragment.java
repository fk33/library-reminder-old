/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutDialogFragment extends CloseableDialogFragment {

    private AboutDialogParent mAboutDialogParent;
    private View mScrollView;
    private boolean mCancelled = false;

    public interface AboutDialogParent {

        public void showLicenseFragment(int licenseType);
    }

    @Override
    public void onResume() {
        super.onResume();

        /* When orientation is changed, to avoid state loss, we have to reconstruct this fragment
        * ourselves, rather than allow the system to recreate it. So we have to manually store
        * and restore the scroll position. */
        final int aboutFragScrollPos = DbFragmentRef.get().getAboutFragScrollPos();

        if (aboutFragScrollPos != 0) {

            mScrollView.post(new Runnable() {

                @Override
                public void run() {

                    mScrollView.scrollTo(0, aboutFragScrollPos);
                }
            });
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String appName = getString(R.string.app_name);
        String versionName;

        try {
            versionName = getActivity().getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException nameNotFoundException) {
            versionName = "";
        }

        final Dialog aboutDialog = new Dialog(getActivity());
        aboutDialog.setContentView(R.layout.about);
        aboutDialog.setTitle(appName + " " + versionName);

        TextView aboutTextView = (TextView) aboutDialog.findViewById(R.id.about_text_view);

        String aboutText = getString(R.string.about);

        SpannableStringBuilder aboutTextSpannableSb = new SpannableStringBuilder(aboutText);


        /* PRIVACY POLICY */

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.PRIVACY_POLICY);
            }
        };

        int spanStart = aboutText.indexOf(getString(R.string.view_privacy_policy));
        int spanEnd = spanStart + 19;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* TERMS_OF_USE OF USE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.TERMS_OF_USE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_terms_of_use));
        spanEnd = spanStart + 17;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* JSOUP LICENSE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.JSOUP_LICENSE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_jsoup_licence));
        spanEnd = spanStart + 18;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* JODA-TIME LICENSE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.JODA_TIME_LICENSE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_joda_time_license));
        spanEnd = spanStart + 22;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* PRETTYTIME LICENSE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.PRETTYTIME_LICENSE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_prettytime_license));
        spanEnd = spanStart + 23;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* JASYPT LICENSE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.JASYPT_LICENSE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_jasypt_license));
        spanEnd = spanStart + 19;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* ACTION BAR SHERLOCK LICENSE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.ACTION_BAR_SHERLOCK_LICENSE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_action_bar_sherlock_license));
        spanEnd = spanStart + 32;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);


        /* APACHE LICENSE */

        clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                mAboutDialogParent.showLicenseFragment(MainActivity.APACHE_LICENSE);
            }
        };

        spanStart = aboutText.indexOf(getString(R.string.view_apache_license));
        spanEnd = spanStart + 19;

        aboutTextSpannableSb.setSpan(clickableSpan, spanStart, spanEnd, 0);

        /* END OF LICENSES */

        aboutTextView.setText(aboutTextSpannableSb, TextView.BufferType.SPANNABLE);

        Linkify.addLinks(aboutTextView, Linkify.WEB_URLS + Linkify.EMAIL_ADDRESSES);

        Button closeButton = (Button) aboutDialog.findViewById(R.id.about_dialog_close_button);

        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                aboutDialog.cancel();
            }
        });

        mScrollView = aboutDialog.findViewById(R.id.about_scroll_view);

        return aboutDialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof AboutDialogParent) {
            mAboutDialogParent = (AboutDialogParent) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement AboutDialogParent.");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        /* This method's called after onCancel(). If we have been cancelled, erase the stored
        * scroll position in the dbfrag which we use to resurrect the frag after orientation
        * change (which we do ourselves, removing and re-adding the frag manually to avoid
        * illegalstateexceptions caused when Android recreates the frag and then the frag tries
        * to call fragment transactions). */
        if (!mCancelled) {

            DbFragmentRef.get().storeAboutFragScrollPos(mScrollView.getScrollY());
        } else {

            DbFragmentRef.get().storeAboutFragScrollPos(0);
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        mCancelled = true;
    }
}