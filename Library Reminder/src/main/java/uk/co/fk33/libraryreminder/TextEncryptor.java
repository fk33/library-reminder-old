/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import org.jasypt.util.text.BasicTextEncryptor;

public class TextEncryptor {

    public static String encrypt(String stringToScramble) {

        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("changeme");
        return textEncryptor.encrypt(stringToScramble);
    }

    public static String decrypt(String stringToDescramble) {

        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("changeme");
        return textEncryptor.decrypt(stringToDescramble);

    }
}
