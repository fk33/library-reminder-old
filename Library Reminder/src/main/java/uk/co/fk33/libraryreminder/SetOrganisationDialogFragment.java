/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentCodeFactory;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentPrefWriter;

public class SetOrganisationDialogFragment extends CloseableDialogFragment {

    private SetOrganisationDialogFragmentParent mParent;
    private boolean mClosed = false;
    private int mRegion;

    public interface SetOrganisationDialogFragmentParent {

        public String[] getOrgNamesArray();
        public void onSetOrganisationListViewClick(SetOrganisationDialogFragment setOrgnisationDialogFragment,
                                                   int selectedItem);
    }

    public SetOrganisationDialogFragment() { }

    public void setParent(SetOrganisationDialogFragmentParent parent, int region) {

        mParent = parent;

        /* We only need to know the region because some regions involve organisations with longs names,
        * so we make the listview rows a bit wider. */
        mRegion = region;
    }

    @Override
    public void onResume() {
        super.onResume();

        /* When orientation is changed, to avoid state loss, we have to reconstruct this fragment
        * ourselves, rather than allow the system to recreate it. So we have to manually store
        * and restore the scroll position. */
        final int setOrgFirstVisiblePos = DbFragmentRef.get().getSetOrgFirstVisiblePos();
        if (setOrgFirstVisiblePos != 0) {

            final ListView listView = (ListView) getDialog().findViewById(R.id.set_organisation_listview);
            listView.post(new Runnable() {

                @Override
                public void run() {

                    listView.setSelection(setOrgFirstVisiblePos);
                }
            });
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        /* This bit of code creates the UI of the dialog for choosing the organisation for a new
        account. It is a dialog which pops up when you click the organisation button.
        It uses and AlertDialog.Builder, with a custom layout (R.layout.set_organisation_dialog). */
        FragmentActivity mainActivity = getActivity();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);

        /* If the orientation changes and the system recreates the fragment, we won't have a
        reference to the parent fragment, so just get rid of this fragment. The add account dialog
        fragment or edit account dialog fragment checks when it is created if a set organisation
        dialog was open and if so, makes a new one to replace the one destroyed here. */
        if (mParent == null) {

            FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(this).commit();
            return dialogBuilder.create();
        }

        LayoutInflater layoutInflater = mainActivity.getLayoutInflater();

        View setOrganisationDialogView =
                layoutInflater.inflate(R.layout.set_organisation_dialog, null);

        ListView setOrganisationListView = (ListView) setOrganisationDialogView
                .findViewById(R.id.set_organisation_listview);

        /* Create the adapter to use with the listview (it is a custom list adapter called
        setOrganisationListAdapter. First get the string array with the names of the organisations
        we'll be showing. */
        String[] orgNames = mParent.getOrgNamesArray();

        /* Check this isn't null. There's no reason it should be at this point as this fragment won't
        be shown until the organisation names have been loaded from xml. But nevertheless the method
        which provides the array can return null so we might as well check. If it is null the list of
        organisations will just be empty as the list adapter won't be set. */
        if (orgNames != null) {

            SetOrganisationListAdapter setOrganisationListAdapter;

            if (mRegion != MainActivity.REGION_UK_HEALTH) {

                setOrganisationListAdapter = new SetOrganisationListAdapter(mainActivity,
                        R.layout.set_organisation_listview_row, orgNames);
            } else {

                setOrganisationListAdapter = new SetOrganisationListAdapter(mainActivity,
                        R.layout.set_organisation_listview_row_big, orgNames);
            }

            setOrganisationListView.setAdapter(setOrganisationListAdapter);
        }

        setOrganisationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parentView, View childView, int selectedItem, long id) {

                mClosed = true;
                DbFragmentRef.get().setSetOrganisationFragmentOpen(false);

                /* Call the parent fragment, send the dialog and the organisation object */
                mParent.onSetOrganisationListViewClick(SetOrganisationDialogFragment.this, selectedItem);
            }
        });

        CheckBox suggestNameCheckBox = (CheckBox) setOrganisationDialogView
                .findViewById(R.id.checkbox_suggest_account_name);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean suggestName = prefs.getBoolean(MainActivity.SUGGEST_NAME, true);
        if (suggestName) {
            suggestNameCheckBox = (CheckBox) setOrganisationDialogView
                    .findViewById(R.id.checkbox_suggest_account_name);
            suggestNameCheckBox.setChecked(true);
        }

        suggestNameCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                ApiDependentPrefWriter apiDependentPrefWriter = ApiDependentCodeFactory.getApiDependentPrefWriter();
                apiDependentPrefWriter.writeBooleanPref(prefs, MainActivity.SUGGEST_NAME, isChecked);
            }
        });

        dialogBuilder.setTitle(getString(R.string.set_organisation))
                .setView(setOrganisationDialogView);

        return dialogBuilder.create();
    }

    @Override
    public void onPause() {
        super.onPause();

        /* This method's called after onCancel(). If we have been cancelled, erase the stored
        * scroll position in the dbfrag which we use to recreate the frag after orientation
        * change. */
         if (!mClosed) {

            AdapterView listView = (AdapterView) getDialog().findViewById(R.id.set_organisation_listview);
            DbFragmentRef.get().storeSetOrgFirstVisiblePos(listView.getFirstVisiblePosition());
        } else {

            DbFragmentRef.get().storeSetOrgFirstVisiblePos(0);
        }
    }

    @Override
    public void onCancel(DialogInterface setOrganisationDialogInterface) {

        mClosed = true;
        DbFragmentRef.get().setSetOrganisationFragmentOpen(false);

        /* If the user hasn't yet set an organisation, it means they are aborting the
        * attempt to add an account, rather than changing the organisation setting to
        * a different organisation. So remove the add account dialog fragment. This
        * only applies when opened from an add account dialog, not an edit account
        * dialog.*/
        if (mParent instanceof AddAccountDialogFragment) {

            AddAccountDialogFragment addAccountDialogFragment = (AddAccountDialogFragment) mParent;
            if (addAccountDialogFragment.getSelectedOrganisation() == null) {

                addAccountDialogFragment.dismiss();
            }
        }
     }



}
