/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SetOrganisationListAdapter extends ArrayAdapter<String> {

    private String[] mOrganisations;
    private int mTextViewResourceId;
    private LayoutInflater mLayoutInflater;

    public SetOrganisationListAdapter(Context context, int textViewResourceId, String[] organisations) {
        super(context, textViewResourceId, organisations);

        mOrganisations = organisations;
        mTextViewResourceId = textViewResourceId;
        mLayoutInflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public View getView(int position, View recycledView, ViewGroup parent) {

        if (recycledView == null) {

            recycledView = mLayoutInflater.inflate(mTextViewResourceId, parent, false);
        }

        ((TextView) recycledView).setText(mOrganisations[position]);

        return recycledView;
    }
}
