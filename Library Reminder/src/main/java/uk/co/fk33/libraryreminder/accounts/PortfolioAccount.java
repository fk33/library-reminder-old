/*
 * Copyright (c) 2015 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;
import uk.co.fk33.libraryreminder.network.PortfolioUpdateTask;

public class PortfolioAccount extends LibAccount {

    public PortfolioAccount(Context context, boolean silentMode, long rowId, String name, String catUrl,
                        String cardNumber, String pinOrPassword, boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.PORTFOLIO;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        PortfolioUpdateTask portfolioUpdateTask = new PortfolioUpdateTask(mContext);
        portfolioUpdateTask.execute(this);
    }
}
