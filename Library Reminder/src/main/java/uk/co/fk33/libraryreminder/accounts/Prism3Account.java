/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;

import uk.co.fk33.libraryreminder.network.Prism3UpdateTask;

public class Prism3Account extends LibAccount {

    public Prism3Account(Context context, boolean silentMode, long rowId, String name, String catUrl, String cardNumber, String pinOrPassword,
                         boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.PRISM3;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        Prism3UpdateTask prism3UpdateTask = new Prism3UpdateTask(mContext);
        prism3UpdateTask.execute(this);
    }
}
