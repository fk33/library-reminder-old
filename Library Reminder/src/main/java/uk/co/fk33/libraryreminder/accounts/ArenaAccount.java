/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;
import uk.co.fk33.libraryreminder.network.ArenaUpdateTask;

public class ArenaAccount extends LibAccount {

    public ArenaAccount(Context context, boolean silentMode, long rowId, String name, String catUrl,
                        String cardNumber, String pinOrPassword, boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.ARENA;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        ArenaUpdateTask arenaUpdateTask = new ArenaUpdateTask(mContext);
        arenaUpdateTask.execute(this);
    }
}
