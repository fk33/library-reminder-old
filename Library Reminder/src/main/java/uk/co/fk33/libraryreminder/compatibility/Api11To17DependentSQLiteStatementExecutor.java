/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.annotation.TargetApi;
import android.database.SQLException;
import android.database.sqlite.SQLiteStatement;

@TargetApi(11)
public class Api11To17DependentSQLiteStatementExecutor extends ApiDependentSQLiteStatementExecutor {

    @Override
    public int executeSQLiteStatement(SQLiteStatement statement) {

        try {
            return statement.executeUpdateDelete();
        } catch (SQLException sqlException) {
            return -1;
        }
    }
}
