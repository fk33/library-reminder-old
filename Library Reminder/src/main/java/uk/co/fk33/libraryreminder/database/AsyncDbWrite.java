/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.database.Cursor;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import uk.co.fk33.libraryreminder.DbFragmentRef;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.LibOrganisation;
import uk.co.fk33.libraryreminder.R;

import java.util.List;

public class AsyncDbWrite extends AsyncTask<Void, Void, Boolean> {

    private LibRemindDbAdapter mDatabaseAdapter;
    private CallingService mCallingService;
    private DataToWrite mDataToWrite;
    private String mName;
    private String mNewName;
    private LibOrganisation mLibOrganisation;
    private String mCardNumber;
    private String mPinOrPassword;
    private String mUniqueIdentifier;
    private boolean mRemind;
    private String[] mAccountsToDelete;
    private List<LibItem> mItemList;
    private int mRegion;
    private final int mFlag;

    public interface CallingService {

        public void checkDueDates(List<LibItem> itemList, String accountName);
        public void decrementNumberOfAccountsBeingSynced();
    }

    /* Constructor to be used to update records, e.g. to update the last_synced date of an account */
    public AsyncDbWrite(DataToWrite dataToWrite, int flag) {

        mDataToWrite = dataToWrite;
        mFlag = flag;
    }

    /* Constructor used to silently update an account after a background sync. */
    public AsyncDbWrite(DataToWrite dataToWrite, LibRemindDbAdapter dbAdapter, int flag) {

        mDataToWrite = dataToWrite;
        mDatabaseAdapter = dbAdapter;
        mFlag = flag;
    }

    /* Constructor used to silently update an account after a background sync, when the sync failed. */
    public AsyncDbWrite(DataToWrite dataToWrite, LibRemindDbAdapter dbAdapter,
                        CallingService callingService, int flag) {

        mDataToWrite = dataToWrite;
        mDatabaseAdapter = dbAdapter;
        mCallingService = callingService;
        mFlag = flag;
    }


    /* Constructor to use when just updating the card number or PIN of an account. */
    public AsyncDbWrite(String name, String cardNumber, String pinOrPassword, int flag) {

        mName = name;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mFlag = flag;
    }

    /* Constructor for a new account row insertion, or to edit details of an account. */
    public AsyncDbWrite(String name, LibOrganisation libOrganisation, String cardNumber,
                        String pinOrPassword, boolean remind, int region, int flag) {

        mName = name;
        mLibOrganisation = libOrganisation;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
        mRegion = region;
        mFlag = flag;
    }

    /* Constructor to edit details of an account when name is being changed. */
    public AsyncDbWrite(String name, String newName, LibOrganisation libOrganisation,
                        String cardNumber, String pinOrPassword, boolean remind, int flag) {

        mName = name;
        mNewName = newName;
        mLibOrganisation = libOrganisation;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
        mFlag = flag;
    }

    /* Constructor to change the name of the account in the accounts table, and for items belonging
    to that account in the items table. (Each item records the name of the account it belongs to). */
    public AsyncDbWrite(String name, String newName, int flag) {

        mName = name;
        mNewName = newName;
        mFlag = flag;
    }

    /* Constructor to change the remind setting of an account or item.*/
    public AsyncDbWrite(String stringA, boolean remind, int flag) {

        switch (flag) {

            case DbInteractionsFragment.DB_WRITE_CHANGE_REMIND_FOR_ACCOUNT: {

                mName = stringA;
                mRemind = remind;
                mFlag = flag;
                break;
            }

            case DbInteractionsFragment.DB_WRITE_CHANGE_REMIND_FOR_ITEM: {

                mUniqueIdentifier = stringA;
                mRemind = remind;
                mFlag = flag;
                break;
            }

            default: {

                mFlag = 0;
                break;
            }

        }
    }

    /* Constructor to change the name of the account in the accounts table, and for items belonging
    to that account if there are any, and also change the remind setting for the account at the
    same time. */
    public AsyncDbWrite(String stringA, String stringB, boolean remind, int flag) {

        switch (flag) {

            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_CHANGE_REMIND:
            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS_CHANGE_REMIND: {

                /* The parameters will be used to rename an account. */
                mName = stringA;
                mNewName = stringB;
                mRemind = remind;
                mFlag = flag;
                break;
            }

            default: {

                mFlag = 0;
                break;
            }
        }
    }


    /* Constructor to delete accounts */
    public AsyncDbWrite(String[] accountsToDelete, int flag) {

        mAccountsToDelete = accountsToDelete;
        mFlag = flag;
    }

    /* Constructor to add multiple items to items table */
    public AsyncDbWrite(String name, List<LibItem> itemList, boolean remind, int flag) {

        mName = name;
        mItemList = itemList;
        mRemind = remind;
        mFlag = flag;
    }

    /* Constructor to add multiple items to items table silently. */
    public AsyncDbWrite(String name, List<LibItem> itemList, boolean remind, LibRemindDbAdapter dbAdapter,
                        CallingService callingService, int flag) {

        mName = name;
        mItemList = itemList;
        mRemind = remind;
        mDatabaseAdapter = dbAdapter;
        mCallingService = callingService;
        mFlag = flag;
    }

    /* Constructor to delete all items for an account */
    public AsyncDbWrite(String name, int flag) {

        mName = name;
        mFlag = flag;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        if (mDatabaseAdapter == null) {

            mDatabaseAdapter = DbFragmentRef.get().getDatabaseAdapter();
        }

        switch (mFlag) {

            case DbInteractionsFragment.DB_WRITE_ADD_ACCOUNT: {

                return mDatabaseAdapter.addAccountRecord(mName, mLibOrganisation, mCardNumber,
                        mPinOrPassword, mRemind, mRegion) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_EDIT_AND_RENAME_ACCOUNT: {

                return mDatabaseAdapter.editAccount(mName, mNewName, mLibOrganisation, mCardNumber,
                        mPinOrPassword, mRemind) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_EDIT_AND_RENAME_ACCOUNT_WITH_ITEMS: {

                return mDatabaseAdapter.editAccount(mName, mNewName, mLibOrganisation, mCardNumber,
                        mPinOrPassword, mRemind) > 0 && mDatabaseAdapter.changeNameOfAccountForItems(mName,
                        mNewName) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT: {

                return mDatabaseAdapter.renameAccount(mName, mNewName) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS: {

                return mDatabaseAdapter.renameAccount(mName, mNewName) > 0
                        && mDatabaseAdapter.changeNameOfAccountForItems(mName, mNewName) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_CHANGE_REMIND: {

                return mDatabaseAdapter.renameAccountChangeReminders(mName, mNewName, mRemind) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS_CHANGE_REMIND: {

                return mDatabaseAdapter.renameAccountChangeReminders(mName, mNewName, mRemind) > 0
                        && mDatabaseAdapter.changeNameOfAccountForItems(mName, mNewName) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_DELETE_ACCOUNTS: {

                /* First delete all items for each account we are deleting */
                for (String accountName : mAccountsToDelete) {
                    if (mDatabaseAdapter.deleteAllItemsForAccount(accountName) < 0) return false;
                }

                /* Then delete account records */
                return mDatabaseAdapter.deleteAccounts(mAccountsToDelete) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS:
            case DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS_SILENTLY:
            case DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS_SILENTLY_ON_SYNC_FAIL: {

                return mDatabaseAdapter.updateRecords(mDataToWrite) == mDataToWrite.size();
            }

            case DbInteractionsFragment.DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT:
            case DbInteractionsFragment.DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT_SILENTLY: {

                if (mItemList.size() > 0) {

                    /* This cursor returns unique id, times renewed, and reminders columns for
                    * each item on the account.*/
                    Cursor currentLoansCursor = mDatabaseAdapter.getItemsForAccount(mName);

                    if (currentLoansCursor.getCount() > 0) {

                        /* If any items from the current loan list match items already in the db, copy
                        across that item's reminder setting (which is set locally), otherwise it would
                        be lost when we overwrite the loans in the db. */
                        for (LibItem item : mItemList) {

                            currentLoansCursor.moveToFirst();
                            while (!currentLoansCursor.isAfterLast()) {

                                String existingUniqueId = currentLoansCursor.getString(0);
                                if (item.getUniqueIdentifier().equals(existingUniqueId)) {

                                    /* New items have remind set to true by default (the sync
                                    * tasks all pass true to the constructor), so we only need
                                    * to do anything if it is set to false in the db. */
                                    if (currentLoansCursor.getInt(2) == 0) {

                                        item.setRemind(false);
                                    }

                                    break;
                                }

                                currentLoansCursor.moveToNext();
                            }

                        }
                    }
                }

                /* First delete all items for the account being updated. If this fails (due to
                SQLException), return false. Then, if the item list is empty, return true, since
                no need to perform any update. If item list contains items, write them to db,
                and return the boolean result (true if all inserts are successul). */
                return mDatabaseAdapter.deleteAllItemsForAccount(mName) >= 0 &&
                        (mItemList.isEmpty() || mDatabaseAdapter.addItems(mItemList));
            }

            case DbInteractionsFragment.DB_WRITE_DELETE_ALL_ITEMS_FOR_ACCOUNT: {

                /* We still return true if 0 items are deleted, since there may not have been any items to delete */
                return mDatabaseAdapter.deleteAllItemsForAccount(mName) >= 0;
            }

            case DbInteractionsFragment.DB_WRITE_EDIT_ACCOUNT: {

                return mDatabaseAdapter.editAccount(mName, mLibOrganisation, mCardNumber, mPinOrPassword,
                        mRemind) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_CHANGE_REMIND_FOR_ACCOUNT: {

                /* Change the reminders setting for an account. */
                return mDatabaseAdapter.changeReminderSetting(mName, mRemind) > 0;
            }

            case DbInteractionsFragment.DB_WRITE_CHANGE_REMIND_FOR_ITEM: {

                return mDatabaseAdapter.changeRemindForItem(mUniqueIdentifier, mRemind) > 0;
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        if (mFlag == DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS_SILENTLY) {

            /* We're in background mode so we don't need to show any toast or update the
            account list. */
            return;
        }

        if (mFlag == DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS_SILENTLY_ON_SYNC_FAIL) {

            /* We're in background mode so we don't need to show any toast or update the
            account list. */
            if (mCallingService != null) {

                /* The call to decrement the number of accounts being synced which is made
                when the items records have been updated in the db (just below) will not be
                made for this account, because it failed to sync, so make the call here. */
                mCallingService.decrementNumberOfAccountsBeingSynced();
            }

            return;
        }

        if (mFlag == DbInteractionsFragment.DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT_SILENTLY) {

            /* We're in background mode so don't call the DbFrag as it might not exist.
            * We must have been called by the auto sync service. */
            if (mCallingService != null) {

                mCallingService.checkDueDates(mItemList, mName);

                mCallingService.decrementNumberOfAccountsBeingSynced();
            }

            return;
        }

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        String resultMessage = "";

        switch (mFlag) {

            case DbInteractionsFragment.DB_WRITE_ADD_ACCOUNT: {

                resultMessage = dbFrag.getString(R.string.account_added);

                dbFrag.writeFinished(result, resultMessage, mFlag);
                break;
            }

            case DbInteractionsFragment.DB_WRITE_DELETE_ACCOUNTS: {

                if (mAccountsToDelete.length > 1) {

                    resultMessage = dbFrag.getString(R.string.accounts_deleted);
                } else {

                    resultMessage = dbFrag.getString(R.string.account_deleted);
                }

                dbFrag.writeFinished(result, resultMessage, mFlag);
                break;
            }

            case DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS: {

                if (!result) {

                    resultMessage = dbFrag.getString(R.string.database_problem);
                }

                dbFrag.writeFinished(result, resultMessage, mFlag);
                break;
            }

            case DbInteractionsFragment.DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT: {

                if (!result) {

                    resultMessage = dbFrag.getString(R.string.database_problem);
                } else {

                    if (mRemind) {

                        dbFrag.checkDueDates(mName, mItemList);
                    }
                }

                dbFrag.writeFinished(result, resultMessage, mFlag);
                break;
            }

            case DbInteractionsFragment.DB_WRITE_EDIT_ACCOUNT:
            case DbInteractionsFragment.DB_WRITE_EDIT_AND_RENAME_ACCOUNT:
            case DbInteractionsFragment.DB_WRITE_EDIT_AND_RENAME_ACCOUNT_WITH_ITEMS:
            case DbInteractionsFragment.DB_WRITE_CHANGE_REMIND_FOR_ACCOUNT:
            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT:
            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS:
            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_CHANGE_REMIND:
            case DbInteractionsFragment.DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS_CHANGE_REMIND: {

                if (result) {

                    resultMessage = dbFrag.getString(R.string.account_updated);
                } else {

                    resultMessage = dbFrag.getString(R.string.database_problem);
                }

                dbFrag.writeFinished(result, resultMessage, mFlag);
                break;
            }

            case DbInteractionsFragment.DB_WRITE_CHANGE_REMIND_FOR_ITEM: {

                if (!result) {

                    resultMessage = dbFrag.getString(R.string.database_problem);
                }

                dbFrag.writeFinished(result, resultMessage, mFlag);
                break;
            }
        }
    }
}
