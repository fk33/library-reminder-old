/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import android.util.Log;
import org.apache.commons.validator.routines.UrlValidator;
import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SpydusUpdateTask extends UpdateTask {

    public SpydusUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        /* Connect to the library catalogue, get the list of user's loans */
        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        String catUrl = mLibAccount.getCatUrl();

        /* If we are connecting to herts.spydus.co.uk, we have to do some things differently. Set a boolean
        * so we know if we are or not later. */
        boolean herts = false;
        if (catUrl.equals("https://herts.spydus.co.uk/")) {

            herts = true;
        }

        String pinOrPassword = mLibAccount.getPinOrPassword();

        /* The relative URLs in the catalogue HTML have slashes at the front, so remove the slash
        from the end of our catalogue URL. */
        catUrl = catUrl.substring(0, catUrl.length() - 1);
        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Locale languageLocale = LocaleDependentClass.getLanguageLocale();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        /* This should give us the login page on Spydus catalogues */
        String loginUrl = catUrl + "/cgi-bin/spydus.exe/PGM/OPAC/CCOPT/LB/2";
        String accountUrl = "";
        String cookies = "";

        try {
            publishProgress(mContext.getString(R.string.sending_login_details));

            Connection connection = Jsoup.connect(loginUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .method(Connection.Method.POST);

            if (pinOrPassword.equals("")) {

                catalogueResponse = connection.data("BRWLID", mLibAccount.getCardNumber(),
                        "ISGLB", "1").execute();
            } else {

                catalogueResponse = connection.data("BRWLID", mLibAccount.getCardNumber(),
                        "BRWLPWD", mLibAccount.getPinOrPassword(),
                        "ISGLB", "1").execute();
            }

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            String brwlidCookie = catalogueResponse.cookie("BRWLID_443");
            String brwlscnCookie = catalogueResponse.cookie("BRWLSCN_443");
            String BRWDL_443 = catalogueResponse.cookie("BRWDL_443");
            String BRWLHDG_443 = catalogueResponse.cookie("BRWLHDG_443");
            String BRWLTTL_443 = catalogueResponse.cookie("BRWLTTL_443");
            String BRWLNAM_443 = catalogueResponse.cookie("BRWLNAM_443");
            String BRWLFNAM_443 = catalogueResponse.cookie("BRWLFNAM_443");
            String BRWLSNAM_443 = catalogueResponse.cookie("BRWLSNAM_443");
            String PINMIN_443 = catalogueResponse.cookie("PINMIN_443");
            String PINMAX_443 = catalogueResponse.cookie("PINMAX_443");
            String BRWL_443 = catalogueResponse.cookie("BRWL_443");
            String BRWCATCODE_443 = catalogueResponse.cookie("BRWCATCODE_443");
            String NAME_443 = catalogueResponse.cookie("NAME_443");

            /* If the above two cookies have not been set, it either means the catalogue is asking the user if they
            accept cookies, or it means incorrect pin or card number entered. */
            if (brwlidCookie == null) {

                cataloguePage = catalogueResponse.parse();

                /* If we are being asked if we accept cookies, there will be a POST form, with an "action" attribute */
                Elements postForm = cataloguePage.getElementsByAttribute("action");
                if (postForm.isEmpty()) {

                    mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                    return null;
                } else {

                    /* Tell the catalogue we accept cookies by submitting the form. Get the POST url. */
                    loginUrl = catUrl + postForm.attr("action");

                    /* Extract some values from the page that get sent with the POST request */
                    Elements acrecirnElements = cataloguePage.getElementsByAttributeValue("name", "ACRECIRN");
                    Elements iappElements = cataloguePage.getElementsByAttributeValue("name", "IAPP");
                    Elements iapplicElements = cataloguePage.getElementsByAttributeValue("name", "IAPPLIC");
                    Elements rdtElements = cataloguePage.getElementsByAttributeValue("name", "RDT");

                    String acrecirn = acrecirnElements.attr("value");
                    String iapp = iappElements.attr("value");
                    String iapplic = iapplicElements.attr("value");
                    String rdt = rdtElements.attr("value");

                    /* Send the POST request */
                    publishProgress(mContext.getString(R.string.setting_account_accept_cookies));

                    connection = Jsoup.connect(loginUrl)
                            .timeout(NetworkConstants.TIMEOUT_MILLIS)
                            .userAgent(mContext.getString(R.string.user_agent))
                            .method(Connection.Method.POST);

                    if (pinOrPassword.equals("")) {

                        catalogueResponse = connection.data("ACALLOWCOOKIES", "1",
                                "ACCANCEL", catUrl + "/cgi-bin/spydus.exe/MSGTRN/OPAC/LOGINB",
                                "ACCONTINUE", "/cgi-bin/spydus.exe/PGM/OPAC/CCOPT/LB/2",
                                "ACRECIRN", acrecirn,
                                "ACSUBMIT", "Submit",
                                "BRWLID", mLibAccount.getCardNumber(),
                                "IAPP", iapp,
                                "IAPPLIC", iapplic,
                                "RDT", rdt)
                                .execute();
                    } else {

                        catalogueResponse = connection.data("ACALLOWCOOKIES", "1",
                                "ACCANCEL", catUrl + "/cgi-bin/spydus.exe/MSGTRN/OPAC/LOGINB",
                                "ACCONTINUE", "/cgi-bin/spydus.exe/PGM/OPAC/CCOPT/LB/2",
                                "ACRECIRN", acrecirn,
                                "ACSUBMIT", "Submit",
                                "BRWLID", mLibAccount.getCardNumber(),
                                "BRWLPWD", mLibAccount.getPinOrPassword(),
                                "IAPP", iapp,
                                "IAPPLIC", iapplic,
                                "RDT", rdt)
                                .execute();
                    }

                    if (catalogueResponse == null) {

                        mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                        return null;
                    }

                    /* Make sure we got the login cookies this time */
                    brwlidCookie = catalogueResponse.cookie("BRWLID_443");
                    brwlscnCookie = catalogueResponse.cookie("BRWLSCN_443");
                    BRWDL_443 = catalogueResponse.cookie("BRWDL_443");
                    BRWLHDG_443 = catalogueResponse.cookie("BRWLHDG_443");
                    BRWLTTL_443 = catalogueResponse.cookie("BRWLTTL_443");
                    BRWLNAM_443 = catalogueResponse.cookie("BRWLNAM_443");
                    BRWLFNAM_443 = catalogueResponse.cookie("BRWLFNAM_443");
                    BRWLSNAM_443 = catalogueResponse.cookie("BRWLSNAM_443");
                    PINMIN_443 = catalogueResponse.cookie("PINMIN_443");
                    PINMAX_443 = catalogueResponse.cookie("PINMAX_443");
                    BRWL_443 = catalogueResponse.cookie("BRWL_443");
                    BRWCATCODE_443 = catalogueResponse.cookie("BRWCATCODE_443");
                    NAME_443 = catalogueResponse.cookie("NAME_443");

                    if (brwlidCookie == null) {
                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }
                }
            }

            /* We have to put the cookies into a http connection if we are connecting to herts'
            * catalogue. Put them in a string for this purpose, to be used later. */
            if (herts) {

                cookies = "";
                if (BRWDL_443 != null) { cookies += "BRWDL_443=" + BRWDL_443 + "; "; }
                cookies += "BRWLID_443=" + brwlidCookie + "; ";
                if (BRWLHDG_443 != null) { cookies += "BRWLHDG_443=" + BRWLHDG_443 + "; "; }
                if (BRWLTTL_443 != null) { cookies += "BRWLTTL_443=" + BRWLTTL_443 + "; "; }
                if (BRWLNAM_443 != null) { cookies += "BRWLNAM_443=" + BRWLNAM_443 + "; "; }
                if (BRWLFNAM_443 != null) { cookies += "BRWLFNAM_443=" + BRWLFNAM_443 + "; "; }
                if (BRWLSNAM_443 != null) { cookies += "BRWLSNAM_443=" + BRWLSNAM_443 + "; "; }
                if (brwlscnCookie != null) { cookies += "BRWLSCN_443=" + brwlscnCookie + "; "; }
                if (PINMIN_443 != null) { cookies += "PINMIN_443=" + PINMIN_443 + "; "; }
                if (PINMAX_443 != null) { cookies += "PINMAX_443=" + PINMAX_443 + "; "; }
                if (BRWL_443 != null) { cookies += "BRWL_443=" + BRWL_443 + "; "; }
                if (BRWCATCODE_443 != null) { cookies += "BRWCATCODE_443=" + BRWCATCODE_443 + "; "; }
                if (NAME_443 != null) { cookies += "NAME_443=" + NAME_443 + "; "; }
            }

            /* On logging in the Spydus catalogue sends a page containing a meta tag with a redirect url embedded in
            it. Extract the redirect URL here, in case for some reason it is not always the same for all organisations. */
            cataloguePage = catalogueResponse.parse();
            Elements meta = cataloguePage.select("meta");

            /* We need to do some extra parsing to extract the url. The Jsoup Elements attr() method gets an
            attribute from the first matched element which has it (in the Elements list). There should only be one
            matched element in the list anyway (the response from the server should only have one meta tag). */
            String content = meta.attr("content");
            String[] contentParts = content.split(" ");
            for (String contentPart : contentParts) {
                if (contentPart.startsWith("url=")) {
                    accountUrl = contentPart.substring(4);
                    break;
                }
            }

            /* Test if we've got a valid url, otherwise try to construct one */
            UrlValidator defaultValidator = new UrlValidator();
            if (!defaultValidator.isValid(accountUrl)) {
                accountUrl = catUrl + "/cgi-bin/spydus.exe/ENQ/OPAC/BRWENQ/" + brwlscnCookie + "?QRY=%23"
                        + brwlscnCookie + "&QRYTEXT=My%20details&ISGLB=0&NRECS=999";
            }

            publishProgress(mContext.getString(R.string.getting_account_page));

            /* Jsoup.connect() doesn't work with herts.spydus.co.uk so use a different method to connect.*/
            if (herts) {
                URL accountUrlObj = new URL(accountUrl);
                HttpsURLConnection httpsConn = (HttpsURLConnection) accountUrlObj.openConnection();
                httpsConn.setConnectTimeout(NetworkConstants.TIMEOUT_MILLIS);
                httpsConn.setRequestProperty("Cookie", cookies);
                InputStream connectionStream = httpsConn.getInputStream();

                if (connectionStream == null) {

                    mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                    return null;
                }

                cataloguePage = Jsoup.parse(connectionStream, null, catUrl);

            } else {

                connection = Jsoup.connect(accountUrl)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .userAgent(mContext.getString(R.string.user_agent));

                if (BRWDL_443 != null ) { connection = connection.cookie("BRWDL_443", BRWDL_443); }
                connection = connection.cookie("BRWLID_443", brwlidCookie);
                if (BRWLHDG_443 != null ) { connection = connection.cookie("BRWLHDG_443", BRWLHDG_443); }
                if (BRWLTTL_443 != null ) { connection = connection.cookie("BRWLTTL_443", BRWLTTL_443); }
                if (BRWLNAM_443 != null ) { connection = connection.cookie("BRWLNAM_443", BRWLNAM_443); }
                if (BRWLFNAM_443 != null ) { connection = connection.cookie("BRWLFNAM_443", BRWLFNAM_443); }
                if (BRWLSNAM_443 != null ) { connection = connection.cookie("BRWLSNAM_443", BRWLSNAM_443); }
                if (brwlscnCookie != null ) { connection = connection.cookie("BRWLSCN_443", brwlscnCookie); }
                if (PINMIN_443 != null ) { connection = connection.cookie("PINMIN_443", PINMIN_443); }
                if (PINMAX_443 != null ) { connection = connection.cookie("PINMAX_443", PINMAX_443); }
                if (BRWL_443 != null ) { connection = connection.cookie("BRWL_443", BRWL_443); }
                if (BRWCATCODE_443 != null ) { connection = connection.cookie("BRWCATCODE_443", BRWCATCODE_443); }
                if (NAME_443 != null ) { connection = connection.cookie("NAME_443", NAME_443); }

                catalogueResponse = connection.execute();

                if (catalogueResponse == null) {

                    mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                    return null;
                }

                cataloguePage = catalogueResponse.parse();
            }

            /* Now get the link which takes you to the user's current loans. It's not a static link so can't be
            hardcoded, and includes variables which are not sent to us as cookies (though they are similar
            to some of the cookie variables). */
            Elements loansPageLink = cataloguePage.select("a:containsOwn(Current loans)");
            if (loansPageLink.isEmpty()) {

                /* If the link is not found, it could mean the user doesn't have any items on
                loan. */
                Elements column1 = cataloguePage.select("div.col1");
                Elements column2 = cataloguePage.select("div.col2");
                int loanCount = -1;

                /* Find the "Current loans" row and check the count in the next column. */
                for (int i = 0; i < column1.size(); i++) {
                    if (column1.get(i).text().toLowerCase(languageLocale).contains("current loans")) {
                        loanCount = Integer.parseInt(column2.get(i).text());
                    }
                }

                /* If the user does not have zero loans, then there must be another reason we
                didn't find the current loans link. We can't proceed. */
                if (loanCount != 0) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                /* Sync succeeded - user has zero loans. Return itemList, leaving it empty. */
                mStatus = LibAccount.SYNC_SUCCEEDED;
                return itemList;

            }

            /* We've got the link to the loans page, follow it. */
            String loansUrl = loansPageLink.attr("href");
            if (loansUrl.equals("")) {
                mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
                return null;
            }

            loansUrl = catUrl + loansUrl;

            publishProgress(mContext.getString(R.string.getting_loan_page));

            if (herts) {
                URL loansUrlObj = new URL(loansUrl);
                HttpsURLConnection httpsConn = (HttpsURLConnection) loansUrlObj.openConnection();
                httpsConn.setConnectTimeout(NetworkConstants.TIMEOUT_MILLIS);
                httpsConn.setRequestProperty("Cookie", cookies);
                InputStream connectionStream = httpsConn.getInputStream();

                if (connectionStream == null) {

                    mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                    return null;
                }

                cataloguePage = Jsoup.parse(connectionStream, null, catUrl);
            } else {

                connection = Jsoup.connect(loansUrl)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .userAgent(mContext.getString(R.string.user_agent));

                if (BRWDL_443 != null ) { connection = connection.cookie("BRWDL_443", BRWDL_443); }
                connection = connection.cookie("BRWLID_443", brwlidCookie);
                if (BRWLHDG_443 != null ) { connection = connection.cookie("BRWLHDG_443", BRWLHDG_443); }
                if (BRWLTTL_443 != null ) { connection = connection.cookie("BRWLTTL_443", BRWLTTL_443); }
                if (BRWLNAM_443 != null ) { connection = connection.cookie("BRWLNAM_443", BRWLNAM_443); }
                if (BRWLFNAM_443 != null ) { connection = connection.cookie("BRWLFNAM_443", BRWLFNAM_443); }
                if (BRWLSNAM_443 != null ) { connection = connection.cookie("BRWLSNAM_443", BRWLSNAM_443); }
                if (brwlscnCookie != null ) { connection = connection.cookie("BRWLSCN_443", brwlscnCookie); }
                if (PINMIN_443 != null ) { connection = connection.cookie("PINMIN_443", PINMIN_443); }
                if (PINMAX_443 != null ) { connection = connection.cookie("PINMAX_443", PINMAX_443); }
                if (BRWL_443 != null ) { connection = connection.cookie("BRWL_443", BRWL_443); }
                if (BRWCATCODE_443 != null ) { connection = connection.cookie("BRWCATCODE_443", BRWCATCODE_443); }
                if (NAME_443 != null ) { connection = connection.cookie("NAME_443", NAME_443); }

                catalogueResponse = connection.execute();

                if (catalogueResponse == null) {

                    mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                    return null;
                }

                cataloguePage = catalogueResponse.parse();
            }

            publishProgress(mContext.getString(R.string.getting_list_of_loans));

            /* Verify we are on a page showing current loans. */
            if (!cataloguePage.text().contains("Brief Display")) {

                mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
                return null;
            }

            DateTime dueDate;
            DateTimeFormatter dateTimeFormatter = LocaleDependentClass.getSpydusDateTimeFormatter();

            Element titleElement;
            Element dueDateElement;
            Element renewCountElement;
            String uniqueId;
            String renewCountText;
            Elements checkboxes = cataloguePage.select("input[type=checkbox]");

            for (Element checkbox : checkboxes) {

                titleElement = checkbox.parent().nextElementSibling().child(0);

                /* Sometimes titles aren't links, so we want the text of the element containing the
                * title, not the text of the child element (i.e. the link).*/
                if (titleElement.text().equals("")) {

                    titleElement = checkbox.parent().nextElementSibling();
                }

                dueDateElement = checkbox.parent().nextElementSibling().nextElementSibling();
                renewCountElement = dueDateElement.nextElementSibling();

                try {

                    dueDate = dateTimeFormatter.parseDateTime(dueDateElement.text());

                } catch (IllegalArgumentException illegalArgumentException) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                if (dueDate == null) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                uniqueId = checkbox.attr("value");

                long dueDateLong = dueDate.getMillis();

                /* Get the number of renewals for the item */
                renewCountText = renewCountElement.text().toLowerCase(languageLocale);
                int renewCount = 0;

                if (renewCountText.startsWith("renewed")
                        && renewCountText.endsWith("times")) {

                    String[] renewCountParts = renewCountText.split(" ");
                    renewCount = Integer.parseInt(renewCountParts[1]);
                }


                /* Put the information into item objects and put them in the item list. We don't know the standard
                identifier so put an empty string. */
                libItem = new LibItem(mLibAccount.getName(), titleElement.text(), "", uniqueId, "",
                        dueDateLong, renewCount, true);

                itemList.add(libItem);

            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;
        } catch (NullPointerException nullPointerException) {

            /* We shouldn't get an NPE unless catalogue starts to spit out different HTML
            for the loan table, which there is no reason to think it should. */
            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (IllegalFieldValueException illegalFieldValueException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
