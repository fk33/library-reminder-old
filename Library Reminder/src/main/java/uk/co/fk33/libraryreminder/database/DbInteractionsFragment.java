/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
//import android.view.ActionMode;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.ActionMode;
import org.joda.time.*;
import uk.co.fk33.libraryreminder.*;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentCodeFactory;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentPrefWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

public class DbInteractionsFragment extends Fragment {

    /* The DB_WRITE... flags are used when doing asynchronous db writes. When the AsyncDbWrite class
    finishes the db write it passes the flag back to the writeFinished() method in this class.
    The value of the flag passed back determines what subsequent actions are taken.
    Flags < 30 cause the account list to refresh.
    Flags > 30 and < 60 cause a displayed account to be closed, and the account list to refresh.
    Flags > 60 and < 100 cause a displayed account to close and the account to sync itself.
    Flags > 90 and < 100 do not trigger any actions upon completion.
    Flags > 100 cause an account to be opened if the device is in large landscape mode and the
    account was being synced by itself. */
    public final static int DB_WRITE_ADD_ACCOUNT = 1;
    public final static int DB_WRITE_UPDATE_ACCOUNTS = 2;
    public final static int DB_WRITE_DELETE_ACCOUNTS = 3;
    public final static int DB_WRITE_CHANGE_REMIND_FOR_ACCOUNT = 4;
    public final static int DB_WRITE_RENAME_ACCOUNT = 31;
    public final static int DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS = 32;
    public final static int DB_WRITE_RENAME_ACCOUNT_CHANGE_REMIND = 33;
    public final static int DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS_CHANGE_REMIND = 34;
    public final static int DB_WRITE_EDIT_ACCOUNT = 61;
    public final static int DB_WRITE_EDIT_AND_RENAME_ACCOUNT = 62;
    public final static int DB_WRITE_EDIT_AND_RENAME_ACCOUNT_WITH_ITEMS = 63;
    public final static int DB_WRITE_DELETE_ALL_ITEMS_FOR_ACCOUNT = 91;
    public final static int DB_WRITE_CHANGE_REMIND_FOR_ITEM = 93;
    public final static int DB_WRITE_UPDATE_ACCOUNTS_SILENTLY = 95;
    public final static int DB_WRITE_UPDATE_ACCOUNTS_SILENTLY_ON_SYNC_FAIL = 96;
    public final static int DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT_SILENTLY = 97;
    public final static int DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT = 101;

    public final static int DB_READ_EDIT_ACCOUNT_DETAILS = 0;
    public final static int DB_READ_GET_ITEM_DETAILS = 1;

    private LibRemindDbAdapter mDatabaseAdapter;
    private AccountListActivity mAccountListActivity;
    private EditAccountActivity mEditAccountActivity = null;
    private FragmentActivity mViewAccountFragParentActivity = null;
    private SimpleCursorAdapter mAccountListCursorAdapter;
    private SimpleCursorAdapter mItemListCursorAdapter;
    private ListView mAccountListListView = null;
    private Context mContext;
    private boolean mFirstResume = true;
    private boolean mItemsLoading = false;
    private boolean mAccountsLoading = false;
    private boolean mRestoreViewAccountActionMode = false;
    private boolean mLargeLandDevice = false;
    private boolean mNeedToRerunItemsQuery = false;
    private boolean mAccountListInActionMode = false;
    private boolean mViewAccountInActionMode = false;
    private boolean mScreenSizeDetermined = false;
    private boolean mAccountBeingEdited = false;
    private boolean mSyncAccountsQueryRunning = false;
    private boolean mSetOrganisationFragmentOpen = false;
    private boolean mRemindSettingOfAccountBeingViewed = false;
    private boolean mRemindSettingOfAccountBeingSynced = false;
    private boolean mPotentialABSBug = false;
    private boolean mAccountRemindSettingChangedSinceAccountOpened = false;
    private boolean mFirstRun = false;
    private ActionMode mActionMode;
    private SavedState mEditFragSavedState = null;
    private int mNumberOfAccountsBeingSynced = 0;
    private int mNotificationCount = 0;
    private String mAccountBeingSynced = "";
    private String mAccountBeingViewed = "";
    private String mNoItemsHintShownFor = "";
    private String mAccountToBeSynced = "";
    private String mItemBeingViewed = "";
    private String mAccountToBeOpenedFromNotification = "";
    private List<String> mAccountNamesList;
    private List<String> mAccountCardNumbersList;
    private List<String> mCheckedAccounts;
    private List<String> mSyncList;
    private List<String> mNotifiedAccounts;
    private List<Long> mCheckedItems;
    private Long[] mCheckedItemsBackup;
    private View mCheckBoxRef;
    private int mNumberOfCheckedAccounts;
    private int mNumberOfCheckedItems;
    private int mSyncAccountsLoaderId;
    private int mLastLicenseViewed = -1;
    private int mAboutFragScrollPos;
    private int mLicenseFragScrollPos;
    private int mSetOrgFragScrollPos;
    private int[] mTimePickerTime = new int[] {-1, -1};
    private int mMainActivityState = 0;
    private int mViewAccountActivityState = 0;
    private int mSettingsActivityState = 0;
    private Bundle mSyncAccountsLoaderData;

    public DbInteractionsFragment() { }

    public void initialise(Context context) {

        /* Store the application context which should remain same throughout app lifecycle, as we aren't destroying
        this fragment on configuration changes. */
        mContext = context.getApplicationContext();

	    /* Set up the database adapter */
        mDatabaseAdapter = new LibRemindDbAdapter(mContext);
        mDatabaseAdapter.open();

        mCheckedAccounts = new ArrayList<String>();
        mSyncList = new ArrayList<String>();
        mNotifiedAccounts = new ArrayList<String>();
        mCheckedItems = new ArrayList<Long>();
    }

    public Context getContext() {

        return mContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Retain this fragment across configuration changes, in case a config change happens
        during a db write. This allows ASyncTasks to retain a ref to this fragment through
        config changes. */
        setRetainInstance(true);
    }

    public interface AccountListActivity {

        public void runCursorLoader(int loaderId, Bundle argsBundle);

        public void updateAccountList();

        public void showAccount(String accountToShow, boolean remindSetting);

        public void syncAccounts(String[] accountsToSync);

        public void hideViewAccountFragment();

        public void notify(String[] notificationStrings, String accountName);
    }

    public interface EditAccountActivity {

        public void populateEditAccountFragment(String[] accountDetails);
    }

    /* When a configuration change causes the main activity to be destroyed and recreated,
    it will detach and then reattach to this fragment, which is retained across config changes. */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof AccountListActivity) {
            mAccountListActivity = (AccountListActivity) activity;

            if (mSyncAccountsQueryRunning) {

                /*
                We were reattached while a query was running, meaning the original callback
                activity for the query has been destroyed (onloadfinished was not called).
                Re-run the query.
                */
                mAccountListActivity.runCursorLoader(mSyncAccountsLoaderId, mSyncAccountsLoaderData);
            }
        } else {
            throw new ClassCastException(activity.toString() + " must implement DbInteractionsFragment.AccountListActivity.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mAccountListActivity = null;
    }

    public void closeDatabase() {

        if (mDatabaseAdapter != null) {

            mDatabaseAdapter.close();
        }
    }

    public LibRemindDbAdapter getDatabaseAdapter() {

        if (mDatabaseAdapter == null) {

            mDatabaseAdapter = new LibRemindDbAdapter(mContext);
        }

        mDatabaseAdapter.open();
        return mDatabaseAdapter;
    }

    public void initAccountListCursorAdapter() {

		/* Set up correspondence we want between the columns from the database and the list_rows in the
		accountlist listview. */
        String[] fromColumns = new String[]{LibRemindDbAdapter.KEY_NAME, LibRemindDbAdapter.KEY_SUMMARY,
                LibRemindDbAdapter.KEY_LAST_SYNCED};
        int[] toFields = new int[]{R.id.list_view_account_name, R.id.list_view_account_summary,
                R.id.list_view_account_message};

		/* Initially, the cursor we pass to the adapter is null. It's set in the onLoadFinished callback of
		the CursorManager interface. */
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(mContext, R.layout.account_list_row,
                null, fromColumns, toFields, 0);

        /* Set the custom binder to format info from db in human-readable way in list view*/
        LibRemindViewBinder viewBinder = new LibRemindViewBinder(mContext, getMargin());
        cursorAdapter.setViewBinder(viewBinder);

        /* Store a ref to this cursorAdapter in the dbInteractionsFragment as that fragment
        persists over orientation changes. The existing adapter can then be placed in the
        recreated listview before the db query has been rerun. This allows artificial
        action mode to be restored more promptly if it is active during an orientation
        change. (If we try to restore it too soon after creating a new cursoradapter and
        putting it into the listview, the listview getChildAt() method returns null.
        Retaining the same adapter through orientation changes avoids this). */
        mAccountListCursorAdapter = cursorAdapter;
    }

    public SimpleCursorAdapter getAccountListCursorAdapter() {

        return mAccountListCursorAdapter;
    }

    public void initItemViewAdapter() {

        /* Set up correspondence we want between the columns from the database and the list_rows in the
		view account listview */
        String[] fromColumns = new String[]{LibRemindDbAdapter.KEY_TITLE, LibRemindDbAdapter.KEY_AUTHOR,
                LibRemindDbAdapter.KEY_DUE_DATE};
        int[] toFields = new int[]{R.id.view_account_title, R.id.view_account_author, R.id.view_account_due_date};

		/* Initially, the cursor we pass to the adapter is null. It's set in the onLoadFinished callback of
		the CursorManager interface (implemented by MainActivity class). */
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(mContext,
                R.layout.view_account_row, null, fromColumns, toFields, 0);

        /* Set the custom binder to format info from db in human-readable way in list view*/
        ItemListViewBinder viewBinder = new ItemListViewBinder(mContext, getMargin());
        cursorAdapter.setViewBinder(viewBinder);

        mItemListCursorAdapter = cursorAdapter;
    }

    public SimpleCursorAdapter getItemListCursorAdapter() {

        return mItemListCursorAdapter;
    }

    public boolean currentAccountHasLoans() {

        return mItemListCursorAdapter.getCount() > 0;
    }

    public void addAccount(String name, LibOrganisation libOrganisation, String cardNumber,
                           String pinOrPassword, boolean remind, int region) {

        /* Encrypt the PIN before writing to db */
        String encryptedPinOrPassword = TextEncryptor.encrypt(pinOrPassword);

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(name, libOrganisation, cardNumber,
                encryptedPinOrPassword, remind, region, DB_WRITE_ADD_ACCOUNT);
        asyncDbWrite.execute();
    }

//    /* This is used to edit an account when the account name is not being changed. */
//    public void updateAndRenameAccount(String name, LibOrganisation libOrganisation, String cardNumber,
//                                       String pinOrPassword) {
//
//        /* Encrypt the PIN before writing to db. */
//        String encryptedPinOrPassword = Scrambler.scramble(pinOrPassword);
//
//        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(name, libOrganisation, cardNumber,
//                encryptedPinOrPassword, true, DB_WRITE_EDIT_AND_RENAME_ACCOUNT);
//        asyncDbWrite.execute();
//    }

    /* This is used to edit an account when only the account name is being changed. */
    public void renameAccount(String oldAccountName, String newAccountName) {

        /*
        If the account has no loans, just rename it. If it has loans, make sure each loan
        is associated with the new account.
        */
        if (!currentAccountHasLoans()) {

            AsyncDbWrite asyncRenameTask = new AsyncDbWrite(oldAccountName, newAccountName,
                    DB_WRITE_RENAME_ACCOUNT);
            asyncRenameTask.execute();

        } else {

            AsyncDbWrite asyncRenameTask = new AsyncDbWrite(oldAccountName, newAccountName,
                    DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS);
            asyncRenameTask.execute();
        }
    }

    public void renameAccountAndChangeRemindSetting(String oldAccountName, String newAccountName,
                                                    boolean remind) {

        /* If the account has no loans, just rename it and change the remind setting. If it has loans,
        make sure each loan is associated with the new account. */
        if (!currentAccountHasLoans()) {

            AsyncDbWrite asyncRenameTask = new AsyncDbWrite(oldAccountName, newAccountName, remind,
                    DB_WRITE_RENAME_ACCOUNT_CHANGE_REMIND);
            asyncRenameTask.execute();

        } else {

            AsyncDbWrite asyncRenameTask = new AsyncDbWrite(oldAccountName, newAccountName, remind,
                    DB_WRITE_RENAME_ACCOUNT_WITH_ITEMS_CHANGE_REMIND);
            asyncRenameTask.execute();
        }
    }



    public void deleteAccounts(String[] selectedItemNames) {

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(selectedItemNames, DB_WRITE_DELETE_ACCOUNTS);
        asyncDbWrite.execute();
    }

    /**
     * @param accountName The account for which to delete items.
     * @param newItemList The list of items to replace them with (which should belong to the same account).
     */
    public void updateItemsForAccount(String accountName, List<LibItem> newItemList, boolean remind) {

        AsyncDbWrite aSyncDbWrite = new AsyncDbWrite(accountName, newItemList, remind,
                DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT);
        aSyncDbWrite.execute();
    }

    public void deleteAllItemsForAccount(String accountName) {

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(accountName, DB_WRITE_DELETE_ALL_ITEMS_FOR_ACCOUNT);
        asyncDbWrite.execute();
    }

    public void editAccount(String accountName, LibOrganisation libOrganisation, String cardNumber,
                            String pinOrPassword, boolean remind, int region) {

        /* The account will have to be synced after this update, create an object now. */
        mAccountToBeSynced = accountName;

        /* Encrypt the PIN before writing to db. */
        String encryptedPinOrPassword = TextEncryptor.encrypt(pinOrPassword);

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(accountName, libOrganisation, cardNumber,
                encryptedPinOrPassword, remind, region, DB_WRITE_EDIT_ACCOUNT);
        asyncDbWrite.execute();

    }

    public void changeRemindSettingForAccount(String accountName, boolean remind) {

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(accountName, remind, DB_WRITE_CHANGE_REMIND_FOR_ACCOUNT);
        asyncDbWrite.execute();
    }

    public void editAndRenameAccount(String oldName, String newName, LibOrganisation libOrganisation,
                                     String cardNumber, String pinOrPassword, boolean remind) {

        /* The account will have to be synced after this update, so store the name (the name it will
        have after the update). */
        mAccountToBeSynced = newName;

        /* Encrypt the PIN before writing to db. */
        String encryptedPinOrPassword = TextEncryptor.encrypt(pinOrPassword);

        if (!currentAccountHasLoans()) {

            AsyncDbWrite asyncDbWrite = new AsyncDbWrite(oldName, newName, libOrganisation, cardNumber,
                    encryptedPinOrPassword, remind, DB_WRITE_EDIT_AND_RENAME_ACCOUNT);
            asyncDbWrite.execute();

        } else {

            AsyncDbWrite asyncDbWrite = new AsyncDbWrite(oldName, newName, libOrganisation, cardNumber,
                    encryptedPinOrPassword, remind, DB_WRITE_EDIT_AND_RENAME_ACCOUNT_WITH_ITEMS);
            asyncDbWrite.execute();
        }
    }

    public void renewAllItemsForAccount(String accountName) {

    }

    public void updateAccounts(DataToWrite dataToWrite) {

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(dataToWrite, DB_WRITE_UPDATE_ACCOUNTS);
        asyncDbWrite.execute();
    }

    public void updateItemRemindSetting(boolean newItemRemindSetting) {

        AsyncDbWrite asyncDbWrite = new AsyncDbWrite(mItemBeingViewed, newItemRemindSetting,
                DB_WRITE_CHANGE_REMIND_FOR_ITEM);
        asyncDbWrite.execute();
    }

    public void runItemInfoQuery(FragmentActivity viewAccountFragParentActivity, String itemUniqueId) {

        mViewAccountFragParentActivity = viewAccountFragParentActivity;
        AsyncDbRead asyncDbRead = new AsyncDbRead(DB_READ_GET_ITEM_DETAILS);
        asyncDbRead.execute(itemUniqueId);
    }

    public void runEditAccountQuery(EditAccountActivity editAccountActivity, String accountName) {

        /* This method starts the async db read to get the details of the account
        to be edited. When the read is finished, the async task calls the readfinished
        method of the DbInteractionsFragment (this class), sending it the details of the
        account, which are then passed to the activity which called this method
        (the "edit account activity"). */
        mEditAccountActivity = editAccountActivity;
        AsyncDbRead asyncDbRead = new AsyncDbRead(DB_READ_EDIT_ACCOUNT_DETAILS);
        asyncDbRead.execute(accountName);
    }

    public void writeFinished(boolean result, String resultMessage, int flag) {

        if (result) {

            if (!resultMessage.equals("")) {
                Toast.makeText(mContext, resultMessage, Toast.LENGTH_SHORT).show();
            }

            /*
            See definition of constants at top of file for explanation of what different db write
            flags do (used in the if statements below).
            */

            if (flag < 30) {

                /* Refresh the account list. */
                mAccountListActivity.updateAccountList();
            }

            if (flag > 30 && flag < 60) {

                /* Close the open account. */
                if (getInLargeLandMode()) {

                    mAccountListActivity.hideViewAccountFragment();
                } else {

                    Intent closeViewAccountActivityIntent =
                            new Intent(ViewAccountActivity.CLOSE_VIEW_ACCOUNT_ACTIVITY);
                    LocalBroadcastManager.getInstance(getActivity())
                            .sendBroadcast(closeViewAccountActivityIntent);
                }

                /* Refresh the account list. */
                mAccountListActivity.updateAccountList();
            }

            if (flag > 60 && flag < 90) {

                /* Close the open account. */
                if (getInLargeLandMode()) {

                    mAccountListActivity.hideViewAccountFragment();

                } else {

                    Intent closeViewAccountActivityIntent =
                            new Intent(ViewAccountActivity.CLOSE_VIEW_ACCOUNT_ACTIVITY);
                    LocalBroadcastManager.getInstance(getActivity())
                            .sendBroadcast(closeViewAccountActivityIntent);
                }

                /*
                Sync the account to be synced, (which is set when the editAccount methods are
                called, before starting the update).
                */
                if (!mAccountToBeSynced.equals("")) {

                    /* These are different variables, although the names look similar. */
                    mAccountBeingSynced = mAccountToBeSynced;

                    mAccountListActivity.syncAccounts(new String[]{mAccountToBeSynced});

                    /* Set this back to empty. */
                    mAccountToBeSynced = "";
                }
            }

            if (flag > 100) {

                /*
                If the flag is > 100 this means the write was to update item list (e.g. following an
                account being synced). If only one account was being synced, and we are in dual-pane
                mode, open it. If multiple accounts were synced, don't open any. mAccountBeingSynced
                is only set when one account is being synced by itself.
                */
                if (!mAccountBeingSynced.equals("")) {

                    if (getInLargeLandMode()) {

                        mAccountListActivity.showAccount(mAccountBeingSynced,
                                mRemindSettingOfAccountBeingSynced);
                    }
                    mAccountBeingSynced = "";
                }
            }

            /* If a new account has been added we should sync it */
            if (flag == DB_WRITE_ADD_ACCOUNT) {

                mNumberOfAccountsBeingSynced = 1;

                /* This loader finds the new account (by checking which account in the db has never been
                synced) and in onLoaderFinished it calls a method to create and sync a library account
                from the query result. */
                mAccountListActivity.runCursorLoader(MainActivity.NEW_ACCOUNT_QUERY, null);
            }
        } else {

            /* If the database write threw an error, show an error dialog asking user to try again. */
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
            alertBuilder.setMessage(getString(R.string.database_problem))
                    .setTitle(getString(R.string.database_error))
                    .setIcon(R.drawable.ic_dialog_alert)
                    .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            AlertDialog alertDialog = alertBuilder.show();
            TextView alertDialogTextView = (TextView) alertDialog.findViewById(android.R.id.message);
            alertDialogTextView.setGravity(Gravity.CENTER);
            alertDialog.show();
        }
    }

    public void readFinished(Cursor cursor, int flag) {

        switch (flag) {

            case DB_READ_EDIT_ACCOUNT_DETAILS: {

                /* Create a string array containing the account details from the cursor
                returned by the db query. */
                cursor.moveToFirst();
                String[] accountDetails = new String[6];
                for (int q = 0; q < 3; q++) {
                    accountDetails[q] = cursor.getString(q);
                }

                /* This one has to be decrypted first so can't do it in the loop above. */
                accountDetails[3] = TextEncryptor.decrypt(cursor.getString(3));

                /* The int in column 4 represents a boolean. For convenience, store it as a
                * string so it can be put in the account details string array.*/
                String remind = "false";
                if (cursor.getInt(4) == 1) remind = "true";
                accountDetails[4] = remind;

                /* The region is stored as an int, convert to string. */
                accountDetails[5] = String.valueOf(cursor.getInt(5));

                if (mEditAccountActivity != null) {

                    mEditAccountActivity.populateEditAccountFragment(accountDetails);
                }

                /* Set this to null so it can be garbage-collected if it points to a view
                account activity which is later closed. */
                mEditAccountActivity = null;

                break;
            }

            case DB_READ_GET_ITEM_DETAILS: {

                if (mViewAccountFragParentActivity != null) {

                    ViewAccountFragment viewAccountFragment = (ViewAccountFragment)
                        mViewAccountFragParentActivity.getSupportFragmentManager()
                            .findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);

                    if (viewAccountFragment != null) {

                        viewAccountFragment.populateItemInfoFragment(cursor);
                    }
                }

                mViewAccountFragParentActivity = null;
                break;
            }
        }
    }

    public void checkDueDates (String accountName, List<LibItem> itemList) {

        int margin = getMargin();

        int dueItems = 0;
        int overdueItems = 0;
        DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());
        LocalDate today = new DateTime(System.currentTimeMillis(), dateTimeZone).toLocalDate();

        for (LibItem item : itemList) {

            if (item.getRemind()) {

                LocalDate dueDate = new DateTime(item.getDueDate(), dateTimeZone).toLocalDate();

                Days differenceInDays = Days.daysBetween(today, dueDate);
                int difference = differenceInDays.getDays();

                if (difference <= margin) {

                    if (difference >= 0) {

                        dueItems++;
                    } else {

                        overdueItems++;
                    }
                }
            }
        }

        if (dueItems > 0 || overdueItems > 0) {

            String[] notificationStrings = new DateParser().makeNotificationStrings(mContext,
                    accountName, margin, dueItems, overdueItems);
            mAccountListActivity.notify(notificationStrings, accountName);
        }
    }

    public void setAccountBeingViewed(String accountBeingViewed) {

        mAccountBeingViewed = accountBeingViewed;
    }

    public void clearAccountBeingViewed() {

        mAccountBeingViewed = "";
    }

    public void storeRemindSettingOfAccountBeingViewed(boolean newSetting) {

        mRemindSettingOfAccountBeingViewed = newSetting;
    }

    public boolean getRemindSettingOfAccountBeingViewed() {

        return mRemindSettingOfAccountBeingViewed;
    }

    public void storeRemindSettingOfAccountBeingSynced(boolean newSetting) {

        mRemindSettingOfAccountBeingSynced = newSetting;
    }

    public boolean getRemindSettingOfAccountBeingSynced() {

        return mRemindSettingOfAccountBeingSynced;
    }

    public String getAccountBeingViewed() {

        return mAccountBeingViewed;
    }

    public void setFirstResumeFalse() {
        mFirstResume = false;
    }

    public boolean thisIsFirstResume() {
        return mFirstResume;
    }

    public String getSyncTimeOrSetRandomOne() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        String remindTimeString = prefs.getString(MainActivity.SYNC_TIME_PREF_KEY, "");

        if (remindTimeString.equals("")) {

            /* If the sync time pref has never been set, this must be the first time the
            * app's been run. */
            mFirstRun = true;

            /* Choose time between 10am and 4pm, on the hour or half-hour, as the default auto-sync time
            (can be changed in settings to any time of day). We just want to avoid all copies of the app
            auto-syncing at the same time of day, it might cause too much server traffic if a lot of
            people have app installed and all sync at the same time. (More randomness - up to an
            additional 29 minutes - is added by the SyncManager each time the alarm is set).*/
            Random random = new Random();
            int syncHour = random.nextInt(7) + 10;

            int syncMins = 0;

            if (random.nextInt(2) == 1) {

                syncMins = 30;
            }

            remindTimeString  = String.valueOf(syncHour) + ":" + String.valueOf(syncMins);

            ApiDependentPrefWriter apiDependentPrefWriter = ApiDependentCodeFactory.getApiDependentPrefWriter();
            apiDependentPrefWriter.writeStringPref(prefs, MainActivity.SYNC_TIME_PREF_KEY, remindTimeString);
        }

        return remindTimeString;
    }

    public void setAccountBeingSynced(String name) {

        /* Note, this string is only set if a single account is being synced by itself. It's not
        * set if more than one account is being synced at the same time, so can't be used as a general
        * test of whether any sync tasks are currently running. This is why there is no get
        * method for this variable. */
        mAccountBeingSynced = name;
    }

    public void clearNoItemsHintShownFor() {
        mNoItemsHintShownFor = "";
    }

    public String getNoItemsHintShownFor() {
        return mNoItemsHintShownFor;
    }

    public void setNoItemsHintShownFor(String noItemsHintShownFor) {
        mNoItemsHintShownFor = noItemsHintShownFor;
    }

    public void setItemsLoading(boolean itemsLoading) {
        mItemsLoading = itemsLoading;
    }

    public boolean getItemsLoading() {
        return mItemsLoading;
    }

    public void setAccountsLoading(boolean accountsLoading) {
        mAccountsLoading = accountsLoading;
    }

    public boolean getAccountsLoading() {
        return mAccountsLoading;
    }

    public int getNumberOfAccountsBeingSynced() {
        return mNumberOfAccountsBeingSynced;
    }

    public void setNumberOfAccountsBeingSynced(int numberOfAccountsBeingSynced) {

        mNumberOfAccountsBeingSynced = numberOfAccountsBeingSynced;
    }

    public void decrementNumberOfAccountsBeingSynced() {

        mNumberOfAccountsBeingSynced--;

        /* If no accounts are being synced, we won't need to hold a reference to the account list listview.
        * (This reference is only held so it can be supplied to accounts which are being synced when the
        * device orientation changes. When the device orientation changes, the original listview will be
        * destroyed and replaced with a new one. When the main activity is resumed, it gets a reference to
        * the new listview and if any accounts are being synced, it passes the reference to this frag, which
        * then passes it on request to any libaccounts which are being synced, so they can show their sync
        * status in the new listview). */
        if (mNumberOfAccountsBeingSynced == 0) {

            mAccountListListView = null;
        }
    }

    public void setAccountNamesList(List<String> accountNamesList) {

        mAccountNamesList = accountNamesList;
    }

    public List<String> getAccountNamesList() {

        return mAccountNamesList;
    }

    public void setAccountCardNumbersList(List<String> accountCardNumbersList) {
        mAccountCardNumbersList = accountCardNumbersList;
    }

    public List<String> getAccountCardNumbersList() {
        return mAccountCardNumbersList;
    }

    public boolean getRestoreViewAccountActionMode() {

        return mRestoreViewAccountActionMode;
    }

    public void setRestoreViewAccountActionMode(boolean newSetting) {

        mRestoreViewAccountActionMode = newSetting;

        if (newSetting) {

            /* If the action mode is going to be restored, store the info about which
            items are checked. Copy the ArrayList<Long> into a Long[]. */
            mCheckedItemsBackup = mCheckedItems.toArray(new Long[mCheckedItems.size()]);

        } else {

            mCheckedItemsBackup = null;
        }
    }

    public Long[] getCheckedItemsBackup() {

        return mCheckedItemsBackup;
    }

    public void setLargeLandDevice(boolean newSetting) {
        mLargeLandDevice = newSetting;
    }

    public boolean getOnLargeLandDevice() {
        return mLargeLandDevice;
    }

    public void setNeedToRerunItemsQuery(boolean newSetting) {

        mNeedToRerunItemsQuery = newSetting;
    }

    public boolean getNeedToRerunItemsQuery() {

        return mNeedToRerunItemsQuery;
    }

    public void writeBooleanPref(String preferenceName, boolean booleanToWrite) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        /* Only write if necessary*/
        Boolean storedBoolean = prefs.getBoolean(preferenceName, false);
        if (booleanToWrite != storedBoolean) {

            ApiDependentPrefWriter apiDependentPrefWriter = ApiDependentCodeFactory.getApiDependentPrefWriter();
            apiDependentPrefWriter.writeBooleanPref(prefs, preferenceName, booleanToWrite);
        }
    }

    public void setScreenSizeDeterminedTrue() {

        mScreenSizeDetermined = true;
    }

    public boolean getScreenSizeDetermined() {

        return mScreenSizeDetermined;
    }

    public boolean getInLargeLandMode() {

        if (mLargeLandDevice) {

            if (getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_LANDSCAPE) return true;
        }

        return false;
    }

    public void storeActionModeRef(ActionMode actionMode) {

        mActionMode = actionMode;
    }

    public ActionMode getActionModeRef() {

        return mActionMode;
    }

    public void setViewAccountInActionMode(boolean newSetting) {

        mViewAccountInActionMode = newSetting;
    }

    public boolean getViewAccountInActionMode() {

        return mViewAccountInActionMode;
    }

    public void setAccountListInActionMode(boolean newSetting) {
        mAccountListInActionMode = newSetting;
    }

    public boolean getAccountListInActionMode() {
        return mAccountListInActionMode;
    }

    public void addCheckedAccount(String accountName) {

        mCheckedAccounts.add(accountName);
    }

    public void removeCheckedAccount(String accountName) {

        mCheckedAccounts.remove(accountName);
    }

    public void clearCheckedAccounts() {

        mCheckedAccounts.clear();
    }

    public List<String> getCheckedAccounts() {
        return mCheckedAccounts;
    }

    public void incrementNumberOfCheckedAccounts() {

        mNumberOfCheckedAccounts++;
    }

    public void decrementNumberOfCheckedAccounts() {

        mNumberOfCheckedAccounts--;
    }

    public int getNumberOfCheckedAccounts() {

        return mNumberOfCheckedAccounts;
    }

    public void resetNumberOfCheckedAccounts() {

        mNumberOfCheckedAccounts = 0;
    }

    public void onCheckBoxClicked(View checkBox) {

        switch (checkBox.getId()) {

            case R.id.account_list_checkbox: {

                AccountListFragment accountListFragment = (AccountListFragment)
                        getActivity().getSupportFragmentManager().findFragmentById(R.id.account_list_fragment);
                if (accountListFragment != null) accountListFragment.onCheckBoxClicked(checkBox);

                break;
            }

            /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

            /* This is part of the code which allows you to put the item list into action mode.
            * There are no bulk functions for items in this version of the app so it has been removed.
            * It may be re-enabled if future versions of the app include bulk item fuctions.*/

 //            case R.id.view_account_checkbox: {
//
//                /*
//                If in dual-pane mode, we'll be able to find the view account frag in the frag manager of
//                the activity we're attached to. If not, we'll have to store a ref to the view, then
//                broadcast a message to find the view account frag, as it'll be hosted in it's own activity.
//                */
//                if (getInLargeLandMode()) {
//
//                    ViewAccountFragment viewAccountFragment = (ViewAccountFragment) getActivity()
//                            .getSupportFragmentManager().findFragmentByTag(ViewAccountActivity.VIEW_ACCOUNT_FRAGMENT_TAG);
//                    if (viewAccountFragment != null)
//                        viewAccountFragment.onCheckBoxClicked(checkBox);
//                } else {
//
//                    storeCheckBoxRef(checkBox);
//                    Intent onCheckBoxClickedIntent = new Intent(ViewAccountActivity.ON_CHECKBOX_CLICKED);
//                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(onCheckBoxClickedIntent);
//                }
//
//                break;
//            }

        /****************************************************************************************/

        }
    }

    public void addCheckedItem(Long itemRow) {

        mCheckedItems.add(itemRow);
    }

    public void removeCheckedItem(Long itemRow) {

        mCheckedItems.remove(itemRow);
    }

    public void clearCheckedItems() {

        mCheckedItems.clear();
    }

    public List<Long> getCheckedItems() {

        return mCheckedItems;
    }

    public void setCheckedItems(Long[] checkedItems) {

        mCheckedItems = new ArrayList<Long>(Arrays.asList(checkedItems));
    }

    public void incrementNumberOfCheckedItems() {

        mNumberOfCheckedItems++;
    }

    public void decrementNumberOfCheckedItems() {

        mNumberOfCheckedItems--;
    }

    public int getNumberOfCheckedItems() {

        return mNumberOfCheckedItems;
    }

    public void setNumberOfCheckedItems(int newNumberOfCheckedItems) {

        mNumberOfCheckedItems = newNumberOfCheckedItems;
    }

    public void resetNumberOfCheckedItems() {

        mNumberOfCheckedItems = 0;
    }

    public void storeCheckBoxRef (View checkBox) {

        mCheckBoxRef = checkBox;
    }

    public View getCheckBoxRef() {

        return mCheckBoxRef;
    }

    public SavedState getEditFragSavedState() {

        return mEditFragSavedState;
    }

    public void storeEditFragSavedState(SavedState editFragSavedState) {

        mEditFragSavedState = editFragSavedState;
    }

    public void setAccountBeingEdited(boolean newSetting) {

        mAccountBeingEdited = newSetting;

        if (!newSetting) {

            storeEditFragSavedState(null);
        }
    }

    public boolean getAccountBeingEdited() {

        return mAccountBeingEdited;
    }

    public void storeSyncAccountsQueryData(int loaderId, Bundle loaderData){

        /* If orientation changes after a query is started, the loader will callback
        to an activity which has been destroyed and replaced. So when we reattach
        to an activity, we check if a query is running, and if so, restart it with the
        new activity as the callback (this is why we are storing the info here to
        re-run the query).
        This situation only arises in this app when the edit account dialog is open
        when the orientation is changed. If the login data is changed, the app tries
        to resync the account when the dialog is closed, but the reattaching of this
        frag to a different activity seems to be triggered at the same time (presumably
        by the dialog closing), so the loader gets started by the old activity, which
        gets destroyed before the query finishes. */
        mSyncAccountsQueryRunning = true;
        mSyncAccountsLoaderId = loaderId;
        mSyncAccountsLoaderData = loaderData;
    }

    public void clearSyncAccountsQueryData() {

        mSyncAccountsQueryRunning = false;
        mSyncAccountsLoaderId = -1;
        mSyncAccountsLoaderData = null;
    }

    public boolean getSyncAccountsQueryRunning() {

        return mSyncAccountsQueryRunning;
    }

    public boolean getSetOrganisationDialogFragmentOpen() {

        return mSetOrganisationFragmentOpen;
    }

    public void setSetOrganisationFragmentOpen(boolean newSetting) {

        mSetOrganisationFragmentOpen = newSetting;
    }

    public void setItemBeingViewed(String uniqueIdentifier) {

        mItemBeingViewed = uniqueIdentifier;
    }

    public void onItemInfoDialogClose() {

        mItemBeingViewed = "";
    }

    public String getItemBeingViewed() {

        return mItemBeingViewed;
    }

    public void setPotentialABSBug(boolean newSetting) {

        mPotentialABSBug = newSetting;
    }

    public boolean getPotentialABSBug() {

        return mPotentialABSBug;
    }

    public void storeAccountListListViewRef (ListView accountListListView) {

        mAccountListListView = accountListListView;
    }

    public ListView getAccountListListViewRef() {

        return mAccountListListView;
    }

    public void addNameToSyncList(String accountName) {

        mSyncList.add(accountName);
    }

    public void removeNameFromSyncList(String accountName) {

        mSyncList.remove(accountName);
    }

    public boolean isThisAccountBeingSynced (String accountName) {

        return mSyncList.contains(accountName);
    }

    public void setLastLicenseViewed(int licenseType) {

        mLastLicenseViewed = licenseType;
    }

    public int getLastLicenseViewed() {

        return mLastLicenseViewed;
    }

    public int getIntPref(String prefName, int defaultValue) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        return prefs.getInt(prefName, defaultValue);
    }

    public int getMargin() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        String marginString = prefs.getString(MainActivity.MARGIN_PREF, "3");
        return Integer.parseInt(marginString);
    }

    public String getMarginAsString() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        return  prefs.getString(MainActivity.MARGIN_PREF, "3");
    }

    public void writeIntPref(String prefName, int valueToWrite) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        ApiDependentCodeFactory.getApiDependentPrefWriter()
                .writeIntPref(prefs, prefName, valueToWrite);
    }

    public boolean accountRemindSettingChangedSinceAccountOpened() {

        return mAccountRemindSettingChangedSinceAccountOpened;
    }

    public void setAccountRemindSettingChangedSinceAccountOpened(boolean newSetting) {

        mAccountRemindSettingChangedSinceAccountOpened = newSetting;
    }

    public void storeAboutFragScrollPos(int aboutFragScrollPos) {

        mAboutFragScrollPos = aboutFragScrollPos;
    }

    public int getAboutFragScrollPos() {

        return mAboutFragScrollPos;
    }

    public void storeLicenseFragScrollPos(int licenseFragScrollPos) {

        mLicenseFragScrollPos = licenseFragScrollPos;
    }

    public int getLicenseFragScrollPos() {

        return mLicenseFragScrollPos;
    }

    public void storeSetOrgFirstVisiblePos(int setOrgFragScrollPos) {

        mSetOrgFragScrollPos = setOrgFragScrollPos;
    }

    public int getSetOrgFirstVisiblePos() {

        return mSetOrgFragScrollPos;
    }

    public void storeTimePickerTime(int hour, int minute) {

        mTimePickerTime[0] = hour;
        mTimePickerTime[1] = minute;
    }

    public int[] getStoredTimePickerTime() {

        return mTimePickerTime;
    }

    public void setMainActivityState(int newState) {

        mMainActivityState = newState;
    }

    public int getMainActivityState() {

        return mMainActivityState;
    }

    public void setViewAccountActivityState(int newState) {

        mViewAccountActivityState = newState;
    }

    public int getViewAccountActivityState() {

        return mViewAccountActivityState;
    }

    public void setSettingsActivityState(int newState) {

        mSettingsActivityState = newState;
    }

    public int getSettingsActivityState() {

        return mSettingsActivityState;
    }

    public void setAccountToBeOpenedFromNotification(String accountToBeOpened) {

        mAccountToBeOpenedFromNotification = accountToBeOpened;
    }

    public String getAccountToBeOpenedFromNotification() {

        return mAccountToBeOpenedFromNotification;
    }

    public int getNotificationCount() {

        return mNotifiedAccounts.size();
    }

    public void clearNotifiedAccounts() {

        mNotifiedAccounts.clear();
    }

    public void addNotifiedAccount(String accountName) {

        if (!mNotifiedAccounts.contains(accountName)) {

            mNotifiedAccounts.add(accountName);
        }
    }

    public boolean notificationHasBeenSentFor(String accountName) {

        return mNotifiedAccounts.contains(accountName);
    }

    public boolean getFirstRun() {

        return mFirstRun;
    }

    public void setFirstRun(boolean newSetting) {

        mFirstRun = false;
    }
}
