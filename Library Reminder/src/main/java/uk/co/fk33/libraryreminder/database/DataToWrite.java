/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.content.ContentValues;

public abstract class DataToWrite {

    protected int mSize;
    protected long[] mRowIds;
    protected String mTable;

    /**
     * The idea of this class is to have an object into which you can put data to be written to either
     * database table. * There is a different implementation of this class for each type of write action
     * to be carried out.
     *
     * @param rowIds The rowIds of the records you want to update
     */
    public DataToWrite(long[] rowIds) {

        mSize = rowIds.length;

        if (mSize == 0) {
            throw new IllegalArgumentException("DataToWrite: rowIds cannot be empty.");
        }

        mRowIds = rowIds;
    }

    public abstract ContentValues[] getContentValues();

    public long[] getRowIds() {
        return mRowIds;
    }

    public int size() {
        return mSize;
    }

    public String table() {

        return mTable;
    }

}
