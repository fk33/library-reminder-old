package uk.co.fk33.libraryreminder.network;

/*
 * Copyright (c) 2014 Dan Huston.
 */

import android.content.Context;
import uk.co.fk33.libraryreminder.LibItem;

import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DummyUpdateTask extends UpdateTask {

    public DummyUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        LibItem libItem;
        String title = "Dummy item";
        String author = "Dummy author";
        String standardIdentifier = "A";
        String uniqueIdentifier = "B";

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", LocaleDependentClass.getDateLocale());
        Date dueDate;

        String[] itemsDatesArray = new String[] {"01/12/1909", "22/08/2544", "19/12/1801",
                "06/05/1066", "12/03/4422", "14/10/0001"};

        List<LibItem> itemList = new ArrayList<LibItem>();

        for (int i = 0; i < 6; i++) {

            try {

                String serial = " " + String.valueOf(i);

                dueDate = dateFormat.parse(itemsDatesArray[i]);
                long dueDateLong = dueDate.getTime();

                libItem = new LibItem(mLibAccount.getName(), title + serial, author + serial, uniqueIdentifier + serial,
                        standardIdentifier + serial, dueDateLong, -1, true);

                itemList.add(libItem);

            } catch (ParseException e) {

                mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                return null;
            }
        }

        mStatus = LibAccount.SYNC_SUCCEEDED;
        return itemList;
    }
}
