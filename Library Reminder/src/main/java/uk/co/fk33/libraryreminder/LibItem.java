/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.content.ContentValues;

import uk.co.fk33.libraryreminder.database.LibRemindDbAdapter;

public class LibItem {

    private final String mAccount;
    private String mTitle;
    private String mAuthor;
    private final String mUniqueIdentifier;
    private final String mStandardIdentifier;
    private long mDueDate;
    private int mTimesRenewed;
    private boolean mRemind;

    public LibItem(String account, String title, String author, String uniqueIdentifier,
                   String standardIdentifier, long dueDate, int timesRenewed, boolean remind) {

        mAccount = account;
        mTitle = title;
        mAuthor = author;
        mUniqueIdentifier = uniqueIdentifier;
        mStandardIdentifier = standardIdentifier;
        mDueDate = dueDate;
        mTimesRenewed = timesRenewed;
        mRemind = remind;
    }

    /**
     * Used for writing the item to the database
     *
     * @return a ContentValues containing a value for each column in the items table.
     */
    public ContentValues getContentValues() {

        /* Convert booleans to 1 or 0 to store in db */
        int remindInt = mRemind ? 1 : 0;

        ContentValues itemValues = new ContentValues();

        itemValues.put(LibRemindDbAdapter.KEY_ACCOUNT, mAccount);
        itemValues.put(LibRemindDbAdapter.KEY_TITLE, mTitle);
        itemValues.put(LibRemindDbAdapter.KEY_AUTHOR, mAuthor);
        itemValues.put(LibRemindDbAdapter.KEY_UNIQUE_IDENTIFIER, mUniqueIdentifier);
        itemValues.put(LibRemindDbAdapter.KEY_STANDARD_IDENTIFIER, mStandardIdentifier);
        itemValues.put(LibRemindDbAdapter.KEY_DUE_DATE, mDueDate);
        itemValues.put(LibRemindDbAdapter.KEY_TIMES_RENEWED, mTimesRenewed);
        itemValues.put(LibRemindDbAdapter.KEY_REMIND, remindInt);

        return itemValues;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAccount() {
        return mAccount;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getUniqueIdentifier() {
        return mUniqueIdentifier;
    }

    public String getStandardIdentifier() {
        return mStandardIdentifier;
    }

    public long getDueDate() {
        return mDueDate;
    }

    public int getTimesRenewed() {
        return mTimesRenewed;
    }

    public void setTimesRenewed(int newRenewCount) {
        mTimesRenewed = newRenewCount;
    }

    public void setRemind(boolean newSetting) {

        mRemind = newSetting;
    }

    public boolean getRemind() {

        return mRemind;
    }
}
