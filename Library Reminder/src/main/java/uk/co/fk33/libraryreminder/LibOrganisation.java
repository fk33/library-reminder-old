/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.os.Parcel;
import android.os.Parcelable;

public class LibOrganisation implements Parcelable {

    private final String mName;
    private final String mShortName;
    private final String mCatType;
    private final String mCatUrl;

    public static final Creator<LibOrganisation> CREATOR = new LibOrgParcelCreator();

    public LibOrganisation(String name, String shortName, String catType, String catUrl) {

        mName = name;
        mShortName = shortName;
        mCatType = catType;
        mCatUrl = catUrl;
    }

    public String getName() {
        return mName;
    }

    public String getShortName() {
        return mShortName;
    }

    public String getCatType() {
        return mCatType;
    }

    public String getCatUrl() {
        return mCatUrl;
    }

    /*
    The following methods implement android.os.Parcelable so LibOrganisations can
    be stored in a Bundle with putParcelable().
    */
    private static class LibOrgParcelCreator implements Creator<LibOrganisation> {

        public LibOrganisation createFromParcel(Parcel in) {

            return new LibOrganisation(in);
        }

        public LibOrganisation[] newArray(int size) {

            return new LibOrganisation[size];
        }
    }

    public int describeContents() {

        return 0;
    }

    private LibOrganisation(Parcel in) {

        mName = in.readString();
        mShortName = in.readString();
        mCatType = in.readString();
        mCatUrl = in.readString();
    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(mName);
        out.writeString(mShortName);
        out.writeString(mCatType);
        out.writeString(mCatUrl);
    }
}
