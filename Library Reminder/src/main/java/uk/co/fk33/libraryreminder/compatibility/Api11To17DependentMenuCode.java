/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.annotation.TargetApi;
import android.app.Activity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import uk.co.fk33.libraryreminder.R;

@TargetApi(11)
public class Api11To17DependentMenuCode extends ApiDependentMenuCode {

    @Override
    public void addViewAccountMenuItems(Menu menu, MenuInflater menuInflater) {

        /* Don't add the options if they're already present. */
        MenuItem editAccountMenuItem = menu.findItem(R.id.action_edit_account);
        if (editAccountMenuItem == null) {

            menuInflater.inflate(R.menu.dual_pane_options, menu);
        }

    }

    @Override
    public void removeViewAccountMenuItems(Menu menu) {

        MenuItem editAccountMenuItem = menu.findItem(R.id.action_edit_account);
        if (editAccountMenuItem != null) {
            menu.removeItem(R.id.action_edit_account);
        }

        MenuItem syncAccountMenuItem = menu.findItem(R.id.action_sync_open_account);
        if (syncAccountMenuItem != null) {
            menu.removeItem(R.id.action_sync_open_account);
        }

        MenuItem deleteAccountMenuItem = menu.findItem(R.id.action_delete_open_account);
        if (deleteAccountMenuItem != null) {
            menu.removeItem(R.id.action_delete_open_account);
        }
    }

    @Override
    public void callInvalidateOptionsMenu(Activity activity) {

        activity.invalidateOptionsMenu();
    }
}
