package uk.co.fk33.libraryreminder.accounts;

import android.content.Context;
import uk.co.fk33.libraryreminder.network.DummyUpdateTask;

public class DummyAccount extends LibAccount {

    public DummyAccount(Context context, boolean silentMode, long rowId, String name, String catUrl, String cardNumber,
                        String pinOrPassword, boolean remind) {

        super(context, silentMode);
        mRowID = rowId;
        mName = name;
        mType = CatType.VIEWPOINT;
        mCatUrl = catUrl;
        mCardNumber = cardNumber;
        mPinOrPassword = pinOrPassword;
        mRemind = remind;
    }

    @Override
    public void sync() {

        super.sync();

        DummyUpdateTask dummyUpdateTask = new DummyUpdateTask(mContext);
        dummyUpdateTask.execute(this);
    }
}
