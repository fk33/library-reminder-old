/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.actionbarsherlock.app.SherlockFragment;

import java.text.DateFormat;
import java.util.Date;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class ViewAccountFragment extends SherlockFragment implements
        AdapterView.OnItemClickListener {

    /* THESE INTERFACES WERE IMPLEMENTED AS PART OF ITEM LIST ACTION MODE FUNCTIONALITY,
    NOT NEEDED IN THIS VERSION OF THE APP. MAY BE REINSTATED IN A FUTURE VERSION.*/
//    ,AdapterView.OnItemLongClickListener,
//    ViewTreeObserver.OnGlobalLayoutListener
    /**********************************************************************/

    /* THESE TWO FIELDS ARE PART OF ITEM LIST ACTION MODE FUNCTIONALITY NOT NEEDED IN THIS VERSION
    OF THE APP. THE FUNCTIONALITY MAY BE RESTORED IN FUTURE VERSIONS. */
//    private ActionMode mActionMode;
//    private boolean mActionModeToBeRestored = false;
    /**********************************************************************/

    protected ViewAccountParent mFragmentParent;

    public interface ViewAccountParent extends FragmentParent {

        public void renewItems(String[] itemsToRenew);

        public void runCursorLoader(int loaderId, Bundle argsBundle);
    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

        if (activity instanceof ViewAccountParent) {
            mFragmentParent = (ViewAccountParent) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement ViewAccountParent.");
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /*
        Rerun the items query if necessary (i.e. if we've switched from portrait mode to landscape mode
        on a device with a large screen while an account was open and while the app was in the
        background.
        */
        if (dbFrag.getNeedToRerunItemsQuery()) {

            Bundle viewAccountBundle = new Bundle();
            viewAccountBundle.putString(ViewAccountActivity.ACCOUNT_NAME, dbFrag.getAccountBeingViewed());
            mFragmentParent.runCursorLoader(ViewAccountActivity.ACCOUNT_ITEMS_QUERY, viewAccountBundle);
            dbFrag.setNeedToRerunItemsQuery(false);
        }

        if (!dbFrag.getItemBeingViewed().equals("")) {

            /* If this fragment is being created, and an item was already open, we must have
            * just changed orientation. If we are in portrait mode, and we are on a large screen,
            * we must have just changed from dual-pane to single-pane mode. Make a new item
            * info dialog to replace the one which was open, as it would have been removed during
            * the change from one activity to two.*/
            if (dbFrag.getOnLargeLandDevice() &&  getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_PORTRAIT) {

                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();
                ItemInfoDialogFragment itemInfoDialogFragment = new ItemInfoDialogFragment();
                transaction.add(itemInfoDialogFragment, MainActivity.ITEM_INFO_FRAGMENT_TAG).commit();
            }
        }

        String accountToBeOpenedFromNotification = dbFrag.getAccountToBeOpenedFromNotification();
        if (!accountToBeOpenedFromNotification.equals("")) {

            dbFrag.setAccountToBeOpenedFromNotification("");
            displayAccount(accountToBeOpenedFromNotification, true);
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();

        mFragmentParent = null;

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /*
        Db Frag may not be attached to an activity at this point so it can't call getResources() to
        find orientation (as it does in getOnLargeLandDevice() method. So find orientation here.
        */
        boolean inLargeLandMode = false;
        boolean onLargeLandDevice = dbFrag.getOnLargeLandDevice();

        if (onLargeLandDevice) {

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                inLargeLandMode = true;
        }

        if (dbFrag.getViewAccountInActionMode()) {

            /*
            This class (ViewAccountFragment) is only used when in single-pane mode. A different class
            (ViewAccountFragmentDualPane) is used in dual-pane mode. When changing from single- to
            dual-pane mode, the new frag is instantiated before this frag is destroyed. If we're
            trying to transfer action mode from this frag to the new one, we mustn't call finish() on
            the action mode here, or it will finish the action mode immediately after it's been
            transferred. In large land mode, this single-pane class (ViewAccountFragment) will only ever
            exist briefly during the switch from portrait, before being destroyed. So if we are in large
            land mode here (during this fragment's onDetach), and in action mode, we mustn't call
            finish() on the action mode, so it has a chance to transfer over to the new frag. In the
            ViewAccountFragmentDualPane class, this check for large land mode is not made, it always
            destroys its action mode when detaching, because when changing orientation from large-land
            mode to portrait, the dual-pane frag always detaches *before* the action mode has been
            restored. The idea is to only ever call finish() on an action mode before it has been
            restored. It doesn't matter that we don't call finish on the ViewAccountFragment's action
            mode when going from single to dual-pane mode, because when we are in single-pane mode, the
            fragment is hosted in its own activity, which is destroyed in the change, so the action mode
            can only transfer onto the main activity, and it is overridden when the new action mode is
            created. When going the other way, we go from one activity to two, so having two action
            modes active causes a problem, as each activity then hosts one of the action modes, meaning
            when you close the view account activity, revealing the account list activity underneath,
            the account list activity is in action mode, when it shouldn't be.
            */
            if (!inLargeLandMode) {

                if (onLargeLandDevice) {

                    dbFrag.setRestoreViewAccountActionMode(true);
                    dbFrag.getActionModeRef().finish();
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.view_account, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        SimpleCursorAdapter cursorAdapter = dbFrag.getItemListCursorAdapter();

        ListView listView = (ListView) this.getView().findViewById(R.id.item_list);
        listView.setAdapter(cursorAdapter);

        /* Set this fragment as the onClick listener for its listview */
        listView.setOnItemClickListener(this);

        /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

        /* This is part of the code which allows you to put the list into action mode. There are
        * no bulk functions for items in this version of the app so it has been removed.
        * It may be re-enabled if future versions of the app include bulk item fuctions.*/

//        mListView.setOnItemLongClickListener(this);
//
//        /*
//        Add the global layout listener so that if the frag is recreated, and was in action mode
//        when it was paused, we can re-start the action mode.
//        */
//        mListView.getViewTreeObserver().addOnGlobalLayoutListener(this);
//
//        mListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
//
//        if (dbFrag.getViewAccountInActionMode()) {
//
//            mActionMode = getSherlockActivity().startActionMode(new
//                    ActionModeCallback(mFragmentParent, mListView, R.menu.view_account_context_menu));
//
//                /* Update the title bar with the number of selected items. */
//            if (mActionMode != null)
//                mActionMode.setTitle(Integer.toString(dbFrag.getNumberOfCheckedItems()) + " selected");
//        }

        /******************************************************************************/

        /*
        Check if the account being shown has never been synced, or has no loans. In either of these
        cases a message will be shown letting the user know why there are no loans displayed.
        This method is also called when an account is clicked in the account list.
        */
        showHintOnEmptyAccount();
    }

    /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

    /* This is part of the code which allows you to put the list into action mode. There are
    * no bulk functions for items in this version of the app so it has been removed.
    * It may be re-enabled if future versions of the app include bulk item fuctions.*/

//    @Override
//    public void onGlobalLayout() {
//
//
//        DbInteractionsFragment dbFrag = DbFragmentRef.get();
//
//        /* Restore action mode after changing from dual-pane to single-pane mode or back. */
//        if (mActionModeToBeRestored) {
//
//            Long[] checkedItems = dbFrag.getCheckedItemsBackup();
//
//            dbFrag.setCheckedItems(checkedItems);
//            dbFrag.setNumberOfCheckedItems(checkedItems.length);
//            dbFrag.setViewAccountInActionMode(true);
//
//            mActionMode = getSherlockActivity().startActionMode(new
//                    ActionModeCallback(mFragmentParent, mListView, R.menu.view_account_context_menu));
//
//                /* Update the title bar with the number of selected items. */
//            mActionMode.setTitle(Integer.toString(dbFrag.getNumberOfCheckedItems()) + " selected");
//
//            dbFrag.setRestoreViewAccountActionMode(false);
//            mActionModeToBeRestored = false;
//        }
//    }
    /******************************************************************************/



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

        /* This is part of the code which allows you to put the list into action mode. There are
        * no bulk functions for items in this version of the app so it has been removed.
        * It may be re-enabled if future versions of the app include bulk item fuctions.*/

        /* If we're in action mode, clicking the item checks that item's checkbox. */
//        if (dbFrag.getViewAccountInActionMode()) {
//
//            CheckBox checkBox = (CheckBox) view.findViewById(R.id.view_account_checkbox);
//            if (checkBox != null) {
//
//                if (checkBox.isChecked()) {
//
//                    checkBox.setChecked(false);
//
//                } else {
//
//                    checkBox.setChecked(true);
//                }
//
//                onCheckBoxClicked(checkBox);
//            }
//        } else {

        /******************************************************************************/


            /* Get the uniqueIdentifier of the item which has been clicked.*/
            Cursor cursor = ((SimpleCursorAdapter) parent.getAdapter()).getCursor();
            cursor.moveToPosition(position);
            String itemUniqueId = cursor.getString(4);

            /* Store the unique id of the item being viewed in the db frag. When the item info
            * dialog is created, it retrieves this id from the dbrag, and runs the appropriate
            * async db query to get the item's info, to populate itself with.
            * The dbFrag itemBeingViewed string is cleared when the item dialog is cancelled, but
            * not when it is destroyed. This is so it will repopulate itself after an orientation
            * change.*/
            dbFrag.setItemBeingViewed(itemUniqueId);

            /*If we're not in action mode, open an item info dialog to show details of the
            * clicked item.*/
            FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                    .beginTransaction();
            ItemInfoDialogFragment itemInfoDialogFragment = new ItemInfoDialogFragment();
            transaction.add(itemInfoDialogFragment, MainActivity.ITEM_INFO_FRAGMENT_TAG).commit();


        /* THE BRACE BELOW IS PART OF THE REMOVED FUNCTIONALITY. */
//        }

    }

//    @Override
//    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//        DbInteractionsFragment dbFrag = DbFragmentRef.get();
//        if (!dbFrag.getViewAccountInActionMode()) {
//
//            CheckBox checkBox = (CheckBox) view.findViewById(R.id.view_account_checkbox);
//            checkBox.setChecked(true);
//            onCheckBoxClicked(checkBox);
//        }
//
//        return true;
//    }

    public void setActionModeToBeRestored(boolean newSetting) {

        /* PART OF REMOVED FUNCTIONALITY, MAY BE RE-ENABLED IN FUTURE */
//        mActionModeToBeRestored = newSetting;
    }

    public void displayAccount(String accountName, boolean accountRemindSetting) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        dbFrag.setAccountBeingViewed(accountName);
        dbFrag.storeRemindSettingOfAccountBeingViewed(accountRemindSetting);

        getSherlockActivity().supportInvalidateOptionsMenu();

        displayAccount();
    }

    public void displayAccount() {

        String accountName = DbFragmentRef.get().getAccountBeingViewed();

        if (!accountName.equals("")) {

            /*
            mFragmentParent might be null if we are recreating a fragment which existed prior to an
            orientation change, as the activity won't have had time to attach.
            In this case the cursor loader will already be loaded so there's no need to rerun
            it anyway, so we won't. In other circumstances, mFragmentParent won't be null.
            */
            if (mFragmentParent != null) {

                /*
                Run the cursor loader for the query that will return items for the account
                we're displaying. The resulting cursor will be put into the view account's listview
                adapter in the onLoadFinished callback of the parent object (a reference to the
                adapter is stored in the DbInteractionsFragment).
                */
                Bundle viewAccountBundle = new Bundle();
                viewAccountBundle.putString(ViewAccountActivity.ACCOUNT_NAME, accountName);

                mFragmentParent.runCursorLoader(ViewAccountActivity.ACCOUNT_ITEMS_QUERY, viewAccountBundle);
            }
        }
    }

    public void showHintOnEmptyAccount() {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        SimpleCursorAdapter itemListCursorAdapter = dbFrag.getItemListCursorAdapter();

        /* If there are no loans, show a message saying so. Find out if the account is empty by checking
        the count of the item list cursor adapter. Also checking that the items query is not still running.
        This method might be called before the items query has finished running, in which case it doesn't
        need to do anything, as it will be called again when the items query finishes, in onLoadFinished
        for that query. */
        if (itemListCursorAdapter.getCount() == 0
                && !dbFrag.getItemsLoading()
                && !dbFrag.getAccountsLoading()) {

            /* Find out what account (if any) is being viewed and check it has been successfully synced. */
            String accountBeingViewed = dbFrag.getAccountBeingViewed();

            if (!accountBeingViewed.equals("")) {

                /* Don't bother going through the following code (checking the status of the account and setting
                an appropriate hint) if it's already been done for this account. This check is useful when
                changing orientation as showHintOnEmptyAccount is called from more than one place, and both
                calls are made during orientation change, so this code would be run twice and the second run
                would be redundant.
                When the code is run, dbInteractionsFragment.setNoItemsHintShownFor is called with the name
                of the account the hint was shown for. */
                if (!accountBeingViewed.equals(dbFrag.getNoItemsHintShownFor())) {

                    SimpleCursorAdapter accountListCursorAdapter = dbFrag.getAccountListCursorAdapter();
                    Cursor accountListCursor = accountListCursorAdapter.getCursor();

                    /* Make sure the cursor isn't null, for extra safety. */
                    if (accountListCursor != null) {

                        /* Determine if the account has never been synced. */
                        accountListCursor.moveToFirst();
                        long accountSyncStatus = -1;
                        while (!accountListCursor.isAfterLast()) {

                            if (accountListCursor.getString(1).equals(accountBeingViewed)) {
                                accountSyncStatus = accountListCursor.getLong(4);
                                break;
                            }
                            accountListCursor.moveToNext();
                        }

                        /* If the no accounts hint textview already exists, update it. Otherwise create it. */
                        TextView noLoansHintTextView = (TextView) getActivity().findViewById(R.id.no_loans_hint_textview);
                        if (noLoansHintTextView == null) {

                            noLoansHintTextView = new TextView(getActivity());
                            noLoansHintTextView.setTextSize(17);
                            noLoansHintTextView.setGravity(Gravity.CENTER_HORIZONTAL);
                            noLoansHintTextView.setPadding(0, 150, 0, 0);
                            noLoansHintTextView.setId(R.id.no_loans_hint_textview);
                            LinearLayout viewAccountLayout = (LinearLayout) getActivity().findViewById(R.id.view_account_layout);
                            viewAccountLayout.addView(noLoansHintTextView, 0);
                        }

                        String noLoansHint;
                        if (accountSyncStatus < 0) {

                            noLoansHint = accountBeingViewed + " " + getString(R.string.has_not_been_synced_yet);

                            /* If an account has never been synced, the displayAccount() method is not called,
                            so if we are in large-land mode we should called invalidateOptionsMenu here,
                            or it won't get called for accounts which fail to sync on the first attempt,
                            but appear open. */
                            if (dbFrag.getInLargeLandMode())

                                getSherlockActivity().supportInvalidateOptionsMenu();
                        } else {

                            noLoansHint = accountBeingViewed + " " + getString(R.string.has_no_loans);
                        }

                        dbFrag.setNoItemsHintShownFor(accountBeingViewed);

                        noLoansHintTextView.setText(noLoansHint);
                    }
                }
            }
        } else {

            /* Remove the hint when no longer needed. */
            if (!dbFrag.getItemsLoading()) {
                dbFrag.clearNoItemsHintShownFor();

                TextView noLoansHintTextView = (TextView) getActivity().findViewById(R.id.no_loans_hint_textview);
                if (noLoansHintTextView != null) {
                    LinearLayout viewAccountLayout = (LinearLayout) getActivity().findViewById(R.id.view_account_layout);
                    viewAccountLayout.removeView(noLoansHintTextView);
                }
            }
        }
    }

    public void onCheckBoxClicked(View checkBox) {

        /******* THE FUNCTIONALITY BELOW IS NOT NEEDED FOR THIS VERSION OF APP ****************/

        /* This is part of the code which allows you to put the list into action mode. There are
        * no bulk functions for items in this version of the app so it has been removed.
        * It may be re-enabled if future versions of the app include bulk item fuctions.*/

 //        DbInteractionsFragment dbFrag = DbFragmentRef.get();
//
//        /* Keep track of the number of items with checked checkboxes. */
//        boolean checked = ((CheckBox) checkBox).isChecked();
//
//        /* We're storing the db row of the item in case there are items with the same names. */
//        int position = mListView.getPositionForView(checkBox);
//        SimpleCursorAdapter cursorAdapter = dbFrag.getItemListCursorAdapter();
//        Cursor cursor = (Cursor) cursorAdapter.getItem(position);
//        Long itemRow = cursor.getLong(0);
//
//        if (checked) {
//
//            dbFrag.incrementNumberOfCheckedItems();
//
//            /* Store the db row of the checked item. */
//            dbFrag.addCheckedItem(itemRow);
//        } else {
//
//            dbFrag.decrementNumberOfCheckedItems();
//
//            /* Remove the db row of the checked item. */
//            dbFrag.removeCheckedItem(itemRow);
//        }
//
//        /* If one or more checkboxes is checked, we should be in context action mode. */
//        if (dbFrag.getNumberOfCheckedItems() > 0) {
//
//            /* Start the action mode if not already started. */
//            if (!dbFrag.getViewAccountInActionMode()) {
//
//                mActionMode = getSherlockActivity().startActionMode(new
//                        ActionModeCallback(mFragmentParent, mListView, R.menu.view_account_context_menu));
//                dbFrag.setViewAccountInActionMode(true);
//            }
//
//            /* Update the title bar with the number of selected items. */
//            mActionMode.setTitle(Integer.toString(dbFrag.getNumberOfCheckedItems()) + " selected");
//
//        } else {
//
//            /* If the last checkbox has been deselected, end the action mode. */
//            if (dbFrag.getViewAccountInActionMode()) {
//
//                /*
//                The accountListInActionMode boolean in db frag will be set
//                false in onDestroyActionMode of the ActionModeCallback.
//                */
//                mActionMode.finish();
//            }
//        }
//
//        /*
//        If we're in action mode and a checkbox is clicked, refresh the listview so the row will be
//        highlighted.
//        */
//        if (dbFrag.getViewAccountInActionMode())
//            dbFrag.getItemListCursorAdapter().notifyDataSetChanged();

        /******************************************************************************/
    }

    public void updateActionModeTitle() {

        /* PART OF REMOVED FUNCTIONALITY, MAY BE RE-ENABLED IN FUTURE */
        /* Update the title bar with the number of selected items. */
//        mActionMode.setTitle(Integer.toString(DbFragmentRef.get()
//                .getNumberOfCheckedItems()) + " selected");

    }

    public ItemInfoDialogFragment getItemInfoDialogFragment() {

        /* This needs to be overridden in the dual-pane view account frag, as the dual pane
        * view account frag needs to find the dual pane mode item info dialog, rather than the
        * single-pane mode one.*/
        return (ItemInfoDialogFragment) getActivity()
                .getSupportFragmentManager().findFragmentByTag(MainActivity.ITEM_INFO_FRAGMENT_TAG);
    }

    public void populateItemInfoFragment(Cursor cursor) {

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        Dialog itemInfoDialog = null;
        ItemInfoDialogFragment itemInfoDialogFragment = getItemInfoDialogFragment();

        if (itemInfoDialogFragment != null) itemInfoDialog = itemInfoDialogFragment.getDialog();

        if (itemInfoDialog != null) {

            TextView itemInfoTextView;
            cursor.moveToFirst();

            itemInfoTextView = (TextView) itemInfoDialog.findViewById(R.id.item_info_title_field);
            itemInfoTextView.setText(cursor.getString(0));

            itemInfoTextView = (TextView) itemInfoDialog.findViewById(R.id.item_info_author_field);
            itemInfoTextView.setText(cursor.getString(1));

            itemInfoTextView = (TextView) itemInfoDialog.findViewById(R.id.item_info_due_date_field);
            long dueDateLong = cursor.getLong(3);
            Date dueDate = new Date(dueDateLong);
            DateFormat dateFormat = LocaleDependentClass.getItemInfoDueDateFormat();
            itemInfoTextView.setText(dateFormat.format(dueDate));

            itemInfoTextView = (TextView) itemInfoDialog.findViewById(R.id.item_info_renew_count_field);
            int timesRenewed = cursor.getInt(4);

            if (timesRenewed < 0) {

                /* If the renew count is -1 that means the catalogue for this item doesn't
                * supply a renew count (i.e. it's a Viewpoint or Arena catalogue).
                * Just hide the part of the ui that displays the renew count.*/
                itemInfoTextView.setVisibility(View.GONE);
                itemInfoTextView = (TextView) itemInfoDialog
                        .findViewById(R.id.item_info_renew_count_heading);
                itemInfoTextView.setVisibility(View.GONE);
                ImageView itemInfoRenewCountSeparator = (ImageView) itemInfoDialog
                        .findViewById(R.id.item_info_renew_count_separator);
                itemInfoRenewCountSeparator.setVisibility(View.GONE);

            } else {

                itemInfoTextView.setText(String.valueOf(timesRenewed));
            }

            /* If reminders are turned off for this account, hide the per-item remind
            * setting checkbox. */
            boolean accountRemind = dbFrag.getRemindSettingOfAccountBeingViewed();

            if (accountRemind) {

                /* (Reminders are on). */

                boolean itemRemindSetting = (cursor.getInt(5) == 1);

//                itemInfoDialogFragment.takeNoteThisIsItemRemindSettingInDb(itemRemindSetting);
//
//                if (dbFrag.hasUserChangedRemindSettingOfOpenItem()) {
//
//                    itemRemindSetting = dbFrag.getNewItemRemindSetting();
//                }

                if (itemRemindSetting) {

                    CheckBox checkBox = (CheckBox) itemInfoDialog.findViewById(R.id.item_info_remind_checkbox);
                    checkBox.setChecked(true);
                }
            } else {

                /* (Reminders are off). */

                View itemRemindCheckBoxLayout = itemInfoDialog
                        .findViewById(R.id.item_info_remind_linear_layout);
                itemRemindCheckBoxLayout.setVisibility(View.GONE);

                ImageView itemInfoRemindSeparator = (ImageView) itemInfoDialog
                        .findViewById(R.id.item_info_remind_separator);
                itemInfoRemindSeparator.setVisibility(View.GONE);

            }
        }
    }


}
