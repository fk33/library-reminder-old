/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.database.SQLException;
import android.database.sqlite.SQLiteStatement;

public class Api8To10DependentSQLiteStatementExecutor extends ApiDependentSQLiteStatementExecutor {

    @Override
    public int executeSQLiteStatement(SQLiteStatement statement) {

        try {
            statement.execute();
            return 1;
        } catch (SQLException sqlException) {
            return -1;
        }
    }
}
