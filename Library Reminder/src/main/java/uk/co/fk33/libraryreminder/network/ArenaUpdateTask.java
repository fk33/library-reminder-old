/*
 * Copyright (c) 2015 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArenaUpdateTask extends UpdateTask {

    public ArenaUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

         /* Connect to the library catalogue, get the list of user's loans */

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        String catUrl = mLibAccount.getCatUrl();
        String pinOrPassword = mLibAccount.getPinOrPassword();
        String loansUrl = catUrl + "protected/loans/";
        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        try {

            /* Try to connect to the catalogue. If the response is null for any reason, don't continue.*/
            publishProgress(mContext.getString(R.string.connecting_to_catalogue));
            catalogueResponse = Jsoup.connect(catUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            String sessionId = catalogueResponse.cookie("JSESSIONID");
            String guestLanguageId = catalogueResponse.cookie("GUEST_LANGUAGE_ID");
            String cookieSupport = catalogueResponse.cookie("COOKIE_SUPPORT");

            String cookies = "JSESSIONID=" + sessionId + "; ";
            cookies += "GUEST_LANGUAGE_ID=" + guestLanguageId + "; ";
            cookies += "COOKIE_SUPPORT=" + cookieSupport + ";";

            if (sessionId == null || guestLanguageId == null || cookieSupport == null) {
                mStatus = LibAccount.SYNC_FAILED_URL_ERROR;
                return null;
            }

            cataloguePage = catalogueResponse.parse();

            /* Parse out the url where the post data has to be sent (the "action" attribute
            of the login form). */
            Element signinElement = cataloguePage.select("div.arena-patron-signin").first();
            Element loginForm = signinElement.select("form[id^=id__patronLogin").first();
            String loginUrl = loginForm.attr("action");

            publishProgress(mContext.getString(R.string.sending_login_details));

            /* For logging in, we can't use JSoup. This is because some Arena catalogues use GoDaddy
            * certificates, which aren't recognised by Android. So we have to use a custom apache HttpClient
            * so we can set the SSLSocketFactory and set it to use a KeyStore bundled with the app,
            * which includes GoDaddy's root and intermediate certificates.*/
            DefaultHttpClient httpClient = new LibRemindHttpClient(mContext);
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, NetworkConstants.TIMEOUT_MILLIS);

            HttpPost httpPost = new HttpPost(loginUrl);
            httpPost.setHeader("Cookie", cookies);
            httpPost.setHeader("User-Agent", mContext.getString(R.string.user_agent));

            if (pinOrPassword.equals("")) {

                List<NameValuePair> postData = new ArrayList<NameValuePair>(1);
                postData.add(new BasicNameValuePair("fragmentCredentials:openTextUsernameContainer:openTextUsername",
                        mLibAccount.getCardNumber()));
                httpPost.setEntity(new UrlEncodedFormEntity(postData));
            } else {

                List<NameValuePair> postData = new ArrayList<NameValuePair>(2);
                postData.add(new BasicNameValuePair("fragmentCredentials:openTextUsernameContainer:openTextUsername",
                        mLibAccount.getCardNumber()));
                postData.add(new BasicNameValuePair("fragmentCredentials:textPassword",
                        pinOrPassword));
                httpPost.setEntity(new UrlEncodedFormEntity(postData));
            }

            HttpResponse httpResponse = httpClient.execute(httpPost);

            if (httpResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            HttpGet httpGet = new HttpGet(loansUrl);
            httpGet.setHeader("Cookie", cookies);
            httpGet.setHeader("User-Agent", mContext.getString(R.string.user_agent));
            httpResponse = httpClient.execute(httpGet);

            if (httpResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            cataloguePage = Jsoup.parse(httpResponse.getEntity().getContent(), "UTF-8", catUrl);

            /* Make sure that the login worked. If it didn't, return null. */
            Elements loginGreeting = cataloguePage.select("span.arena-login-greeting");
            if (loginGreeting.isEmpty()) {

                mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                return null;
            }

            publishProgress(mContext.getString(R.string.getting_list_of_loans));

            Element recordDetails;
            Element titleElement;
            DateFormat dateFormat = LocaleDependentClass.getArenaDateFormat();
            Date dueDate;

            String title;
            String titleLink;
            String author;
            String due;
            String standardIdentifier;
            String uniqueIdentifier;

            Elements loans = cataloguePage.select("div.arena-record");

            List<String> uniqueIdList = new ArrayList<String>();

            for (Element loan : loans) {

                recordDetails = loan.select("div.arena-record-details").first();
                titleElement = recordDetails.select("div.arena-record-title").first();
                title = titleElement.text();
                titleLink = titleElement.select("a").first().attr("href");
                standardIdentifier = titleLink.split("search_item_id=")[1];
                standardIdentifier = standardIdentifier.split("&")[0];

                author = recordDetails.select("div.arena-record-author").first()
                        .select("span.arena-value").first().text();

                Element loanDetails = loan.select("div.arena-loan-details").first();
                due = loanDetails.select("tr.arena-record-expire").first()
                        .select("span.arena-value").first().text();

                dueDate = dateFormat.parse(due);
                if (dueDate == null) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                long dueDateLong = dueDate.getTime();

                /* Generate a unique ID since Arena doesn't provide one. Just use the
                * standardIdentifier plus a serial number.*/
                uniqueIdentifier = standardIdentifier + "-1";
                int uniqueIdSerial = 1;

                while (uniqueIdList.contains(uniqueIdentifier)) {

                    uniqueIdSerial += 1;
                    uniqueIdentifier = uniqueIdentifier.substring(0, uniqueIdentifier.lastIndexOf("-"));
                    uniqueIdentifier = uniqueIdentifier + "-" + String.valueOf(uniqueIdSerial);
                }

                uniqueIdList.add(uniqueIdentifier);

                libItem = new LibItem(mLibAccount.getName(), title, author, uniqueIdentifier,
                        standardIdentifier, dueDateLong, -1, true);

                itemList.add(libItem);
            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            Log.i("dbug", "Got an ioException");
            Log.i("dbug", ioException.getClass().toString());

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;
        } catch (IndexOutOfBoundsException indexException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (ParseException parseException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (NullPointerException nullPointerException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (IllegalArgumentException illegalArgumentException) {

            mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
            return null;
        }

        return itemList;
    }
}
