/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.CursorLoader;

/*
Adapt CursorLoader to get cursors directly from database queries rather than from a "content provider"
Which we don't need since the data is only used internally to the app
*/
public class DbCursorLoader extends CursorLoader {

    String mTable;
    String[] mProjection;
    String mOrderBy;
    String mSelectionString;
    String[] mSelectionArgs;
    private LibRemindDbAdapter mDbAdapter;

    public DbCursorLoader(Context context) {
        super(context);

    }

    public DbCursorLoader(Context context, LibRemindDbAdapter dbAdapter, String table, String[] projection,
                          String selectionString, String[] selectionArgs, String orderBy) {

        super(context);

        mDbAdapter = dbAdapter;
        mTable = table;
        mProjection = projection;

        /* Note: selectionString mustn't include the WHERE keyword. These can all be null. */
        mSelectionString = selectionString;
        mSelectionArgs = selectionArgs;
        mOrderBy = orderBy;

    }

    @Override
    public Cursor loadInBackground() {

        Cursor cursor = mDbAdapter.getDatabase().query(mTable, mProjection, mSelectionString, mSelectionArgs,
                null, null, mOrderBy);

        if (cursor != null) {

			/* Ensure the cursor window is filled */
            cursor.getCount();
        }

        return cursor;
    }
}
