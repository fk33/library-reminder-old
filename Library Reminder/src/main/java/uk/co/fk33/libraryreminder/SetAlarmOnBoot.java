package uk.co.fk33.libraryreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Calendar;
import java.util.Random;
import java.util.TimeZone;

import uk.co.fk33.libraryreminder.compatibility.ApiDependentCodeFactory;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentPrefWriter;
import uk.co.fk33.libraryreminder.network.AutoSyncService;

public class SetAlarmOnBoot extends BroadcastReceiver {

    public final static int DAYTIME = 0;
    public final static int NIGHTTIME = 1;
    public final static long ONE_MINUTE = 1000 * 60;
    public final static long FIVE_MINUTES = ONE_MINUTE * 5;
    public final static long ONE_HOUR = ONE_MINUTE * 60;

    private long mCurrentTime;
    private int mCurrentHour;
    private int mCurrentMinute;
    private int mAlarmHour;
    private int mAlarmMinute;

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SyncManager syncManager = SyncManager.getInstance();

        /* Clear any saved state from a previous autosync run which may have been
        * interrupted by the device being switched off.*/
        ApiDependentPrefWriter apiDependentPrefWriter =
                ApiDependentCodeFactory.getApiDependentPrefWriter();
        apiDependentPrefWriter
                .writeIntPref(prefs, AutoSyncService.AUTO_SYNC_FAILED_ATTEMPTS, 0);
        apiDependentPrefWriter
                .writeIntPref(prefs, AutoSyncService.AUTO_SYNC_FAILED_ATTEMPTS_DUE_TO_APP_OPEN, 0);
        apiDependentPrefWriter.writeBooleanPref(prefs, AutoSyncService.CATCHUP_SYNC, false);

        /* Set the alarm for the next auto sync. (We won't have received the
        * boot_completed broadcast unless auto syncing is enabled, so no need
        * to check here whether it is enabled or not). To decide whether we need
        * to reschedule the next auto-sync to make up for any missed ones,
        * split the day into two periods: daytime (5am - 10pm), and nighttime
        * (10pm - 5am). If the regular auto-sync time is during the daytime, and
        * we are currently in the daytime period, and today's run has been missed
        * or didn't reach completion (i.e. the last completed autosync was 12 or
        * more hours ago and the next regular run is not due for more than 12 hours),
        * schedule a run in 5 minutes. If all those conditions apply but the next
        * run is in less than 12 hours, schedule at the normal time. If the regular
        * time is during the daytime, and it is currently nighttime, and the last
        * run was missed, and the regular run is later than 10am, set the next run
        * for a random time between 10am and 11am (it will be set back to normal
        * after the run). If the regular auto-sync run is during the nighttime,
        * schedule at the normal time, to avoid disturbing someone working a late
        * shift who might sleep at unusual times. */
        String alarmTimeString = prefs.getString(MainActivity.SYNC_TIME_PREF_KEY,
                context.getString(R.string.default_remind_time));
        String[] alarmTimeStringPieces = alarmTimeString.split(":");
        mAlarmHour = Integer.parseInt(alarmTimeStringPieces[0]);
        mAlarmMinute = Integer.parseInt(alarmTimeStringPieces[1]);

        DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());
        DateTime currentDate = new DateTime(System.currentTimeMillis(), dateTimeZone);
        mCurrentTime = currentDate.getMillis();
        mCurrentHour = currentDate.getHourOfDay();
        mCurrentMinute = currentDate.getMinuteOfHour();

        int alarmPeriod = findPeriod(mAlarmHour);
        int currentPeriod = findPeriod(mCurrentHour);

        if (alarmPeriod == DAYTIME) {

            long lastAutoSyncCompletionTime =
                    Long.parseLong(prefs.getString(AutoSyncService.LAST_AUTOSYNC_COMPLETION_TIME, "0"));

            int hoursSinceLastAutoSyncCompletion = (int) ((mCurrentTime - lastAutoSyncCompletionTime) / ONE_HOUR);

            if (currentPeriod == DAYTIME) {

                if (hoursSinceLastAutoSyncCompletion >= 12) {

                    if (hoursUntilRegularAlarm() > 12) {

                        syncManager.setNextSyncTime(context, 5);
                    } else {

                        /* There next regular sync will be within 12 hours so don't bother rescheduling.
                        * The reason for not running immediately here is that it's too far away from the
                        * user's regular sync time. If the next regular sync is within 12 hours, their
                        * regular sync time must have been missed by more than 12 hours. We don't
                        * want to do a sync too far away from their preferred time.
                        * findHoursBetween() checks if the scheduled sync time is within five minutes
                        * of the current time, so we don't need to worry about that. */
                        syncManager.setNextSyncTime(context, alarmTimeString);
                    }
                } else {

                    /* The last run wasn't missed, so just set alarm for the normal sync time. */
                    syncManager.setNextSyncTime(context, alarmTimeString);
                }
            } else {

                /* If the autosync is scheduled for the day time (i.e. after 5am and before
                10pm), and it's currently nighttime (i.e. after 10pm and before 5am), and it's
                been 21 or more hours since the last completion of an autosync run, and the
                regularly scheduled time is after 10am, we must have missed the preceding
                day's run, so schedule one for between 10am - 11am of the ensuing day. */
                if (hoursSinceLastAutoSyncCompletion > 20) {

                    if (mAlarmHour >= 10) {

                        Random random = new Random();
                        int minutes = random.nextInt(60);
                        String rescheduleAlarmTimeString = "10:" + String.valueOf(minutes);
                        syncManager.setNextSyncTime(context, rescheduleAlarmTimeString);
                    } else {

                        /* The regular autosync time is early in the morning, and it's night now,
                        * so just set it for the regular time (unless that would be in less than
                        * five minutes, in which case, set it for five minutes). It doesn't really
                        * make a difference whether the last autosync run was missed or not.*/
                        if (regularAlarmTimeIsInLessThanFiveMinutes()) {

                            syncManager.setNextSyncTime(context, 5);
                        } else {

                            syncManager.setNextSyncTime(context, alarmTimeString);
                        }
                    }
                } else {

                    /* The last run wasn't missed, so just set alarm for the normal sync time,
                    unless that would be within five minutes, in which case, set it for five
                    minutes. */
                    if (regularAlarmTimeIsInLessThanFiveMinutes()) {

                        syncManager.setNextSyncTime(context, 5);
                    } else {

                        syncManager.setNextSyncTime(context, alarmTimeString);
                    }
                }
            }
        } else {

            /* The alarm is set during the night, set it to the normal time, unless that would be
            within five minutes from now, in which case, set it to five minutes from now. */
            if (regularAlarmTimeIsInLessThanFiveMinutes()) {

                syncManager.setNextSyncTime(context, 5);
            } else {

                syncManager.setNextSyncTime(context, alarmTimeString);
            }
        }
    }

    private int findPeriod(int hour) {

        if (hour >= 5 && hour < 22) {

            return DAYTIME;
        }

        return NIGHTTIME;
    }

    private int hoursUntilRegularAlarm() {

        /* If the hours match, check the minutes. If we have reached or gone past the minute
        of the regularly scheduled sync, or it is in less than five minutes, return 24, to
        schedule an auto-sync task to run in 5 minutes. If we haven't reached the minute,
        and it is at least five minutes in the future, return 0, so the auto-sync task will
        be scheduled at the regular time. */
        if (mCurrentHour == mAlarmHour) {

            /* The alarm time is within the current hour. Check if it's five minutes or more
             * in the future */
            if (mCurrentMinute < (mAlarmMinute - 4)) {

                /* The regular alarm time is five or more minutes in the future, despite being within the
                current hour, so it's fine to schedule it for the regular time. Return 0, which, since
                it's less than 12, will cause the alarm to be scheduled at the regular time.*/
                return 0;
            } else {

                /* This will cause the auto-sync to be scheduled for five minutes from now, which we
                should do, as the regular alarm time is in less than five minutes. (Returning 24 mimics
                having just gone past the regular alarm time for today, as it implies that the next
                regular alarm time is not for 24 hours).*/
                return 24;
            }
        }

        /* If the alarm is set for the hour following this one, also check the minutes, in case,
        for example, the current time is 59 past the hour, and the alarm is set for zero minutes
        past the hour, meaning, if scheduled for the regular time, we would be setting it for a
        time very close to the current one, which could conceivably lead to it being missed,
        for example if we are only a few milliseconds away from the regular time. It would be
        better to set it for at least five minutes in the future. */
        if (mCurrentHour == (mAlarmHour - 1)) {

            if (((60 - mCurrentMinute) + mAlarmMinute) > 4) {

                /* This will cause the auto-sync to be scheduled at the regular alarm time, since the
                regular alarm time is at least five minutes in the future.*/
                return 0;
            } else {

                /* This will cause the auto-sync to be scheduled for five minutes from now, which we
                should do, as the regular alarm time is in less than five minutes. (Returning 24 mimics
                having just gone past the regular alarm time for today, as it implies that the next
                regular alarm time is not for 24 hours).*/
                return 24;
            }
        }

        /* The current hour is neither the same as the alarm hour, nor is it one less. If we have not
        reached the alarm hour, subtract the current hour from the alarm hour to find out how many hours
        from now it is. If we have gone past the alarm hour for today, subtract the difference from 24,
        which gives the number of hours in the future of an hour of the day on the other side of midnight. */
        if (mCurrentHour < mAlarmHour) {

            return mAlarmHour - mCurrentHour;
        } else {

            return 24 - (mCurrentHour - mAlarmHour);
        }
    }

    private boolean regularAlarmTimeIsInLessThanFiveMinutes() {

        Calendar alarmTimeCalendar = Calendar.getInstance(LocaleDependentClass.getDateLocale());
        alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, mAlarmHour);
        alarmTimeCalendar.set(Calendar.MINUTE, mAlarmMinute);
        long alarmTime = alarmTimeCalendar.getTimeInMillis();

        if (alarmTime < mCurrentTime) {

            alarmTimeCalendar.add(Calendar.DAY_OF_YEAR, 1);
            alarmTime = alarmTimeCalendar.getTimeInMillis();
        }

        return (alarmTime - mCurrentTime) < FIVE_MINUTES;
    }
}
