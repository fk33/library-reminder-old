/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

public class ViewpointUpdateTask extends UpdateTask {

    public ViewpointUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

         /* Connect to the library catalogue, get the list of user's loans */

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        String catUrl = mLibAccount.getCatUrl();
        String pinOrPassword = mLibAccount.getPinOrPassword();
        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        String loginUrl = catUrl + "00_002_login.aspx";
        String loansUrl = catUrl + "01_YourAccount/01_002_YourLoans.aspx";

        try {

            /* Try to connect to the catalogue. If the response is null for any reason, don't continue.*/
            publishProgress(mContext.getString(R.string.connecting_to_catalogue));
            catalogueResponse = Jsoup.connect(loginUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            /* If we don't get a session cookie the URL might be wrong */
            String sessionId = catalogueResponse.cookie("ASP.NET_SessionId");
            if (sessionId == null) {

                mStatus = LibAccount.SYNC_FAILED_URL_ERROR;
                return null;
            }

            /* These Asp.Net variables are in the page source. We need to send their values with the POST request.
            Some of them have empty values, so Jsoup's val() method returns null. But we should still send them as
            empty strings. Therefore initialise them to empty strings, then if val() is not null, set them to the
            value it returns. */
            String eventTargetValue = "";
            String eventArgumentValue = "";
            String viewStateValue = "";
            String eventValidationValue = "";
            String loginButtonValue = "";

            cataloguePage = catalogueResponse.parse();
            Element eventTargetElement = cataloguePage.getElementById("__EVENTTARGET");
            Element eventArgumentElement = cataloguePage.getElementById("__EVENTARGUMENT");
            Element viewStateElement = cataloguePage.getElementById("__VIEWSTATE");
            Element eventValidationElement = cataloguePage.getElementById("__EVENTVALIDATION");
            Element loginButtonElement = cataloguePage
                    .getElementById("ctl00_ContentPlaceCenterContent_login2_LoginButton");

            /* Check the elements were found. If so, and they have non-empty values, put the values in the
            appropriate strings. */
            if (eventTargetElement != null) {
                if (eventTargetElement.val() != null) {
                    eventTargetValue = eventTargetElement.val();
                }
            }
            if (eventArgumentElement != null) {
                if (eventArgumentElement.val() != null) {
                    eventArgumentValue = eventArgumentElement.val();
                }
            }
            if (viewStateElement != null) {
                if (viewStateElement.val() != null) {
                    viewStateValue = viewStateElement.val();
                }
            }
            if (eventValidationElement != null) {
                if (eventValidationElement.val() != null) {
                    eventValidationValue = eventValidationElement.val();
                }
            }
            if (loginButtonElement != null) {
                if (loginButtonElement.val() != null) {
                    loginButtonValue = loginButtonElement.val();
                }
            }

            /* Log in with credentials and get the login cookie (the cookie called "viewpoint") */
            publishProgress(mContext.getString(R.string.sending_login_details));

            Connection connection = Jsoup.connect(loginUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .followRedirects(false)
                    .cookie("ASP.NET_SessionId", sessionId)
                    .method(Connection.Method.POST);

            if (pinOrPassword.equals("")) {

                catalogueResponse = connection.data("__EVENTTARGET", eventTargetValue,
                        "__EVENTARGUMENT", eventArgumentValue,
                        "__VIEWSTATE", viewStateValue,
                        "__EVENTVALIDATION", eventValidationValue,
                        "ctl00$ContentPlaceCenterContent$login2$username", mLibAccount.getCardNumber(),
                        "ctl00$ContentPlaceCenterContent$login2$LoginButton", loginButtonValue)
                        .execute();
            } else {

                catalogueResponse = connection.data("__EVENTTARGET", eventTargetValue,
                        "__EVENTARGUMENT", eventArgumentValue,
                        "__VIEWSTATE", viewStateValue,
                        "__EVENTVALIDATION", eventValidationValue,
                        "ctl00$ContentPlaceCenterContent$login2$username", mLibAccount.getCardNumber(),
                        "ctl00$ContentPlaceCenterContent$login2$password", mLibAccount.getPinOrPassword(),
                        "ctl00$ContentPlaceCenterContent$login2$LoginButton", loginButtonValue)
                        .execute();

            }

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            String viewpointCookie = catalogueResponse.cookie("viewpoint");
            if (viewpointCookie == null) {
                mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                return null;
            }

            /* Request list of loans, sending session cookie and login cookie */
            publishProgress(mContext.getString(R.string.getting_loan_page));
            catalogueResponse = Jsoup.connect(loansUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .cookie("ASP.NET_SessionId", sessionId)
                    .cookie("viewpoint", viewpointCookie)
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            publishProgress(mContext.getString(R.string.getting_list_of_loans));
            cataloguePage = catalogueResponse.parse();

            DateFormat dateFormat = LocaleDependentClass.getViewpointDateFormat();
            Date dueDate;

            /* We should now have the page listing the user's current loans. Parse out the
            information about each loan, if there are any. */

            Elements itemRows = cataloguePage.select("div.TitleListResultsItemContainerStyle4");

            List<String> uniqueIdList = new ArrayList<String>();
            String uniqueIdentifier;

            for (Element itemRow : itemRows) {

                Element titleLink = itemRow.select("a").get(0);
                String title = titleLink.text();
                String standardIdentifier = titleLink.attr("title");

                String author = "";

                Elements itemElements = titleLink.parent().parent().select("h2");

                if (itemElements.size() > 1 ) {

                    author = itemElements.get(1).text();
                }

                /* Remove the word "By" from the start of the author string */
                if (author.startsWith("By ")) {
                    author = author.substring(3, author.length());
                }

                /* Find the due date by searching the text of the row. Use lastIndexOf, just in
                case the phrase "Due Date:" appears in an item title (There is a film called
                Due Date but it doesn't have a colon in the name, however they could make a
                sequel). */
                String itemRowText = itemRow.text().toLowerCase(LocaleDependentClass.getLanguageLocale());
                int dueDateIndex = itemRowText.lastIndexOf("due date:") + 9;

                String dueDateString = itemRowText.substring(dueDateIndex, dueDateIndex + 8);

                /* There is no space between the colon and the date, but this is just in case there
                ever is one. (There wouldn't be more than 1 as Jsoup strips out excess whitespace
                when returning text). */
                if (dueDateString.startsWith(" ")) {

                    dueDateString = itemRowText.substring(dueDateIndex + 1, dueDateIndex + 9);
                }

                try {

                    dueDate = dateFormat.parse(dueDateString);
                } catch (IllegalArgumentException illegalArgumentException) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                if (dueDate ==  null) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                long dueDateLong = dueDate.getTime();

                /* Generate a unique ID since Viewpoint doesn't always provide one (it only gives
                * one for items which are renewable). */
                uniqueIdentifier = standardIdentifier + "-1";
                int uniqueIdSerial = 1;

                while (uniqueIdList.contains(uniqueIdentifier)) {

                    uniqueIdSerial +=1;
                    uniqueIdentifier = uniqueIdentifier.substring(0, uniqueIdentifier.lastIndexOf("-"));
                    uniqueIdentifier = uniqueIdentifier + "-" + String.valueOf(uniqueIdSerial);
                }

                uniqueIdList.add(uniqueIdentifier);

                /* Put the information into item objects and put them in the item list. Viewpoint
                doesn't tell us times renewed so just put 0. We will keep our own count. */
                libItem = new LibItem(mLibAccount.getName(), title, author, uniqueIdentifier,
                        standardIdentifier, dueDateLong, -1, true);

                itemList.add(libItem);
            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;
        } catch (IndexOutOfBoundsException indexException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (ParseException parseException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }  catch (NullPointerException nullPointerException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
