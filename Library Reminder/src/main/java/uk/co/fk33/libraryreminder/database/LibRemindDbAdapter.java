/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.support.v4.content.CursorLoader;

import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.LibOrganisation;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentCodeFactory;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentSQLiteStatementExecutor;

import java.util.List;

public class LibRemindDbAdapter {

    public static final String KEY_ROWID = "_id";

    /* Columns for the accounts db table */
    public static final String KEY_ACCOUNTS_TABLE = "accounts";
    public static final String KEY_NAME = "name";
    public static final String KEY_ORGANISATION = "organisation";
    public static final String KEY_CAT_TYPE = "cat_type";
    public static final String KEY_CAT_URL = "cat_url";
    public static final String KEY_CARD_NUMBER = "card_number";
    public static final String KEY_PIN_OR_PASSWORD = "pin_or_password";
    public static final String KEY_SUMMARY = "summary";
    public static final String KEY_LAST_SYNCED = "last_synced";
    public static final String KEY_FIRST_ITEM_DUE = "first_item_due";
    public static final String KEY_REMIND = "remind";
    public static final String KEY_REGION = "region";

    /* Columns for the items db table */
    public static final String KEY_ITEMS_TABLE = "items";
    public static final String KEY_ACCOUNT = "account";
    public static final String KEY_TITLE = "title";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_UNIQUE_IDENTIFIER = "unique_identifier";
    public static final String KEY_STANDARD_IDENTIFIER = "standard_identifier";
    public static final String KEY_DUE_DATE = "due_date";
    public static final String KEY_TIMES_RENEWED = "times_renewed";

    public static final String ORDER_BY_NAME = KEY_NAME + " COLLATE NOCASE ASC";
    public static final String ORDER_BY_FIRST_ITEM_DUE = KEY_FIRST_ITEM_DUE + " ASC";

    private DatabaseHandler mDatabaseHandler;
    private SQLiteDatabase mDatabase;

    private static final String DATABASE_CREATE_ACCOUNTS_TABLE = "create table accounts " +
            "(_id integer primary key autoincrement, name text not null, organisation text not null, " +
            "cat_type text not null, cat_url text not null, card_number text not null, " +
            "pin_or_password text not null, summary text not null, last_synced integer not null, " +
            "first_item_due integer not null, remind integer not null, region integer not null);";

    private static final String DATABASE_CREATE_ITEMS_TABLE = "create table items " +
            "(_id integer primary key autoincrement, account text not null, title text not null, " +
            "author text not null, unique_identifier text not null, standard_identifier text not null, " +
            "due_date integer not null, times_renewed integer not null, remind integer not null);";

    private static final String DATABASE_NAME = "libreminderdb";
    public static final String DATABASE_ACCOUNTS_TABLE = "accounts";
    public static final String DATABASE_ITEMS_TABLE = "items";
    private static final int DATABASE_VERSION = 2;
    private final Context mContext;

    private static class DatabaseHandler extends SQLiteOpenHelper {

        DatabaseHandler(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase database) {

            database.execSQL(DATABASE_CREATE_ACCOUNTS_TABLE);
            database.execSQL(DATABASE_CREATE_ITEMS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

            /*Currently no modifications to be made on upgrade*/
        }
    }

    public LibRemindDbAdapter(Context context) {

        mContext = context;
    }

    public LibRemindDbAdapter open() throws SQLException {

        if (mDatabaseHandler == null) {

            mDatabaseHandler = new DatabaseHandler(mContext);
        }

        mDatabase = mDatabaseHandler.getWritableDatabase();
        return this;
    }

    public void close() {

        mDatabaseHandler.close();
    }

    /* Returns rowID for new item record, -1 if record insertion fails */
    public synchronized long addItemRecord(String account, String title, int due_date, int times_renewed) {

        ContentValues itemValues = new ContentValues();
        itemValues.put(KEY_ACCOUNT, account);
        itemValues.put(KEY_TITLE, title);
        itemValues.put(KEY_DUE_DATE, due_date);
        itemValues.put(KEY_TIMES_RENEWED, times_renewed);

        return mDatabase.insert(DATABASE_ITEMS_TABLE, null, itemValues);
    }

    public synchronized boolean addItems(List<LibItem> itemList) {

        for (LibItem item : itemList) {

            if (mDatabase.insert(DATABASE_ITEMS_TABLE, null, item.getContentValues()) < 0)
                return false;
        }

        return true;
    }

    /* Returns rowID for new account record, -1 if record insertion fails.
    (PIN is encrypted prior to being passed to this method). */
    public synchronized long addAccountRecord(String name, LibOrganisation libOrganisation, String cardNumber,
                                              String pinOrPassword, boolean remind, int region) {

        int remindInt = 0;
        if (remind) remindInt = 1;

        ContentValues accountValues = new ContentValues();
        accountValues.put(KEY_NAME, name);
        accountValues.put(KEY_ORGANISATION, libOrganisation.getName());
        accountValues.put(KEY_CAT_TYPE, libOrganisation.getCatType());
        accountValues.put(KEY_CAT_URL, libOrganisation.getCatUrl());
        accountValues.put(KEY_CARD_NUMBER, cardNumber);
        accountValues.put(KEY_PIN_OR_PASSWORD, pinOrPassword);
        accountValues.put(KEY_SUMMARY, mContext.getString(R.string.no_data));
        accountValues.put(KEY_LAST_SYNCED, LibAccount.SYNC_NEVER_ATTEMPTED);
        accountValues.put(KEY_FIRST_ITEM_DUE, 0);
        accountValues.put(KEY_REMIND, remindInt);
        accountValues.put(KEY_REGION, region);

        return mDatabase.insert(DATABASE_ACCOUNTS_TABLE, null, accountValues);
    }

    /* (PIN is encrypted prior to being passed to this method). */
    public synchronized int editAccount(String name, LibOrganisation libOrganisation,
                                        String cardNumber, String pinOrPassword, boolean remind) {

        int remindInt = 0;
        if (remind) remindInt = 1;

        ContentValues newValues = new ContentValues();
        newValues.put(KEY_ORGANISATION, libOrganisation.getName());
        newValues.put(KEY_CAT_TYPE, libOrganisation.getCatType());
        newValues.put(KEY_CAT_URL, libOrganisation.getCatUrl());
        newValues.put(KEY_CARD_NUMBER, cardNumber);
        newValues.put(KEY_PIN_OR_PASSWORD, pinOrPassword);
        newValues.put(KEY_REMIND, remindInt);

        String whereClause = KEY_NAME + "=?";

        return mDatabase.update(KEY_ACCOUNTS_TABLE, newValues, whereClause, new String[]{name});
    }

    /* (PIN is encrypted prior to being passed to this method). */
    public synchronized int editAccount(String oldName, String newName, LibOrganisation libOrganisation,
                                        String cardNumber, String pinOrPassword, boolean remind) {

        int remindInt = 0;
        if (remind) remindInt = 1;

        ContentValues newValues = new ContentValues();
        newValues.put(KEY_NAME, newName);
        newValues.put(KEY_ORGANISATION, libOrganisation.getName());
        newValues.put(KEY_CAT_TYPE, libOrganisation.getCatType());
        newValues.put(KEY_CAT_URL, libOrganisation.getCatUrl());
        newValues.put(KEY_CARD_NUMBER, cardNumber);
        newValues.put(KEY_PIN_OR_PASSWORD, pinOrPassword);
        newValues.put(KEY_REMIND, remindInt);

        String whereClause = KEY_NAME + "=?";

        return mDatabase.update(KEY_ACCOUNTS_TABLE, newValues, whereClause,
                new String[] {oldName});
    }

    public synchronized int renameAccount(String oldName, String newName) {

        ContentValues newNameValue = new ContentValues();
        newNameValue.put(KEY_NAME, newName);

        String whereClause = KEY_NAME + "=?";

        return mDatabase.update(KEY_ACCOUNTS_TABLE, newNameValue, whereClause, new String[]{oldName});
    }

    public synchronized int renameAccountChangeReminders(String oldName, String newName,
                                                         boolean remind) {

        int remindInt = 0;
        if (remind) remindInt = 1;

        ContentValues newValues = new ContentValues();
        newValues.put(KEY_NAME, newName);
        newValues.put(KEY_REMIND, remindInt);

        String whereClause = KEY_NAME + "=?";

        return mDatabase.update(KEY_ACCOUNTS_TABLE, newValues, whereClause, new String[] {oldName});
    }

    public synchronized int changeReminderSetting(String name, boolean remind) {

        int remindInt = 0;
        if (remind) remindInt = 1;

        ContentValues newValue = new ContentValues();
        newValue.put(KEY_REMIND, remindInt);

        String whereClause = KEY_NAME + "=?";

        return mDatabase.update(KEY_ACCOUNTS_TABLE, newValue, whereClause, new String[] {name});
    }

    public synchronized int changeNameOfAccountForItems(String oldName, String newName) {

        ContentValues newValue = new ContentValues();
        newValue.put(KEY_ACCOUNT, newName);

        String whereClause = KEY_ACCOUNT + "=?";

        return mDatabase.update(KEY_ITEMS_TABLE, newValue, whereClause,
                new String[] {oldName});
    }

    public synchronized int deleteAccounts(String[] accountsToDelete) {

        /*
        Construct an SQL delete statement (as a String) with a number of terms equivalent to
        number of acts to delete.
        */
        int numberOfAccountsToDelete = accountsToDelete.length;
        String deleteString = "DELETE FROM " + DATABASE_ACCOUNTS_TABLE + " WHERE " + KEY_NAME + "=?";
        if (numberOfAccountsToDelete > 1) {

            for (int i = 1; i < numberOfAccountsToDelete; i++) {

                deleteString = deleteString + " OR " + KEY_NAME + "=?";
            }
        }

        /* Compile the String created above into an SQLiteStatement and bind the account names as args */
        SQLiteStatement deleteStatement = mDatabase.compileStatement(deleteString);
        for (int i = 0; i < numberOfAccountsToDelete; i++) {

            deleteStatement.bindString(i + 1, accountsToDelete[i]);
        }

        /*
        Can't use SQLiteStatment.executeUpdateDelete on APIs before 11, so get code which will use different
        calls depending on API. On older APIs execute is used.
        */
        ApiDependentSQLiteStatementExecutor executor = ApiDependentCodeFactory.getApiDependentSQLiteStatementExecutor();
        return executor.executeSQLiteStatement(deleteStatement);
    }

    public synchronized int updateRecords(DataToWrite dataToWrite) {

        ContentValues[] newValues = dataToWrite.getContentValues();
        long[] rowIds = dataToWrite.getRowIds();
        int updateSize = dataToWrite.size();
        String whereClause = KEY_ROWID + "=?";
        int result = 0;

        /* We could be updating multiple rows, so iterate through the rowIds array to update each one */
        for (int i = 0; i < updateSize; i++) {

            /* The database update method returns the no. of rows affected, we can add it to total, to return */
            result = result + mDatabase.update(dataToWrite.table(), newValues[i], whereClause,
                    new String[]{String.valueOf(rowIds[i])});
        }

        return result;
    }

    public synchronized int changeRemindForItem(String uniqueIdentifier, boolean remind) {

        ContentValues newValues = new ContentValues();
        newValues.put(KEY_REMIND, remind);

        String whereClause = KEY_UNIQUE_IDENTIFIER + "=?";

        return mDatabase.update(KEY_ITEMS_TABLE, newValues, whereClause,
                new String[] { uniqueIdentifier});
    }

    /**
     * Return a cursor over LibItems in items table belonging to the specified account.
     * Only return uniqueID, times renewed and reminder setting as these are all we need
     * to check whether the items are still on loan when syncing the account and to copy
     * over information which is not supplied by the catalogue (or not always supplied).
     *
     * @param accountName The account to get the items for
     * @return the cursor pointing to the results of the db query (items belonging to the account).
     */
    public synchronized Cursor getItemsForAccount(String accountName) {

        String[] columns = new String[] { KEY_UNIQUE_IDENTIFIER, KEY_TIMES_RENEWED, KEY_REMIND};
        String selection = KEY_ACCOUNT + "=?";
        String[] selectionArgs = new String[] { accountName };

        return mDatabase.query(KEY_ITEMS_TABLE, columns, selection, selectionArgs, null,
                null, null);
    }

    public synchronized Cursor getAllAccounts() {

        String[] columns = new String[] {KEY_ROWID, KEY_NAME, KEY_CAT_TYPE, KEY_CAT_URL, KEY_CARD_NUMBER,
                KEY_PIN_OR_PASSWORD, KEY_REMIND};

        return mDatabase.query(KEY_ACCOUNTS_TABLE, columns, null, null, null, null, null);
    }

    public synchronized Cursor getAccountsWithSyncProblems() {

        String[] columns = new String[] {KEY_ROWID, KEY_NAME, KEY_CAT_TYPE, KEY_CAT_URL, KEY_CARD_NUMBER,
                KEY_PIN_OR_PASSWORD, KEY_REMIND};
        String selection = KEY_LAST_SYNCED + "<?";
        String[] selectionArgs = new String[] {"0"};

        return mDatabase.query(KEY_ACCOUNTS_TABLE, columns, selection, selectionArgs, null, null, null);
    }

    public synchronized Cursor getAccountDetails(String accountName) {

        String[] columns = new String[] { KEY_NAME, KEY_ORGANISATION, KEY_CARD_NUMBER,
                KEY_PIN_OR_PASSWORD, KEY_REMIND, KEY_REGION};
        String selection = KEY_NAME + "=?";
        String[] selectionArgs = new String[] { accountName };

        return mDatabase.query(KEY_ACCOUNTS_TABLE, columns, selection, selectionArgs, null,
                null, null);
    }

    public synchronized Cursor getItemDetails(String uniqueIdentifier) {

        String[] columns = new String[] { KEY_TITLE, KEY_AUTHOR, KEY_UNIQUE_IDENTIFIER,
                KEY_DUE_DATE, KEY_TIMES_RENEWED, KEY_REMIND };
        String selection = KEY_UNIQUE_IDENTIFIER + "=?";
        String[] selectionArgs = new String[] { uniqueIdentifier };

        return mDatabase.query(KEY_ITEMS_TABLE, columns, selection, selectionArgs, null,
                null, null);
    }

    public synchronized int deleteAllItemsForAccount(String accountName) {

        String deleteString = "DELETE FROM " + DATABASE_ITEMS_TABLE + " WHERE " + KEY_ACCOUNT + "=?";
        SQLiteStatement deleteStatement = mDatabase.compileStatement(deleteString);
        deleteStatement.bindString(1, accountName);

        /* Can't use SQLiteStatment.executeUpdateDelete on APIs before 11, so get code which will use different
        calls depending on API. On older APIs execute is used. */
        ApiDependentSQLiteStatementExecutor executor = ApiDependentCodeFactory.getApiDependentSQLiteStatementExecutor();

        /* Wrapper for SQLiteStatment methods execute and executeUpdateDelete (depending on API level), returns number
        of rows affected by statement, or -1 if SQLException caught */
        return executor.executeSQLiteStatement(deleteStatement);
    }

    public synchronized boolean deleteItem(long rowId) {

        return mDatabase.delete(DATABASE_ITEMS_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public synchronized SQLiteDatabase getDatabase() {

        boolean needToInitDb = false;

        if (mDatabase == null) {

            needToInitDb = true;
        } else {

            if (!mDatabase.isOpen()) {

                needToInitDb = true;
            }
        }

        if (needToInitDb) {

            if (mDatabaseHandler == null) {

                mDatabaseHandler = new DatabaseHandler(mContext);
            }

            mDatabase = mDatabaseHandler.getWritableDatabase();
        }

        return mDatabase;
    }

    public synchronized CursorLoader createCursorLoader(
            String table, String[] columns, String selectionString, String[] selectionArgs,
            String orderBy) {

        return new DbCursorLoader(mContext, this, table, columns, selectionString, selectionArgs, orderBy);
    }
}
