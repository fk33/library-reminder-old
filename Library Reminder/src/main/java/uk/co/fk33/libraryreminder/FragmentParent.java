/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

public interface FragmentParent {

    /*
    This interface only includes one method as it's the only method
    common to both of the fragment classes which define interfaces
    which extend this one. Classes which implement the interfaces
    can then be treated as specific implementations, or general
    ones. (This design was more useful at the beginning of the project
    before ActionBarSherlock was introduced).
     */

    public void updateAccountListActionModeTitle();

    public void updateViewAccountActionModeTitle();

}