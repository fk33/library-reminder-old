/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.database;

import android.content.ContentValues;

public class DataToWriteOnSyncFailed extends DataToWrite {

    private long[] mNewLastSyncedDates;

    public DataToWriteOnSyncFailed(long[] rowIds, long[] newLastSyncedDates) {

        super(rowIds);

        if (newLastSyncedDates.length != mSize) {
            throw new IllegalArgumentException("DataToWrite: The newLastSyncedDates array must be the same size as the" +
                    "rowIds array.");
        }

        mNewLastSyncedDates = newLastSyncedDates;
        mTable = LibRemindDbAdapter.DATABASE_ACCOUNTS_TABLE;

    }

    /**
     * Put the values of each element from the member arrays into a ContentValues object in an array of ContentValues
     * objects. Set the key for each value to be the column to be written to in the db.
     *
     * @return The array of ContentValues instances, one for each db row to be updated
     */
    @Override
    public ContentValues[] getContentValues() {

        ContentValues[] newValues = new ContentValues[mSize];

        for (int i = 0; i < mSize; i++) {
            newValues[i] = new ContentValues();
            newValues[i].put(LibRemindDbAdapter.KEY_LAST_SYNCED, mNewLastSyncedDates[i]);
        }
        return newValues;
    }
}
