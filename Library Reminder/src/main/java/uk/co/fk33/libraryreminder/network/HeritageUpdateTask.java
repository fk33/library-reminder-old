package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import android.util.Log;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.HeritageBase64Encoder;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HeritageUpdateTask extends UpdateTask {

    public HeritageUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        /* Connect to the library catalogue, get the list of user's loans */

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        Connection.Response catalogueResponse;
        List<String> loanDetails = new ArrayList<String>();
        List<LibItem> itemList = new ArrayList<LibItem>();

        String loginUrl = mLibAccount.getCatUrl();
        String pinOrPassword = mLibAccount.getPinOrPassword();
        String renewUrl = loginUrl + "renewall";
        String sessionId;

        try {

            publishProgress(mContext.getString(R.string.sending_login_details));

            if (pinOrPassword.equals("")) {

                catalogueResponse = Jsoup.connect(loginUrl)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .method(Connection.Method.POST)
                        .data("LoginName", mLibAccount.getCardNumber(),
                                "Ajax", "1")
                        .execute();
            } else {

                String base64Pin = new String(HeritageBase64Encoder.encodeBase64(pinOrPassword.getBytes()));
                String urlBase64Pin = new URLCodec().encode(base64Pin);

                catalogueResponse = Jsoup.connect(loginUrl)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .data("LoginName", mLibAccount.getCardNumber(),
                                "Password", "",
                                "base64password", urlBase64Pin,
                                "Ajax", "1")
                        .method(Connection.Method.POST)
                        .execute();
            }

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            if (catalogueResponse.parse().text().contains("\"result\": false")) {

                mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                return null;
            }

            sessionId = catalogueResponse.cookie("SessionID");

            publishProgress(mContext.getString(R.string.getting_list_of_loans));

            catalogueResponse = Jsoup.connect(renewUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .cookie("SessionID", sessionId)
                    .ignoreContentType(true)
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            BufferedReader reader = new BufferedReader(new StringReader(catalogueResponse.body()));
            boolean foundBody = false;
            String line;

            while ((line = reader.readLine()) != null) {

                if (foundBody) {

                    loanDetails.add(line);
                } else {

                    if (line.contains("\"body\":")) {

                        /* If the "body:" line ends with [] that means there are no loans. */
                        if (line.contains("[]")) {

                            break;
                        }

                        foundBody = true;
                    }
                }
            }

            int loanDetailsSize = loanDetails.size();

            if (loanDetailsSize == 0) {

                mStatus = LibAccount.SYNC_SUCCEEDED;
                return itemList;
            }

	        /* Remove the last 3 items and the first one. This works even though the loanDetails list
            * gets shorter as each item is removed, because loanDetailsSize stays the same, so each call
	        * removes the last item as the list shrinks (rather than the list staying the same size
	        * and each call removing the item at the last position minus 1, 2 and 3.*/
            loanDetails.remove(loanDetailsSize - 1);
            loanDetails.remove(loanDetailsSize - 2);
            loanDetails.remove(loanDetailsSize - 3);
            loanDetails.remove(0);

            String uniqueIdentifier;
            String standardIdentifier;
            String author;
            String title;
            String renewCountString;
            String dueDateString;
            int renewCount;
            Date dueDate;

            LibItem libItem;

            for (String loanLine : loanDetails) {

                loanLine = loanLine.substring(loanLine.indexOf("[") + 2);
                String[] loanLineParts = loanLine.split("\",\"");
                uniqueIdentifier = loanLineParts[0];
                standardIdentifier = loanLineParts[1];
                author = loanLineParts[2];
                title = loanLineParts[3];
                dueDateString = loanLineParts[5];

                DateFormat dateFormat = LocaleDependentClass.getHeritageDateFormat();
                dueDate = dateFormat.parse(dueDateString);

                if (dueDate == null) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                long dueDateLong = dueDate.getTime();

                renewCountString = loanLineParts[6];
                String[] renewCountStringParts = renewCountString.split(" ");
                renewCount = Integer.valueOf(renewCountStringParts[0]);

                libItem = new LibItem(mLibAccount.getName(), title, author, uniqueIdentifier, standardIdentifier,
                        dueDateLong, renewCount, true);

                itemList.add(libItem);
            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }

                return null;
            }
        } catch (EncoderException encoderException) {

            mStatus = LibAccount.SYNC_FAILED_ENCODER_EXCEPTION;
            return null;
        } catch (IllegalArgumentException illegalArgumentException) {

            mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
            return null;
        } catch (ParseException parseException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
