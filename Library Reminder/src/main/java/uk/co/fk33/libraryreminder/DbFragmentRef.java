/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class DbFragmentRef {

    private static DbInteractionsFragment mDbInteractionsFragment = null;

    public static void set (DbInteractionsFragment dbInteractionsFragment) {

        mDbInteractionsFragment = dbInteractionsFragment;
    }

    public static DbInteractionsFragment get () {

        return mDbInteractionsFragment;
    }
}
