/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Locale;

import uk.co.fk33.libraryreminder.MainActivity;

public class LocaleDependentClass {

    /* UK-specific methods and variables. */

    public static final int ALARM_ID = 20;
    public static final int ACTION_NEEDED_NOTIFICATION_ID = 1;
    public static final int LOCALE = MainActivity.LOCALE_UK;
    private static final Locale mDateLocale = Locale.UK;
    private static final Locale mLanguageLocale = Locale.ENGLISH;

    public static Locale getDateLocale() {

        return mDateLocale;
    }

    public static Locale getLanguageLocale() {

        return mLanguageLocale;
    }

    public static SimpleDateFormat getItemInfoDueDateFormat() {

        return new SimpleDateFormat("dd/MM/yy", mDateLocale);
    }

    public static SimpleDateFormat getArenaDateFormat() {

        return new SimpleDateFormat("dd/MM/yy", mDateLocale);
    }

    public static SimpleDateFormat getElibraryDateFormat() {

        return new SimpleDateFormat("dd/MM/yyyy", mDateLocale);
    }

    public static SimpleDateFormat getHeritageDateFormat() {

        return new SimpleDateFormat("dd MMM yyyy", mDateLocale);
    }

    public static SimpleDateFormat getPrism2DateFormat() {

        return new SimpleDateFormat("dd/MM/yyyy", mDateLocale);
    }

    public static SimpleDateFormat getViewpointDateFormat() {

        return new SimpleDateFormat("dd/MM/yy", mDateLocale);
    }

    public static SimpleDateFormat getPortfolioDateFormat() {

        return new SimpleDateFormat("dd/MM/yy", mDateLocale);
    }

    public static DateTimeFormatter getPrism3DateTimeFormatter() {

        return DateTimeFormat.forPattern("dd MMM yyyy");
    }

    public static DateTimeFormatter getSpydusDateTimeFormatter() {

        return DateTimeFormat.forPattern("dd MMM yyyy");
    }
}