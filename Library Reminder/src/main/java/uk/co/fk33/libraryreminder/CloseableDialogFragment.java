/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;

public abstract class CloseableDialogFragment extends DialogFragment {

    /* All the dialogfragments in the app extend this class, ensuring they can
    * all be dismissed (losing state) with a broadcast. This is used when
    * responding to the user clicking a notification. We can't have multiple
    * instances of the main activity because we can't have multipled instances
    * of the dbinteractions frag (this causes crashes). So if the app is open
    * when the user taps a notification, we have to bring it to the front, clear
    * any fragments from it, and show the account relating to the notification.*/

    protected BroadcastReceiver mMessageReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                dismissAllowingStateLoss();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter(MainActivity.CLOSE_ALL_FRAGMENTS));

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }
}
