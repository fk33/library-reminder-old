package uk.co.fk33.libraryreminder.network;

import android.app.*;
import android.content.*;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import uk.co.fk33.libraryreminder.*;
import uk.co.fk33.libraryreminder.accounts.LibAccount;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentCodeFactory;
import uk.co.fk33.libraryreminder.compatibility.ApiDependentPrefWriter;
import uk.co.fk33.libraryreminder.database.AsyncDbWrite;
import uk.co.fk33.libraryreminder.database.DataToWrite;
import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;
import uk.co.fk33.libraryreminder.database.LibRemindDbAdapter;

import java.util.*;

public class AutoSyncService extends Service implements AsyncDbWrite.CallingService {

    public static final String AUTO_SYNC_FAILED_ATTEMPTS = "auto_sync_attempts";
    public static final String CATCHUP_SYNC = "catchup_sync";
    public static final String AUTO_SYNC_FAILED_ATTEMPTS_DUE_TO_APP_OPEN = "auto_sync_failed_attempts_due_to_app_open";
    public static final String LAST_AUTOSYNC_COMPLETION_TIME = "last_autosync_completion_time";
    public static final String CLEAR_NOTIFIED_ACCOUNTS = "uk.co.fk33.libraryreminder.CLEAR_NOTIFIED_ACCOUNTS";
    public static final int AUTOSYNC_FAIL_NOTIFICATION_ID = 101;

    private LibRemindDbAdapter mDatabaseAdapter;
    private BroadcastReceiver mMessageReceiver;
    private boolean mReceiverRegistered = false;
    private List<String> mNotifiedAccounts;
    private int mNumberOfAccountsBeingSynced = 0;
    private boolean mNoAccountFailedToSync = true;
    private boolean mAppOpened = false;
    private boolean mAbandoned = false;
    private SharedPreferences mPrefs;
    private List<LibAccount> mListOfAccountObjects;

    @Override
    public void onCreate() {

        registerMessageReceiver();

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        boolean appInForeground = false;

        if (dbFrag == null) {

            mDatabaseAdapter = new LibRemindDbAdapter(this);
            mDatabaseAdapter.open();
        } else {

            mDatabaseAdapter = dbFrag.getDatabaseAdapter();

            /* The database might have been closed when app backgrounded. */
            mDatabaseAdapter.open();

            /* Since we have a reference to the dbfrag, the app might be in
            the foreground. Check whether it's resumed and store as a boolean
            which will be checked against if a valid network is found. If
            a valid network is found and app is not in foreground, auto-sync
            will proceed.*/
            int mainActivityState = dbFrag.getMainActivityState();
            int viewAccountActivityState = dbFrag.getViewAccountActivityState();
            int settingsActivityState = dbFrag.getSettingsActivityState();

            appInForeground = (mainActivityState == MainActivity.STATE_RESUMED ||
                    viewAccountActivityState == MainActivity.STATE_RESUMED ||
                    settingsActivityState == MainActivity.STATE_RESUMED);
        }

        boolean catchupSync = mPrefs.getBoolean(CATCHUP_SYNC, false);

        /* Get list of accounts now, before going any further, because if there are
        * no accounts, there's no point in continuing.
        * If we're running a catch-up sync, there must be accounts.*/
        if (catchupSync) {

            /* We are only syncing accounts which failed to sync on one of today's previous automatic
            * sync attempts. Get the accounts with negative sync statuses from the db. By passing true
            * as the last argument, the created accounts will all be set to silent mode, so no attempt
            * will be made to update the ui while they are being synced. */
            mListOfAccountObjects = MainActivity.createAccountObjectsFromCursor(
                    mDatabaseAdapter.getAccountsWithSyncProblems(), this, true);
        } else {

            /* By passing true as the last argument, the created accounts will all be set to
            * silent mode, so no attempt will be made to update the ui while they are being synced. */
            mListOfAccountObjects = MainActivity.createAccountObjectsFromCursor(
                    mDatabaseAdapter.getAllAccounts(), this, true);

            if (mListOfAccountObjects.size() == 0) {

                setNextRunTime(SyncManager.TOMORROW);
                stopSelf();
                return;
            }
        }

        /* Check if there's a valid network connection available. */
        if (UpdateTask.validNetworkAvailable(this)) {

            mNotifiedAccounts = new ArrayList<String>();

            /* If the app is open and visible, don't run the background sync task. Set an alarm for
            * five minutes then check again.*/
            if (!appInForeground) {

                backgroundSyncAllAccounts();
            } else {

                /* The app is at the front of the task stack, so don't auto-sync.
                If fewer than 12 attempts have been made, another attempt will
                be rescheduled for 5 minutes. */
                abandonAttemptDueToAppOpen();
            }

        } else {

            abandonAttemptDueToNoNetwork();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    private void registerMessageReceiver() {

        mMessageReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                if (action.equals(CLEAR_NOTIFIED_ACCOUNTS)) {

                    clearNotifiedAccounts();
                }

                if (action.equals(MainActivity.APP_OPENED)) {

                    mAppOpened = true;
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(CLEAR_NOTIFIED_ACCOUNTS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(MainActivity.APP_OPENED));

        mReceiverRegistered = true;
    }

    private void backgroundSyncAllAccounts() {

        /* Before starting the account syncs, make sure the app hasn't been opened since the
        initial db reads took place to get the list of accounts. If the app has been opened,
        abandon the auto-sync task. */
        if (mAppOpened) {

            abandonAttemptDueToAppOpen();
        } else {

            mNumberOfAccountsBeingSynced = mListOfAccountObjects.size();

            for (LibAccount libAccount : mListOfAccountObjects) {

                libAccount.sync();
            }
        }
    }

    private void abandonAttemptDueToAppOpen() {

        mAbandoned = true;

        /*Check how many auto-sync attempts have failed  today due to the app being open.
        If fewer than 18 attempts have been made, try again in 5 minutes. If 12 attempts
        have been made notify user we couldn't sync today*/
        int failedAttemptsDueToAppOpen = mPrefs.getInt(AUTO_SYNC_FAILED_ATTEMPTS_DUE_TO_APP_OPEN, 0);
        failedAttemptsDueToAppOpen++;

        if (failedAttemptsDueToAppOpen < 12) {

            ApiDependentCodeFactory.getApiDependentPrefWriter()
                    .writeIntPref(mPrefs, AUTO_SYNC_FAILED_ATTEMPTS_DUE_TO_APP_OPEN,
                            failedAttemptsDueToAppOpen);

            /* Try again in five minutes.*/
            setNextRunTime(5);
            stopSelf();
        } else {

            /* Reset both failed sync attempt counters, as it's not impossible that
            we might failures of both kinds before this point, meaning that both
            counters could be non-zero. */
            clearSavedState();

            /* Record the time of the run completion. */
            ApiDependentCodeFactory.getApiDependentPrefWriter().writeStringPref(mPrefs,
                    LAST_AUTOSYNC_COMPLETION_TIME, String.valueOf(System.currentTimeMillis()));

            setNextRunTime(SyncManager.TOMORROW);

            /* Notify user that we couldn't auto-sync today. */
            notifyAutoSyncAbandonedDueToAppAlwaysOpen();
            stopSelf();
        }
    }

    private void abandonAttemptDueToNoNetwork() {

        /* Check how many attempts have been made today to auto-sync accounts. If fewer than 5
        * attempts have been made, try again in 10 minutes. If 5 attempts have been made and
        * no network was available, notify user we couldn't sync today. */
        int failedAttempts = mPrefs.getInt(AUTO_SYNC_FAILED_ATTEMPTS, 0);
        failedAttempts++;

        if (failedAttempts < 5) {

            ApiDependentCodeFactory.getApiDependentPrefWriter()
                    .writeIntPref(mPrefs, AUTO_SYNC_FAILED_ATTEMPTS, failedAttempts);

            /* There's no network connection, so wait 10 minutes in the hope there will be one.
            * In reality, more than 10 minutes will elapse between one attempt and the next,
            * because the priority of the autosync service is quite low. In testing some runs
            * lasted around 15 minutes, which would mean 25 minutes between attempts. Also,
            * using setInexactRepeating to set the alarm usually adds 11-12 minutes on to the
            * alarm time. */
            setNextRunTime(10);
            stopSelf();
        } else {

            /* Reset both failed sync attempt counters, as it's not impossible that
            we might failures of both kinds before this point, meaning that both
            counters could be non-zero. */
            clearSavedState();

            /* Record the time of the run completion. */
            ApiDependentCodeFactory.getApiDependentPrefWriter().writeStringPref(mPrefs,
                    LAST_AUTOSYNC_COMPLETION_TIME, String.valueOf(System.currentTimeMillis()));

            setNextRunTime(SyncManager.TOMORROW);

            /* Notify user that we couldn't auto-sync today.*/
            notifyAutoSyncAbandoned();

            stopSelf();
        }
    }

    public void writeToDbOnAccountSyncSuccess(DataToWrite newSummaryAndDates, String accountName,
                                              List<LibItem> newItemList, boolean remind) {

        /* If the app has been opened since the sync began (i.e. if the message receiver
        * rec'd the APP_OPENED broadcast), don't perform any db writes, because if for
        * example a user is viewing an account whilst a db write takes place for that
        * account, we get unpredictable behaviour and crashes. (Updating the item table
        * deletes all items and writes the new items with different row ids, so all item
        * rows of an account being viewed would disappear while it was still being viewed).
        * Call abandonAttemptDueToAppOpen(), but make sure we only call it once. To do this,
        * check the mAbandoned boolean. The reason we have to check this boolean is because
        * there may be other AsyncUpdateTasks still running, and they will call this method
        * again when they complete. If we call abandonAttemptDueToAppOpen() for every account
        * which finishes syncing after the app is opened, we will increment the count of times
        * an autosync attempt has failed due to the app being open too many times. It should
        * only be incremented once each time we abandon an attempt to sync all accounts, not
        * for every account whose sync is abandoned.*/
        if (mAppOpened) {

            if (!mAbandoned) {

                abandonAttemptDueToAppOpen();
            }
        } else {

            /* Remove the account object from the list of account objects. This is so that if
            any accounts fail to sync on this run-through, we will know how many failed, because
            they will be the only ones left in the list. Then, if this happens to be the final
            auto-sync attempt today, and we therefore have to notify the user that some accounts
            couldn't be synced, we will be able to tell them how many couldn't be synced. */
            Iterator<LibAccount> accountListIterator = mListOfAccountObjects.iterator();
            for (; accountListIterator.hasNext(); ) {

                LibAccount libAccount = accountListIterator.next();
                if (libAccount.getName().equals(accountName)) {

                    accountListIterator.remove();
                    break;
                }
            }

            AsyncDbWrite accountsAsyncDbWrite = new AsyncDbWrite(newSummaryAndDates, mDatabaseAdapter,
                    DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS_SILENTLY);
            accountsAsyncDbWrite.execute();

            AsyncDbWrite itemsASyncDbWrite = new AsyncDbWrite(accountName, newItemList, remind, mDatabaseAdapter, this,
                    DbInteractionsFragment.DB_WRITE_UPDATE_ITEMS_FOR_ACCOUNT_SILENTLY);
            itemsASyncDbWrite.execute();
        }
    }

    public void writeToDbOnAccountSyncFail(DataToWrite dataToWrite) {

        /* Just to be on the safe side, don't perform the writes if the app has been
        * opened since the sync started. We don't want any unexpected behaviour. */
        if (mAppOpened) {

            if (!mAbandoned) {

                abandonAttemptDueToAppOpen();
            }
        } else {

            mNoAccountFailedToSync = false;

            AsyncDbWrite asyncDbWrite = new AsyncDbWrite(dataToWrite, mDatabaseAdapter, this,
                    DbInteractionsFragment.DB_WRITE_UPDATE_ACCOUNTS_SILENTLY_ON_SYNC_FAIL);
            asyncDbWrite.execute();
        }
    }

    private void setNextRunTime(int minutes) {

        SyncManager syncManager = SyncManager.getInstance();
        syncManager.setNextSyncTime(this, minutes);
    }

    @Override
    public void checkDueDates(List<LibItem> itemList, String accountName) {

        String marginString = mPrefs.getString(MainActivity.MARGIN_PREF, "3");
        int margin = Integer.parseInt(marginString);

        int dueItems = 0;
        int overdueItems = 0;
        DateTimeZone dateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault());
        LocalDate today = new DateTime(System.currentTimeMillis(), dateTimeZone).toLocalDate();

        for (LibItem item : itemList) {

            if (item.getRemind()) {

                LocalDate dueDate = new DateTime(item.getDueDate(), dateTimeZone).toLocalDate();

                Days differenceInDays = Days.daysBetween(today, dueDate);
                int difference = differenceInDays.getDays();

                if (difference <= margin) {

                    if (difference >= 0) {

                        dueItems++;
                    } else {

                        overdueItems++;
                    }
                }
            }
        }

        if (dueItems > 0 || overdueItems > 0) {

            String[] notificationStrings = new DateParser().makeNotificationStrings(this,
                    accountName, margin, dueItems, overdueItems);
            notify(notificationStrings, accountName);
        }
    }

    public void notify(String[] notificationStrings, String accountName) {

        /* This will only add the account to the list of accounts notification have been
        sent for if it isn't already in the list (i.e. if a notification hasn't already been
        sent for this account). This prevents a multi-account notification string being sent
        when both notifications are for the same account. */
        addNotifiedAccount(accountName);
        int notificationCount = getNotificationCount();

        NotificationCompat.Builder notificationBuilder = createNoticationBuilderWithDefaults();

        Intent notificationIntent = new Intent(this, RespondToNotificationService.class);
        notificationIntent.setAction(MainActivity.ACTION_RESPOND_TO_NOTIFICATION);

        if (notificationCount == 1) {

            notificationBuilder.setContentText(notificationStrings[0])
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(notificationStrings[1]));

            notificationIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, accountName);
        } else {

            notificationBuilder.setContentText(getString(R.string.loans_need_action_on) + " "
                    + String.valueOf(notificationCount) + " " + getString(R.string.accounts));

            /* If more than one account needs action, put an empty account name, as no
            * account will be opened.*/
            notificationIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, "");
        }

        Intent notificationDeleteIntent = new Intent(this, RespondToNotificationService.class);
        notificationDeleteIntent.setAction(MainActivity.ACTION_REGISTER_NOTIFICATION_DISMISSAL);

        PendingIntent notificationPendingIntent = PendingIntent.getService(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent notificationDeletePendingIntent = PendingIntent.getService(this, 0,
                notificationDeleteIntent, 0);

        notificationBuilder.setContentIntent(notificationPendingIntent);
        notificationBuilder.setDeleteIntent(notificationDeletePendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(LocaleDependentClass.ACTION_NEEDED_NOTIFICATION_ID, notificationBuilder.build());
    }

    private void notifyAutoSyncAbandoned() {

        NotificationCompat.Builder notificationBuilder = createNoticationBuilderWithDefaults();

        Intent notificationIntent = new Intent(this, RespondToNotificationService.class);
        notificationIntent.setAction(MainActivity.ACTION_RESPOND_TO_NOTIFICATION);

        /* This is a general failure, so no account needs to be opened. */
        notificationIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, "");

        notificationBuilder.setContentText(getString(R.string.cant_sync))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(getString(R.string.cant_sync_big)));

        PendingIntent notificationPendingIntent = PendingIntent.getService(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(notificationPendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(AUTOSYNC_FAIL_NOTIFICATION_ID, notificationBuilder.build());
    }

    private void notifyAutoSyncAbandonedDueToAppAlwaysOpen() {

        NotificationCompat.Builder notificationBuilder = createNoticationBuilderWithDefaults();

        Intent notificationIntent = new Intent(this, RespondToNotificationService.class);
        notificationIntent.setAction(MainActivity.ACTION_RESPOND_TO_NOTIFICATION);

        /* This is a general failure, so no account needs to be opened. */
        notificationIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, "");

        notificationBuilder.setContentText(getString(R.string.autosync_failed_app_open))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(getString(R.string.autosync_failed_app_open_big)));

        PendingIntent notificationPendingIntent = PendingIntent.getService(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(notificationPendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(AUTOSYNC_FAIL_NOTIFICATION_ID, notificationBuilder.build());
    }

    private void notifySomeAccountsHadProblems() {

        /* Successfully synced accounts are removed from this list, so any remaining
        * ones must have had a problem. */
        int numberOfAccountsWithProblems = mListOfAccountObjects.size();

        NotificationCompat.Builder notificationBuilder = createNoticationBuilderWithDefaults();

        Intent notificationIntent = new Intent(this, RespondToNotificationService.class);

        /* We have to put a different action than the action used to notify the user
        * that items need action, otherwise, if notifications are sent both for items
        * needing action and for problems syncing, meaning two notifications are on the
        * notification bar at the same time, the string extra of the items need action
        * notification is overwritten by the string extra from the sync problem
        * notification. */
        notificationIntent.setAction(MainActivity.ACTION_RESPOND_TO_NOTIFICATION_SYNC_PROBLEM);

        /* No point in opening any account which couldn't sync, better to show the
        account list, which will show all accounts and their sync status. */
        notificationIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, "");

        String shortNotificationString;

        if (numberOfAccountsWithProblems == 1) {

            String accountName = mListOfAccountObjects.get(0).getName();
            shortNotificationString = accountName + " " + getString(R.string.failed_to_sync);
        } else {

            shortNotificationString = String.valueOf(numberOfAccountsWithProblems) + " "
                    + getString(R.string.accounts_failed_to_sync);
        }

        String longNotificationString = shortNotificationString + " "
                + getString(R.string.next_attempt_tomorrow);

        notificationBuilder.setContentText(shortNotificationString)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(longNotificationString));

        PendingIntent notificationPendingIntent = PendingIntent.getService(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(notificationPendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(AUTOSYNC_FAIL_NOTIFICATION_ID, notificationBuilder.build());
    }

    private NotificationCompat.Builder createNoticationBuilderWithDefaults() {

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.libreminder_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.drawable.libreminder_notification_lrg))
                        .setContentTitle(getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setOnlyAlertOnce(true);

        boolean playSound = mPrefs.getBoolean(MainActivity.PREF_PLAY_A_SOUND, true);
        boolean vibrate = mPrefs.getBoolean(MainActivity.PREF_VIBRATE, false);

        if (playSound) {

            if (vibrate) {

                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND |
                        Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
            } else {

                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
            }
        } else {

            if (vibrate) {

                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
            }
        }

        return notificationBuilder;
    }

    private void addNotifiedAccount(String accountName) {

        if (!mNotifiedAccounts.contains(accountName)) {

            mNotifiedAccounts.add(accountName);
        }
    }

    private int getNotificationCount() {

        return mNotifiedAccounts.size();
    }

    private void clearNotifiedAccounts() {

        mNotifiedAccounts.clear();
    }

    public void decrementNumberOfAccountsBeingSynced() {

        mNumberOfAccountsBeingSynced--;

        if (mNumberOfAccountsBeingSynced < 1) {

            if (mNoAccountFailedToSync) {

                /* All accounts have been auto-synced successfully. */
                clearSavedState();

                /* Re-set the alarm from the saved preference time, as the alarm time
                * might have been altered due to a failed sync attempt.*/
                setNextRunTime(SyncManager.TOMORROW);

                /* Record the time of the run completion. */
                ApiDependentCodeFactory.getApiDependentPrefWriter().writeStringPref(mPrefs,
                        LAST_AUTOSYNC_COMPLETION_TIME, String.valueOf(System.currentTimeMillis()));

                stopSelf();
            } else {

                /* Check how many attempts have been made today to auto-sync accounts. If fewer than 5
                * attempts have been made, try again in 10 minutes. If 5 attempts have been made,
                * notify user that some accounts couldn't be synced. */
                int failedAttempts = mPrefs.getInt(AUTO_SYNC_FAILED_ATTEMPTS, 0);
                failedAttempts++;

                if (failedAttempts < 5) {

                    /* Schedule another auto-sync attempt, but set a preference so that, on
                    * the next attempt, the app will only attempt to sync accounts with a status
                    * showing their last sync attempt failed. This run of the auto-sync service
                    * is counted as an attempt which failed due to network unavailability,
                    * even if only one account failed to sync while others synced succesfully,
                    * and even if that account failed to sync for a reason not related to
                    * network issues. */
                    ApiDependentPrefWriter apiDependentPrefWriter =
                            ApiDependentCodeFactory.getApiDependentPrefWriter();
                    apiDependentPrefWriter.writeIntPref(mPrefs, AUTO_SYNC_FAILED_ATTEMPTS, failedAttempts);
                    apiDependentPrefWriter.writeBooleanPref(mPrefs, CATCHUP_SYNC, true);

                    /* Make next auto-sync run in 10 minutes in the hope that the account(s) which failed
                    to sync this time will sync then. (In reality there may be more than 10 mins between
                    attempts as the priority of the service is quite low. In testing some runs took 15
                    minutes between starting and setting the alarm for next run. Also, setInexactRepeating
                    will slow things down). */
                    setNextRunTime(10);
                    stopSelf();
                } else {

                    /* Reset both failed sync attempt counters, as it's not impossible that
                    we might have had failures of both kinds before this point, meaning that both
                    counters could be non-zero. Also, this resets the catchup_sync boolean
                    to false.*/
                    clearSavedState();

                    /* Record the time of the run completion. */
                    ApiDependentCodeFactory.getApiDependentPrefWriter().writeStringPref(mPrefs,
                            LAST_AUTOSYNC_COMPLETION_TIME, String.valueOf(System.currentTimeMillis()));

                    setNextRunTime(SyncManager.TOMORROW);

                    /* Notify user that even though we had a network connection and the app
                    was in the background (meaning the background auto-sync service could run)
                    that there was a problem syncing one or more accounts.*/
                    notifySomeAccountsHadProblems();
                    stopSelf();
                }

            }
        }
    }

    private void clearSavedState() {

        /* Reset both counters which count failed auto-sync attempts, and clear the
        * setting to do a catchup sync.*/
        ApiDependentPrefWriter apiDependentPrefWriter =
                ApiDependentCodeFactory.getApiDependentPrefWriter();
        apiDependentPrefWriter
                .writeIntPref(mPrefs, AUTO_SYNC_FAILED_ATTEMPTS, 0);
        apiDependentPrefWriter
                .writeIntPref(mPrefs, AUTO_SYNC_FAILED_ATTEMPTS_DUE_TO_APP_OPEN, 0);
        apiDependentPrefWriter.writeBooleanPref(mPrefs, CATCHUP_SYNC, false);
    }

    @Override
    public void onDestroy() {

        if (mDatabaseAdapter != null) {

            /* Don't close the database if the app has been opened since the auto sync
            * service started, as it will still be being used by the app. */
            if (!mAppOpened) {

                mDatabaseAdapter.close();
            }
        }

        if (mReceiverRegistered) {

            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }
}
