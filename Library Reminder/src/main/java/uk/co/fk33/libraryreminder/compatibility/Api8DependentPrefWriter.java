/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;

import android.content.SharedPreferences;

public class Api8DependentPrefWriter extends ApiDependentPrefWriter {

    private final static int WRITE_BOOLEAN = 0;
    private final static int WRITE_INT = 1;
    private final static int WRITE_STRING = 2;

    /* API 8 needs commit() instead of apply(), this class will be loaded when running on API 8
    As commit() is not asynchronous, this class runs it in a thread. */

    @Override
    public void writeBooleanPref(SharedPreferences sharedPreferences, String key,
                                 boolean valueToWrite) {

        AsyncSharedPrefsWriter asyncSharedPrefsWriter = new AsyncSharedPrefsWriter(
                sharedPreferences, key, valueToWrite, WRITE_BOOLEAN);
        (new Thread(asyncSharedPrefsWriter)).start();
    }

    @Override
    public void writeIntPref(SharedPreferences sharedPreferences, String key, int valueToWrite) {

        AsyncSharedPrefsWriter asyncSharedPrefsWriter = new AsyncSharedPrefsWriter(
                sharedPreferences, key, valueToWrite, WRITE_INT);
        (new Thread(asyncSharedPrefsWriter)).start();
    }

    @Override
    public void writeStringPref(SharedPreferences sharedPreferences, String key, String value) {

        AsyncSharedPrefsWriter asyncSharedPrefsWriter = new AsyncSharedPrefsWriter(
                sharedPreferences, key, value, WRITE_STRING);
        (new Thread(asyncSharedPrefsWriter)).start();
    }

    private class AsyncSharedPrefsWriter implements Runnable {

        private final SharedPreferences mSharedPreferences;
        private final String mKey;
        private boolean mBooleanValue;
        private int mIntValue;
        private String mStringValue;
        private int mFlag;

        public AsyncSharedPrefsWriter(SharedPreferences sharedPreferences, String key, boolean value, int flag) {

            mSharedPreferences = sharedPreferences;
            mKey = key;
            mBooleanValue = value;
            mFlag = flag;
        }

        public AsyncSharedPrefsWriter(SharedPreferences sharedPreferences, String key, int value, int flag) {

            mSharedPreferences = sharedPreferences;
            mKey = key;
            mIntValue = value;
            mFlag = flag;
        }

        public AsyncSharedPrefsWriter(SharedPreferences sharedPreferences, String key, String value, int flag) {

            mSharedPreferences = sharedPreferences;
            mKey = key;
            mStringValue = value;
            mFlag = flag;
        }

        @Override
        public void run() {

            switch (mFlag) {

                case WRITE_BOOLEAN: {

                    SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
                    sharedPreferencesEditor.putBoolean(mKey, mBooleanValue);
                    sharedPreferencesEditor.commit();

                    break;
                }

                case WRITE_INT: {

                    SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
                    sharedPreferencesEditor.putInt(mKey, mIntValue);
                    sharedPreferencesEditor.commit();

                    break;
                }

                case WRITE_STRING: {

                    SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
                    sharedPreferencesEditor.putString(mKey, mStringValue);
                    sharedPreferencesEditor.commit();

                    break;
                }
            }
        }
    }
}
