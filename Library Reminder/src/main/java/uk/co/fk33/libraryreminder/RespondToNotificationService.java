/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;
import uk.co.fk33.libraryreminder.network.AutoSyncService;

public class RespondToNotificationService extends Service {

    /* This class exists just to close any open fragments after the user clicks a notification,
    and then to send the intents or broadcasts necessary to open the account which the notification
    is about. It can't really be done any other way, because of the db interactions fragment,
    which stops us creating multiple instances of the Main Activity (which is what the Android
    guidelines recommend).*/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getAction();
        if (action.equals(MainActivity.ACTION_RESPOND_TO_NOTIFICATION)
                || action.equals(MainActivity.ACTION_RESPOND_TO_NOTIFICATION_SYNC_PROBLEM)) {

            handleNotificationClick(intent);
        } else {

            if (action.equals(MainActivity.ACTION_REGISTER_NOTIFICATION_DISMISSAL)) {

                handleNotificationDismissal();
            }
        }

        stopSelf();

        return START_NOT_STICKY;
    }

    private void handleNotificationClick(Intent intent) {

        String accountToOpen = intent.getStringExtra(MainActivity.ACCOUNT_TO_OPEN);

        if (accountToOpen != null) {

            DbInteractionsFragment dbFrag = DbFragmentRef.get();

            if (dbFrag != null) {

                /* Send broadcast which will close any open frags, and another which
                will close the settings activity if it is open. */
                Intent closeFragsIntent = new Intent(MainActivity.CLOSE_ALL_FRAGMENTS);
                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                localBroadcastManager.sendBroadcast(closeFragsIntent);

                Intent closeSettingsActivity = new Intent(MainActivity.CLOSE_SETTINGS_ACTIVITY);
                localBroadcastManager.sendBroadcast(closeSettingsActivity);

                dbFrag.onItemInfoDialogClose();
                dbFrag.setAccountBeingEdited(false);

                /* Find out if the app is in the foreground.*/
                int mainActivityState= dbFrag.getMainActivityState();
                int viewAccountActivityState = dbFrag.getViewAccountActivityState();
                int settingsActivityState = dbFrag.getSettingsActivityState();

                boolean appInForeground = (mainActivityState == MainActivity.STATE_RESUMED ||
                        viewAccountActivityState == MainActivity.STATE_RESUMED ||
                        settingsActivityState == MainActivity.STATE_RESUMED);

                if (appInForeground) {

                    Intent openAccountIntent = new Intent(MainActivity.ACTION_OPEN_ACCOUNT);
                    openAccountIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, accountToOpen);

                    localBroadcastManager.sendBroadcast(openAccountIntent);
                } else {

                    if (mainActivityState == MainActivity.STATE_DESTROYED) {

                        startApp(accountToOpen);
                    } else {

                        if (viewAccountActivityState == MainActivity.STATE_PAUSED) {

                            /* This will use the existing (paused) view account activity to show the
                            * account we want to look at, because a test will be made to see whether
                            * there is a view account activity open (by checking if there's a view
                            * account fragment in the main activity's fragment manager, and if not,
                            * checking if there is, nevertheless, an account being viewed, which would
                            * imply a view account activity must be open.*/
                            Intent openAccountIntent = new Intent(MainActivity.ACTION_OPEN_ACCOUNT);
                            openAccountIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, accountToOpen);
                            localBroadcastManager.sendBroadcast(openAccountIntent);

                            /* Bring the app to the front. */
                            Intent resumeAppIntent = new Intent(this, MainActivity.class);
                            resumeAppIntent.setAction(Intent.ACTION_MAIN);
                            resumeAppIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                            resumeAppIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(resumeAppIntent);

                        } else {

                            startApp();

                            /* Send broadcast to open the account. Can't put the account
                            * name in the intent as the new intent doesn't get processed
                            * if the main activity is paused and we use the same action and
                            * category it was originally started with, which we have to do,
                            * because on other occasions we want to make sure android does
                            * not create a new instance, which it will if the action and
                            * category of an existing but stopped activity are different
                            * from those in the intent we use.*/
                            Intent openAccountIntent = new Intent(MainActivity.ACTION_OPEN_ACCOUNT);
                            openAccountIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, accountToOpen);
                            localBroadcastManager.sendBroadcast(openAccountIntent);
                         }
                    }
                }

            } else {

                /* The db frag doesn't exist, maybe the app has not run since device was
                switched on, or maybe it was destroyed. Start a new activity.. */
                startApp(accountToOpen);
            }
        }
    }

    private void handleNotificationDismissal() {

        /* We don't know if the notification followed a manual sync, or an auto-sync.
        * In each case, the count of notifications is kept in a different place. So
        * just clear both counters to be safe.*/

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        if (dbFrag != null) {

            DbFragmentRef.get().clearNotifiedAccounts();
        }

        Intent clearNotifiedAccountsIntent = new Intent(AutoSyncService.CLEAR_NOTIFIED_ACCOUNTS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(clearNotifiedAccountsIntent);
    }

    private void startApp() {

        Intent startAppIntent = new Intent(this, MainActivity.class);
        startAppIntent.setAction(Intent.ACTION_MAIN);
        startAppIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        startAppIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startAppIntent);
    }

    private void startApp(String accountToOpen) {

        Intent startAppIntent = new Intent(this, MainActivity.class);
        startAppIntent.setAction(Intent.ACTION_MAIN);
        startAppIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        startAppIntent.putExtra(MainActivity.ACCOUNT_TO_OPEN, accountToOpen);
        startAppIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startAppIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
