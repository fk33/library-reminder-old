/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.network;

import android.content.Context;
import android.util.Log;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.fk33.libraryreminder.LibItem;
import uk.co.fk33.libraryreminder.R;
import uk.co.fk33.libraryreminder.accounts.LibAccount;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ElibraryUpdateTask extends UpdateTask {

    public ElibraryUpdateTask(Context context) {

        super(context);
    }

    @Override
    protected List<LibItem> doInBackground(LibAccount... libAccounts) {

        /* Connect to the library catalogue, get the list of user's loans */

        mLibAccount = libAccounts[0];

        if (!validNetworkAvailable()) {

            mStatus = LibAccount.SYNC_FAILED_NO_CONNECTION;
            return null;
        }

        String catUrl = mLibAccount.getCatUrl();
        String pinOrPassword = mLibAccount.getPinOrPassword();
        LibItem libItem;
        List<LibItem> itemList = new ArrayList<LibItem>();
        Connection.Response catalogueResponse;
        Document cataloguePage;

        DateFormat dateFormat = LocaleDependentClass.getElibraryDateFormat();
        Date dueDate;
        String uniqueId;
        String author = "";
        String title = "";
        String dueDateString;
        int renewCount = -1;
        long dueDateLong;

        /* Remove the part of the catalogue url starting with /uhtbin as this will
        be replaced when we find the POST url (which starts with /uhtbin). */
        String baseCatUrl = catUrl.substring(0, catUrl.indexOf("/uhtbin"));

        try {

            /* Try to connect to the catalogue. If the response is null for any reason,
            don't continue. */
            publishProgress(mContext.getString(R.string.connecting_to_catalogue));
            catalogueResponse = Jsoup.connect(catUrl)
                    .timeout(NetworkConstants.TIMEOUT_MILLIS)
                    .userAgent(mContext.getString(R.string.user_agent))
                    .execute();

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            cataloguePage = catalogueResponse.parse();
            String sessionNumber = catalogueResponse.cookie("session_number");

            Elements loginForm = cataloguePage.select("form[class=renew_materials]");

            if (loginForm.isEmpty()) {

                mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                return null;
            }

            String relPostUrl = loginForm.attr("action");
            String loginUrl = baseCatUrl + relPostUrl;

            publishProgress(mContext.getString(R.string.sending_login_details));

            if (sessionNumber != null) {

                Connection connection = Jsoup.connect(loginUrl)
                        .followRedirects(false)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .userAgent(mContext.getString(R.string.user_agent))
                        .cookie("session_number", sessionNumber)
                        .method(Connection.Method.POST);

                if (pinOrPassword.equals("")) {

                    catalogueResponse = connection.data("user_id", mLibAccount.getCardNumber())
                            .execute();
                } else {

                    catalogueResponse = connection.data("user_id", mLibAccount.getCardNumber(),
                            "password", mLibAccount.getPinOrPassword()).execute();
                }
            } else {

                Connection connection = Jsoup.connect(loginUrl)
                        .followRedirects(false)
                        .timeout(NetworkConstants.TIMEOUT_MILLIS)
                        .userAgent(mContext.getString(R.string.user_agent))
                        .method(Connection.Method.POST);

                if (pinOrPassword.equals("")) {

                    catalogueResponse = connection.data("user_id", mLibAccount.getCardNumber())
                            .execute();
                } else {

                    catalogueResponse = connection.data("user_id", mLibAccount.getCardNumber(),
                            "password", mLibAccount.getPinOrPassword()).execute();
                }
            }

            if (catalogueResponse == null) {

                mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;
                return null;
            }

            publishProgress(mContext.getString(R.string.getting_list_of_loans));

            cataloguePage = catalogueResponse.parse();

            /* Check if the login failed due to incorrect card no/PIN, or if user has no loans.*/
            Elements errorContainer = cataloguePage.select("div[class=content_container error]");

            if (!errorContainer.isEmpty()) {

                if (errorContainer.text().contains("Invalid login")) {

                    mStatus = LibAccount.SYNC_FAILED_LOGIN_ERROR;
                    return null;
                }

                /* If you have no loans e-library says you have no charges (at least it does
                on the page we request).*/
                if (errorContainer.text().contains("User has no charges")) {

                    mStatus = LibAccount.SYNC_SUCCEEDED;
                    return itemList;
                }
            }

            /* Parse out loan details from the page. */
            Element renewForm = cataloguePage.select("form[class=renew_form]").first();
            Element loanTable = renewForm.select("table").first();
            Elements loanRows = loanTable.select("tr");

            /* Remove the first and last rows of the table as they don't contain
            loan information. */
            Element firstRow = loanRows.first();
            Elements firstRowItemInfo = firstRow.select("td[class^=itemlisting]");
            if (firstRowItemInfo.isEmpty()) {
                loanRows.remove(0);
            }

            Element lastRow = loanRows.last();
            Elements lastRowItemInfo = lastRow.select("td[class^=itemlisting]");
            if (lastRowItemInfo.isEmpty()) {
                loanRows.remove(loanRows.size() - 1);
            }

            for (Element loanRow : loanRows) {

                Elements columns = loanRow.select("td");

                Element checkBoxColumn = columns.get(0);
                Element authorTitleColumn = columns.get(1);
                Element dueDateColumn = columns.get(2);

                Element checkBox = checkBoxColumn.select("input[type=checkbox]").first();

                String checkboxName = checkBox.attr("name");

                /*
                The name of the checkbox includes the item barcode, the
                author, and the title, amongst other info, and separates
                them with a caret. This gives an easy way to parse them.
                */
                String[] checkboxNameParts = checkboxName.split("\\^");

                if (checkboxNameParts.length == 6) {

                    uniqueId = checkboxNameParts[1];
                    author = checkboxNameParts[4];
                    title = checkboxNameParts[5];

                    /* The author usually has a full stop at the end. Get rid of it. */
                    if (author.endsWith(".")) {

                        author = author.substring(0, author.length() - 1);
                    }

                } else {

                    /*
                    If the checkbox name doesn't split into the right number of
                    parts, there may be no author for the work. Get the title details
                    from the page text rather than from the html.
                    The following code just parses out the details and if anything isn't
                    as expected, it bails out and we can't sync the account.
                    */

                    String loanIdDetailsAsHtml = authorTitleColumn.toString();

                    /* Strip line breaks, 2 spaces in a row, and nbsps. */
                    loanIdDetailsAsHtml = loanIdDetailsAsHtml.replaceAll("\\n", "");
                    loanIdDetailsAsHtml = loanIdDetailsAsHtml.replaceAll("\\s{2,}", " ");
                    loanIdDetailsAsHtml = loanIdDetailsAsHtml.replaceAll("&nbsp;", "");

                    int titleStartIndex = loanIdDetailsAsHtml
                            .indexOf("Print the title, if it exists -->");

                    if (titleStartIndex > -1) {

                        titleStartIndex += 33;
                    } else {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }

                    int titleEndIndex = loanIdDetailsAsHtml
                            .indexOf("Print the author, if it exists -->");

                    if (titleEndIndex > -1) {

                        titleEndIndex -= 6;
                    } else {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }

                    if (titleEndIndex > titleStartIndex) {

                        title = loanIdDetailsAsHtml.substring(titleStartIndex, titleEndIndex);
                    }

                    title = title.trim();

                    /* Find the author. */
                    int authorStartIndex = loanIdDetailsAsHtml
                            .indexOf("Print the author, if it exists -->");

                    if (authorStartIndex > -1) {

                        authorStartIndex += 34;
                    } else {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }

                    int authorEndIndex = loanIdDetailsAsHtml
                            .lastIndexOf("<br />");

                    if (authorEndIndex > -1) {

                        authorEndIndex -= 1;
                    } else {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }

                    if (authorEndIndex > authorStartIndex) {

                        author = loanIdDetailsAsHtml.substring(authorStartIndex, authorEndIndex);
                    }

                    author = author.trim();

                    if (author.endsWith(".")) {

                        author = author.substring(0, author.length() - 1);
                    }

                    /* Find the unique id (barcode). */
                    int uniqueIdStart = loanIdDetailsAsHtml
                            .indexOf("<br />");

                    if (uniqueIdStart > -1) {

                        uniqueIdStart += 6;
                    } else {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }

                    if (loanIdDetailsAsHtml.length() >= uniqueIdStart + 14) {

                        uniqueId = loanIdDetailsAsHtml.substring(uniqueIdStart, uniqueIdStart + 14);

                    } else {

                        mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                        return null;
                    }
                }

                /*
                Now get the due date, and times renewed. These are easier to get as they
                are prefaced with "Due:" and "Times renewed:".
                */
                String dueDateColumnText = dueDateColumn.text();

                int dueDateStartIndex = dueDateColumnText.indexOf("Due: ");

                if (dueDateStartIndex > -1) {

                    dueDateStartIndex += 5;
                } else {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                /* Find the beginning of the year. */
                int dueDateEndIndex = dueDateColumnText.lastIndexOf("/2");

                if (dueDateEndIndex > -1) {

                    dueDateEndIndex += 5;
                } else {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                    return null;
                }

                dueDateString = dueDateColumnText.substring(dueDateStartIndex, dueDateEndIndex);

                /*
                The catalogue doesn't always use 2 digits for the month, so put a zero
                in front of months 1 to 9 to make them 01 to 09 so we can parse all dates
                with the same date formatter.
                */
                if (dueDateString.length() == 9) {

                    dueDateString = new StringBuilder(dueDateString).insert(3, "0").toString();
                }

                dueDate = dateFormat.parse(dueDateString);
                dueDateLong = dueDate.getTime();

                /* Now get the times renewed. */
                String dueDateColumnHtml = dueDateColumn.toString();

                int timesRenewedStartIndex = dueDateColumnHtml.indexOf("Times renewed:");

                int timesRenewedValueStartIndex = -1;
                int timesRenewedValueEndIndex = -1;

                if (timesRenewedStartIndex > -1) {

                    timesRenewedValueStartIndex = dueDateColumnHtml.indexOf("<strong>",
                            timesRenewedStartIndex);
                    timesRenewedValueEndIndex = dueDateColumnHtml.lastIndexOf("</strong>");
                }

                /* If this doesn't work, renewCount will be left as -1. */
                if (timesRenewedValueStartIndex > -1 && timesRenewedValueEndIndex > -1) {

                    timesRenewedValueStartIndex += 8;

                    if (timesRenewedValueEndIndex > timesRenewedValueStartIndex) {

                        String timesRenewedValue = dueDateColumnHtml.substring(
                                timesRenewedValueStartIndex, timesRenewedValueEndIndex);
                        renewCount = Integer.parseInt(timesRenewedValue);
                    }
                }

                libItem = new LibItem(mLibAccount.getName(), title, author, uniqueId, "", dueDateLong,
                        renewCount, true);
                itemList.add(libItem);

            }

            mStatus = LibAccount.SYNC_SUCCEEDED;

        } catch (IOException ioException) {

            mStatus = LibAccount.SYNC_FAILED_NETWORK_ERROR;

            if (ioException.getClass().toString().equals(NetworkConstants.TIMEOUT_EXCEPTION)) {

                mStatus = LibAccount.SYNC_FAILED_TIMED_OUT;
            }

            if (ioException.getMessage() != null) {

                if (ioException.getMessage().equals(NetworkConstants.HTTP_STATUS_ERROR)) {

                    mStatus = LibAccount.SYNC_FAILED_UNEXPECTED_RESPONSE;
                }
            }

            return null;

        } catch (IndexOutOfBoundsException outOfBoundsException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;

        } catch (ParseException parseException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        } catch (NullPointerException nullPointerException) {

            mStatus = LibAccount.SYNC_FAILED_FORMAT_ERROR;
            return null;
        }

        return itemList;
    }
}
