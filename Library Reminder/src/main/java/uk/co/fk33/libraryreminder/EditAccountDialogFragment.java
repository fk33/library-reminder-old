/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class EditAccountDialogFragment extends CloseableDialogFragment
        implements SetOrganisationDialogFragment.SetOrganisationDialogFragmentParent {

    private static final String SELECTED_ORGANISATION = "SELECTED_ORGANISATION";
    private static final String ORGANISATION_NAME = "ORGANISATION_NAME";
    private static final String ACCOUNT_NAME = "ACCOUNT_NAME";
    private static final String CARD_NUMBER = "CARD_NUMBER";
    private static final String PIN_OR_PASSWORD = "PIN_OR_PASSWORD";
    private static final String EXISTING_ACCOUNT_DETAILS = "EXISTING_ACCOUNT_DETAILS";
    private static final String REMINDER_BOOLEAN = "REMINDER_BOOLEAN";

    private LibOrganisation mSelectedOrganisation = null;
    private volatile boolean mOrgsLoaded = false;
    private volatile String mSelectedOrganisationName = "";
    private volatile List<LibOrganisation> mOrgList = null;
    private String[] mOrgNames = null;
    private OrgsLoader mOrgsLoader;
    private Thread mOrgsLoaderThread = null;
    private String[] mExistingAccountDetails;
    private EditFragParent mFragmentParent;
    private int mRegion;

    /* Any parent activity which implements this interface is declaring it
    will disappear if we change from single to dual-pane mode. */
    public interface EditFragParent {

        public void storeEditFragSavedState();
    }

    /* Class to load list of supported organisations from an xml file, in a thread */
    private static class OrgsLoader implements Runnable {

        private final WeakReference<EditAccountDialogFragment> mEditAccountFragRef;
        private final XmlResourceParser mParser;
        private volatile boolean mKilled = false;

        public OrgsLoader(EditAccountDialogFragment editAccountDialogFragment, XmlResourceParser parser) {

            mEditAccountFragRef = new WeakReference<EditAccountDialogFragment>(editAccountDialogFragment);
            mParser = parser;
        }

        public void killThread() {

            mKilled = true;
        }

        @Override
        public void run() {

            try {

                /* Write the organisation data from organisations.xml into a list of LibOrganisation objects*/
                List<LibOrganisation> orgList = new ArrayList<LibOrganisation>();
                LibOrganisation libOrganisation;

                mParser.next();
                int eventType = mParser.getEventType();
                while (eventType != XmlResourceParser.END_DOCUMENT) {

                    if (eventType == XmlResourceParser.START_TAG && mParser.getName().equals("organisation")) {

                        /* Create new LibOrganisation object with values from the xml org definition */
                        libOrganisation = new LibOrganisation(
                                mParser.getAttributeValue(null, "name"),
                                mParser.getAttributeValue(null, "short_name"),
                                mParser.getAttributeValue(null, "cat_type"),
                                mParser.getAttributeValue(null, "cat_url"));

                        /* Add it to the list */
                        orgList.add(libOrganisation);
                    }

                    eventType = mParser.next();

                    if (mKilled) {

                        break;
                    }
                }

                if (!mKilled) {

                    /* Update the mOrgList variable of the EditAccountDialogFragment */
                    EditAccountDialogFragment editAccountDialogFragment = mEditAccountFragRef.get();
                    if (editAccountDialogFragment != null) {

                        editAccountDialogFragment.setOrgList(orgList);
                        editAccountDialogFragment.setOrgsLoaded(true);
                        editAccountDialogFragment.showSetOrgDialogIfProgBarShown();
                        if (!editAccountDialogFragment.getSelectedOrganisationName().equals("")) {

                            editAccountDialogFragment.setSelectedOrganisationFromStoredName();
                        }
                    }
                }
            } catch (Exception exception) {

                if (mEditAccountFragRef != null) {
                    final Activity activity = mEditAccountFragRef.get().getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                new AlertDialog.Builder(activity)
                                        .setTitle(activity.getString(R.string.error))
                                        .setMessage(activity.getString(R.string.error_loading_organisations))
                                        .setPositiveButton(activity.getString(R.string.OK),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) { /*Do nothing */}
                                                }
                                        )
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof EditFragParent) {

            mFragmentParent = (EditFragParent) activity;
        } else {

            throw new ClassCastException(activity.toString() + " must implement EditFragParent.");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Activity activity = getActivity();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater layoutInflater = activity.getLayoutInflater();

        /* Null is passed here according to Google docs for inflating a view in a dialog,
        although it causes a lint warning. */
        View editAccountDialogView = layoutInflater.inflate(R.layout.edit_account_dialog, null);

        if (savedInstanceState != null) {

            /* Start the thread which loads the organisation info from an xml file into memory. */
            mRegion = savedInstanceState.getInt(MainActivity.REGION);
            XmlResourceParser parser = getResourceParserForRegion();
            mOrgsLoader = new OrgsLoader(this, parser);
            mOrgsLoaderThread = new Thread(mOrgsLoader);
            mOrgsLoaderThread.start();

            mSelectedOrganisation = savedInstanceState.getParcelable(SELECTED_ORGANISATION);

            /* If the device orientation changed while the edit account frag was reading organisation
            * data from the xml file in a background thread, meaning that mSelectedOrganisation wouldn't
            * have been parceled into the savedInstanceState, the name of the organisation would have
            * been put into the savedInstanceState instead. Check for this string, and set the member variable
            * mSelectedOrganisationName to this string, if it is found. If it is not found, set
            * mSelectedOrganisationName to an empty string. When the thread which loads organisation data
            * finishes, the fact that mSelectedOrganisationName is not an empty string will cause
            * mSelectedOrganisation to be set to the appropriate organisation.*/
            mSelectedOrganisationName = savedInstanceState.getString(ORGANISATION_NAME);
            if (mSelectedOrganisationName == null) {

                mSelectedOrganisationName = "";
            }
            String accountName = savedInstanceState.getString(ACCOUNT_NAME);
            String cardNumber = savedInstanceState.getString(CARD_NUMBER);
            String pinOrPassword = savedInstanceState.getString(PIN_OR_PASSWORD);
            boolean reminderSwitchState = savedInstanceState.getBoolean(REMINDER_BOOLEAN);

            TextView organisationTextView = (TextView) editAccountDialogView
                    .findViewById(R.id.edit_account_organisation_textview);
            if (organisationTextView != null) {
                if (mSelectedOrganisation != null) {

                        organisationTextView.setText(mSelectedOrganisation.getName());
                } else {

                        organisationTextView.setText(mSelectedOrganisationName);
                }
            }

            CompoundButton reminderSwitch = (CompoundButton) editAccountDialogView
                    .findViewById(R.id.edit_account_reminder_switch);
            reminderSwitch.setChecked(reminderSwitchState);

            EditText addAccountEditText;

            if (accountName != null) {

                addAccountEditText = (EditText) editAccountDialogView
                        .findViewById(R.id.edit_account_account_name_edittext);
                addAccountEditText.setText(accountName);
            }

            if (cardNumber != null) {

                addAccountEditText = (EditText) editAccountDialogView
                        .findViewById(R.id.edit_account_card_number);
                addAccountEditText.setText(cardNumber);
            }

            if (pinOrPassword != null) {

                addAccountEditText = (EditText) editAccountDialogView
                        .findViewById(R.id.edit_account_pin_or_password);
                addAccountEditText.setText(pinOrPassword);
            }

            mExistingAccountDetails = savedInstanceState.getStringArray(EXISTING_ACCOUNT_DETAILS);
        }

        /* Set the onClickListener for the set organisation button */
        View setOrganisationButton = editAccountDialogView.findViewById(R.id.edit_account_set_organisation_button);
        setOrganisationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditAccountDialogFragment.this.setOrganisation();
            }
        });

        /* Put the view we've just made into the dialogBuilder*/
        dialogBuilder.setView(editAccountDialogView)
                .setTitle(R.string.edit_account_dialog_title).setIcon(R.drawable.ic_menu_edit)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {

                    /*
                    Do nothing with this onClickListener, we add a new onclicklistener below where we check
                    the form is filled in. If we use this onClickListener there is no way to stop the dialog
                    being dismissed automatically when the OK button is pressed.
                    */
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) { /* do nothing */}
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog editAccountDialog = dialogBuilder.create();

        /* Add a custom OnShowListener so we can check the form is filled in fully before dismissing
        the dialog. The EditAccountOnShowListener type includes an onClickListener which attaches to
        the OK button. This listener has methods to validate form input, which it then passes to the
        updateAndRenameAccount method (in the DbInteractionsFragment). */
        EditAccountOnShowListener editAccountOnShowListener = new EditAccountOnShowListener(getActivity(),
                editAccountDialog, this);
        editAccountDialog.setOnShowListener(editAccountOnShowListener);

        return editAccountDialog;
    }

    public void loadOrganisationData(int region) {

        mRegion = region;
        XmlResourceParser parser = getResourceParserForRegion();
        mOrgsLoader = new OrgsLoader(this, parser);
        mOrgsLoaderThread = new Thread(mOrgsLoader);
        mOrgsLoaderThread.start();
    }

    private XmlResourceParser getResourceParserForRegion() {

        XmlResourceParser parser = null;

        switch (mRegion) {
            case MainActivity.REGION_ENGLAND_NORTH_MID: {
                parser = getResources().getXml(R.xml.english_north_mid_organisations);
                break;
            }
            case MainActivity.REGION_ENGLAND_EAST_SOUTH: {
                parser = getResources().getXml(R.xml.english_east_south_organisations);
                break;
            }
            case MainActivity.REGION_IRELAND: {
                parser = getResources().getXml(R.xml.irish_organisations);
                break;
            }
            case MainActivity.REGION_SCOTLAND: {
                parser = getResources().getXml(R.xml.scottish_organisations);
                break;
            }
            case MainActivity.REGION_WALES: {
                parser = getResources().getXml(R.xml.welsh_organisations);
                break;
            }
            case MainActivity.REGION_UK_IE_COLLEGE: {
                parser = getResources().getXml(R.xml.uk_ie_college_organisations);
                break;
            }
            case MainActivity.REGION_UK_IE_UNIVERSITY: {
                parser = getResources().getXml(R.xml.uk_ie_university_organisations);
                break;
            }
            case MainActivity.REGION_UK_HEALTH: {
                parser = getResources().getXml(R.xml.uk_ie_health_organisations);
                break;
            }
        }

        return parser;
    }

    @Override
    public void onStart() {
        super.onStart();

        /* We have to manually restore the set organisation dialog fragment if it was open
        during an orientation change, otherwise it will go beneath this fragment rather than
        on top of it. */
        if (DbFragmentRef.get().getSetOrganisationDialogFragmentOpen()) {

            setOrganisation();
        }
    }

    public void setOrganisation() {

        /* Check organisation data has been loaded from xml by thead started in onCreateDialog. If it
        hasn't and the thread is still running, show an indeterminate progress dialog which the user
        can dismiss if they don't want to wait. The thread won't be killed though unless they close the
        EditAccountDialog, so they can always press the Set Organisation button again. */
        if (!mOrgsLoaded) {

            if (mOrgsLoaderThread != null) {

                if (mOrgsLoaderThread.isAlive()) {

                    /* When the thread which loads the data from the xml file finishes, it will check if
                    this progress indicator exists, and get rid of it if it does. If the indicator does
                    exist, the thread will call setOrganisationDialog() itself, as otherwise it wouldn't
                    be called. */
                    WaitForThreadProgressFragment waitForProgressFrag = new
                            WaitForThreadProgressFragment();
                    waitForProgressFrag.show(getActivity().getSupportFragmentManager(),
                            MainActivity.WAIT_FOR_PROGRESS_FRAGMENT_TAG);
                }
            }
        } else {

            showSetOrganisationDialog();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (dbFrag.getAccountBeingEdited()) {

            mFragmentParent.storeEditFragSavedState();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {

        /* Set the boolean in the dbfrag which we use to know if an account is currently being edited
        (needed for when orientation destroys and we have to manually recreate the edit account
        fragment attached to a different activity (because you have 2 activites in single-pane mode
        but only 1 in dual-pane mode.)) */
        DbFragmentRef.get().setAccountBeingEdited(false);
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);

        /* Kill the loader thread if it exists, we don't want multiple threads if user reopens this dialog */
        if (mOrgsLoaderThread != null) {

            if (mOrgsLoaderThread.isAlive()) {

                mOrgsLoader.killThread();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putInt(MainActivity.REGION, mRegion);

        Dialog editAccountDialog = getDialog();

        if (editAccountDialog != null) {
            EditText editAccountEditText;

            if (mSelectedOrganisation != null) {

                outState.putParcelable(SELECTED_ORGANISATION, mSelectedOrganisation);
            } else {

                /* The organisation data hasn't yet been loaded. Kill the thread which loads the data,
                and get the organisation name from the textview. The organisation data thread will be
                restarted after the orientation change. */
                if (mOrgsLoaderThread != null) {

                    if (mOrgsLoaderThread.isAlive()) {

                        if (mOrgsLoader != null) {

                            mOrgsLoader.killThread();
                        }
                    }
                }

                TextView orgNameTextView = (TextView) editAccountDialog
                        .findViewById(R.id.edit_account_organisation_textview);
                String organisationName = orgNameTextView.getText().toString();
                outState.putString(ORGANISATION_NAME, organisationName);
            }

            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_account_name_edittext);
            String accountName = editAccountEditText.getText().toString();

            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_card_number);
            String cardNumber = editAccountEditText.getText().toString();

            editAccountEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_pin_or_password);
            String pinOrPassword = editAccountEditText.getText().toString();

            CompoundButton reminderSwitch = (CompoundButton) editAccountDialog
                    .findViewById(R.id.edit_account_reminder_switch);
            boolean reminderSwitchState = reminderSwitch.isChecked();

            if (!accountName.equals("")) outState.putString(ACCOUNT_NAME, accountName);
            if (!cardNumber.equals("")) outState.putString(CARD_NUMBER, cardNumber);
            if (!pinOrPassword.equals("")) outState.putString(PIN_OR_PASSWORD, pinOrPassword);
            outState.putBoolean(REMINDER_BOOLEAN, reminderSwitchState);

            if (mExistingAccountDetails != null) {

                outState.putStringArray(EXISTING_ACCOUNT_DETAILS, mExistingAccountDetails);
            }
        }
    }

    public void showSetOrganisationDialog() {

        /* This dialog is just a list of library organisations supported by the app
		The user picks one to set up an account for that organisation */
        SetOrganisationDialogFragment setOrganisationDialogFragment = new SetOrganisationDialogFragment();
        setOrganisationDialogFragment.setParent(this, mRegion);
        setOrganisationDialogFragment.show(getActivity().getSupportFragmentManager(),
                MainActivity.SET_ORGANISATION_FRAGMENT_TAG);

        DbFragmentRef.get().setSetOrganisationFragmentOpen(true);
    }

    public void showSetOrgDialogIfProgBarShown() {

        /* If the progress indicator is shown, that means the thread which loads the xml data took a long time to
        run and the user has in the meantime clicked the button to show the Set Organisation dialog, and is waiting
        for it to appear. We should get rid of the progress indicator and show the dialog.
        (If getActivity or getSupportFragmentManager are null, which can happen if orientation has changed on a large
        land device while the edit account frag is open, causing us to go from single- to dual-pane mode, we can't
        perform this check. In that case, the wait for progress frag will be checked for and removed by the
        main activity. */
        SherlockFragmentActivity activity = (SherlockFragmentActivity) getActivity();
        if (activity != null) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            if (fragmentManager != null) {
                DialogFragment waitForProgressFrag = (DialogFragment) fragmentManager
                        .findFragmentByTag(MainActivity.WAIT_FOR_PROGRESS_FRAGMENT_TAG);
                if (waitForProgressFrag != null) {

                    waitForProgressFrag.dismiss();
                    showSetOrganisationDialog();
                }
            }
        }
    }

    public void onSetOrganisationListViewClick(SetOrganisationDialogFragment setOrganisationDialogFragment,
                                               int selectedItem) {

		/* The user has selected an organisation from the list of organisations. Get the organisation name
        and put it into the appropriate textview in the EditAccountDialogFragment. */
        mSelectedOrganisation = mOrgList.get(selectedItem);

//        View suggestNameCheckBox = setOrganisationDialogFragment.getDialog().findViewById(R.id.checkbox_suggest_account_name);
//        boolean suggestName = ((CheckBox) suggestNameCheckBox).isChecked();
//        DbFragmentRef.get().setSuggestNameBoolean(suggestName);

        setOrganisationDialogFragment.dismiss();

        String selectedOrganisationName = mSelectedOrganisation.getName();
        TextView organisationTextView = (TextView) getDialog().findViewById(R.id.edit_account_organisation_textview);
        organisationTextView.setText(selectedOrganisationName);

		/* Create a name for the account, if pref enabled, and put it into accountname text field.  */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean suggestName = prefs.getBoolean(MainActivity.SUGGEST_NAME, true);
        if (suggestName) {

            fillInSuggestedName();
        } else {

            focusOnFirstEmptyField(false);
        }
    }

    private void focusOnFirstEmptyField(boolean skipAccountName) {

        Dialog addAccountDialog = getDialog();
        String contents;

        if (!skipAccountName) {

            /* Check the account name field. */
            final EditText accountNameEditText = (EditText) addAccountDialog
                    .findViewById(R.id.edit_account_account_name_edittext);
            contents = accountNameEditText.getText().toString();

            if (contents.length() == 0) {

                accountNameEditText.requestFocus();
                accountNameEditText.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager)
                                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(accountNameEditText, 0);
                    }
                }, 100);

                return;
            }
        }

        /* There was text in the account field, try the next field. */
        final EditText cardNumberEditText = (EditText) addAccountDialog
                .findViewById(R.id.edit_account_card_number);
        contents = cardNumberEditText.getText().toString();

        if (contents.length() == 0) {

            cardNumberEditText.requestFocus();

            cardNumberEditText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(cardNumberEditText, 0);
                }
            }, 100);

            return;
        }

        /* There was text in the card number field, check the pin number field. */
        final EditText pinOrPasswordEditText = (EditText) addAccountDialog
                .findViewById(R.id.edit_account_pin_or_password);
        contents = pinOrPasswordEditText.getText().toString();
        if (contents.length() == 0) {

            pinOrPasswordEditText.requestFocus();

            pinOrPasswordEditText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(pinOrPasswordEditText, 0);
                }
            }, 100);
        }
    }

    public void setSelectedOrganisationOrStoreNameToSetLater(String organisationName) {

        /* If the organisation data has been loaded, go through it and find the appropriate organisation.
        If the data hasn't been loaded, store the name of the organisation in a string, so that when the orgs loader
        thread finishes, it will be able to set the organisation itself. */
        if (mOrgsLoaded) {
            if (mOrgList != null) {
                for (LibOrganisation libOrganisation : mOrgList) {
                    if (libOrganisation.getName().equals(organisationName)) {
                        mSelectedOrganisation = libOrganisation;
                        break;
                    }
                }
            }
        } else {

            mSelectedOrganisationName = organisationName;
        }
    }

    public void setSelectedOrganisationFromStoredName() {

        /* This method is only called if mSelectOrganisationName is not an empty string. */
        if (mOrgList != null) {
            for (LibOrganisation libOrganisation : mOrgList) {
                if (libOrganisation.getName().equals(mSelectedOrganisationName)) {
                    mSelectedOrganisation = libOrganisation;
                    mSelectedOrganisationName = "";
                    break;
                }
            }
        }
    }

    public String getSelectedOrganisationName() {

        return mSelectedOrganisationName;
    }

    public LibOrganisation getSelectedOrganisation() {

        return mSelectedOrganisation;
    }

    public String[] getOrgNamesArray() {

        if (mOrgList != null) {

            /* Check whether the names array has already been loaded, if not, load it*/
            if (mOrgNames == null) {
            /* Load organisation names into a string array so they can be used in the setorganisation listview */
                mOrgNames = new String[mOrgList.size()];
                int i = 0;
                for (LibOrganisation libOrganisation : mOrgList) {

                    mOrgNames[i++] = libOrganisation.getName();
                }
            }
        }

        /* mOrgnames will still be null if the orglist hasn't finished loading, this is checked for by calling code.
        In any case this method isn't called until orgs have finished loading. */
        return mOrgNames;
    }

    public int getRegion() {

        return mRegion;
    }

    private void fillInSuggestedName() {

        String suggestedName = "";

        /* If the organisation selection has been changed away from the organisation already set for this account,
        and then set back to the original organisation without the changes being saved, we should suggest the name
        the account already has. Otherwise, generate one. */
        if (getExistingAccountDetail(1).equals(mSelectedOrganisation.getName())) {

            suggestedName = getExistingAccountDetail(0);

        } else {

            /* Enter a suggested account name into the account name field. It will be the short name
		    of the selected organisation plus a number to make it unique. The code checks against existing names. */
            String suggestedNameStub = mSelectedOrganisation.getShortName();

            Integer i = 0;

            List<String> accountNamesList = DbFragmentRef.get().getAccountNamesList();
            /* Add a number to the end of the short name until we find a unique name. */
            do {
                i++;
                suggestedName = suggestedNameStub + " " + i.toString();
            } while (accountNamesList.contains(suggestedName));
        }

        /* Update the text in the edit account dialog, and move focus appropriately. */
        Dialog editAccountDialog = getDialog();
        EditText accountNameEditText = (EditText) editAccountDialog.findViewById(R.id.edit_account_account_name_edittext);
        accountNameEditText.setText(suggestedName);

        focusOnFirstEmptyField(true);
    }

    private void setOrgList(List<LibOrganisation> orgList) {

        mOrgList = orgList;
    }

    private void setOrgsLoaded(boolean orgsLoaded) {

        mOrgsLoaded = orgsLoaded;
    }

    public boolean orgsLoaded() {

        return mOrgsLoaded;
    }

    public void storeExistingAccountDetails(String[] existingAccountDetails) {

        mExistingAccountDetails = existingAccountDetails;
    }

    public String getExistingAccountDetail(int index) {

        if (mExistingAccountDetails != null) {

            if (index <= mExistingAccountDetails.length) return mExistingAccountDetails[index];
        }

        return "";
    }
}
