/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.accounts;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import uk.co.fk33.libraryreminder.*;
import uk.co.fk33.libraryreminder.LibRemindViewBinder;
import uk.co.fk33.libraryreminder.database.DataToWrite;
import uk.co.fk33.libraryreminder.database.DataToWriteOnSyncFailed;
import uk.co.fk33.libraryreminder.database.DataToWriteOnSyncSucceeded;
import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;
import uk.co.fk33.libraryreminder.network.AutoSyncService;

import java.util.*;

public abstract class LibAccount {

    public final static int SYNC_NEVER_ATTEMPTED = -1;
    public final static int SYNC_FAILED_NO_CONNECTION = -2;
    public final static int SYNC_FAILED_LOGIN_ERROR = -3;
    public final static int SYNC_FAILED_NETWORK_ERROR = -4;
    public final static int SYNC_FAILED_URL_ERROR = -5;
    public final static int SYNC_FAILED_FORMAT_ERROR = -6;
    public final static int SYNC_FAILED_UNEXPECTED_RESPONSE = -7;
    public final static int SYNC_FAILED_TIMED_OUT = -8;
    public final static int SYNC_FAILED_ENCODER_EXCEPTION = -9;
    public final static int SYNC_SUCCEEDED = 1;

    public static enum CatType {ARENA, PRISM2, PRISM3, SPYDUS, VIEWPOINT, ELIBRARY, HERITAGE, PORTFOLIO, DUMMY}

    protected Context mContext;
    protected long mRowID;
    protected int mPosition = -1;
    protected String mName;
    protected CatType mType;
    protected String mCatUrl;
    protected String mCardNumber;
    protected String mPinOrPassword;
    protected boolean mRemind;
    protected View mListViewEntry = null;
    protected ListView mListView = null;
    protected boolean mFirstSync = false;
    private BroadcastReceiver mMessageReceiver;
    protected boolean mTaskCompleted = false;
    protected boolean mSilentMode = false;

    public LibAccount() {

    }

    public LibAccount(Context context, boolean silentMode) {

        mSilentMode = silentMode;

        if (!mSilentMode) {

            if (context instanceof AccountListFragment.AccountListParent && context instanceof FragmentActivity) {
                mContext = context;
            } else {
                throw new ClassCastException(context.toString() + " must implement AccountListParent and extend" +
                        "FragmentActivity.");
            }

            mListView = (ListView) ((Activity) mContext).findViewById(R.id.accountListListView);
        } else {
            if (context instanceof AutoSyncService) {
                mContext = context;
            } else {
                throw new ClassCastException(context.toString() + " must be AutoSyncService when account in" +
                        "silent mode.");
            }
        }
    }

    public void firstSync() {

        mFirstSync = true;

        /* If the account is off the bottom of the listview, this will scroll to it. */
        scrollTo();

        showSyncProgress(mContext.getString(R.string.syncing));

        sync();
    }

    public void sync() {

        /* If the listview is visible, we need a broadcast receiver so the account can
        reposition itself in the listview when the listview is resorted after a db write.
        If we're syncing in silent mode, mListView will be null, so the broadcast
        receiver won't be set up (it won't be needed).*/
        if (mListView != null) setupBroadcastReceiver();
    }

    private void setupBroadcastReceiver() {

        /* Set up a broadcast receiver to recive messages telling the account to scroll
        itself onscreen in the listview after syncing (in case it is not visible due to the
        listview being scrolled, or resorted after the update). */
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction() == null) return;

                if (intent.getAction().equals(MainActivity.SCROLL_TO_ACCOUNT)) {

                    scrollTo();
                }

                if (intent.getAction().equals(MainActivity.LIB_ACCOUNT_NULLIFY_LISTVIEW_REF)) {

                    mListView = null;
                    mListViewEntry = null;
                }

                if (intent.getAction().equals(MainActivity.LIB_ACCOUNT_UPDATE_POSITION)) {

                    findPosition();
                }

                /* If there is no oustanding task running, we won't need to receive any further
                messages telling us to scroll on screen, as there will be no updates to
                the account in the listview. */
                if (mTaskCompleted) unregisterReceiver();
            }
        };

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver,
                new IntentFilter(MainActivity.SCROLL_TO_ACCOUNT));
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver,
                new IntentFilter(MainActivity.LIB_ACCOUNT_NULLIFY_LISTVIEW_REF));
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver,
                new IntentFilter(MainActivity.LIB_ACCOUNT_UPDATE_POSITION));
    }

    private void scrollTo() {

        /* Scroll to the account in case it is no visible in the listview. We should call
        findPosition every time scrollTo is called as accounts' positions do change after
        syncing, as the list is resorted by each account's first due date. */
        mPosition = findPosition();
        if (mPosition > -1) mListView.setSelection(mPosition);
    }

    public void showSyncProgress(String progressStatus) {

        if (mPosition < 0) {

            /* If the listview adapter cursor hasn't updated to include this account yet, don't continue. */
            mPosition = findPosition();
            if (mPosition < 0) return;
        }

        TextView textView;

        /* Each account in the account list shows its last synced status. We will update this status during
        the sync attempt so the user knows what's going on. First, check if we already have a reference the
        list view entry for this account */
        if (mListViewEntry == null) {

            getListViewEntry();
        } else {

            /* If we already have a reference to a list view entry, we should check it hasn't been recycled, (as it
            would be if the user scrolled through a long list) in which case it would be showing information for a
            different account. Compare the names. */
            textView = (TextView) mListViewEntry.findViewById(R.id.list_view_account_name);
            if (textView != null) {
                if (!textView.getText().toString().equals(mName)) {

                    /* If the view we had a reference to has been recycled, set our reference to null then try to
                    get a reference to the correct view. */
                    mListViewEntry = null;
                    getListViewEntry();
                }
            }
        }

        /* mListViewEntry could still be null if the list view entry for the account was not found */
        if (mListViewEntry != null) {

            /* Find the sync status text view and update the status message there */
            textView = (TextView) mListViewEntry.findViewById(R.id.list_view_account_message);
            textView.setText(progressStatus);
        }
    }

    private int findPosition() {

        if (mListView == null) {

            mListView = DbFragmentRef.get().getAccountListListViewRef();
        }

        if (mListView != null) {

            /* Get the cursoradapter and iterate through it to find the position in the adapter of
            this account's entry, by checking the rowId against this account's rowId, which we
            know (mRowId). */
            SimpleCursorAdapter cursorAdapter = (SimpleCursorAdapter) mListView.getAdapter();
            int j = cursorAdapter.getCount();

            for (int i = 0; i < j; i++) {

                if (mRowID == cursorAdapter.getItemId(i)) {

                    return i;
                }
            }
        }

        return -1;
    }

    private void getListViewEntry() {

        /* Get a reference to the list view entry showing details for this account. Note, if we can't find the list view
        entry, the mListViewEntry member variable for this account will be left unchanged.
        First get a ref to the account list listview.
        (mListView will be null if we've changed orientation while syncing.) */
        if (mListView == null) {

            /* If the dbFrag has been given a ref to the new listview created after orientation change,
             * retrieve it. We should also update mPosition as it could change if another account has finished
             * syncing since this sync was started and the accountlist has resorted itself.*/
            mListView = DbFragmentRef.get().getAccountListListViewRef();

            if (mListView != null) {

                findPosition();
            } else {

                return;
            }
        }

        /* If the listview entry showing this account's details is onscreen (i.e. not scrolled off),
        calling getChildAt(mPosition - firstVisiblePostion) should return a reference to it. */
        if (mPosition > -1) {

            mListViewEntry = mListView.getChildAt(mPosition - mListView.getFirstVisiblePosition());
        }
    }

    public void onBackgroundSyncComplete(List<LibItem> itemList, int syncStatus) {

        if (mContext != null) {

            if (mContext instanceof AutoSyncService) {

                AutoSyncService autoSyncService = (AutoSyncService) mContext;

                if (syncStatus < 1) {

                    DataToWrite failureData = new DataToWriteOnSyncFailed(new long[]{mRowID},
                            new long[]{(long) syncStatus});
                    autoSyncService.writeToDbOnAccountSyncFail(failureData);

                } else {

                    DataToWrite newSummaryAndDates = describeAccount(itemList);
                    autoSyncService.writeToDbOnAccountSyncSuccess(newSummaryAndDates, mName, itemList, mRemind);
                }
            }
        }
    }

    public void writeSyncResults(List<LibItem> itemList) {

        /* This method is called when the thread performing the network sync is finished so
        set mTaskCompleted true here. This means the broadcast receiver set up in sync() will
        be unregistered when the listview adapter's cursor is reloaded after the db write
        called below. After that reload we won't need the listview to reposition itself any
        more so we should unregister the receiver. If it's not unregistered if would cause a
        memory leak.*/
        mTaskCompleted = true;

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* Decrement the number of accounts being synced. If this is the last account being synced,
        this will prevent any scrollTo() messages being sent following the account headers query
        being re-run, as these messages are only sent if the number of accounts being synced is 1.*/
        dbFrag.decrementNumberOfAccountsBeingSynced();

        /* Remove the name of this account from the dbfrag's sync list. */
        dbFrag.removeNameFromSyncList(mName);

        /* Set this true here as we are going to reload the accounts below and if the item query
        finishes before the accounts query starts, the code which shows the no items hint won't
        know it has to wait for the account headers query to finish. */
        dbFrag.setAccountsLoading(true);

        dbFrag.updateItemsForAccount(mName, itemList, mRemind);

        DataToWrite newSummaryAndDates = describeAccount(itemList);

        /* This will write the new summary text and current time to the accounts table in the
        summary column and last_synced column respectively. */
        dbFrag.updateAccounts(newSummaryAndDates);

        /* Update the reference time the View Binder is using to construct the PrettyTime phrase.
        If we don't, and it is using an old time, it will state that the account was synced in
        the future. */
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(
                LibRemindViewBinder.VIEW_BINDER_UPDATE_TIME));
    }

    private DataToWrite describeAccount(List<LibItem> itemList) {

        /* The catalogue may return items sorted by due date already, but sort them anyway in case
        not. First check we have more than one item, otherwise no point in sorting. */
        int numberOfLoans = itemList.size();

        if (numberOfLoans > 1) {
            Collections.sort(itemList, new Comparator<LibItem>() {
                public int compare(LibItem itemA, LibItem itemB) {
                    return ((Long) itemA.getDueDate()).compareTo(itemB.getDueDate());
                }
            });
        }

        /* If there is at least one item, find its due date, put it in the account summary */

        long currentTimeInMillis = System.currentTimeMillis();

        /* If the account has no items put it at the bottom of the list when sorted by first due
        dates. Set the due date to a high number to ensure the account is sorted last. */
        long firstItemDueBack = 3000000000000L;

        /* Create a string describing the account. If the user has loans, use the format
        "x loans. First due back :[date]" where [date] is the due date of the first item, as a
        string representation of a long value. When displaying the summary in the account list,
        it will be converted back to human-readable form. If the user has no loans, just set the
        summary to  "No loans." */
        String summary;

        if (numberOfLoans > 0) {

            String summaryMiddle;
            if (numberOfLoans == 1) {

                summaryMiddle = " " + mContext.getString(R.string.loan_summary_single);
            } else {

                summaryMiddle = " " + mContext.getString(R.string.loan_summary_plural);
            }

            firstItemDueBack = itemList.get(0).getDueDate();

            summary = String.valueOf(numberOfLoans) + summaryMiddle
                    + String.valueOf(firstItemDueBack);

        } else {

            summary = mContext.getString(R.string.no_loans);
        }

        /* Put the summary and current time in an object used in this kind of db write. Set the sync
        time five seconds ago to ensure PrettyTime won't say the account was synced in the future.*/
        return new DataToWriteOnSyncSucceeded(new long[]{mRowID}, new String[]{summary},
                new long[]{currentTimeInMillis - 5000}, new long[]{firstItemDueBack});
    }

    public void syncFailed(int reason) {

        /* This method is called when the thread performing the network sync is finished so set
        mTaskCompleted true here. This means the broadcast receiver set up in sync() will
        be unregistered when the listview adapter's cursor is reloaded after the db write
        called below. After that reload we won't need the listview to reposition itself any
        more so we should unregister the receiver. If it's not unregistered if would cause a
        memory leak. */
        mTaskCompleted = true;

        DbInteractionsFragment dbFrag = DbFragmentRef.get();

        /* Decrement the number of accounts being synced. If this is the last account being synced, this will
        prevent any scrollTo() messages being sent following the account headers query being re-run,
        as these messages are only sent if the number of accounts being synced is 1. */
        dbFrag.decrementNumberOfAccountsBeingSynced();

        /* Remove the name of this account from the dbfrag's sync list. */
        dbFrag.removeNameFromSyncList(mName);

        /* Show a toast informing user sync failed for this account. */
        Toast.makeText(mContext, mContext.getString(R.string.sync_failed_for)
                + " " + mName, Toast.LENGTH_LONG).show();

        if (mFirstSync) {

            if (dbFrag.getInLargeLandMode()) {

                /* If in large land mode, set these two strings, so that the account will be opened in the view
                account pane (a hint will be shown there telling the user the account has never been synced. */
                dbFrag.setAccountBeingSynced(mName);
                dbFrag.setAccountBeingViewed(mName);
            }
        }

        /* Write the int representing the failure reason to the last_synced_date column of the account in the db. */
        DataToWrite failureData = new DataToWriteOnSyncFailed(new long[]{mRowID}, new long[]{(long) reason});
        dbFrag.updateAccounts(failureData);
    }

    public String getName() {
        return mName;
    }

    public boolean getRemindSetting() {

        return mRemind;
    }

    public String getCatUrl() {
        return mCatUrl;
    }

    public String getCardNumber() {
        return mCardNumber;
    }

    public String getPinOrPassword() {
        return mPinOrPassword;
    }

    public void setCatUrl(String newUrl) {
        mCatUrl = newUrl;
    }

    public void unregisterReceiver() {

        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
    }

    public void registerNameInSyncList() {

        DbFragmentRef.get().addNameToSyncList(mName);
    }

    public boolean getInSilentMode() {

        return mSilentMode;
    }
}
