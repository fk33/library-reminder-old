/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ConfirmDeleteDialogFragment extends CloseableDialogFragment {

    private String[] mAccountsToDelete;
    private DeleteFragParent mParent;
    public static final String ACCOUNTS_TO_DELETE = "accounts_to_delete";

    public ConfirmDeleteDialogFragment() {}

    public interface DeleteFragParent {

        public void deleteAccounts(String[] accountsToDelete);
    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

        if (activity instanceof DeleteFragParent) {
            mParent = (DeleteFragParent) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement DeleteFragParent.");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mAccountsToDelete = getArguments().getStringArray(ACCOUNTS_TO_DELETE);

        int numberOfAccountsToDelete = mAccountsToDelete.length;

        String confirmDeleteTitle;
        String confirmDeleteMessage;

        if (numberOfAccountsToDelete > 1) {

            confirmDeleteTitle = getString(R.string.confirm_delete_title_multiple);
            confirmDeleteMessage = getString(R.string.confirm_delete_multiple_accounts);
        } else {

            confirmDeleteTitle = getString(R.string.confirm_delete_title_single);
            confirmDeleteMessage = getString(R.string.confirm_delete_single_account);
        }

        for (String account : mAccountsToDelete) {

            confirmDeleteMessage = confirmDeleteMessage + account + "\n";
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(confirmDeleteTitle)
                .setMessage(confirmDeleteMessage)
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mParent.deleteAccounts(mAccountsToDelete);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }
}
