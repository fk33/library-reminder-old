/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import uk.co.fk33.libraryreminder.database.DbInteractionsFragment;

public class ItemInfoDialogFragmentDualPane extends ItemInfoDialogFragment {

    @Override
    protected void runItemInfoQueryOrRemoveThisFragmentIfNecessary(Activity activity) {

        /* This particular fragment is only ever created when in dual-pane mode, so if we
        are in single-pane mode here, it means the configuration has changed while this fragment
        was open, and the system has recreated the fragment. But this fragment will have to be
        replaced with one hosted in the ViewAccountActivity which is used in single-pane mode.
        Don't bother running the db query, and remove this fragment, as it's not needed (a new
        one will be created by the new ViewAccountFragment created by the new ViewAccountActivity). */
        DbInteractionsFragment dbFrag = DbFragmentRef.get();
        if (dbFrag.getInLargeLandMode()) {

            String itemBeingViewed = dbFrag.getItemBeingViewed();
            if (!itemBeingViewed.equals("")) {

                dbFrag.runItemInfoQuery((FragmentActivity) activity, itemBeingViewed);
            }
        } else {

            mItemInfoDialogParent.removeDualPaneItemInfoFrag();
        }
    }
}
