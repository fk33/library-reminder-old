/*
 * Copyright (c) 2013 Dan Huston.
 */

package uk.co.fk33.libraryreminder.compatibility;


import android.annotation.TargetApi;
import android.content.SharedPreferences;

public class Api9To17DependentPrefWriter extends ApiDependentPrefWriter {

    @Override
    @TargetApi(9)
    public void writeBooleanPref(SharedPreferences sharedPreferences, String key, boolean value) {

        /* APIs 9+ can use apply(), this class will be loaded when running on APIs 9+ */
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(key, value);
        sharedPreferencesEditor.apply();
    }

    @Override
    @TargetApi(9)
    public void writeIntPref(SharedPreferences sharedPreferences, String key, int value) {

        /* APIs 9+ can use apply(), this class will be loaded when running on APIs 9+ */
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putInt(key, value);
        sharedPreferencesEditor.apply();
    }

    @Override
    @TargetApi(9)
    public void writeStringPref(SharedPreferences sharedPreferences, String key, String value) {

        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(key, value);
        sharedPreferencesEditor.apply();
    }
}
