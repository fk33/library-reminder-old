package uk.co.fk33.libraryreminder;

public class Flavour {

    public final static String PRICE = MainActivity.FREE;

    /* We don't need a public key for the free version, as no need to check for a license. */
    public static final String BASE64_PUBLIC_KEY = "";
}
