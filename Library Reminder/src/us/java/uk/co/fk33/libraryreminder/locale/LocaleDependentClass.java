/*
 * Copyright (c) 2014 Dan Huston.
 */

package uk.co.fk33.libraryreminder.locale;

import android.app.*;
import android.content.*;

import android.os.Bundle;

import uk.co.fk33.libraryreminder.AddAccountDialogFragment;
import uk.co.fk33.libraryreminder.MainActivity;
import uk.co.fk33.libraryreminder.DbFragmentRef;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import uk.co.fk33.libraryreminder.R;

import java.util.Locale;

public class LocaleDependentClass {

    public static final int ALARM_ID = 22;
    public static final int ACTION_NEEDED_NOTIFICATION_ID = 3;
    public static final int LOCALE = MainActivity.LOCALE_US;
    private static final Locale mDateLocale = Locale.US;
    private static final Locale mLanguageLocale = Locale.ENGLISH;

    public static Locale getDateLocale() {

        return mDateLocale;
    }

    public static Locale getLanguageLocale() {

        return mLanguageLocale;
    }

    public static SimpleDateFormat getItemInfoDueDateFormat() {

        return new SimpleDateFormat("MM/dd/yy", mDateLocale);
    }


}
